<!DOCTYPE html>
<html>
<head>
	<title>CDTRS Tutorials</title>
	<?php
		include("theme/theme.php");
	?>
</head>
<body style="overflow-x: hidden;">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1 id="a1" class="display-4">
			CDTRS <small>Help</small>
		</h1>
	</div>
</div>
<div style="padding: 1.4vh;">
		<div class="row">
		<div class="col-sm-4">
			<h4>Topics</h4>
<div class="btn-group-vertical">
<a href="#a" class="btn btn-primary">Update LAMS & DTR</a>
<a href="#b" class="btn btn-primary">Create new entitlements</a>
<a href="#c" class="btn btn-primary">Forgot LAMS Password</a>
<a href="#d" class="btn btn-primary">DTR not working</a>
<a href="#e" class="btn btn-primary">Dispute a timelog</a>
<a href="#f" class="btn btn-primary">Apply a Leave</a>
<a href="#g" class="btn btn-primary">Change Lams SCHOOL Name</a>
<a href="#h" class="btn btn-primary">Change Lams PRINCIPAL Name</a>
<a href="#i" class="btn btn-primary">Change Lams HRMO Name</a>
<a href="#j" class="btn btn-primary">Change Lams ADMIN OFFICE V Name</a>
<a href="#k" class="btn btn-primary">Change Lams OIC, OSDS name</a>
<a href="#l" class="btn btn-primary">Set LAMS to School/Division Account</a>
<a href="#m" class="btn btn-primary">Set Wamp PHP version to 7.2.4</a>
</div>
		</div>
		<div class="col-sm-8">
			<table class="table table-bordered " id="hpx" >
				<thead>
					<tr>
						<th>Instructions & Description</th>
					</tr>
				</thead>
				<tbody>
					<tr id="a">
						<td>
						<h4>Update LAMS and DTR</h4>
						<p>It's never been easier now. with new new LAMS!</p>
						<h5><i class="fas fa-arrow-circle-right"></i> Updating LAMS</h5>
						<hr>
						<p>Steps: </p>
						<ol>
							<li>Press <i class="fab fa-windows"></i> <strong>Windows + R</strong> and the "run" application will open<br><a title="click to view in original size" href="images/runapp.png" target="_blank"><img src="images/runapp.png" class="xi"></a></li>
							<li>Paste C:\wamp64\www\sdo_marikina_systems and click "Ok" or hit "Enter" key on your keyboard. It will open the main directory or LAMS.</li>
							<li>Right click with the "Directory Window" selected and a menu will appear<br>
								<a title="click to view in original size" href="images/syetemwindow.png" target="_blank"><img src="images/syetemwindow.png" class="xi"></a>
							</li>
							<li>In the menu, click <strong>Git Bash Here</strong></li>
							<li>Wait until the Git Bash command window looks like this<br>
								<a title="click to view in original size" href="images/gitbashhere.png" target="_blank"><img src="images/gitbashhere.png" class="xi"></a>
							</li>
							<li>In the command window type git pull and click the "Enter" key on your keyboard.</li>
							<li>Wait for a few seconds. If the command says "Already up to date" then your system is already updated to the latest version. Else the command will do its job to update your system to the latest version.</li>
							<li>All done. Congratulations! you can now start using your LAMS.</li>
						</ol>
						<hr>
							<h5><i class="fas fa-arrow-circle-right"></i> Updating DTR</h5>
							<p>DTR and LAMS are different systems so <strong>UPDATING the DTR is not the same as updating LAMS</strong>. they are two different things.</p>
							<hr>
							<p>Steps: </p>
							<ol>
								<li>Press <strong> <i class="fab fa-windows"></i> Windows + R</strong> and the "run" application will open<br><a title="click to view in original size" href="images/runapp.png" target="_blank"><img src="images/runapp.png" class="xi"></a></li>
							<li>Paste <strong>C:\wamp64\www\sdo_marikina_systems</strong> and click "Ok" or hit "Enter" key in your keyboard. It will open the main directory or <strong>DTR</strong></li>
							<li>Right click with the "Directory Window" selected and a menu will appear<br>
								<a title="click to view in original size" href="images/dtrdir.png" target="_blank"><img src="images/dtrdir.png" class="xi"></a>
							</li>
							<li>In the menu, click <strong>Git Bash Here</strong></li>
							<li>Wait until the Git Bash command window looks like this<br>
								<a title="click to view in original size" href="images/gitbashhere.png" target="_blank"><img src="images/gitbashhere.png" class="xi"></a>
							</li>
							<li>In the command window type <strong>git pull</strong> and click the "Enter" key in your keyboard.</li>
							<li>Wait for a few seconds. If the command says "Already up to date" then your system is already updated to the latest version. else the command will do its job to update your system to the latest version.</li>
							<li>All done. Congratulations! you can now start using your <strong>DTR</strong>.</li>
							</ol>
						<hr>
						<h4></h4>
						</td>
					</tr>
					<tr id="b">
						<td>
						<h4>Create new entitlements</h4>
						<p>Creating new entitlements for employee(s) in LAMS</p>
						<p>Steps:</p>
						<ol>
							<li>If you are not already logged-in to LAMS then don't skip this step.<br>
							<a title="click to view in original size" href="images/login_nav.png" target="_blank"><img src="images/login_nav.png" class="xi"></a>
							<p>In the navigation bar click the LAMS link, then a Login Dialog will open, it looks like this<br>
							<a title="click to view in original size" href="images/lams_login.png" target="_blank"><img src="images/lams_login.png" class="xi"></a>
							</p>
							</li>
							<li>
								<p>Enter your Username and password to login.</p>
							</li>
							<li>
								<p>After you login to lams you will see your <strong>dashboard's navigation</strong> bar and hit <strong>Manage Leave <i class="fas fa-arrow-circle-right"></i> Add Entitlements</strong>
								<br>
							<a title="click to view in original size" href="images/lams_manageleave.png" target="_blank"><img src="images/lams_manageleave.png" class="xi"></a>
							<br>
							and an <strong>Add Entitlements Modal</strong> will open. In here you can now add entitlements for your employees that can be viewed in <strong>Manage Leave <i class="fas fa-arrow-circle-right"></i> Employee Entitlements</strong>
						</p>

							</li>
						</ol>
						</td>
					</tr>
					<tr id="c">
						<td>
						<h4>Forgot LAMS Password or What is DTR credentials</h4>
						<p>For this matter you can go to the <strong>HELPDESK</strong>.</strong></p>
						<hr>
						<h5>Reminder</h5>
						<p>You DTR and LAMS username and password are the same. LAMS Account is DTR account too.</p>
						<h5>How to access the <strong>HELPDESK</strong>?</h5>
						<p>Steps:</p>
						<ul>
							<li>Go to the main page by clicking this <a href="../" target="_blank"><u>Link</u></a>.</li>
							<li><p>Find the little cute blue button on the top right side corner of the page and click it to submit your inquiry for direct solutions.<br>
									<a title="click to view in original size" href="images/helpdesk.png" target="_blank"><img src="images/helpdesk.png" class="xi"></a>
							</p></li>
						</ul>
						</td>
					</tr>
					<tr id="d">
						<td>
						<h4>DTR not working</h4>
						<p>There are many reasons why DTR is not working.</p>
						<hr>
						<h5>Causes & Solution<br>
							<small>Let's explore the problem even deeper</small></h5>
							<hr>
						<ul>
							<li>
								<h6>No internet connectivity</h6>
								<p>Please make sure you are connected to the internet. DTR System requires internet connectivity to be fully functional</p>
							</li>
							<li>
								<h6>DTR won't open because it's not even in your computer.</h6>
								<p>Steps to re-install <strong>DTR</strong></p>
								<ul>
									<li>Press <i class="fab fa-windows"></i> <strong>Windows + R</strong> to show the "Run" application <br>
										<a title="click to view in original size" href="images/runapp.png" target="_blank"><img src="images/runapp.png" class="xi"></a>
									</li>
									<li>Copy this text - <strong>C:\wamp64\www</strong></li>
									<li>And paste to the Run applicatipon textbox and click <strong>OK</strong> or press the <strong>Enter</strong> key in your keyboard.</li>
								</ul>
							</li>
						</ul>
						</td>
					</tr>
					<tr id="e">
						<td>
						<h4>Dispute a timelog</h4>
						<p>Steps:</p>
						<ol>
							<li>Go to <strong>Employee Dashboard</strong></li>
							<li>Type your <strong>Employee Code</strong></li>
							<li>Hit "Enter" button in your keyboard</li>
							<li>Choose a <strong>Time Log</strong> you want to dispute in your Logs table</li>
							<li>You will see a dropdown button on the right side of the screen, on the Action column and click it.</li>
							<li>A menu will show and choose <strong>Dispute</strong></li>
							<li>Fill up the given fields and submit.</li>
							<li>Wait for the <strong>LAMS Admin to approve your dispute.</strong></li>
						</ol>
						</td>
					</tr>
					<tr id="f">
						<td>
								<h4>Apply a Leave</h4>
						<p>Steps:</p>
						<ol>
							<li>Go to <strong>Employee Dashboard</strong></li>
							<li>Type your <strong>Employee Code</strong></li>
							<li>Hit "Enter" button in your keyboard</li>
							<li>Choose a <strong>Time Log</strong> you want to "Apply Leave" in your Logs table</li>
							<li>You will see a dropdown button on the right side of the screen, on the Action column and click it.</li>
							<li>A menu will show and choose <strong>Dispute</strong></li>
							<li>Fill up the given fields and submit.</li>
							<li>Wait for the <strong>LAMS Admin to approve your "Applied Leave".</strong></li>
						</ol>
						</td>
					</tr>
					<tr id="g">
						<td>
						<h4>Change Lams SCHOOL Name</h4>
						<p>Steps:</p>
						<ol>
							<li>Login to <strong>LAMS</strong></li>
							<li>After you log-in, you will see "Settings" on the navigation bar. Click it.</li>
							<li>Now you will be able to modify your <strong>School Name</strong>.</li>
							<li>When you type, your settings will be automatically saved.</li>
						</ol>
						</td>
					</tr>
					<tr id="h">
						<td>
					
						<h4>Change Lams PRINCIPAL Name</h4>
						<p>Steps:</p>
						<ol>
							<li>Login to <strong>LAMS</strong></li>
							<li>After you log-in, you will see "Settings" on the navigation bar. Click it.</li>
							<li>Now you will be able to modify your <strong>PRINCIPAL Name</strong>.</li>
							<li>When you type, your settings will be automatically saved.</li>
						</ol>
						</td>
					</tr>
					<tr id="i">
						<td>

						<h4>Change Lams HRMO Name</h4>
						<p>Steps:</p>
						<ol>
							<li>Login to <strong>LAMS</strong></li>
							<li>After you log-in, you will see "Settings" on the navigation bar. Click it.</li>
							<li>Now you will be able to modify your <strong>HRMO Name</strong>.</li>
							<li>When you type, your settings will be automatically saved.</li>
						</ol>
						</td>
					</tr>
					<tr id="j">
						<td>

						<h4>Change Lams ADMIN OFFICE V Name</h4>
						<p>Steps:</p>
						<ol>
							<li>Login to <strong>LAMS</strong></li>
							<li>After you log-in, you will see "Settings" on the navigation bar. Click it.</li>
							<li>Now you will be able to modify your <strong>ADMIN OFFICE V Name</strong>.</li>
							<li>When you type, your settings will be automatically saved.</li>
						</ol>
						</td>
					</tr>
					<tr id="k">
						<td>
						<h4>Change Lams OIC/ OSDS name</h4>
						<p>Steps:</p>
						<ol>
							<li>Login to <strong>LAMS</strong></li>
							<li>After you log-in, you will see "Settings" on the navigation bar. Click it.</li>
							<li>Now you will be able to modify your <strong>OIC/ OSDS name</strong>.</li>
							<li>When you type, your settings will be automatically saved.</li>
						</ol>
						</td>
					</tr>
					<tr id="l">
						<td>
						<h4>Change Lams School/Division Account</h4>
						<p>Steps:</p>
						<ol>
							<li>Login to <strong>LAMS</strong></li>
							<li>After you log-in, you will see "Settings" on the navigation bar. Click it.</li>
							<li>Now you will be able to modify your <strong>School/Division Account</strong>.</li>
							<li>When you type, your settings will be automatically saved.</li>
						</ol>
						</td>
					</tr>
					<tr id="m">
						<td>
						<h4>How to set Wamp PHP version to 7.2.4</h4>
						<p>Steps:</p>
						<ol>
							<li>Make sure Wamp Server is open and running on your Windows Taskbar</li>
							<li>Find the green "W" icon on the taskbar.</li>
							<li>Left click it.</strong>.</li>
							<li>find PHP > Versions</li>
							<li>Select 7.2.4</li>
							<li>Wait for wamp to turn green again then refresh LAMS on your browser</li>
							<li>Now LAMS should work just fine now.</li>
						</ol>
						</td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>
</div>

<a href="#a1" title="Go back on top" class="btn btn-primary" style="bottom:0; left:0; position: fixed; margin: 20px; box-shadow: 0px 20px 50px rgba(0,0,0,0.5);"><span><i class="fas fa-arrow-up"></i></span></a>
</body>
</html>
<script type="text/javascript">
	$("#hpx").DataTable({
		  paging: false,
		   "ordering": false
	});


	// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
</script>