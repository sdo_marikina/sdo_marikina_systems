<?php
      function genPdfThumbnail($source, $target)
      {
            //$source = realpath($source);
            $target = dirname($source).DIRECTORY_SEPARATOR.$target;
            $im     = new Imagick($source."[0]"); // 0-first page, 1-second page
            $im->setImageColorspace(255); // prevent image colors from inverting
            $im->setimageformat("jpeg");
            $im->thumbnailimage(160, 120); // width and height
            $im->writeimage($target);
            $im->clear();
            $im->destroy();
      }

      genPdfThumbnail('ff.pdf','my.jpg');

      ?>