<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	?>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<div class="container">
		<div style="margin-top:20px; ">
			<h1 class="ultrabold">LOGS</h1>
		</div>
		<div class="card blurbg">
			<div class="card-body">
				<h1 class="ultralight">Today</h1>
				<hr>
				<table class="table table-striped" id="t1">
					<thead>
						<tr>
							<th>Time</th>
							<th>Performer</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php GetActionLogsForToday(); ?>
					</tbody>
				</table>
				<br>
				<br>
				<br>
				<h1 class="ultralight">Recent Days</h1>
				<p>Displaying all recent logs</p>
				<hr>
				<table class="table table-striped" id="t2">
					<thead>
						<tr>
							<th>Time</th>
							<th>Performer</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php GetActionLogsAll(); ?>
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	$("#t1").DataTable({
		  "ordering": false
	});
	$("#t2").DataTable({
		  "ordering": false
	});
</script>