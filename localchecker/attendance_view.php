<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	include("inc/weblink.php");
	?>
</head>
<body class="blurbg">
	<?php
		include("comp/nav.php");
	?>
	<div class="container-fluid">
		<h1 class="ultrabold">Attendance View</h1>
		<p>Showing data of the last 31 days</p>
		<table class="table table-striped table-bordered table-sm">
			<thead>
				<th>School Name</th>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>
				<th>14</th>
				<th>15</th>
				<th>16</th>
				<th>17</th>
				<th>18</th>
				<th>19</th>
				<th>20</th>
				<th>21</th>
				<th>22</th>
				<th>23</th>
				<th>24</th>
				<th>25</th>
				<th>26</th>
				<th>27</th>
				<th>28</th>
				<th>29</th>
				<th>30</th>
				<th>31</th>
			</thead>
			<tbody id="av">
				
			</tbody>
		</table>
	</div>
</body>
</html>
<script type="text/javascript">
	$.ajax({
		type: "POST",
		url: "func/server.php",
		data: {tag:"get_data"},
		success: function(data){
			// alert(data);
			$("#av").html(data);
		}
	})
</script>
