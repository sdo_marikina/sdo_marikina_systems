<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	include("inc/weblink.php");
	?>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<div class="container">
		
		<div style="margin-top:20px; ">
			<h1 class="ultrabold">LOG MERGER (Experimental)</h1>
		</div>
		<div class="card blurbg">
			<div class="card-body">
				 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Get Log JSON from a Server</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Insert JSON Log to a Server</a>
		  </li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
		  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
	
	<input type="hidden" name="tag" value="TransferAttendanceLogs">
	<div class="form-group">
		<label>Select Server to recieve the Attendance Logs</label>
		<select class="form-control" id="server_link_s">
		<option value="http://vaerch.com/cdtrs_webservice/index.php">Vaerch Server</option>
		<option value="https://depedmarikina.ph/cdtrs_webservices/cdtrs_backup/index.php">Deped Server</option>
		</select>
	</div>
	<div class="form-group">
		<label>Company Name</label>
	<input type="text" name="schoolname" id="schoolname" class="form-control" value="">
	</div>
	<div class="form-group">
		<label>Starting Date</label>
	<input type="date" name="date_start" id="date_start" class="form-control">
</div>
<div class="form-group">
	<label>Ending Date</label>
	<input type="date" name="date_end" id="date_end" class="form-control">
</div>

		<div class="form-group">
			<button type="submit" id="sub_getLogs" class="btn btn-primary">Get JSON Logs</button>
</div>
<div class="form-group">
		<textarea rows="10" id="txt_res_json" class="form-control" placeholder="Result goes here..."></textarea>
</div>
<script type="text/javascript">
		$("#sub_getLogs").click(function(){
			var inp_server_link_s = $("#server_link_s").val();
			var inp_schoolname = $("#schoolname").val();
			var inp_date_start = $("#date_start").val();
			var inp_date_end = $("#date_end").val();
			$.ajax({
				type: "POST",
				url: "func/online_syncher.php",
				data: {
					tag: "TransferAttendanceLogs",
					server_link_url: inp_server_link_s,
					schoolname: inp_schoolname,
					date_start: inp_date_start,
					date_end: inp_date_end
				},
				success: function(data){
					$("#txt_res_json").val(data);
				}
			})
		})
	</script>

		  </div>
		  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
		  	<div class="card-body">
				<div class="form-group">
					<label>Logs JSON</label>
					<textarea class="form-control" rows="10" id="json_cocoa"></textarea>
				</div>
				<div class="form-group">
					<label>Select Server to recieve the Attendance Logs</label>
					<select class="form-control" id="server_link">
						<option value="https://depedmarikina.ph/cdtrs_webservices/cdtrs_backup/index.php">Deped Server</option>
						<option value="http://vaerch.com/cdtrs_webservice/index.php">Vaerch Server</option>
					</select>
				</div>
				<div class="form-group">
					<button class="btn btn-primary" id="btninsertlogsjson">Insert Logs</button>
				</div>
				
			</div>
			<br>
			<div id="log_res_cont">
				
			</div>
		  </div>

		</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	// alert($("#server_link").val());
	$("#btninsertlogsjson").click(function(){
		var server_link = $("#server_link").val();
		var json_cocoa = JSON.parse($("#json_cocoa").val());
		for(var i =0; i < json_cocoa.length;i++){
			$.ajax({
				type : "POST",
				url: "func/online_syncher.php",
				data: {tag: "sync_a_single_log_online",
				 eid:json_cocoa[i]["eid"]
				,access_type:json_cocoa[i]["access_type"]
				,access_image:json_cocoa[i]["access_image"]
				,date:json_cocoa[i]["date"]
				,timestamp:json_cocoa[i]["timestamp"]
				,ismanual:json_cocoa[i]["ismanual"]
				,company:json_cocoa[i]["company"]
				,local_id:json_cocoa[i]["local_id"]
				,server_link_url:server_link},

				success: function(data){
					$("#log_res_cont").append(i + " - " + data +"<br>");
				}
			})
		}
	})
</script>
