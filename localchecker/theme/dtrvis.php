<style type="text/css">
@font-face{
font-family: 'sanfranc';
src: url('../theme/fonts/sanfrancisco_pro.ttf'); /* IE9 Compat Modes */
}
body{
font-family: sanfranc;
}
.has_watermark{
background-image: url("../images/sdo.png");
background-repeat: no-repeat;
background-position: center center;
height: 1350px;
/*border: 1px solid black;*/
}
.table tr th{
border-color : rgba(0,0,0,0.2) !important;
background-color: rgba(0,0,0,0.1);
}
.table tr td{
border-color : rgba(0,0,0,0.2) !important;
}
.table{
background-color: transparent;
}
@media print { 
.table td, .table th { 
background-color: transparent !important; 
} 
.table tr th{
background-color: rgba(0,0,0,0.1) !important;
}
}
</style>