<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	include("inc/weblink.php");
	?>
	<style type="text/css">
		.notifbox{
			position: fixed;
			padding: 10px;
			border-radius: 10px;
			background-color: white;
			box-shadow: 0px 10px 50px rgba(0,0,0,0.5);
			width: 400px;
			display: block;
			bottom: 0;
			right: 0;
			margin: 20px;
			z-index: 100;
			transition: 1s all;
			animation-name: shownotif;
			animation-duration: 1s;
			display: none;
		}
		@keyframes shownotif{
			0%{
				margin-right: -700px;
			}
		}
	</style>
	<script type="text/javascript" src="popnotif/pop.js"></script>
</head>
<body class="defaultbg">
		
	<?php
		include("comp/nav.php");
	?>
	<div class="container-fluid">
		<div style="margin-top:20px; ">
			<h1 class="ultrabold">CDTRS Activation</h1>
			<span class="badge badge-warning">CDTRS 2.5</span>
		</div>
		<div class="card">
			<div class="card-body">
				<h1 class="ultralight">Registered Users</h1>
				<table class="table table-striped" id="regschoolstable">
					<thead>
						<tr>
							<th>School Name</th>
							<th><i class="fas fa-cog"></i></th>
						</tr>
					</thead>
					<tbody>
						<?php GetLamsRegisteredSchools(); ?>
					</tbody>
				</table>
				<br>
				<h1 class="ultralight">Pending Users</h1>
								<table class="table table-striped" id="pendingtbl">
					<thead>
						<tr>
							<th>School Name</th>
							<th><i class="fas fa-cog"></i></th>
						</tr>
					</thead>
					<tbody>
						<?php GetLamsRegisteredSchools_Unregistered(); ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="modal" tabindex="-1" role="dialog" id="modal_copylogbydate">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      	<input type="hidden" id="selectedid" name="">
	      	 <h1 class="modal-title ultrabold">COPY LOGS</h1>
	      	 <br>
	        <div class="row">
	        	<div class="col-sm-12">
	        		<div class="form-group">
	        	<label>Choose Date</label>
	        	<input type="date" value="<?php echo date('Y-m-d'); ?>" class="form-control" id="datetocopy_id"name="">
	        </div>
	        	</div>
	        </div>
	        	        <div class="form-group">
	        	<button class="btn btn-block btn-primary" id="btn_copylogs" data-dismiss="modal">Copy</button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div> 
	<div class="modal fade" tabindex="-1" role="dialog" id="pendingmodalview">
	  <div class="modal-dialog modal-sm"  role="document">
	    <div class="modal-content" style="background-color: black; color: white; height: 300px; overflow: hidden;">
	      <div class="modal-body">
	      	<!-- FINISHED : http://3.bp.blogspot.com/-KzBRjxntRzk/UtheVM5bQuI/AAAAAAAAkBQ/wy_KjI01r9s/s1600/energy_smack-4b.gif -->
	      	<!-- LOADING : https://i.imgur.com/NqtcSDX.jpg -->
	     
	      <center>
	      	<h4 class="ultralight" id="com_text">Communicating with LAMS</h4>
	      	 <img src="https://www.spinpalace.com/content/Shared/images/homepage/loading_circle.gif" id="lod_pic" style="width: 200px;">
	      	</center>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="modal" tabindex="-1" role="dialog" id="rem_back_activ">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">

	      <div class="modal-body">
	      	<center>
	      		<h1 class="ultrabold">ACTIVATION</h1>
	      		<p>Do you want to activate this LAMS for Remote Backup?</p>
	      		<input type="hidden" id="toactiv_id" name="">
	      		<br>
	      		<button class="btn btn-primary" id="actid_btn">Yes</button>
	      		<button class="btn btn-light" data-dismiss="modal">No</button>
	      	</center>
	      </div>
	    </div>
	  </div>
	</div>
	
</body>
</html>
<script type="text/javascript">
	$("#regschoolstable").DataTable();
	
	$("#pendingtbl").DataTable();
	$(".copyclicky").click(function(){
		$("#selectedid").val($(this).data("idname"));
	})
	$(".activclicky").click(function(){
		$("#toactiv_id").val($(this).data("idname"));
	})
	
	
	$("#btn_copylogs").click(function(){
$("#lod_pic").attr("src","https://www.spinpalace.com/content/Shared/images/homepage/loading_circle.gif");
$("#com_text").html("Communicating to LAMS");
		$("#pendingmodalview").modal("show");
		var currentdate = $("#datetocopy_id").val();
		var idtoupdate = $("#selectedid").val();
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data: {tag: "copy_logs_signal",id_to_update:idtoupdate ,fetch_date:currentdate},
				success: function(data){
					//popnotification("Message",data,true);
					ischecking = true;
					currid = idtoupdate;
				}
		})
	})
	var ischecking = false;
	var currid = "";
	setInterval(function(){

		if(ischecking == true){
			$.ajax({
				type: "POST",
				url: "<?php weblink(); ?>",
				data: {tag:"getstatusoflams",idtocheck: currid},
				success: function(data){
					// alert(data);
					// if(ischecking == true){

						if(data == "true" && ischecking == true){
							ischecking = false;
						currid = "";
						$("#com_text").html("Backup request sent!");
						$("#lod_pic").attr("src","https://i.gifer.com/embedded/download/7cH7.gif");
						setTimeout(function(){
						$("#pendingmodalview").modal("hide");
							popnotification("Let me handle it.","Logs is copying in the background...",false);

						},2000)
					}
					// }
				}
			})
		}
	},500)

	$("#actid_btn").click(function(){
		
		var activ_id = $("#toactiv_id").val();
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data: {tag: "approve_a_lams_user", id_to_update:activ_id},
			success: function(data){
				location. reload(true);
			}
		})
	})

</script>


