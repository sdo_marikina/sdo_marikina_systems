<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include("theme/theme.php");
include("inc/weblink.php");
	?>
	<style type="text/css">
		.card{
			margin-bottom: 30px;
		}
	</style>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<style type="text/css">
		.fontbg{
			display: block;
			position: absolute;
			margin: 0px;
			padding: 0px;
			bottom: 0;
			right: 0;
			font-size: 170px;
			opacity: 0.2;
			margin-bottom: -50px;
			animation-name: slide_right;
			animation-duration: 0.8s;
			z-index: 0;
		}
		@keyframes slide_right{
			0%{
				opacity: 0;
				transform:  scale(1.5);
			}
			100%{
				margin-top: 0px;
			}
		}
	</style>
	<div class="container mt-3">
		<div style="margin-top:20px; ">
			<h3 class="ultrabold"><i class="fas fa-bolt"></i> Quick Access</h3>
			<div class="row">
				
				<div class="col-sm-4">
					<div class="card" style="overflow: hidden; height: 180px; background-position: center;">
						<div class="card-body">
							<h4 class="ultrabold">CDTRS Activation</h4>
							<p>Activate a new CDTRS installed system</p>
							<a href="man_lams.php"  class="btn btn-primary btn-sm"><i class="fas fa-arrow-circle-right"></i> Manage CDTRS Activation</a>
							
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card" style="overflow: hidden; height: 180px; background-position: center;">
						<div class="card-body">
							<h4 class="ultrabold">CDTRS Monitoring</h4>
							<p>See all online/offline CDTRS</p>
							<a href="monitor.php"  class="btn btn-primary btn-sm"><i class="fas fa-arrow-circle-right"></i> Monitor System</a>
							
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card" style="overflow: hidden; height: 180px; background-position: center;">
						<div class="card-body">
							<h4 class="ultrabold">Generate DTR</h4>
							<p>Generate a Centralized Daily Time Record</p>
							<a  href="storage.php" class="btn btn-primary btn-sm"><i class="fas fa-arrow-circle-right"></i> Go to DTR Generation</a>
							
						</div>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</body>
</html>
<script type="text/javascript">

</script>