<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	?>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<div class="container">
		<div style="margin-top:20px; ">
			<h1 class="ultrabold">FEEDBACK</h1>
		</div>
		<div class="card">
			<div class="card-body">
				<h1 class="ultralight">Most Recent</h1>
				<p>Displaying today's logs</p>
				<hr>
				<table class="table" id="cocoa">
					<thead>
						<tr>
							<th>Time</th>
							<th>System</th>
							<th>Feedback</th>
							<th>Emotion</th>
						</tr>
					</thead>
					<tbody>
						<?php getallfeed(); ?>
					</tbody>
				</table>
				</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	$("#cocoa").DataTable({
		  "ordering": false,
		  "order": [[ 1, "desc" ]]
	});
</script>