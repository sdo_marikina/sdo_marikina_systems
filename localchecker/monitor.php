<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	include("inc/weblink.php");
	?>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<br>

	<div class="container-fluid">
		<h4 class="ultrabold">CDTRS Monitor</h4>
		<div class="card">
			<div class=" card-body">
	<div class="row">
		
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-3">
					<center>
						<h5 class="text-muted">Overall Attendance Logs</h5>
						<h1 id="allattlogs" class="mb-5">1,000</h1>
					</center>
				</div>
				<div class="col-sm-3">
					<center>
						<h5 class="text-muted">Overall Employees</h5>
						<h1 id="allempscc" class="mb-5">1,000</h1>
					</center>
				</div>
				<div class="col-sm-3">
					<center>
						<h5 class="text-muted">All Whereabouts</h5>
						<h1 id="allwhereaboutsz" class="mb-5">1,000</h1>
					</center>
				</div>
				<div class="col-sm-3">
					<center>
						<h5 class="text-muted">Overall Authority to Attend</h5>
						<h1 id="ovallata" class="mb-5">1,000</h1>
					</center>
				</div>
				<div class="col-sm-3">
					<h5>Overall Leave Report</h5>
					<h4 id="allleavereports">0</h4>

					<script type="text/javascript">
						setInterval(function(){
							GetCoreInfos();
						},30000)
					</script>
					<h5>Leave by Station</h5>
					<table class="table table-sm">
						<thead>
							<tr>
								<th>Station</th>
								<th>Count</th>
							</tr>
						</thead>
						<tbody id="tbl_leavebystation">
							
						</tbody>
					</table>
				</div>
				<div class="col-sm-3">
					<h5>Overall Dispute Report</h5>
					<h4 id="overalldispute">0</h4>
		
					<h5>Most Disputed Station</h5>
					<table class="table table-sm">
						<thead>
							<tr>
								<th>Station</th>
								<th>Count</th>
							</tr>
						</thead>
						<tbody id="mostdisputedstation">
							
						</tbody>
					</table>
				</div>
				<div class="col-sm-6">

					<div class="row">
						
						<div class="col-sm-6">
							<h4>Version Chart</h4>
			<canvas id="myChart" style="margin: 0px !important;"></canvas>
						</div>
						<div class="col-sm-6" style="min-height: 300px;">
							<h5>Most Applied Leave Type</h5>
					<table class="table table-sm">
						<thead>
							<tr>
								<th>Leave Type</th>
								<th>Count</th>
							</tr>
						</thead>
						<tbody id="thetbl_most_appliedlvtype">
							
						</tbody>
					</table>



						</div>
						<div class="col-sm-12">
							<h5>Principals by Station</h5>
<table class="table table-sm">
<thead>
							<tr>
								<th>Station</th>
								<th>Principal</th>
							</tr>
						</thead>
						<tbody id="princtbl">
							
						</tbody>

</table>

						</div>
					</div>
					


				</div>
				

			</div>
				<h4>CDTRS Monitor</h4>
	<table class="table table-bordered table-striped table-sm" id="ol_tbl">
		<thead>
			<tr>
				<th>System Name</th>
				<th>IP Address</th>
				<th>Version</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody id="onliner">
			
		</tbody>
	</table>
		</div>
	</div>


		<div class="modal" tabindex="-1" role="dialog" id="modal_getleavereportsoftoday">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Leave Reports applied for today.</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <table class="table table-sm table-bordered" id="thelvttt">
	        	<thead>
	        		<tr>
	        			<th>Employee Name</th>
	        			<th>Leave Type</th>
	        			<th>Time Applied</th>
	        			<th>Status</th>
	        		</tr>
	        	</thead>
	        	<tbody id="appl_leavtblx">
	        		
	        	</tbody>
	        </table>
	      </div>

	    </div>
	  </div>
	</div>


	<div class="modal" tabindex="-1" role="dialog" id="modal_disputed_today">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Employees who applied for dispute today.</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <table class="table table-sm table-bordered" id="thedtofdisptods">
	        	<thead>
	        		<tr>
	        			<th>Name</th>
	        			<th>Reason of dispute</th>
	        		</tr>
	        	</thead>
	        	<tbody id="disputetbltoday">
	        		
	        	</tbody>
	        </table>
	      </div>

	    </div>
	  </div>
	</div>
<script>
	function LoadLeaveToday(){
		$("#thelvttt").DataTable().destroy();
			$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "lodleavetbltodayx"},
							success: function(data){
								$("#appl_leavtblx").html(data);
$("#thelvttt").DataTable({
									"ordering": false
								});
							}
						})
	}
	function LoadDisputedToday(){
			$("#thedtofdisptods").DataTable().destroy();
		$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "loadempdisptodayintbl"},
							success: function(data){
								$("#disputetbltoday").html(data);
								$("#thedtofdisptods").DataTable({
									"ordering": false
								});
							}
						})
	}
	GetCoreInfos();
	function GetCoreInfos(){

		
$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "getovata"},
							success: function(data){
								$("#ovallata").html(data);
							},timeout:3000
						})

			$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "getallwhereabouts"},
							success: function(data){
								$("#allwhereaboutsz").html(data);
							},timeout:3000
						})

			$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "getallteacherscount"},
							success: function(data){
								$("#allempscc").html(data);
							},timeout:3000
						})
		$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "getoverallattlogs"},
							success: function(data){
								$("#allattlogs").html(data);
							},timeout:3000
						})

		$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "gelvcount"},
							success: function(data){
								$("#allleavereports").html(data);
							},timeout:3000
						})

$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "get_all_school_princ"},
							success: function(data){
								$("#princtbl").html(data);
							},timeout:3000
						})

$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "get_most_applied_lvtype"},
							success: function(data){
								$("#thetbl_most_appliedlvtype").html(data);
							},timeout:3000
						})

	$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "getmostdiputedss"},
							success: function(data){
								$("#mostdisputedstation").html(data);
							},timeout:3000
						})


	$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "getovdisp"},
							success: function(data){
								$("#overalldispute").html(data);
							},timeout:3000
						})

		$.ajax({
							type: "POST",
							url: "func/online_syncher.php",
							data : {tag: "get_lv_by_station"},
							success: function(data){
								$("#tbl_leavebystation").html(data);
							},timeout:8000
						})

	}
var ctx = document.getElementById('myChart').getContext('2d');
var versions = [];
var countsPerVersion =[];
$.ajax({
	type:"POST",
	url: "func/online_syncher.php",
	data: {tag:"get_all_versions"},
	success: function(data){
		// alert(data);
		var version_names = data.split("|");
		for(var i =0; i < version_names.length;i++){
			if(version_names[i] != ""){
				 var single_item = version_names[i].split(",");
		versions.push("CDTRS - " + single_item[0]);
		countsPerVersion.push(single_item[1]);
			}
		
		}
		var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: versions,
        datasets: [{
            label: '# of Votes',
            data: countsPerVersion,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                 'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
		            }]
		        }
			}
		});
	}
})

</script>

			</div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
	LoadOnlines();

	function LoadOnlines(){
		$.ajax({
			type: "POST",
			url: "func/online_syncher.php",
			data: {tag: "get_all_active_cdtrs"},
			success: function(data){
				// alert(data);
				$("#onliner").html(data);
				$("#ol_tbl").DataTable();
			}
		})
	}
</script>