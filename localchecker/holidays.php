<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	include("inc/weblink.php");
	?>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<div class="container">
		<div style="margin-top:20px; ">
			<button class="btn btn-primary float-right" data-toggle="modal" data-target="#modal_newholiday"><i class="fas fa-magic"></i> New Holiday</button>
			<h1 class="ultrabold">Holidays</h1>
		</div>
		<div class="card">
			<div class="card-body">
							<table class="table table-bordered table-striped" id="emp_tbl">
			<thead>
				<tr>
					<th>Name</th>
					<th>Date</th>
					<th>Options</th>
				</tr>
			</thead>
			<tbody>
				<?php ShowHolidays(); ?>
			</tbody>
		</table>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	
	$("#backupstorage_manual").DataTable({
		  "ordering": false
	});
	$("#backupstorage").DataTable({
		  "ordering": false
	});
</script>

<?php
include("comp/storagemodals.php");
?>
<form action="func/direct_access.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="modal_newholiday">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultrabold"><i class="far fa-sun"></i> New Holiday</h4>
       <div class="form-group">
       	<input type="hidden" name="tag" value="addholdi">
       	<label>Holiday Name</label>
       	<input class="form-control" required="" autocomplete="off" type="text" placeholder="Type holiday name here..." name="nameOfHoliday">
       </div>
        <div class="form-group">
       	<label>Date</label>
       	<input class="form-control" required="" autocomplete="off" type="date" name="dateOfHoliday">
       </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="AddNewHoliday" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add Holiday</button>
        <button type="button" class="btn btn-light" data-dismiss="modal"><i class="far fa-window-close"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<form action="func/direct_access.php" method="POST">
<div class="modal" tabindex="-1" role="dialog" id="modal_edit_holiday">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultrabold">Edit Holiday</h4>
      	<input type="hidden" name="holID" id="updt_id" value="">
      	<input type="hidden" name="tag" value="EditHoliday">
        <div class="form-group">
        	<label>New Holiday Name</label>
        	<input type="text"  id="updt_holname" class="form-control" required="" name="newHolName">
        </div>
        <div class="form-group">
        	<label>New Date</label>
        	<input type="date"  id="updt_date" class="form-control" required="" name="newHolDate">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>	
</form>

<script type="text/javascript">
	function ShowUpdateModal(c_object){
	 	// PREPARE UPDATE INFORMATION
		$("#updt_id").val($(c_object).data("updt_id"));
		$("#updt_holname").val($(c_object).data("updt_holname"));
		$("#updt_date").val($(c_object).data("updt_date"));
	}
</script>


<form action="func/direct_access.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="modal_del_holiday">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
<input type="hidden" name="tag" value="delholiday">
      	<h4 class="ultrabold">Delete This Holiday?</h4>
        <p>This will be deleted forever!</p>
        <input type="hidden" id="inp_holiID" name="inp_holiID">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="far fa-trash-alt"></i> Delete</button>
        <button type="button" class="btn btn-light" data-dismiss="modal"><i class="far fa-window-close"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
	function DeleteHol(c_object){
		$("#inp_holiID").val($(c_object).data("holid"));
	}
</script>
<script type="text/javascript">
	$("#emp_tbl").DataTable();
</script>