<?php  	include("func/auth.php"); ?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
	<title></title>
	<?php
	include("theme/theme.php");
	include("func/displayer.php");
	include("inc/weblink.php");
	?>
</head>
<body class="defaultbg">
	<?php
		include("comp/nav.php");
	?>
	<div class="container-fluid">
		<div style="margin-top:20px; ">





					<div class="row">
						<div class="col-sm-12">
							<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Manage Local Storage
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a href="#" class="dropdown-item"><i class="fas fa-sync"></i>Update Employees</a>
<a href="#" class="dropdown-item" data-toggle="modal" data-target="#sync_schedules_modal"><i class="fas fa-sync"></i> Update Schedule</a>
<a href="#" class="dropdown-item" data-toggle="modal" data-target="#sync_attendance_modal"><i class="fas fa-sync"></i> Update Locally Stored Logs</a>
  </div>
</div>
	<h4 class="ultrabold">Centralized DTR Generation</h4>		
						</div>
					<div class="col-sm-6">
						<h4>ATTENDANCE LOGS</h4>
						<table class="table table-striped table-bordered table-sm">
					<thead>
						<th>School/Division Name</th>
						<th>Device logs</th>
						<th>Manual logs</th>
						<th>Overall Logs</th>

						<th>Action</th>
					</thead>
					<tbody id="realtimebackupper">

					</tbody>
				</table>
					</div>
					<div class="col-sm-6">
						<h4>EMPLOYEES</h4>

						<table class="table table-striped table-bordered table-sm">
					<thead>
						<th>School/Division Name</th>
						<th>Employees</th>
						<th>Action</th>
					</thead>
					<tbody id="realtimeemployeesbackupper">

					</tbody>
				</table>
					</div>
				</div>



			
		</div>
	</div>




<div class="modal" tabindex="-1" role="dialog" id="sync_schedules_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Sync All Schedules to Local Database</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>This will store all the Multiple Schedule data to your local database.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="GetMultipleScheduledData()">Yes, sync all data</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	
function GetMultipleScheduledData(){
		$.ajax({
		type: "POST",
		url: "func/online_syncher.php",
		data: {tag:"sync_multiple_sched_data"},
		success: function(data){
			alert(data);
		}
	})
}
</script>
</body>
</html>
<script type="text/javascript">
	
	$("#backupstorage_manual").DataTable({
		  "ordering": false
	});
	$("#backupstorage").DataTable({
		  "ordering": false
	});
</script>

<?php
include("comp/storagemodals.php");
?>

<div class="modal" tabindex="-1" role="dialog" id="modal_renschool">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultrabold"><i class="fas fa-signature"></i> Rename School</h4>
        <div class="form-group">
        	<label>Old Name</label>
        	<input type="text" class="form-control" disabled="" id="sc_ol_name" name="">
        </div>
        <div class="form-group">
        	<label>New Name</label>
        	<input type="text" class="form-control" id="sc_new_name" name="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_saveNewSCName" class="btn btn-primary">Save Rename</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function openRenameSchool(c_obj){
		$("#sc_ol_name").val($(c_obj).data("oldname"));
		$("#btn_saveNewSCName").click(function(){
			var sc_ol_name = $("#sc_ol_name").val();
			var sc_new_name = $("#sc_new_name").val();
				$.ajax({
					type: "POST",
					url: "func/server.php",
					data: {tag: "renameSchool",oldName:sc_ol_name,newName:sc_new_name},
					success: function(data){
						$("#sc_new_name").val("");
						alert(data);
					}
				})
		})
	}
</script>

<div class="modal" tabindex="-1" role="dialog" id="sync_attendance_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h1 class="ultrabold">Sync Attendance Logs</h1>
      	<div class="form-group">
      		<label>Sync From</label>
      		<input type="date" id="dd_from" class="form-control">
      	</div>
      	<div class="form-group">
      		<label>To</label>
      		<input type="date" id="dd_to" class="form-control">
      	</div>
      	<div class="form-group">

      		<div class="card" id="pro_ui" style="display: none;">
      			<div class="card-body">
      				<div id="s_1">
      					<h4>Step 1/2</h4>
      				<p>Downloading Attendance from SDO Online Server.</p>
      				<div class="form-group">
      					<p><img src="https://i.gifer.com/7plQ.gif" style=" width: 80px;"> Loading</p>
      				</div>
      				<hr>
      				</div>
      				<div id="s_2">
  					<h4>Step 2/2</h4>
      				<p>Storing Attendance Logs from Local Database.</p>
      				<div id="alllog">
      					
      				</div>
      			<div style="display: none;">
      					<small>New: <span id="lcountui">0</span></small><br>
      				<small>Synched: <span id="currentlgoprocessed">0</small><br>
      				<small>Already Existing: <span id="errTxt">0</span></small><br>
      				<progress id="progs" value="0" max="0" style="width: 100%;">
      				
      			</div>
      				<button id="btn_fin" class="btn btn-primary">Finish</button>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>
     
      <div class="modal-footer" id="processbutts">
        <button type="button" class="btn btn-primary" onclick="SyncAndDownloadLogs()">Begin Synchronization</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

	$("#btn_fin").click(function(){
		$("#pro_ui").css("display","none");
		$("#processbutts").css("display","block");
		$("#alllog").html("");
	})
	
	function SyncAndDownloadLogs(){
		$.ajax({
			type: "POST",
			url: "func/direct_access.php",
			data: {tag: "cleardata"},
			success: function(data){
				alert(data);

			}
		})
		$("#processbutts").css("display","none");
		$("#btn_fin").css("display","block");
		$("#currentlgoprocessed").html("0");
		$("#errTxt").html("0");
		$("#pro_ui").css("display","block");
		$("#s_1").css("display","block");
		$("#s_2").css("display","none");
		$("#progs").attr("max","0");
		$("#progs").attr("value","0");
		var input_from = $("#dd_from").val();
		var input_to = $("#dd_to").val();

var start = new Date(input_from);
var end = new Date(input_to);

var loop = new Date(start);
var cc = 0;
while(loop <= end){
	cc++;
	var theid = "c_" + cc;
// alert(loop.getFullYear() +"-"+ loop.getMonth() +"-"+ loop.getDate());           
var theactualdate = loop.getFullYear() +"-"+ (loop.getMonth() + 1) +"-"+ loop.getDate();
$("#alllog").append("<p >"+ theactualdate+" - <span id='" + theid + "'><i style='color:#f39c12;' class='far fa-circle'></i></span></p>");
syncDayByDay(theactualdate,theid);
var newDate = loop.setDate(loop.getDate() + 1);
loop = new Date(newDate);

}
}
	function syncDayByDay(theactualdate,theid){
		$("#s_2").css("display","block");
		// alert(theactualdate);
		$.ajax({
		type : "POST",
		url: "func/online_syncher.php",
		data: {tag:"Download_AttendanceLogs",dd_from: theactualdate,dd_to: theactualdate},
		success: function(data){
			$("#s_1").css("display","none");
			// SyncStep_2(data);
			DIRECTINSERT(data,theid);
		}
	})
	}
	var tot = 0;
	var succ = 0;
	var err = 0;

	function DIRECTINSERT(attlogs,theid){
		$.ajax({
		type : "POST",
		url: "func/online_syncher.php",
		data: {tag:"direct_insert_attlogs",attendance:attlogs},
		success: function(data){
			$("#" + theid).html(data);
		}
		})
	}

	function SyncStep_2(att_data){
		tot = 0;
		succ = 0;
		err = 0;
		

		$("#s_2").css("display","block");
		att_data = JSON.parse(att_data);
		// alert(att_data[0]["eid"]);
		var logcount = att_data.length;
		tot = logcount;
		// alert(logcount);
		$("#lcountui").html(logcount);
		$("#progs").attr("max",logcount);
		for(var i =0; i < att_data.length;i++){
		var i_eid =att_data[i]["eid"];
		var i_access_type =att_data[i]["access_type"];
		var i_access_image =att_data[i]["access_image"];
		var i_date =att_data[i]["date"];
		var i_timestamp =att_data[i]["timestamp"];
		var i_ismanual =att_data[i]["ismanual"];
		var i_company =att_data[i]["company"];
		var i_local_id =att_data[i]["local_id"];

		// alert(i_eid);
		// setTimeout(function(){
			InsertDataSingle(i_eid,i_access_type,i_access_image,i_date,i_timestamp,i_ismanual,i_company,i_local_id);
		// },1)

		}
	}

	function InsertDataSingle(i_eid,i_access_type,i_access_image,i_date,i_timestamp,i_ismanual,i_company,i_local_id){
		// if(err <= succ){
			$.ajax({
		type : "POST",
		url: "func/online_syncher.php",
		data: {tag:"InsertALog",
		eid:i_eid,
		access_type:i_access_type,
		access_image:i_access_image,
		date:i_date,
		timestamp:i_timestamp,
		ismanual:i_ismanual,
		company:i_company,
		local_id:i_local_id},
		success: function(data){
			if(data == "1"){
			var cl = $("#currentlgoprocessed").html();
			cl = parseInt(cl) + parseInt(1);
			$("#currentlgoprocessed").html("" + cl);
				succ += 1;
			}else{
				var ec = $("#errTxt").html();
				ec = parseInt(ec) + parseInt(1);
				$("#errTxt").html("" + ec);
				err += 1
			}
			$("#progs").attr("value",$("#currentlgoprocessed").html());
			if((err + succ) == tot){
				// $("#pro_ui").css("display","none");
		$("#s_1").css("display","none");
		// $("#s_2").css("display","none");
				// alert("Finished downloading all logs!");
				$("#btn_fin").css("display","block");
				// $("#btn_fin").html("display","none");
			}
		}
		})
			

			// }
	}
</script>