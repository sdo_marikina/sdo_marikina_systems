<?php
$webservice_url = "http://localhost/sdo_marikina_systems/lams_backup/index.php";
// $webservice_url = "https://depedmarikina.ph/cdtrs_webservices/cdtrs_backup/index.php";

function ShowHolidays(){
	$form_data = array(
	"tag"=>"getholidays",
	);
	send_request($form_data);
}
function getallfeed(){
		// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"getallfeed",
	);
	send_request($form_data);
// END CURL ACCESS
}
function GetLamsRegisteredSchools(){
	// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"get_lams_registered_schools",
	);
	send_request($form_data);
// END CURL ACCESS
}
function GetLamsRegisteredSchools_Unregistered(){
		// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"get_lams_registered_schools_unregistered",
	);
	send_request($form_data);
// END CURL ACCESS
}
function GetLamsBackup(){
// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"get_lams_backup_data",
	);
	send_request($form_data);
// END CURL ACCESS
}
function GetLamsBackup_manual(){
// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"get_lams_backup_data_manual",
	);
	send_request($form_data);
// END CURL ACCESS
}
function GetActionLogsForToday(){
	// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"get_action_logs_today",
	);
	send_request($form_data);
// END CURL ACCESS
}
function GetActionLogsAll(){
	// BEGIN CURL ACCESS
	$form_data = array(
	"tag"=>"get_action_logs_all",
	);
	send_request($form_data);
// END CURL ACCESS
}
function send_request($form_data){
	global $webservice_url;
	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$webservice_url);
	curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
?>