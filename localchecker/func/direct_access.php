<?php
$c = mysqli_connect("localhost","root","","lams_backup");
mysqli_set_charset($c,"utf8");
date_default_timezone_set('Asia/Manila');
$tag = $_POST["tag"];
switch($tag){
	case 'addnewacc':
		$add_empnumber = mysqli_real_escape_string($c,htmlentities($_POST["add_empnumber"]));
		$add_empname = mysqli_real_escape_string($c,htmlentities($_POST["add_empname"]));
		$add_emp_desc = mysqli_real_escape_string($c,htmlentities($_POST["add_emp_desc"]));
		$add_empmac_add = $_POST["add_empmac_add"];

		$q = "INSERT INTO online_bio_macs SET 
		physical_add='" . $add_empmac_add . "',
		name='" . $add_empname . "',
		description='" . $add_emp_desc . "',
		eid='" . $add_empnumber . "'
		";
		if (mysqli_query($c,$q) or die(mysqli_error($c))) {
			echo "Break Access Added!";
		}else{
			echo "Can't add break access, try again later.";
		}
		break;
	case "removeempfromcdtrsbreak":
	$idofemptoremovefromaccess = $_POST["idofemptoremovefromaccess"];
	$q = "DELETE FROM online_bio_macs WHERE id='" . $idofemptoremovefromaccess . "'";
	if (mysqli_query($c,$q)) {
		echo "Access is successfully deleted to the database.";
	}else{
		echo "There's a problem deleting this access to the database. Please try again.";
	}
	break;
	case "allemp_regtobreak":
		$q = "SELECT * FROM online_bio_macs";
		$res = mysqli_query($c,$q);
		while($row = mysqli_fetch_array($res)){
			echo "<tr>
			<td>" . $row["eid"] . "</td>
			<td>" . $row["name"] . "</td>
			<td>" . $row["description"] . "</td>
			<td>" . $row["physical_add"] . "</td>
			<td><button onclick='opendeleteempfromcdtrsbreak(this)' data-empidofemp='" . $row["name"] . "' data-theobjid='" . $row["id"] . "' class='btn btn-sm btn-danger btn-block' data-toggle='modal' data-target='#modal_empacc_delete'>Remove</button></td>
			</tr>";
		}
	break;
	case "cleardata":
	$q = "DELETE FROM attendance_logs";
	if(mysqli_query($c,$q)){
		echo "Logs cleared!";
	}else{
		echo "There's an error clearing the attendance logs.";
	}
	break;
	case "addholdi":
	$nameOfHoliday = mysqli_real_escape_string($c,htmlentities($_POST["nameOfHoliday"]));
	$dateOfHoliday = date("m-d", strtotime($_POST["dateOfHoliday"]));

	$q = "SELECT * FROM holidays WHERE name='" . $nameOfHoliday . "' AND date='" . $dateOfHoliday . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "INSERT INTO holidays(name,date) VALUES('" . $nameOfHoliday . "','" . $dateOfHoliday . "')";
		if(mysqli_query($c,$q)){
BackToPage_Message("Holiday added successfully!","../holidays.php");
		}else{
BackToPage_Message("Failed to insert holiday! Please report this malfunction of SDO Marikina ICTU Department.","../holidays.php");
		}
		
	}else{
		BackToPage_Message("Your holiday is already added into the database. No Changes has been made.","../holidays.php");
	}
	break;
	case "delholiday":
	$inp_holiID = $_POST["inp_holiID"];
	$q = "DELETE FROM holidays WHERE id='" . $inp_holiID . "'";
	if(mysqli_query($c,$q)){
		BackToPage_Message("Holiday Deleted!","../holidays.php");
	}else{
		BackToPage_Message("Can't be deleted, Please report this problem to the SDO ICTU Department of Marikina.","../holidays.php");
	}
	break;
	case "EditHoliday":
	
	$holID = $_POST["holID"];
	$newHolName = $_POST["newHolName"];
	$newHolDate = date("m-d", strtotime($_POST["newHolDate"]));

	$q = "SELECT * FROM holidays WHERE name='" . $newHolName . "' AND date='" . $newHolDate  . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "UPDATE holidays SET name='" . $newHolName . "', date='" . $newHolDate . "' WHERE id='" . $holID ."'";
		if(mysqli_query($c,$q)){
			BackToPage_Message("Changes has been saved!","../holidays.php");
		}else{
			BackToPage_Message("Unable to save changes. Please report this problem to SDO ICTU Department of Marikina.","../holidays.php");
		}
	}else{
		BackToPage_Message("New changes you made in this Holiday data is already exisiting in the database. No Changes has been made.","../holidays.php");
	}
	break;
}


//Universal page returner
function BackToPage_Message($res,$pagename){

	echo "<script>
	alert('" . $res . "');
	window.location.href = '" . $pagename . "';
	</script>";

}

?>