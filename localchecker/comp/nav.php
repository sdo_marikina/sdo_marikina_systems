<nav class="navbar navbar-expand-lg navbar-light bg-light" style="display: block; position: fixed; width: 100%; top: 0; z-index: 100;">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="dashboard.php"><i class="fas fa-bolt"></i> Quick Access</a>
      </li>
      <a class="nav-link" href="man_lams.php"><i class="far fa-check-circle"></i> CDTRS Activation</a>
      <li class="nav-item">
        <a class="nav-link" href="storage.php"><i class="fas fa-universal-access"></i> DTR Generation</a>
      </li>
      <a class="nav-link" href="monitor.php"><i class="fas fa-desktop"></i> CDTRS Monitor</a>
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Miscellaneous
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
           
          <a class="dropdown-item" href="holidays.php">Holidays</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#managecdtrsbreakmodal" onclick="RefreshAllgranteduserslist()">Manage: CDTRS Break</a>
          <a class="dropdown-item" href="logmerger.php">Log Merger (Experimental)</a>
        </div>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="logs.php"><i class="fas fa-cog"></i> Logs</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="feed.php"><i class="fas fa-comment-alt"></i> Feedback</a>
      </li>
    </ul>
<!--     <form class="form-inline my-2 my-lg-0" action="func/server.php" method="POST">
      <input type="hidden" name="tag" value="logout">
      <button class="btn btn-primary" type="submit">Sign-out</button>
    </form> -->
  </div>
</nav>
<br>
<br>


<div class="modal" tabindex="-1" role="dialog" id="managecdtrsbreakmodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="ultrabold">CDTRS Break</h4>
        <br>
         <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#allregusers" role="tab" aria-controls="allregusers" aria-selected="true"><i class="fas fa-arrow-circle-right"></i> All Registered Users</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#addnewacc" role="tab" aria-controls="addnewacc" aria-selected="false"><i class="fas fa-arrow-circle-right"></i> Add New Access</a>
          </li>
  
        </ul>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="allregusers" role="tabpanel" aria-labelledby="pills-home-tab">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Employee ID</th>
                    <th>Employee Number</th>
                    <th>Employee Description</th>
                    <th>Mac</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="allemp_regtobreak">

                </tbody>
              </table>

          </div>
          <div class="tab-pane fade" id="addnewacc" role="tabpanel" aria-labelledby="pills-profile-tab">

              <div class="form-group">
              <label>Employee Number</label>
              <input type="text" class="form-control" required="" placeholder="Type here..." id="add_empnumber">
            </div>
            <div class="form-group">
              <label>Employee Name</label>
              <input type="text" class="form-control" required="" placeholder="Type here..." id="add_empname">
            </div>
            <div class="form-group">
              <label>Employee Description</label>
              <textarea class="form-control" required="" placeholder="Type here..." id="add_emp_desc"></textarea>
            </div>
            <div class="form-group">
              <label>Employee Mac Address</label>
              <input type="text" class="form-control" required="" placeholder="Type here..." id="add_empmac_add">
            </div>
            <div class="form-group">
               <button type="button" onclick="addaa()" class="btn btn-primary">Add Access</button>
            </div>
   
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


  <div class="modal" tabindex="-1" role="dialog"  id="modal_empacc_delete">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h4 class="ultrabold">Remove Access?</h4>
         <p>Do you want to remove this employee access to <u id="sibjectnameofemployee">Sample Employee Name</u>?</p>
         <input type="hidden" name="tag" value="removeempfromcdtrsbreak">
         <input type="hidden" name="idofemptoremovefromaccess" id="idofemptoremovefromaccess">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="delaccbreak()">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">

  function delaccbreak(){
    var theidtodelacc = $("#idofemptoremovefromaccess").val();
    $.ajax({
      type: "POST",
      url : "func/direct_access.php",
      data: {tag: "removeempfromcdtrsbreak", idofemptoremovefromaccess: theidtodelacc},
      success: function(data){
        // alert(data);
          RefreshAllgranteduserslist();
      }
    })
  }
  function opendeleteempfromcdtrsbreak(controlOBJ){
    // alert($(controlOBJ).data("empidofemp"));

    
    $("#idofemptoremovefromaccess").val($(controlOBJ).data("theobjid"));
    $("#sibjectnameofemployee").html($(controlOBJ).data("empidofemp"));
  }
  function addaa(){
    var add_empnumber_a = $("#add_empnumber").val();
    var add_empname_a = $("#add_empname").val();
    var add_emp_desc_a = $("#add_emp_desc").val();
    var add_empmac_add_a = $("#add_empmac_add").val();

    if(add_empnumber_a != "" &&  add_empname_a != "" && add_emp_desc_a != "" && add_empmac_add_a != ""){
  $.ajax({
                  type: "POST",
                  url : "func/direct_access.php",
                  data: {tag: "addnewacc",add_empnumber:add_empnumber_a,add_empname:add_empname_a,add_emp_desc:add_emp_desc_a,add_empmac_add:add_empmac_add_a},
                  success: function(data){

                    $("#add_empnumber").val("");
                    $("#add_empname").val("");
                    $("#add_emp_desc").val("");
                    $("#add_empmac_add").val("");
                    alert(data);
                      // $("#allemp_regtobreak").html(data);
                      RefreshAllgranteduserslist()
                  }
                })
    }else{
      alert("Please fill-up all blank fields.");
    }
  }

  function RefreshAllgranteduserslist(){
    $.ajax({
      type: "POST",
      url : "func/direct_access.php",
      data: {tag: "allemp_regtobreak"},
      success: function(data){
          $("#allemp_regtobreak").html(data);
      }
    })
  }
</script>