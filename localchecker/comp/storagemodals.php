

<div class="modal" tabindex="-1" role="dialog" id="logsquicksightmodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">  
		<div id="quicklookuiattlogs">
			<h4 class="ultrabold">ATTENDANCE</h4>
				<table class="table table-striped table-bordered" id="attlogsinfo">
				<thead>
				<th>Month</th>
				<th>Logs Count</th>
				</thead>
				<tbody id="quicklook_logsinfo">
				</tbody>
				</table>
		</div>
		<center>
		<div id="loadqluiloadingattlogs">
			<br>
			<br>
				<h2 class="ultrabold" style="color: rgba(0,0,0,0.5);"><i class="far fa-eye"></i> QUICK SIGHT</h2>
				<img src="https://steamuserimages-a.akamaihd.net/ugc/267223080226459171/985912CAD9866AE8997702BB87823D78E1E66110/" style="width:100px;">
			<br>
			<br>
		</div>
		</center>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_quicklook_manualentries">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div id="qs_manualatt">
        	<h4 class="ultrabold">MANUAL ENTRIES</h4>

        		<table class="table table-striped table-bordered" id="attlogsinfo_man_entry">
				<thead>
				<th>Employee Name</th>
				<th>Manual Count</th>
				</thead>
				<tbody id="quicklook_manualentry_quicklook_table">

				</tbody>
				</table>
        </div>	      
        <center>
        	<div id="qs_manualatt_loading">
        		<br>
        		<br>
        		<h2 class="ultrabold" style="color: rgba(0,0,0,0.5);"><i class="far fa-eye"></i> QUICK SIGHT</h2>
        	<img src="https://steamuserimages-a.akamaihd.net/ugc/267223080226459171/985912CAD9866AE8997702BB87823D78E1E66110/" style="width:100px;">
        	<br>
        	<br>
        	</div>
        	
        </center>

      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modal_quicklook_overalllogs">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<div id="qs_overallatt">
      		 <h4 class="ultrabold">OVERALL ATTENDANCE</h4>
      		 <table class="table table-bordered table-striped" id="qs_ov_tbl">
      		 	<thead>
	      		 	<th>Employee Name</th>
					<th>Overall Count</th>
      		 	</thead>
      		 	<tbody id="qs_overalltbl">
      		 		
      		 	</tbody>
      		 </table>
      	</div>
       

                <center>
        	<div id="qs_overallentry_loading">
        		<br>
        		<br>
        		<h2 class="ultrabold" style="color: rgba(0,0,0,0.5);"><i class="far fa-eye"></i> QUICK SIGHT</h2>
        	<img src="https://steamuserimages-a.akamaihd.net/ugc/267223080226459171/985912CAD9866AE8997702BB87823D78E1E66110/" style="width:100px;">
        	<br>
        	<br>
        	</div>
        	
        </center>

      </div>
    </div>
  </div>
</div>


	<div class="modal" tabindex="-1" role="dialog" id="emp_modal">
				  <div class="modal-dialog modal-lg" role="document">
				    <div class="modal-content">
				      <div class="modal-body">

				      	<div id="empworkableui">
				      			
				        <h4 class="ultrabold">EMPLOYEES</h4>
						<p>Some information are only available on the Client Machine.</p>
				        <table class="table table-striped table-bordered" id="empqll">
				        	<thead>
				        		<th>Employee ID</th>
				        		<th>RFID</th>
				        		<th>Name</th>
				        		<th>Pay</th>
				        		<th>Schedule</th>
				        	</thead>
				        	<tbody id="quicklook_emp_container">
				        		
				        	</tbody>
				        </table>
				      	</div>
				      
				        <center>
				        	<div id="loadingempgif">
				        		<br>
				        		<br>
				        		<h2 class="ultrabold" style="color: rgba(0,0,0,0.5);"><i class="far fa-eye"></i> QUICK SIGHT</h2>
				        	<img src="https://steamuserimages-a.akamaihd.net/ugc/267223080226459171/985912CAD9866AE8997702BB87823D78E1E66110/" style="width:100px;">
				        	<br>
				        	<br>
				        	</div>
				        	
				        </center>
				      </div>
				    </div>
				  </div>
				</div>



<form action="comp/dtr_visualizer.php" method="POST" target="_blank">
	<div class="modal" tabindex="-1" role="dialog" id="dtrvis_modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      <center>  <h4 class="ultrabold">Generate DTR</h4>
	      <h4 id="school_name"></h4>
	      </center>
	      <br>
	      <input type="hidden" id="theschoolbnametogenerate" name="sc_name">
	     <div class="row">
	     	<div class="col-sm-6">
	      <div class="form-group">
	      	<label>From</label>
	      	<input id="gen_dtr" class="form-control" type="date" name="dt_from">
	      </div>
	     	</div>
	     <div class="col-sm-6">
	      <div class="form-group">
	      	<label>To</label>
	      	<input id="geh_to" class="form-control" type="date" name="dt_to">
	      </div>
	     	</div>
	     </div>
	      <div class="form-group">
	      	<label>Employee Type</label>
	      	<select class="form-control" name="emp_type">
	      		<option value="0">All</option>
	      		<option value="1">Non-Teaching</option>
	      		<option value="2">Teaching</option>
	      		<option value="3">Division</option>
	      	</select>
	      </div>
	<br>
	<p id="gendtrmess"></p>
	<button class="btn btn-primary btn-block" id="subdtrbtn" type="submit"><i class="fas fa-sync"></i> Generate DTR</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>

<div class="modal" tabindex="-1" role="dialog" id="cdtrs_unit_statistics_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
			<h4 class="ultrabold">STATISTICS</h4>
			<p>Showing log records last 31 days</p>
				<div id="thachart">
					
				</div>
<script>
	function get_data_statistics(school_name){
		$("#thachart").html('<canvas id="myChart" width="400" height="1000"></canvas>');
	$.ajax({
		type: "POST",
		url: "func/server.php",
		data: {tag:"get_recorded_data",sc_name:school_name},
		success: function(data){
			data = JSON.parse(data);


			var xlabels = [];
			var xvalues = [];
			for(var i =0; i < data.length;i++){
				xlabels.push(data[i]["date"]);
				xvalues.push(data[i]["logcount"]);
			}

			// xlabels.reverse();
// xvalues.reverse();
			var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: xlabels,
        datasets: [{
            label: '# of Logs',
            data: xvalues,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
		}
	})
}
</script>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

		setInterval(function(){
			validate_days();
		},100)

	function validate_days(){
		var gen_dtr = $("#gen_dtr").val();
		var geh_to = $("#geh_to").val();
		var date_diff_indays = countdatediff(gen_dtr,geh_to);
		// $("#gendtrmess").html(gen_dtr);

		if(gen_dtr != "" && geh_to != ""){
		if(date_diff_indays > 0){
			if(date_diff_indays <= 31){
				$("#gendtrmess").html("");
			$("#subdtrbtn").prop("disabled",false);
		}else{
			$("#subdtrbtn").prop("disabled",true);
			$("#gendtrmess").html("<i class='fas fa-exclamation-circle'></i> Cannot generate DTR higher than 31 days.");
		}
	}else{
		$("#subdtrbtn").prop("disabled",true);
		$("#gendtrmess").html("<i class='fas fa-exclamation-circle'></i> (From) must be earlier than (To) date.");

		// alert(date_diff_indays);
	}
		}else{
			$("#gendtrmess").html("<i class='far fa-question-circle'></i> Complete the given fields to generate DTR.");
		}

	}

	function countdatediff (date1, date2) {
dt1 = new Date(date1);
dt2 = new Date(date2);
return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24)) + 1;
}

	function gen_dtr(comp_name){
		$("#theschoolbnametogenerate").val(comp_name);
		$("#school_name").html(comp_name);
	}

	function getoverallemployees(comp_name) {
		// alert(comp_name);
		 $("#qs_ov_tbl").dataTable().fnDestroy();

			$("#qs_overallatt").css("display","none");
			$("#qs_overallentry_loading").css("display","block");
				$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data: {tag:"getoverallemployees",companyname: comp_name},
			success: function(data){

				// alert(data);
					$("#qs_overallatt").css("display","block");
					$("#qs_overallentry_loading").css("display","none");
				$("#qs_overalltbl").html(data);
				$("#qs_ov_tbl").DataTable();
			}
		})

	}
	function manentqltblload(cmp_name){
		// alert(cmp_name);
		 $("#attlogsinfo_man_entry").dataTable().fnDestroy();
			$("#qs_manualatt").css("display","none");
			$("#qs_manualatt_loading").css("display","block");
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data: {tag:"getmanentryemployees",companyname: cmp_name},
			success: function(data){
				// alert(data);
						$("#qs_manualatt").css("display","block");
		$("#qs_manualatt_loading").css("display","none");
				$("#quicklook_manualentry_quicklook_table").html(data);
						$("#attlogsinfo_man_entry").DataTable();
			}
		})
	}


					getrt_backup();
						getempbackups();
					setInterval(function(){
						getrt_backup();
						getempbackups();
					},5000)
					function getlogsinfo(comp_name){
										$("#quicklookuiattlogs").css("display","none");
						$("#loadqluiloadingattlogs").css("display","block");
						  $("#attlogsinfo").dataTable().fnDestroy();
								$("#quicklook_logsinfo").html("");
							$.ajax({
						type: "POST",
						url: "<?php weblink(); ?>",
						data: {tag:"getlogsinfo",companyname: comp_name},
						success: function(data){
							// alert(data);
							// alert(data);
							$("#quicklookuiattlogs").css("display","block");
							$("#quicklook_logsinfo").html(data);
							$("#attlogsinfo").DataTable( {
							  "ordering": false
							} );
							$("#loadqluiloadingattlogs").css("display","none");
						}
					})

					}
					function getemployeesinfo(comp_name){
						$("#empworkableui").css("display","none");
						$("#loadingempgif").css("display","block");
						  $("#empqll").dataTable().fnDestroy();
								$("#quicklook_emp_container").html("");
							$.ajax({
						type: "POST",
						url: "<?php weblink(); ?>",
						data: {tag:"getemployee_basicinfo",companyname: comp_name},
						success: function(data){
							// alert(data);
							$("#empworkableui").css("display","block");
							$("#quicklook_emp_container").html(data);
							$("#empqll").DataTable();
							$("#loadingempgif").css("display","none");
						}
					})


					}
					function getempbackups(){
							$.ajax({
						type: "POST",
						url: "<?php weblink(); ?>",
						data: {tag:"getempbackups"},
						success: function(data){
							// alert(data);
							$("#realtimeemployeesbackupper").html(data);
						}
					})
					}
					function getrt_backup(){
						$.ajax({
						type: "POST",
						url: "<?php weblink(); ?>",
						data: {tag:"getrealtimebackupdata"},
						success: function(data){
							// alert(data);
							$("#realtimebackupper").html(data);
						}
					})
					}
				</script>
