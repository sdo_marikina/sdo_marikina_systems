<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="../favs/application-vnd.ms-excel.ico" type="text/css" href="">
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

	<!--Font Awesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
	<?php include("../theme/dtrvis.php")?>
	<title>I.S Manager | DTR Visualizer</title>
</head>
<body>
<?php
// USER INPUT
	$sc_name = $_POST["sc_name"];
	$dt_from = $_POST["dt_from"];
	$dt_to = $_POST["dt_to"];
	$emp_type = $_POST["emp_type"];
?>
<div id="lod">
	<center>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<img style="width: 200px;" src="https://media.giphy.com/media/GMlsghNhStp6g/giphy.gif">
	<h1>Generating DTR</h1>
	<br>
	<p><strong><?php echo $sc_name; ?></strong><br>
		<span style="color: rgba(0,0,0,0.5);"><?php echo $dt_from . " - " . $dt_to; ?></span><br><b><?php 
		switch ($emp_type) {
			case 0:
				echo "All Employees";
				break;
					case 1:
				echo "Non-Teaching";
				break;
					case 2:
				echo "Teaching";
				break;
					case 3:
				echo "Division Personnel";
				break;
		}
		 ?></b></p>
	</center>
</div>
<div id="vis">
<div class="container" id="thedtrcontainer">

</div>
</div>
</body>
</html>
<script type="text/javascript">
	$("#lod").css("display","block");
	$("#vis").css("display","none");
	var x_sc_name = <?php echo json_encode($sc_name); ?>;
	var x_dt_from = <?php echo json_encode($dt_from); ?>;
	var x_dt_to = <?php echo json_encode($dt_to); ?>;
	var x_emp_type = <?php echo json_encode($emp_type); ?>;

	switch(x_emp_type){
		case 0:
		x_emp_type = 0;
		break;
		case 1:
    	x_emp_type = 1;
		break;
		case 2:
  		x_emp_type = 2;
		break;
		case 3:
    	x_emp_type = 3;
		break;
	}

	$.ajax({
		type: "POST",
		url: "dtr_webservice.php",
		data: {sc_name:x_sc_name,dt_from:x_dt_from,dt_to:x_dt_to,emp_type:x_emp_type},
		success: function(data){
				$("#lod").css("display","none");
	$("#vis").css("display","block");
			$("#thedtrcontainer").html(data);
		}
	})
</script>
