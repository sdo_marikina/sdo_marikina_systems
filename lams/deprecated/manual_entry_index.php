<!DOCTYPE html>
<html>
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="../favs/document-preview.ico" type="text/css" href="">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
  <!--Font Awesome-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>


<style type="text/css">
@font-face{
    font-family: 'sanfranc';
  src: url('../theme/fonts/sanfrancisco_pro.ttf'); /* IE9 Compat Modes */
}
@font-face{
    font-family: 'sanfranc_black'; src: url('theme/fonts/sanfrancisco_black.ttf'); /* IE9 Compat Modes */
}
      .blurbg{
          background-image: url(../images/header.jpg) !important;
      background-position: center !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
</style>
<title>LAMS | Manual Entry</title>
</head>
<body style="background-image: url(https://images.pexels.com/photos/921294/pexels-photo-921294.png?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);
      background-position: center;
      background-size: cover;
      background-attachment: fixed;
      font-family: sanfranc;
      ">
      <br>
	<div class="container" >
	<div class="jumbotron blurbg" >
		      <div class="row">
    <div class="col-sm-9">
     
<div class="row">
  <div class="col-sm-4">
    <div class="form-group">
      <label><i class="fas fa-search"></i> Search Name:</label>
  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." class="form-control" title="Type in a name">
</div>
  </div>
</div>
    </div>
    <div class="col-sm-3">
      <div class="form-group">
    <label>Choose Date:</label>
  <input type="date" id="LiveActionGetAllLogs" class="form-control" value="<?php echo date('Y-m-d'); ?>" name="">
  
  </div>
  <div class="form-group">
    <button id="btn_getlive" class="btn btn-sm btn-primary float-right"><i class="fas fa-arrow-circle-right"></i> Generate</button>
  </div>
    </div>
  </div>
   <center> <h3 id="mess"></h3></center>
<center>
   <img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" style="height: 100px; border-radius: 20px;" id="lod">
</center>
<div id="tb">

    <table class="table table-bordered table-striped" id="myTable" style="width: 100%;">
    <thead>
      <tr>
        <th>Employee Name</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="LogUX">
      
    </tbody>
  </table>
  <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</div>

	</div>

	</div>
</body>
</html>
<script type="text/javascript">
 
	var currval = $("#LiveActionGetAllLogs").val();
	GetAllEmployeesLogs(currval);
	// $("#LiveActionGetAllLogs").change(function(){
	// 	var ontimeval = $("#LiveActionGetAllLogs").val();
	// 	GetAllEmployeesLogs(ontimeval);
	// })
  $("#btn_getlive").click(function(){
   var ontimeval = $("#LiveActionGetAllLogs").val();
   GetAllEmployeesLogs(ontimeval);
  })

	function GetAllEmployeesLogs(curval){
    $("#lod").css("display","block");
       $("#mess").html("");
    $("#tb").css("display","none");
		$.ajax({
			type : "POST",
		 	url : "server.php",
		 	data : {tag: "getallemplogs",currdate: curval},
		 	success: function(data){
		 	if(data != ""){
  // alert(data);
        $("#LogUX").html(data);
    $("#lod").css("display","none");
    $("#tb").css("display","inline-block");
      $("#tb").css("width","100%");
      $("#mess").html("");
      }else{
            $("#lod").css("display","none");
    $("#tb").css("display","none");
        $("#mess").html("There are no error logs on " + curval);
      }
		 	}
		})
	}
</script>


  <div class="modal" tabindex="-1" role="dialog" id="modal_dis_approve">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-body">
      	<input type="hidden" value="manualizeentry" id="thetag" name="tag">
          <div class="row">
            <div class="col-sm-6">
              <h3 class="ultrabold" id="emp_name_header">Employee Name Here</h3>
            </div>
            <div class="col-sm-6">
              <input id="dt_ap_for" readonly="" type="text" class="form-control form-control-lg display-4 labelinput" style="background-color: transparent; border: none; font-size: 50px; text-align: right;" name="date_to_apply">
            </div>
          </div>
          <br>
        <div class="row">
            <div class="col-sm-3">
               <center>AM <small>IN</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t1" name="t1"> -->
                             <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input list="timelist" id="t1" name="t1" type="text" placeholder="00:00 --" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <center>AM <small>OUT</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t2" name="t2"> -->
                              <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                    <input list="timelist" id="t2" name="t2" type="text" placeholder="00:00 --" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
               <center>PM <small>IN</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t3" name="t3"> -->
                              <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                    <input list="timelist" id="t3" name="t3" type="text" placeholder="00:00 --" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
               <center>PM <small>OUT</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t4" name="t4"> -->
                              <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input list="timelist" id="t4" name="t4" type="text" placeholder="00:00 --" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
        </div>
                <datalist id="timelist">
          <option>7:00 AM</option>
          <option>7:30 AM</option>
          <option>8:00 AM</option>
          <option>8:30 AM</option>
          <option>9:00 AM</option>
          <option>9:30 AM</option>
          <option>10:00 AM</option>
          <option>10:30 AM</option>
          <option>11:00 AM</option>
          <option>11:30 AM</option>
          <option>12:00 PM</option>
          <option>12:30 PM</option>
          <option>1:00 PM</option>
          <option>1:30 PM</option>
          <option>2:00 PM</option>
          <option>2:30 PM</option>
          <option>3:00 PM</option>
          <option>3:30 PM</option>
          <option>4:00 PM</option>
          <option>4:30 PM</option>
          <option>5:00 PM</option>
          <option>5:30 PM</option>
          <option>6:00 PM</option>
           <option>6:30 PM</option>
          <option>7:00 PM</option>
          <option>7:30 PM</option>
          <option>8:00 PM</option>
          <option>8:30 PM</option>
          <option>9:00 PM</option>
          <option>9:30 PM</option>
          <option>10:00 PM</option>
          <option>10:30 PM</option>
          <option>11:00 PM</option>
          <option>11:30 PM</option>
          <option>12:00 AM</option>
          <option>12:30 AM</option>
        </datalist>
                  <br>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                format: 'LT'
                });
                $('#datetimepicker2').datetimepicker({
                format: 'LT'
                });
                $('#datetimepicker3').datetimepicker({
                format: 'LT'
                });
                $('#datetimepicker4').datetimepicker({
                format: 'LT'
                });
            });
        </script>
        <div class="form-group">
          <label><input type="checkbox" id="subdoc" name="doc_sub_stats"> Supporting document submitted</label>
        </div>
            <input type="hidden" id="id_dispute_approve" name="dispute_control">
            <input type="hidden" id="theeid" name="eid">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success"  data-dismiss="modal" name="btn_Approve" id="updatetimelog"><i class="fas fa-check"></i> Apply</button>
        <button type="button" class="btn" data-dismiss="modal"><i class="fas fa-times"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>





<script type="text/javascript">
  var globaltocontrolbutton = "";
  $("#updatetimelog").click(function(){
var tu_tag = $("#thetag").val();
var tu_date_to_apply = $("#dt_ap_for").val();
var tu_t1 = $("#t1").val();
var tu_t2 = $("#t2").val();
var tu_t3 = $("#t3").val();
var tu_t4 = $("#t4").val();
var tu_doc_sub_stats = $("#subdoc").val();
var tu_dispute_control = $("#id_dispute_approve").val();
var tu_eid = $("#theeid").val();
if(tu_t1 != "" && tu_t2 != "" && tu_t3 != "" && tu_t4 != ""){
    $.ajax({
      type: "POST",
      url: "server.php",
      data: {tag:tu_tag,date_to_apply:tu_date_to_apply,t1:tu_t1,t2:tu_t2,t3:tu_t3,t4:tu_t4,doc_sub_stats:tu_doc_sub_stats,dispute_control:tu_dispute_control,eid:tu_eid},
      success: function(data){
        // alert(data);
        // GetAllEmployeesLogs($("#LiveActionGetAllLogs").val());
        $("#" + globaltocontrolbutton).css("display","none");
        $("#" + globaltocontrolbutton + "_check").css("display","inline-block");
      }
    })
}else{
  alert("Please complete all 4 timelogs. Nothing has been changed.");
}

  })
  function OpenModalApprove(tocontrol){
    globaltocontrolbutton = $(tocontrol).data("setid");
    // alert(globaltocontrolbutton);
    $("#dt_ap_for").val($(tocontrol).data("apdte"));

    $("#id_dispute_approve").val($(tocontrol).data("disid"));

    $("#emp_name_header").html($(tocontrol).data("empname"));
    $("#t1").val($(tocontrol).data("tt1"));
    $("#t2").val($(tocontrol).data("tt2"));
    $("#t3").val($(tocontrol).data("tt3"));
    $("#t4").val($(tocontrol).data("tt4"));
    $("#theeid").val($(tocontrol).data("theeid"));

  }
  function OpenModalDissaprove(tocontrol){
    $("#id_dispute_dissaprove").val($(tocontrol).data("disid"));
  }

</script>