<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Dispute</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	   <?php
include("components/dispute_reports_modals.php");
include("components/modals.php");
?>
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>

	<div class="rightbar">
 <script type="text/javascript">
   TriggerDisputeSync();
 </script>


         <nav class="navbar navbar-expand-lg" style="margin-top: 5px; margin-bottom: 15px;">
      
      <a class="navbar-brand" href="#"><i class="far fa-hourglass"></i> DISPUTE REQUEST</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
      </div>
    </nav>
<div class="container-fluid">
	<div class="form-group">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title "><i class="fas fa-user-clock"></i> Missing Logs Disputes</h5>
    <h6 class="card-subtitle mb-4 text-muted">Disputes for missing timelogs.</h6>
      </div>
        <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-bullhorn"></i> Pending Dipsute Reports</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-check-circle"></i> Approved</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-times-circle"></i> Disapproved</a>
  </li>
</ul>
    <div class="container-fluid">
 
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <?php include("components/module_pendingdispute.php"); ?>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <table class="table table-sm table-bordered" id="l2">
        <thead>
          <th>ID</th>
          <th>Name</th>
          <th>Date Applied for</th>
          <th>Justification</th>
          <th>Submission</th>
          <th>Action</th>
        </thead>
        <tbody>
        <?php
          Disp_Dispute(1);
        ?>
        </tbody>
      </table>
  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
      <table class="table table-sm table-bordered" id="l3">
    <thead>
          <th>ID</th>
          <th>Name</th>
          <th>Date Applied for</th>
          <th>Justification</th>
          <th>Submission</th>
          <th>Reason for Disapproval</th>
          <th>Action</th>
        </thead>
        <tbody>
        <?php
          Disp_Dispute(2);
        ?>
        </tbody>
      </table>
  </div>
</div>
    </div>
      <script type="text/javascript">
    TriggerDisputeSynching();
  </script>
  </div>
		
<div class="form-group">

  <div class="card">
  <div class="card-body">
    <h5 class="card-title "><i class="fas fa-hourglass-half"></i> Undertime Disputes</h5>
    <h6 class="card-subtitle mb-4 text-muted">Prevent undertime automatic deduction to Vacation Leave.</h6>

  </div>
     <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#utdisp_pending" role="tab" aria-controls="utdisp_pending" aria-selected="true"><i class="fas fa-bullhorn"></i> Pending Undertime Dispute</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#utdisp_approved" role="tab" aria-controls="utdisp_approved" aria-selected="false"><i class="fas fa-check-circle"></i> Approved Undertime Dispute</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#utdisp_dissaproved" role="tab" aria-controls="utdisp_dissaproved" aria-selected="false"><i class="fas fa-times-circle"></i> Disapproved Undertime Dispute</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="utdisp_pending" role="tabpanel" aria-labelledby="pills-home-tab">
    <div class='container-fluid'>
    <table class="table table-bordered table-sm table-striped" id="dd1_table">
      <thead>
        <tr>
          <th>
            ID
          </th>
          <th>
            Name
          </th>
          <th>
            Date Applied for
          </th>
          <th>
            Justification
          </th>
          <th>
            Submission
          </th>
          <th>
            Action
          </th>
        </tr>
      </thead>
        <tbody>
        <?php
          Disp_Dispute_UT(0);
        ?>
        </tbody>
    </table>
    </div>
  </div>
  <div class="tab-pane fade" id="utdisp_approved" role="tabpanel" aria-labelledby="pills-profile-tab">
    <div class='container-fluid'>
    <table class="table table-bordered table-sm table-striped" id="dd2_table">
      <thead>
        <tr>
          <th>
            ID
          </th>
          <th>
            Name
          </th>
          <th>
            Date Applied for
          </th>
          <th>
            Justification
          </th>
          <th>
            Submission
          </th>
          <th>
            Action
          </th>
        </tr>
      </thead>
        <tbody>
        <?php
          Disp_Dispute_UT(1);
        ?>
        </tbody>
    </table>
    </div>
  </div>
  <div class="tab-pane fade" id="utdisp_dissaproved" role="tabpanel" aria-labelledby="pills-contact-tab">
    <div class='container-fluid'>
    <table class="table table-bordered table-sm table-striped" id="dd3_table">
      <thead>
        <tr>
          <th>
            ID
          </th>
          <th>
            Name
          </th>
          <th>
            Date Applied for
          </th>
          <th>
            Justification
          </th>
          <th>
            Submission
          </th>
          <th>
            Reason for Disapproval
          </th>
          <th>
            Action
          </th>
        </tr>
      </thead>
  <tbody>
        <?php
          Disp_Dispute_UT(2);
        ?>
        </tbody>
    </table>
    </div>
  </div>
</div>
  </div>
</div>
</div>

</div>

</body>
</html>

<script type="text/javascript">
  $(function () {
  $('.eztime').datetimepicker({
  format: 'LT'
  });
  });
setTimeout(function(){
      // TriggerWhereaboutsSync();
      // TriggerLeaveSynching();
      TriggerAttendanceLogs();
  },5000)
  $("#l2").DataTable();
  $("#l3").DataTable();

  $("#dd1_table").DataTable();
  $("#dd2_table").DataTable();
  $("#dd3_table").DataTable();
  highlight_pagelink("#page_managedisputes");
</script>
