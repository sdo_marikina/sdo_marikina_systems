<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Attendance History</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">

    <nav class="navbar navbar-expand-lg" style="margin-top: 5px; margin-bottom: 15px;">
      <a class="navbar-brand" href="#"><i class="fas fa-scroll"></i> ATTENDANCE REPORTS</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
      </div>
    </nav>

		<div class="container">
<div class="row">
  <div class="col-sm-9">
    <div class="form-group">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Today's Summary</h5>
      <h6 class="text-muted mb-2 card-subtitle">This are the quick summary of attendance logs for today.</h6>
      <?php include("components/dash_summary.php");?>
        </div>
      </div>
    </div>
<div class="card">
  <div class="card-body" style="max-height: 600px; overflow: hidden;">
    <div class="loading_indicator" id="lod_1"></div>
        <h5><i class="fas fa-table"></i> Generated Logs Report</h5>
    <table id="tbl_tableofhistory" class="table table-sm table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Stat</th>
        </tr>
      </thead>
      <tbody id="tbl_atthistory_core">
        
      </tbody>
    </table>
  </div>
</div>
  </div>
  <div class="col-sm-3">
   <div class="card">
     <div class="card-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <label><i class="far fa-calendar"></i> Select Date</label>
          <input id="inp_dateofatt" class="form-control" type="date" name="" value="<?php echo date('Y-m-d'); ?>">
        </div>
      </div>
      <div class="col-sm-12" style="display: none;">
        <div class="form-group">
          <label><i class="fas fa-male"></i> Select Employee Type</label>
         <select  id="inp_emptypeofatt" class="form-control">
           <option value="0">All</option>
           <option value="2">Teaching</option>
           <option value="1">Non-Teaching</option>
           <option value="3">Division Personnel</option>
         </select>
        </div>
      </div>
    </div>
    <div class="form-group">
      <button class="btn btn-primary" onclick="GenerateDateAttLogs()"><i class="fas fa-sync-alt"></i> Generate Log</button>
    </div>
     </div>
   </div>
      <div class="card">
     <div class="card-body">
        <h4><i class="fas fa-map-marker-alt"></i> Legend</h4>
        <table class="table table-sm table-bordered">
          <tbody>
            <tr>
              <td><img src='images/whereabouts_nano.png' style='width:30px;'></td>
              <td>

              <span>Whereabouts</span>
          <!--     <p>Employee is currently departured.</p> -->
              </td>
              <td><span id="lbl_wa"></span></td>
            </tr>
            <tr>
              <td><img src='images/authority_to_attend_nano.png' style='width:30px;'></td>
              <td>
                 
              <span>Authority to Attend</span>
              <!-- <p>Employee is currently on a place where he/she needs to attend.</p> -->
              </td>
              <td ><span id="lbl_ata"></span></td>
            </tr>
            <tr>
              <td><img src='images/leave_nano.png' style='width:30px;'></td>
              <td>  
              <span>On Leave</span>
              <!-- <p>Employee is currently on leave.</p></td> -->
              <td ><span id="lbl_ol"></span></td>
            </tr>
            <tr>
              <td><img src='images/late_nano.png' style='width:30px;'></td>
              <td> 
            <span>Late</span>
              <!-- <p>Employee is present but late.</p></td> -->
              <td ><span id="lbl_lt"></span></td>
            </tr>
            <tr>
              <td><img src='images/nologs_nano.png' style='width:30px;'></td>
              <td>  
            <span>Absent/No Logs</span>
              <!-- <p>Employee is identified as absent because he/she has no logs recorded in the system.</p></td> -->
              <td ><span id="lbl_ab"></span></td>
            </tr>
            <tr>
              <td> <img src='images/out_nano.png' style='width:30px;'></td>
              <td>
            <span>Timed-Out</span>
              <!-- <p>Employee is present and has timed-out.</p></td> -->
              <td ><span id="lbl_to"></h5></td>
            </tr>
          </tbody>
        </table>
     </div>
   </div>
   </div>


    <script type="text/javascript">
      
setTimeout(function(){
  GenerateDateAttLogs();
},1000)

  function GenerateDateAttLogs(){
    $("#lod_1").css("display","block");
      LoadAttReportInsights();
    var idt = $("#inp_dateofatt").val();
    var inp_emptypeofatt = $("#inp_emptypeofatt").val();
    // var inp_searchforatt = $("#inp_searchforatt").val();
    $('#tbl_tableofhistory').DataTable().destroy();
    $.ajax({
    type: "POST",
    url: "php/external_server.php",
    data: {get_att_report:"x",inpdate:idt,emp_type:inp_emptypeofatt},
    success: function(data){
    // alert(data);
    $("#tbl_atthistory_core").html(data);
    $("#tbl_tableofhistory").DataTable({
    dom: 'Bfrtip',
    buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    } );

  $("#lod_1").css("display","none");
    },componentWillUnmount: function(){
  if (this.ajaxRequest && this.ajaxRequest.abort){
    this.ajaxRequest.abort()
  }
}
    })
  }
  function LoadAttReportInsights(){
        var idt = $("#inp_dateofatt").val();
    var inp_emptypeofatt = $("#inp_emptypeofatt").val();
    $.ajax({
      type: "POST",
      url: "php/external_server.php",
      data: {attendance_report_insights:"x",inpdate:idt,emp_type:inp_emptypeofatt},
      success: function(data){
        // echo $departured  . "," . $authoritytoattend . "," . $onleave . "," . $late . "," . $absent . "," . $timedout;
        data = data.split(",");
          $("#lbl_wa").html(data[0]);
          $("#lbl_ata").html(data[1]);
          $("#lbl_ol").html(data[2]);
          $("#lbl_lt").html(data[3]);
          $("#lbl_ab").html(data[4]);
          $("#lbl_to").html(data[5]);
      },componentWillUnmount: function(){
  if (this.ajaxRequest && this.ajaxRequest.abort){
    this.ajaxRequest.abort()
  }
}
    })
  }

  setTimeout(function(){
      // TriggerWhereaboutsSync();
      // TriggerLeaveSynching();
      TriggerAttendanceLogs();
  },5000)
    </script>

  </div>
</div>
		</div>
	</div>

</body>
</html>

<script type="text/javascript">
    highlight_pagelink("#page_attendacereports");
</script>
<?php
  include("components/modals.php");
?>

