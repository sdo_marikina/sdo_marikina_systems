
		<h4 class="ultrathin"><span id="sync_whereabouts_indic" style="font-size: 15px; color:#4CAF50;"></span> Whereabouts</h4>
		<div class="btn-group" role="group" aria-label="Basic example">
		<button type="button" data-toggle="modal" title="Submit a Departure" data-target="#modal_Departure" class="btn btn-secondary"><i class="far fa-paper-plane"></i> Departure</button>
		<button type="button" data-toggle="modal" title="Log Arrival" data-target="#modal_Arrival" class="btn btn-secondary"><i class="fas fa-home"></i> Arrival</button>
		<button type="button" data-toggle="modal" title="Recent Where-Abouts Logs" data-target="#modal_Logs" onclick="loadwhereabouts_logs()" class="btn btn-secondary"><i class="fas fa-history"></i></button>
		</div>