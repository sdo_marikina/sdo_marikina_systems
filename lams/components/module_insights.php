		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h5 class="ultratitle"><i class="far fa-chart-bar"></i> STATISTICS</h5>
				</div>
				<div class="col-sm-4">
					<div class="card">
							<div class="loading_indicator" id="lod_insight_emp1">

						</div>
						<div class="card-body" style='min-height:300px;'>
						
							<h5 class="ultratitle"><i class="far fa-user-circle"></i> EMPLOYEES <small style="color: rgba(0,0,0,0.5);">SUMMARY</small></h5>
							<hr>
							<div class="row">
								<div class="col-sm-4">
									<small><i class="fas fa-user-friends"></i> Teaching<br></small>
									<p id="persTeach">0</p>

								</div>
								<div class="col-sm-4">
									<small><i class="fas fa-user-friends"></i> Division<br></small>
									<p id="persDiv">0</p>

								</div>
								<div class="col-sm-4">
									<small><i class="fas fa-user-friends"></i> Non-Teaching<br></small>
									<p id="persNon">0</p>

								</div>
								<div class="col-sm-4">
									<small><i class="fas fa-user-alt"></i> Single Parent<br></small>
									<p id="issinglePers">0</p>

								</div>
								<div class="col-sm-4">
									<small><i class="fas fa-user-clock"></i> Recently Added<br></small>
									<p id="empresadded">0</p>

								</div>
								<div class="col-sm-4">
									<small><i class="fas fa-users"></i> Overall<br></small>
									<p id="ovempcount">0</p>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<div class="loading_indicator" id="lod_insight_emp2">

						</div>
						<div class="card-body" style='min-height:300px;'>
							<h5 class="ultratitle"><i class="fas fa-building"></i> STATION REPORT <small style="color: rgba(0,0,0,0.5);">FOR TODAY</small></h5>
							<hr>
							<div class="row">
								<div class="col-sm-4">
									<small><i class="fas fa-walking"></i> On leave<br></small>
									<a href="LeaveReports.php"><p id="emponleavetoday">0/0</p></a>

								</div>
								<div class="col-sm-4">
									<small><i class="far fa-id-badge"></i> On Departure<br></small>
									<a href="managewhereabouts.php"><p id="empdepcount">0</p></a>

								</div>
								<div class="col-sm-4">
									<small><i class="far fa-id-badge"></i> Temporary ID #<br></small>
									<a href="emp_management.php"><p id="emp_with_id_tmp">0</p></a>

								</div>
								<div class="col-sm-6">
									<small ><i class="fas fa-layer-group"></i> Departments<br></small>
									<a href="emp_departments.php"><p id="iddeptcount">0</p></a>

								</div>
								<div class="col-sm-6">
									<small><i class="fas fa-project-diagram"></i> Positions<br></small>
									<a href="emp_positions.php"><p id="idposcount">0</p></a>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="loading_indicator" id="lod_insight_emp3">

						</div>
					<!-- <div class="card"> -->
						<!-- <div class="card-body"  style='min-height:300px;'> -->
							<h5 class="ultratitle"><i class="far fa-calendar-alt"></i> ATTENDANCE <small style="color: rgba(0,0,0,0.5);">IN LAST 14 DAYS</small></h5>
							<hr>
							<canvas id="myChart" style="margin: 0px !important;"></canvas>
							<script type="text/javascript">
	$("#lod_insight_emp1").css("display","block");
	$("#lod_insight_emp2").css("display","block");
	$("#lod_insight_emp3").css("display","block");
var versions = [];
var countsPerVersion =[];
$.ajax({
	type:"POST",
	url: "php/external_server.php",
	data: {getlastlogs_chart:"x"},
	success: function(data){
		// alert(data);
		data = data.split("|");
		versions = data[0].split(",");
		countsPerVersion = data[1].split(",");
		$("#ovempcount").html(data[2]);
$("#persDiv").html(data[3]);
		$("#persTeach").html(data[4]);
		
		$("#persNon").html(data[5]);

		$("#issinglePers").html(data[6]);

		$("#iddeptcount").html(data[7]);
		$("#idposcount").html(data[8]);
		
		$("#empresadded").html(data[9]);
		$("#emponleavetoday").html(data[10]);
		$("#empdepcount").html(data[11]);
		$("#emp_with_id_tmp").html(data[12]);
		// alert(JSON.stringify(data));

var ctx = document.getElementById('myChart').getContext('2d');
var ctx = document.getElementById("myChart");
versions =  versions.reverse();
countsPerVersion =  countsPerVersion.reverse();
ctx.height = 170;
									var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: versions,
        datasets: [{
            label: 'Employees',
            data: countsPerVersion,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                 'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
		});

			$("#lod_insight_emp1").css("display","none");
			$("#lod_insight_emp2").css("display","none");
			$("#lod_insight_emp3").css("display","none");
	}
})




							</script>
						<!-- </div> -->
					<!-- </div> -->
				</div>
			</div>
		</div>