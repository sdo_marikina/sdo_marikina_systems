<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_addentitlements">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <h5 class="ultratitle mb-3"><i class="fas fa-star"></i> ADD ENTITLEMENTS</h5>
        <div id="res">
        </div>
        <input type="hidden" id="id_add_entitlement_type" name="name_add_entitlement_type">
        <table class="table table-striped table-bordered" style="display: none;">
          <tr>
            <td>Code</td>
            <td><input type="text" id="employeecode" name="empid"></td>
           </tr>
           <tr>
           <td>Type</td>
            <td><input type="text" id="employeetype" name=""></td>
          </tr>
        </table>
        <div class="form-group" id='addmult_emp'>
        </div>
        <div class="form-group" id="ad_addentitlement_container_department_selection">
         <label>Department</label>
         <select class="form-control" name="addEntitlement_name_department">
          <option value="0">All</option>
            <?php
              GetAllDepartments();
            ?>
         </select>
        </div>

        <div class="form-group" id="ad_addentitlement_container_employee_selection">
          <!-- FORM CONTROL NAMES -->
          <!-- ename -->
          <!-- ltype -->
          <!-- fr,to-->
          <!-- entitlement -->
          <label>Employee Name</label>
          <input class="form-control" type="text" autocomplete="off" list="e_names" name="ename" required="" id="emp_name_txt">
          <datalist id="e_names">
             <?php Disp_EmployeeNames(); ?>
          </datalist>
        </div>
        <div class="form-group">
          <label>Leave Type</label>
          <input type="text" id="leavetypebar" autocomplete="off" list="leave_types" class="form-control" required="" name="ltype">
          <datalist id="leave_types">
            <?php Disp_LeaveTypes(); ?>
          </datalist>
        </div>
<script type="text/javascript">
  $("#leavetypebar").change(function(){
      if($(this).val() == "Special Leave"){
          $("#ent_val").attr("max","3");
      }else if($(this).val() == "Solo Parent Leave"){
          $("#ent_val").attr("max","7");
      }else{
          $("#ent_val").attr("max","1000");
      }
  })
</script>
        <div class="form-group" id="leaveperiodmenu">
          <label>Leave Period</label>
          <div class="row">
            <div class="col-sm-6">
              <span class="badge badge-light">From</span>
              <input class="form-control" autocomplete="off" type="date" name="fr" id="ff">
            </div>
            <div class="col-sm-6">
              <span class="badge badge-light">To</span>
              <input class="form-control" autocomplete="off" type="date" name="to" id="tt">
            </div>
          </div>
        </div>

        <div class="form-group" id="ent_val_inp">
          <label>Entitlement</label>
          <input class="form-control" id="ent_val" type="number" min="0" step=".001" autocomplete="off" name="entitlement" required="">
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" onclick="disablemomentum()" id="adder" name="Add_LeaveEntitlement">Add</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Close</span></button>
      </div>
    </div>
  </div>
</div>
</form>

<!-- ADD ENTITLEMENT VALIDATION SCRIPT -->
<script type="text/javascript">
    AnalizeLeaveType();

function disablemomentum(){
$("#adder").css("display","none");
setTimeout(function(){
$("#adder").css("display","block");
},1000)
}
    $("#settingviewer").click(function(){

      $.ajax({
        type: "POST",
        url: "php/server.php",
        data: {getsettings: "usertype"},
        success: function(data){
            if(data == "1"){
            $("#addmult_emp").html('<label><input type="checkbox" disabled  name="name_add_multiple" id="id_add_multiple"> Add to multiple employees</label>');
            }else{
            //Not School
            $("#addmult_emp").html('<label><input type="checkbox" name="name_add_multiple" id="id_add_multiple"> Add to multiple employees</label>');
            }
        }
      })
    })

    $("#emp_name_txt").change(function(){
      var code = $("#emp_name_txt").val();
      $.ajax({
        type: "POST",
        url: "php/validation.php",
        data: {GetNameByCode:"x",empcode: code},
        success: function(data){
          data = JSON.parse("[" + data + "]");
          for(var i =0; i < data.length;i++){
            $("#employeecode").val(data[i]["eid"]);
            $("#emp_name_txt").val(data[i]["lname"] + ", " + data[i]["fname"] + " " + data[i]["mname"]);
             $("#employeetype").val(data[i]["type"]);
             ChangeLeaveTypeSelection(data[i]["eid"],data[i]["type"]);
          }
        }
      })
    })





    function ChangeLeaveTypeSelection(eid, data){
        $("#leave_types").html("");
      if(data == "2"){
        //If teaching then use only one selection
        $.ajax({
          type: "POST",
          url: "php/validation.php",
          data: {GetTeaching_LeaveType: "x",emp_id: eid},
          success: function(data){
              $("#leave_types").append(data);
          }
        })
      }else{
        $.ajax({
          type: "POST",
          url: "php/validation.php",
          data: {GetUsual_LeaveType: "x",emp_id: eid},
          success: function(data){
              $("#leave_types").append(data);
          }
        })
      }
          ValidateLeavePeriod("0");
    }
  $("#leavetypebar").change(function(){
      AnalizeLeaveType();
    var myval = $(this).val();
    if(myval == "Maternity Leave" || myval == "Paternity Leave"){
      $("#ent_val_inp").css("display","none");
      $('#ent_val').prop('required',false);
    }else{
      $("#ent_val_inp").css("display","block");
      $('#ent_val').prop('required',true);
    }
  
  })
  function AnalizeLeaveType(){
    var name_leave = $("#leavetypebar").val();
    $.ajax({
    type: "POST",
    url: "php/validation.php",
    data: {LeaveTypeAnalisis:"x",leavename:name_leave},
    success: function(data){
      ValidateLeavePeriod(data);
    }
    })
  }
  function ValidateLeavePeriod(data){
    if(data == "0"){
      $("#leaveperiodmenu").css("display","none");
      $("#ff").prop('required',false);
      $("#tt").prop('required',false);

      $("#ff").val(null);
      $("#tt").val(null);
    }else{
      $("#leaveperiodmenu").css("display","block");
      $("#ff").prop('required',true);
      $("#tt").prop('required',true);

      $("#ff").val(null);
      $("#tt").val(null);
    }
  }
  // VALIDATION OF ADDING MULTIPLE ENTITLEMENTS
  setInterval(function(){
    if($("#id_add_multiple").prop("checked") == true){
      $("#ad_addentitlement_container_department_selection").css("display","block");
      $("#ad_addentitlement_container_employee_selection").css("display","none");
      $("#emp_name_txt").prop("required",false);
      $("#id_add_entitlement_type").val("1");
    }else{
      $("#ad_addentitlement_container_department_selection").css("display","none");
      $("#ad_addentitlement_container_employee_selection").css("display","block");
      $("#emp_name_txt").prop("required",true);
      $("#id_add_entitlement_type").val("0");
    }
  },10)

</script>
<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_modify">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
<h5 class="ultratitle mb-3">Modify Entitlements<br><span class="ultralight" style="color:gray;" id="mod_empname">Renz Steven D. Madrigal</span></h5>
<div class="row">
  <div class="col-sm-4">
         <input type="hidden" name="name_modify_id" id="id_modify_id">
         <div class="form-group">
             <label>Solo Parent Leave</label>
             <input class="form-control" type="number" step=".001" max="7" name="name_singleparentleave" id="id_singleparentleave">
         </div>
         <div class="form-group">
             <label>Service Credit</label>
             <input class="form-control" type="number" step=".001" name="name_ServiceCredit" id="id_ServiceCredit">
         </div>
         <div class="form-group">
            <label>Sick Leave</label>
            <input class="form-control" type="number" step=".001" name="name_SickLeave" id="id_SickLeave">
         </div>
         <div class="form-group">
             <div class="row">
               <div class="col-sm-12">
                  <label>Vacation Leave</label>
              <input class="form-control" type="number" step=".001" name="name_VacationLeave" id="id_VacationLeave">
               </div>
 
             </div>
         </div>
         <div class="form-group">
                 <label>Special Leave</label>
                 <input class="form-control" type="number" max="3.000" step=".001" name="name_SpecialLeave" id="id_SpecialLeave">
         </div>
  </div>
  <div class="col-sm-8">
    <div class="form-group">
      <label>CTO <small>(Changes are autosaved)</small></label>
          <table class="table table-sm table-bordered">
            <thead>
              <tr>
                <th>From-To</th>
                <th>Value</th>
                <th><i class="fas fa-cog"></i></th>
              </tr>
            </thead>
            <tbody id="allentitlementscontainer">
              
            </tbody>
          </table>
    </div>

  </div>
</div>
   
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="btn_save_entitlement_modification"><i class="far fa-save"></i> Save Modifications</button>
        <button type="button" class="btn" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>


<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_cancel_vl_leave">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h4 class="ultrabold">Cancel Leave?</h4>
          <input type="hidden" id="idofdatetocancel" name="idofdatetocancel">
          <p>Are you really sure you want to cancel this scheduled leave?</p>
          <div class="alert alert-info" role="alert">
            <i class="fas fa-info-circle"></i> Cancelling this Forced Leave will restore +1 in your Forced Leave Entitlement.
          </div>
        </div>
        <div class="modal-footer">
        <button type="submit" name="yuptocancelleave" class="btn btn-primary"><i class="far fa-check-circle"></i> Yes, Cancel this leave</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-arrow-circle-right"></i> No</button>
        </div>
      </div>
    </div>
  </div>
</form>


<script type="text/javascript">

  function btn_cancelLeave(tocontrol){
    $("#idofdatetocancel").val($(tocontrol).data("idofitem"));
  }
  //MANAGES DELETE ENTITLEMENT BUTTONS GLOBALLY
  function del_ent(elemetus,deletus){

  var r = confirm("Do you really want to permanently remove this entitlement?");
  if (r == true) {
var delete_enetilemnt_cto_id = $(elemetus).data("destroyid");
var from = $(elemetus).data("from");
var to = $(elemetus).data("to");
var entitlement = $(elemetus).data("entitlement");
  $.ajax({
    type: "POST",
    url: "php/external_server.php",
    data: {dent:"x",ent_id:delete_enetilemnt_cto_id,ff: from,tt: to,ee: entitlement},
    success: function(data){
      // alert(data);
        $(deletus).remove();
    }
  })
  }
   
  }
  // ADDS VALUE TO THE SEPECIFIED DATA
  $(".btn_modify_entitlements").click(function(){
  var var_id= $(this).data("id");
  var var_ServiceCredit = $(this).data("service_credit");
  var var_SingleParentLeave = $(this).data("single_parent_leave");
  var var_SickLeave = $(this).data("sick_leave");
  var var_VacationLeave = $(this).data("vacation_leave");
  var var_SpecialLeave = $(this).data("special_leave");
  var var_CTO = $(this).data("cto");
  var var_date_from = $(this).data("date_from");
  var var_date_to = $(this).data("date_to");
  var empname = $(this).data("emp_name");

  $("#id_modify_id").val(var_id);
  $("#id_ServiceCredit").val(var_ServiceCredit);
  $("#id_singleparentleave").val(var_SingleParentLeave);
  $("#id_SickLeave").val(var_SickLeave);
  $("#id_VacationLeave").val(var_VacationLeave);
  $("#id_SpecialLeave").val(var_SpecialLeave);
  $("#id_CTO").val(var_CTO);
  $("#id_date_from").val(var_date_from);
  $("#id_date_to").val(var_date_to);
  $("#mod_empname").html(empname);


  if($(this).data("isempsingle") == "0"){
    $("#id_singleparentleave").attr("readonly",true);
  }else{
    $("#id_singleparentleave").attr("readonly",false);
  }

$.ajax({
  type: "POST",
  url: "php/external_server.php",
  data: {get_cto_entitlements: "x",id_of_emp: var_id},
  success: function(data){
    $("#allentitlementscontainer").html(data);
  }
})
  if($(this).data("utype") == "2"){
    //teaching
  $("#id_ServiceCredit").attr("readonly",false);
  $("#id_SickLeave").attr("readonly",true);
  $("#id_VacationLeave").attr("readonly",true);
  $("#id_SpecialLeave").attr("readonly",true);
  $("#id_CTO").attr("readonly",true);
  $("#id_date_from").attr("readonly",true);
  $("#id_date_to").attr("readonly",true);
 // $("#id_ForcedLeave").attr("readonly",true);
  }else{
    //division or non teaching
  $("#id_ServiceCredit").attr("readonly",false);
  $("#id_SickLeave").attr("readonly",false);
  $("#id_VacationLeave").attr("readonly",false);
  $("#id_SpecialLeave").attr("readonly",false);
  $("#id_CTO").attr("readonly",false);
  $("#id_date_from").attr("readonly",false);
  $("#id_date_to").attr("readonly",false);
   // $("#id_ForcedLeave").attr("readonly",false);
  }
  })
  // VALIDATES THE CTO VALUE OF MODIFY MODAL
setInterval(function(){

if(parseFloat($("#id_CTO").val()) < 0.001){
  $("#id_date_from, #id_date_to").val(null);
  $("#id_date_from, #id_date_to").attr("disabled",true);
  $("#id_date_from, #id_date_to").attr("required",false);
}else{
  $("#id_date_from, #id_date_to").attr("disabled",false);
  $("#id_date_from, #id_date_to").attr("required",true);
}
},1)
</script>
<style type="text/css">
  .xcoco .card{
    border: 1px solid rgba(0,0,0,0.2) !important;
  }
</style>

  <div class="modal" tabindex="-1" role="dialog" id="LamsConfirguration">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content blurbg">
      <div class="jumbotron" style="padding: 5px; padding-left: 20px; padding-right: 20px; margin-bottom: 10px;">
          <img src='images/sys_settings.png' class='content_icon'>
        <h5 class="ultratitle">SETTINGS</h5>
       </div>
      <div class="modal-body xcoco">
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
           <li class="nav-item">
             <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#bis" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-arrow-circle-right"></i> System Information</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#ua" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-arrow-circle-right"></i> User Accounts</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#mp" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-arrow-circle-right"></i> My Profile</a>
           </li>
         </ul>
         <div class="tab-content" id="pills-tabContent">
           <div class="tab-pane fade show active" id="bis" role="tabpanel" aria-labelledby="pills-home-tab">
            <h4 onclick="EnableSettings()">System Information</h4>
      <div id="systemconfigs" style="display: none;">
              <?php
               LoadAccountTypeSettings();
            ?>
      </div>
             <div style="overflow: hidden; overflow-y:auto; max-height: 500px;">
                <?php
           
                echo "<div class='row'>";
              LoadSettings();
                echo "</div>";
              ?>
             </div>

             <script type="text/javascript">
              var has_show = true;
               function EnableSettings(){
                if(has_show == true){
                    $("#systemconfigs").css("display","block");
                  has_show = false;
                }else{
                    $("#systemconfigs").css("display","none");
                  has_show = true;
                }
              
               }
             </script>
           </div>
           <div class="tab-pane fade" id="ua" role="tabpanel" aria-labelledby="pills-profile-tab">
             <h4>User Accounts</h4>
             <button  class="btn btn-sm btn-primary" data-toggle="modal" data-target="#newaccmodal"><i class="fas fa-plus-circle"></i> Add new</button>
             <hr>
             <table class="table table-striped table-bordered" id="uxxtable">
               <thead>
                 <tr>
                   <th>Name</th>
                   <th>User Type</th>
                   <th>Action</th>
                 </tr>
               </thead>
               <tbody id="tbl_accountuser">
                 
               </tbody>

             </table>
             <script type="text/javascript">
               setTimeout(function(){
                  $.ajax({
                  type: "POST",
                  url: "php/external_server.php",
                  data: {DisplayAccounts:"x"},
                  success: function(data){
                  $("#tbl_accountuser").html(data);
                  }
                  })
               },1000)
             </script>
           </div>
           <div class="tab-pane fade" id="mp" role="tabpanel" aria-labelledby="pills-contact-tab">
             <h4 style="text-align: center;"><small>My Profile Card</small></h4>
              <div class="container">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">My Information</h5>
                    <h6 class="card-subtitle mb-3 text-muted">Your CDTRS Preferences</h6>
                    <span>Username: <strong><?php echo $_SESSION["username"]; ?></strong></span><br>
                    <span>Type: <strong><?php echo $_SESSION["account_type"]; ?></strong></span><hr>
                    <h5 class="card-title">Actions</h5>
                    <h6 class="card-subtitle mb-3 text-muted">Things you can do on your own account.</h6>
                     <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_change_username"><i class="fas fa-arrow-circle-right"></i> Change Username</button>
             <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_change_password"><i class="fas fa-arrow-circle-right"></i> Change Password</button>
                  </div>
                </div>
              </div>
           </div>
         </div>
      </div>
    </div>
  </div>
</div>

<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_change_username">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5 class="ultratitle mb-5">Change Username</h5>
            <div class="form-group">
            <label>Current Username</label>
            <input type="text" readonly="" class="form-control" value="<?php echo $_SESSION['username']; ?>" placeholder="Enter the new username..." name="oldusername">
          </div>
          <div class="form-group">
            <label>Enter new username</label>
            <input type="text" class="form-control" autocomplete="off"  placeholder="Enter the new username..." name="newusername">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="changename" class="btn btn-primary"><i class="far fa-save"></i> Change Username</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_change_password">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5 class="ultratitle mb-5">Change Password</h5>
          <input type="hidden" value="<?php echo $_SESSION['username']; ?>" name="account_username">
          <div class="form-group">
            <label>Old Password</label>
            <input type="password" class="form-control" autocomplete="off" placeholder="Type password here..." name="oldpass">
          </div>
           <div class="form-group">
            <label>New Password</label>
            <input type="password" class="form-control" autocomplete="off" placeholder="Type password here..." name="newpass">
          </div>
           <div class="form-group">
            <label>Re-type New Password</label>
            <input type="password" class="form-control" autocomplete="off" placeholder="Type password here..." name="newconfirmpass">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="changepass" class="btn btn-primary"><i class="far fa-save"></i> Change Password</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</form>

<form action="php/external_server.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="newaccmodal">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-body">
        <h5 class="ultratitle mb-5">New CDTRS Account</h5>
        <div class="form-group">
          <label>Username</label>
          <input type="text" class="form-control" autocomplete="off" required="" name="username">
        </div>
        <div class="form-group">
          <label>Account Type</label>
         <select class="form-control" name="acc_type">
           <option value="1">Clerk Account</option>
           <option value="0">ICT Coordinator</option>
         </select>
        </div>
        <div class="row">
          <div class="col-sm-6">
              <div class="form-group">
          <label>Password</label>
          <input type="text" class="form-control" autocomplete="off" required="" name="password">
        </div>
          </div>
           <div class="col-sm-6">
              <div class="form-group">
          <label>Re-type Password</label>
          <input type="text" class="form-control" autocomplete="off" required="" name="repass">
        </div>
          </div>
        </div>
       </div>
       <div class="modal-footer">
         <button type="submit" name="AddNewAccount" class="btn btn-primary">Save changes</button>
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
  </div>
</form>

             
<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_addprof">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title ptitle">Add profile picture for an employee</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-image: url('icons/blueprint.jpg') !important; background-size: cover;">

        <div style="padding: 50px;">
          <?php

          include("excuse.php");
        ?>
        </div>
      </div>
    </div>
  </div>
</div>
</form>


<script type="text/javascript">
// $(".modal").draggable({
// handle: ".modal-header"
// });
</script>

