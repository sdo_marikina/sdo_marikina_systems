	<div class="form-group">
			<div class="card">
<div class="card-body">
<span class="float-right" id="statusofintellisync">...</span>
<span><h5 class="card-title">Intellisync</h5></span>
<div class="alert alert-primary" role="alert">
  <i class="fas fa-sync-alt"></i> <span id="syncmoves">Idle....</span>
</div>
<h6 class="card-subtitle text-muted mb-3">Syncs all necessary system data to be accessible in the cloud.</h6>


<div id="ex_mess">
	<div class="alert alert-warning" role="alert">
	 <strong>Sync is on sleep.</strong>
	 <p>We'll wake up if it needs to. <i class="far fa-smile"></i></p>
	</div>
</div>
<div id="moccordion">

  <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" href="#quickaccor">
        Basic Sync Information
      </a>
    </div>
    <div id="quickaccor" class="collapse show" data-parent="#moccordion">
      <div class="card-body">
        <table class="table table-sm table-striped table-bordered">
	<thead>
		<tr>
			<td>Data Name</td>
			<td>Device</td>
			<td>Cloud</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Attendance Logs</td>
			<td id="attlogs">0</td>
			<td id="ol_1">0</td>
		</tr>
		<tr>
			<td>Whereabouts</td>
			<td id="whereabouts">0</td>
			<td id="ol_2">0</td>
		</tr>
		<tr>
			<td>Employees</td>
			<td id="employees">0</td>
			<td id="ol_3">0</td>
		</tr>

		<tr>
			<td>Leave Reports</td>
			<td id="leaverep">0</td>
			<td id="ol_6">0</td>
		</tr>
		<tr>
			<td>Dispute</td>
			<td id="dispute">0</td>
			<td id="ol_7">0</td>
		</tr>


				<tr>
			<td>Authority to Attend</td>
			<td id="authtoatt">0</td>
			<td id="ol_10">0</td>
		</tr>

				<tr>
			<td>Entitlements</td>
			<td id="entle">0</td>
			<td id="ol_13">0</td>
		</tr>
	</tbody>
</table>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#advaccor">
        Extras
      </a>
    </div>
    <div id="advaccor" class="collapse" data-parent="#moccordion">
      <div class="card-body">
               <table class="table table-sm table-striped table-bordered">
	<thead>
		<tr>
			<td>Data Name</td>
			<td>Device</td>
			<td>Cloud</td>
		</tr>
	</thead>
	<tbody>

		<tr>
			<td>Positions</td>
			<td id="posit">0</td>
			<td id="ol_4">0</td>
		</tr>
		<tr>
			<td>Departments</td>
			<td id="dept">0</td>
			<td id="ol_5">0</td>
		</tr>
		<tr>
			<td>Holidays</td>
			<td id="holli">0</td>
			<td id="ol_8">0</td>
		</tr>
		<tr>
			<td>Multiple Schedules</td>
			<td id="multisched">0</td>
			<td id="ol_9">0</td>
		</tr>
				<tr>
			<td>CTO Data</td>
			<td id="ent_cto">0</td>
			<td id="ol_11">0</td>
		</tr>
		<tr>
			<td>Scheduled Leaves</td>
			<td id="schefl">0</td>
			<td id="ol_12">0</td>
		</tr>
		<tr>
			<td>CDTRS Settings</td>
			<td id="sett">0</td>
			<td id="ol_14">0</td>
		</tr>
</tbody>
</table>
      </div>
    </div>
  </div>


</div>

	<script type="text/javascript">


		var Intellisync_Feature = false;
		var isAllowed = true;
		
		var to_continue = true;
		var syncstats = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];


		setTimeout(function(){
			ShowStatistics();
		},60000)


		function ShowStatistics(){
		setTimeout(function(){

		if (to_continue == true && Intellisync_Feature == true) {
			to_continue = false;
			if (isAllowed) {
				$("#syncmoves").html("Started!");
			}else{
				$("#syncmoves").html("Synchronization is turned-off at the moment.");
			}
			$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {statusofintellisync:"x"},
			success: function(data){
				$("#statusofintellisync").html(data);

				if (data == "<i class='fas fa-check'></i> Activated") {
					isAllowed = true;
					 ContinueSync();
				}else{
					isAllowed = false;
				}

			}
		})

			}
		},1000)

		}

	function ContinueSync(){
						$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {get_numbers:"x"},
			success: function(data){
				// alert(data);
				data = data.split("-");
				$("#attlogs").html(data[0]);
				$("#whereabouts").html(data[1]);
				$("#employees").html(data[2]);
				$("#posit").html(data[3]);
				$("#dept").html(data[4]);
				$("#leaverep").html(data[5]);
				$("#dispute").html(data[6]);
				$("#holli").html(data[7]);
				$("#multisched").html(data[8]);
				$("#authtoatt").html(data[9]);
				$("#ent_cto").html(data[10]);
				$("#schefl").html(data[11]);
				$("#entle").html(data[12]);
				$("#sett").html(data[13]);

				Get_OL_Data();
			}
		})
	}
		

	function Get_OL_Data(){
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {get_numbers_online:"x"},
			success: function(data){
				// alert(data);
				
				data = data.split("-");
				$("#ol_1").html(data[0]);
				if($("attlogs").html() != $("#ol_1").html()){ syncstats[0] = 1; }
				$("#ol_2").html(data[1]);
				if($("whereabouts").html() != $("#ol_2").html()){ syncstats[1] = 1; }
				$("#ol_3").html(data[2]);
				if($("employees").html() != $("#ol_3").html()){ syncstats[2] = 1; }
				$("#ol_4").html(data[3]);
				if($("posit").html() != $("#ol_4").html()){ syncstats[3] = 1; }
				$("#ol_5").html(data[4]);
				if($("dept").html() != $("#ol_5").html()){ syncstats[4] = 1; }
				$("#ol_6").html(data[5]);
				if($("leaverep").html() != $("#ol_6").html()){ syncstats[5] = 1; }
				$("#ol_7").html(data[6]);
				if($("dispute").html() != $("#ol_7").html()){ syncstats[6] = 1; }
				$("#ol_8").html(data[7]);
				if($("holli").html() != $("#ol_8").html()){ syncstats[7] = 1; }
				$("#ol_9").html(data[8]);
				if($("multisched").html() != $("#ol_9").html()){ syncstats[8] = 1; }
				$("#ol_10").html(data[9]);
				if($("authtoatt").html() != $("#ol_10").html()){ syncstats[9] = 1; }
				$("#ol_11").html(data[10]);
				if($("ent_cto").html() != $("#ol_11").html()){ syncstats[10] = 1; }
				$("#ol_12").html(data[11]);
				if($("schefl").html() != $("#ol_12").html()){ syncstats[11] = 1; }
				$("#ol_13").html(data[12]);
				if($("entle").html() != $("#ol_13").html()){ syncstats[12] = 1; }
				$("#ol_14").html(data[13]);
				if($("sett").html() != $("#ol_14").html()){ syncstats[13] = 1; }
				SyncAll(syncstats);
			}
		})
	}

	




		function SyncAll(){

			// ATTENDANCE LOGS
			if (syncstats[0] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_attlogs:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[0] = 0;
						},5000)
				// 		// alert("xx");
					}
				})
			}

			// WHEREABOUTS
			if (syncstats[1] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_whereabouts:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[1] = 0;
				// 		},5000)
				// 		// alert("xx");
				// 	}
				// })
			}

				// EMPLOYEES
			if (syncstats[2] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_empl:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[2] = 0;
				// 		},5000)
				// 		// alert("xx");
				// 	}
				// })
			}
				// POSTIONS
			if (syncstats[3] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_positi:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[3] = 0;
						},5000)
						// alert("xx");
					}
				})
			}

				// DEPARTMENTS
			if (syncstats[4] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_departm:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[4] = 0;
						},5000)
						// alert("xx");
					}
				})
			}

			// LEAVE REPORTS
			if (syncstats[5] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_applyl:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[5] = 0;
				// 		},5000)
				// 		// alert("xx");
				// 	}
				// })
			}

			// DIPSUTE
			if (syncstats[6] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_disp:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[6] = 0;
				// 		},5000)
				// 		// alert("xx");
				// 	}
				// })
			}

			// HOLIDAYS
			if (syncstats[7] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_holi:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[7] = 0;
						},5000)
						// alert("xx");
					}
				})
			}

			// MULTPLE SCHEDULES
			if (syncstats[8] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_multips:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[8] = 0;
						},5000)
						// alert("xx");
					}
				})
			}

			// AUTHORITY TO ATTEND
			if (syncstats[9] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_ata:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[9] = 0;
						},5000)
						// alert("xx");
					}
				})
			}

			// ENTITLEMENTS CTO
			if (syncstats[10] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_cto:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[10] = 0;
				// 		},5000)
				// 		// alert("xx");
				// 	}
				// })
			}


			// SCHEDULED FORCED LEAVE
			if (syncstats[11] == 1) {
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_schedfl:"x"},
					success: function(data){
						// alert(data);
						setTimeout(function(){
							syncstats[11] = 0;
						},5000)
						// alert("xx");
					}
				})
			}

			// ENTITLEMENTS
			if (syncstats[12] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_ent:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[12] = 0;
				// 		},5000)
				// 		// alert("xx");
				// 	}
				// })
			}

			// SETTINGS
			if (syncstats[13] == 1) {
				// $.ajax({
				// 	type: "POST",url: "php/external_server.php",
				// 	data:{sync_sett:"x"},
				// 	success: function(data){
				// 		// alert(data);
				// 		setTimeout(function(){
							syncstats[13] = 0;
							Intellisync_Feature = false;
						// },5000)
						// alert("xx");
				// 	}
				// })
			}
		}
setInterval(function(){
		var sendbot = $.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {	send_bot_signal:"x"},
    timeout: 1000
		})
	},600000)
	setInterval(function(){
		if (Intellisync_Feature == true) {
				// alert("YIZ");
			$("#moccordion").css("display","block");
			$("#ex_mess").css("display","none");
				var is_ok = true;
		for(var i =0; i < syncstats.length;i++){
			// alert(syncstats[i]);
				if(syncstats[i] == 1){
					is_ok = false;
					// alert(syncstats[i] + " == 1" );
				}
		}

		if(to_continue == false){
			// CHECK IF CONTINUE IS ELLEGIBLE
		

		// alert(is_ok);
		if (is_ok == true) {
			to_continue = true;
			

			if (isAllowed) {
				$("#syncmoves").html("Finished...");
			}
			
			ShowStatistics();
		}else{
			to_continue = false;
			$("#syncmoves").html("Still working...");
		}


		}
		
		}else{
			// alert("block");
			$("#moccordion").css("display","none");
			$("#ex_mess").css("display","block");
		}		
	},20000)


	function TriggerSynchingOfSignatoryPreferences(){
		$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_signatories:"x"},
					success: function(data){
						// alert("Settings is synched.");
},
    timeout: 3000 
				})
}



	function TriggerSyncSettings(){


			$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{get_settings_from_central:"x"},
					success: function(data){
						// alert(data);
						// alert("Settings is synched.");
},
    timeout: 3000 
				})




		$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_sett:"x"},
					success: function(data){
						// alert("Settings is synched.");
},
    timeout: 3000 
				})
}
		function TriggerATASync(){


	$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_ata:"x"},
					success: function(data){
						// alert("ATA is synched!");
						},
    timeout: 3000 
				})
	}
	function TriggerHolidaySync(){
$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_holi:"x"},
					success: function(data){
						// alert("Holidays is synched!");
},
    timeout: 3000 
				})
	}
	function TriggerWhereaboutsSync(){
		$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_whereabouts:"x"},
					success: function(data){
						// alert("Whereabouts is synched!");
},
    timeout: 3000 
				})
	}
	 function TriggerDisputeSync(){
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_disp:"x"},
					success: function(data){
						// alert("Dispute is synched!");
		},
    timeout: 3000 
				})
	}
 function TriggerEmployeeSync(){
					$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_empl:"x"},
					success: function(data){
						// alert("Employee is synched!");
			},
    timeout: 3000 
		})
	}

	function TriggerDisputeSynching(){
						$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_disp:"x"},
					success: function(data){
						// alert("Dispute syched!");
					},
    timeout: 3000 
				})
	}
	function TriggerAttendanceLogs(){
		// alert("Requested");
			$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_attlogs:"x"},
					success: function(data){
						// alert("synched");
}
				})
	}
	function TriggerLeaveSynching(){
				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_applyl:"x"},
					success: function(data){
						// alert("Leave synched!");
					},
    timeout: 3000 
				})

				$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_schedfl:"x"},
					success: function(data){
						// alert("Scheduled FL synched!");
},
    timeout: 3000 
				})
	}
	function TiggerSyncEntitlements(){
						$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_ent:"x"},
					success: function(data){
						// alert("Entitlements synched!");
					},
    timeout: 3000 
				})


						$.ajax({
					type: "POST",url: "php/external_server.php",
					data:{sync_cto:"x"},
					success: function(data){
						// alert("Entitlements CTO synched!");
						},
    timeout: 3000 
				})
	}
	</script>
	</div>
	</div>
</div>