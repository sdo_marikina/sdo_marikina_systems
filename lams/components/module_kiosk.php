   <div class="modal" tabindex="-1" role="dialog" id="nowebcammodal">
                                                  <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-body">
                                                       <div class="container">
                                                        <span class="ultrabold">Kiosk Ajustments</span>
                                                           <div id="page1" style="display: none;" class="softslide">
                                                      <div class="padder">
                                                        <img src="images/bio_kiosk_nano.png">
                                                           <h1 class="ultrathin">No Webcam. No problem.</h1>
                                                       <p>Enables kiosk to work without camera with less log data integrity.</p>
                                                       <button type="button" onclick="ContinueToSetup()" class="btn btn-primary btn-sm">Continue</button>
                                                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                                                      </div>
                                                           </div>
                                                           <div id="page2" style="display: none;" class="flipper">
                                                               <div class="padder">
                                                                  <form action="php/external_server.php" method="POST">
                                                                     <h1 class="ultrathin">Confirmation</h1>
                                                                   <p>To save this changes, we need you to confirm it with your CDTRS Account.</p>
                                                                   <div class="row">
                                                                       <div class="col-sm-6">
                                                                           <div class="form-group">
                                                                       <label>Username: </label>
                                                                       <input type="text" autocomplete="off" required="" placeholder="Type here..." class="form-control" name="uuu">
                                                                   </div>
                                                                       </div>
                                                                        <div class="col-sm-6">
                                                                           <div class="form-group">
                                                                       <label>Password: </label>
                                                                       <input type="password" autocomplete="off" required="" placeholder="Type here..." class="form-control" name="ppp">
                                                                   </div>
                                                                       </div>
                                                                       <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                               <button class="btn btn-primary btn-sm" type="submit" name="AllowNoCamKiosk">Confirm</button>
                                                                                <button onclick="LoadAuthorityPersmission()" type="button" class="btn btn-light btn-sm">Back</button>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                                  </form>
                                                               </div>
                                                           </div>
                                                       </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>


                                                <div class="modal" tabindex="-1" role="dialog" id="facerecogmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="borderface" style="border: 2px solid #f1c40f;">
      <div class="modal-body" style="text-align: center;">

        <h4 id="empnametopic"></h4>
        <div style="margin-left: -780px; margin-top: -100px;" >
        <video id="video" width="320"  style="border-radius: 20px; border: 2px solid white;" height="240" preload autoplay loop muted ></video>
      <canvas id="canvas" width="320" height="240" ></canvas>
      </div>

<div style="margin-top: 400px;">
  <progress id="proggieface" value="22" max="100"></progress><br>
  <p>Face Verification</p>
  <center><h2 id="kioskmessage">Idle....</h2></center>
</div>
<!-- SECONDARY CAM -->
<div style="display: none;" id="my_camera"></div>
<div id="results">
</div>
  <input type="hidden" id="tome" name="moki">

<!-- SECONDARY CAM -->

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<style type="text/css">
  .fullscreen{
    transition: all 0.5s;
 background-color: #34495e;
    background-repeat: no-repeat;
    background-position: center;
    /*background-size: 100% auto;*/
    background-size: cover;
    position: relative;
    display: block;
     float:right;
  }
  .fullscreen .profilepic{
    display: block;
    position: relative;
    background-color: black;
    top: 0;
    bottom: 0;
    padding: 50px;
    height: 100%;
  }
  .fullscreen .profilepic{
    display: block;
    position: relative;
    background-color: black;
    top: 0;
    bottom: 0;
    padding: 50px;
    height: 100%;
  }
  .fullscreen .profileinfo{
    color: white;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    /*background-color: #34495e;*/
    padding: 10px;
    text-align: left;
  }
.newsbar{
  text-align: left;

  color: white;
}
</style>

<div class="wholebar">
<div class="wholebar_left fullscreen" id="full_alert" style="background-image: url(images/idle_cover.png); background-color: rgba(0,0,0,0.1); border-radius: 15px; 

box-shadow: 0px 0px 10px rgba(0,0,0,0.2);
  border: 1px solid rgba(255,255,255,0.1) !important; ">
  <div class="newsbar container">
    <!-- <h5 class="mt-3">Why do we always suffer?</h5> -->
  </div>
    <div class="profileinfo poptop_anim" >

    <h1 class="poptop_anim ultratitle" id="full_alert_name" style="font-size: 40px; color: rgba(0,0,0,0.5);"></h1>
  <h1 class="poptop_anim" id="full_alert_secondary"  style="font-size: 25px; color: rgba(0,0,0,0.5);"></h1>
</div>
</div>
<div class="wholebar_right" style="color: black; background: transparent;">
<div class="row">
            <div class="col-sm-12" style="text-align: center;">
        <div class="form-group">
            <div class="card card-less" style=" color: black; background-color: transparent; box-shadow: none; border:none;">
                <div class="card-body" style=" padding:10px;">
                <style type="text/css">
                  @media only screen and (max-width: 1750px) {
  #tt_head {
    font-size: 25px !important;
  }
  #tt_body {
    font-size: 60px !important;
  }
  .attlogtxt{
     font-size:25px;
  }
}
                </style>
                    <div class="row">
                      <div class="col-sm-6" style="text-align: right;">
                     <h2 style="font-size: 35px; margin-top: 10px;"  id="tt_head" class="ultrathin">...</h2>
                      </div>
                      <div class="col-sm-6" style="text-align: left;">
                       <h1  class="ultrathin" style="font-size: 90px;" id="tt_body" >...</h1>
                      </div>
                    </div>
                    <script type="text/javascript">
                        Disp_Time();
                      setInterval(function(){
                       Disp_Time();
                     },60000)

                     function Disp_Time(){
                          $.ajax({
                            type: "POST",
                            url: "php/external_server.php",
                            data: {get_time_format:"x"},
                            success: function(data){
                                data = data.split("|");
                                $("#tt_head").html(data[0]);
                                $("#tt_body").html(data[1]);
                            }
                        })
                     }
                    </script>
                      <span >
                        <input type="" style="
                        text-align: center;
                         border: none !important;
                        border-top : 1px solid rgba(255,255,255,0.5) !important;
                         font-size: 32px;
                          background-color: #666769;
                          box-shadow: 0px 5px 20px rgba(0,0,0,0.1);
                          color: white;
                            border-radius: 10px;" placeholder="Search" autocomplete="off" id="searchnametext" class="form-control" name="">
                        <span id="theresemptable" style="display: none; position: relative; background-color: 212225; height: 300px; z-index: 20; border: 1px solid rgba(255,255,255,0.2); box-shadow: 0px 2px 20px rgba(0,0,0,0.3); overflow: hidden; border-radius: 5px; margin-top: 20px;">

                            <button class="float-right close" onclick="$('#theresemptable').css('display','none'); $('#searchnametext').val('');">&times;</button>
                            <div class="loading_indicator" id="lod_indix">
                              
                            </div>
                             <div id="restable">

                                </div>
                            <?php
                            if(CheckIfKioskIsEnabled() == "true"){
                            if (CheckIfNeedCamera() == "0") {

                            ?>

                            <center id="kiosk_loading" >
                                <br>
                                <br>
                                <br>
                            <p class="flipper" style="color: white"><br>Loading Camera...</p>
                            </center>
                                
                            <div id="kiosk_ui" style="padding-top: 100px; padding-bottom: 100px;">
                            <h4 class="flipper" id="kiosk_alert" style="color: white">Please re-connect your webcam.<br><button class="btn btn-light btn-sm" onclick="location.reload();">Refresh</button><button class="btn btn-light btn-sm" data-toggle='modal' data-target="#nowebcammodal" onclick="LoadAuthorityPersmission()">I don't have a Webcam</button></h4>
                            </div>
                            <?php
                            }
                            }
                            ?>
                            <div>
                            </div>
                            <?php if(CheckIfKioskIsEnabled() == "true"){ ?>
                            <script type="text/javascript">
                            // IF KIOSK IS ENABLED - START
                            $("#searchnametext").change(function(){

                            $("#employee_num").val("");
                            $("#searchnametext").prop("placeholder","Please wait...");
                            var myval = $(this).val();
                            $("#searchnametext").attr("disabled",true);
                            if(myval != ""){
                            $.ajax({
                            type: "POST",
                            url: "php/external_server.php",
                            data: {checkifitisempnum: "x",eid: myval},
                            success: function(data){
                            setTimeout(function(){
                              $("#searchnametext").attr("disabled",false);
                              $("#searchnametext").prop("placeholder","Search");
                            },2000)
                            // alert(data);
                            if(data != "false"){
                            $("#empnametopic").html(data);
                            $("#employee_num").val(myval);
                            $("#searchnametext").val("");
                            $("#theresemptable").css("display","none");
                            performlog(); 
                            }}})
                            }
                            })
                            // IF KIOSK IS ENABLED - END
                            </script>
                            <?php } ?>
                            <script type="text/javascript">
                              $("#searchnametext").change(function() {
                              $("#searchnametext").attr("disabled",false);
                              $("#searchnametext").prop("placeholder","Search");

                                    $("#lod_indix").css("display","block");
                                    var searchnametext = $("#searchnametext").val();
                                    if (searchnametext == "") {
                                        $("#theresemptable").css("display", "none");
                                        $("#cbodytime").css("display", "block");
                                    } else {
                                        $("#theresemptable").css("width", "100%");
                                        $("#theresemptable").css("display", "block");
                                        $("#cbodytime").css("display", "none");
                                        $.ajax({
                                            type: "POST",
                                            url: "php/external_server.php",
                                            data: {
                                                searchalog: "x",
                                                tosearch: searchnametext
                                            },
                                            success: function(data) {

                                              // alert(data);
                                              $("#restable").html(data);
                                              $("#lod_indix").css("display","none");
                                            }
                                        });
                                    }
                                })
                            </script>

                        </span>
                    </span>
                </div>
            </div>

        </div>
    </div>
      <div class="col-sm-12">
       <center>
            <div class="form-group">
            <div class="card card-less" style=" color: black; background-color: transparent; box-shadow: none; border:none;">
                <div class="card-body" style="padding:10px;">
                    
                     <div class="row">
                       <div class="col-sm-4">
                         <center>
                           <button type="button" data-toggle="modal" title="Submit a Departure" data-target="#modal_Departure" onclick="setupwhereabouts()" class="btn  btn-block btn-special"><i class="far fa-paper-plane"></i></button>

                         </center>
                       </div>
                       <div class="col-sm-4">
                         <center>
                           
                            <button type="button" data-toggle="modal" title="Log Arrival" data-target="#modal_Arrival" class="btn  btn-block btn-special"><i class="fas fa-home"></i></button>
                         </center>
                       </div>
                       <div class="col-sm-4">
                         <center>
                            <button type="button" data-toggle="modal" title="Recent Where-Abouts Logs" data-target="#modal_Logs" onclick="loadwhereabouts_logs()" class="btn  btn-block btn-special"><i class="fas fa-history"></i></button>

                         </center>
                       </div>
                     </div>

                </div>
            </div>
        </div>
       </center>
    </div>

      

</div>
<div style=" position: relative; height: 45%; float: top; padding:10px; overflow: auto;">
  <div id="lod_attlogsxxxx" style="border-radius: 20px !important;
  background-image: linear-gradient(#454649,  #38393A);
  color: white;
  border: 1px solid rgba(255,255,255,0.0.8) !important;
  box-shadow: 0px 15px 20px rgba(0,0,0,0.0.5);
   border-top : 1px solid rgba(255,255,255,0.1) !important;">
    <center>
      <img class="mt-5 mb-5" src="images/loading.gif" style="width: 70px; border-radius: 50%;">
    </center>
  </div>
  <div id="kiosk_logs_container" style=" position: relative; height: 100%; float: top; padding:10px; overflow: auto; color: white;">
  </div>
</div>

<div  style="display: block; position: absolute;  float: bottom; padding:10px; bottom: 0; left: 0; right: 0;">
        <?php include("components/dash_summary.php"); ?>
</div>
</div>



     <div class="form-group">
       <!-- <div class="container"> -->
        <div >
        <div style="display: none;" class="card poptop_anim" id="loginkeys">
        <div class="card-body">
          
          <!-- <div class="container" > -->
            <!-- <div class="container"> -->
            <div class="form-group ">
              <input type="text" class="form-control form-control-lg" placeholder="Please type your Employee ID..." name="" id="employee_num">
            </div>
            <div class="row">
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">1</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">2</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">3</button>
              <br>
            </div>

            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">4</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">5</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">6</button>
              <br>
            </div>

            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">7</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">8</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">9</button>
              <br>
            </div>

            <div class="col-sm-4">
              <button class="btn btn-danger btn-block" id="clearempid"><i class="fas fa-times"></i></button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">0</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-primary btn-block"  id="btn_logg"><i class="far fa-arrow-alt-circle-right"></i></button>
              <br>
            </div>
          </div>
          <!-- </div> -->
        <!-- </div> -->
        </div>
      </div>
        </div>
     </div>



    <script>

var IsNoCameraWorthy = false;
var IsKioskEnabled  = false; 
var verifyface = false;
var face_time = 0;


CheckKioskSettings();


function CheckKioskSettings(){
  $.ajax({
    type : "POST",
    url: "php/external_server.php",
    data: {CheckIfNeedCamera:"x"},
    success: function(data){
    if(data == "1"){
 IsNoCameraWorthy = true;
  PushNotification("Webcam Support","Configured to work with no camera.");
    }else{
         IsNoCameraWorthy = false;
          PushNotification("Webcam Support","Configured to work with camera.");
    }
     CheckKioskSettings_stage_2();
    }
  })

}
function CheckKioskSettings_stage_2(){
       $.ajax({
    type : "POST",
    url: "php/external_server.php",
    data: {CheckIfKioskEnabled:"x"},
    success: function(data){
      if(data == "1"){
        PushNotification("Kiosk Engine","Kiosk is enabled.");
        IsKioskEnabled = true;
      }else{
         PushNotification("Kiosk Engine","Kiosk is disabled.");
         IsKioskEnabled = false;
      }
      SetupKiosk();
    }
  })
}


function SetupKiosk(){
  if(IsKioskEnabled == true && IsNoCameraWorthy == false){
    $("#searchnametext").prop('disabled', true);
    $("#searchnametext").prop('placeholder', "Loading...");
  }


  if (IsKioskEnabled == true) {

 if (IsNoCameraWorthy == false) {
$("#theresemptable").css("display","block");
// alert("xx");
    
 }
}
}


$("#btn_logg").click(function(){
performlog();
})


function performlog(){
    if($("#employee_num").val() != ""){
  if(IsNoCameraWorthy == false){



  $("#facerecogmodal").modal("show");
  verifyface = true;
// $("#proggieface").max('40');
document.getElementById("proggieface").max = "40";
$("#proggieface").val("0");
$("#kioskmessage").html("Please let me see your face....");
$("#loginkeys").css("display","none");
      var video = document.getElementById('video');
      var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');
      var tracker = new tracking.ObjectTracker('face');
      tracker.setInitialScale(4);
      tracker.setStepSize(2);
      tracker.setEdgesDensity(0.1);
      tracking.track('#video', tracker, { camera: true });
      tracker.on('track', function(event) {
        context.clearRect(0, 0, canvas.width, canvas.height);


        event.data.forEach(function(rect) {

          if(verifyface){
            $("#borderface").css("border","10px solid #3498db");
            $("#kioskmessage").html("I saw it. Keep it still...");
            face_time += 1;
            $("#proggieface").val(face_time);
          console.log(face_time);
          if(face_time > 40){
            take_snapshot();
            $("#borderface").css("border","5px solid #2ecc71");
            $("#kioskmessage").html("<img src='https://i.gifer.com/7efs.gif' style='width:160px;'><br>Log Verified!");
            // $("#loginkeys").css("display","block");
            Submit_Attendance_Kiosk($("#employee_num").val());
            $("#employee_num").val("");
            verifyface = false;
            face_time = 0;

            $("#proggieface").val(face_time);
            setTimeout(function(){
              $("#borderface").css("border","2px solid #f1c40f");
        $("#kioskmessage").html("Idle...");
        $("#facerecogmodal").modal("hide");
          GetKioskLogs();
            },2000)
            $("#searchnametext").focus();
          }
          }


      
          context.strokeStyle = 'white';
          context.strokeRect(rect.x, rect.y, rect.width, rect.height);
          context.font = '11px sanfranc';
          context.fillStyle = "#fff";
          context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
          context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
        });



      });


      var gui = new dat.GUI();
      gui.add(tracker, 'edgesDensity', 0.1, 0.5).step(0.01);
      gui.add(tracker, 'initialScale', 1.0, 10.0).step(0.1);
      gui.add(tracker, 'stepSize', 1, 5).step(0.1);
    }else{
      Submit_Att_NoCam($("#employee_num").val(),"1");
      $("#employee_num").val("");
      GetKioskLogs();
    }
  }else{
      popnotification("Type your Employee ID!","Attendance Kiosk",false);
    }
}
      $(".num_key").click(function(){
        // alert($(this).html());
        $("#employee_num").val($("#employee_num").val() + $(this).html());
      });

      $("#clearempid").click(function(){
        $("#employee_num").val("");
      });



    GetKioskLogs();
    function GetKioskLogs(){
      $("#lod_attlogsxxxx").css("display","block");
      setTimeout(function(){
          $.ajax({
          type: "POST",
          url:"php/external_server.php",
          data: {get_kiosk_logs:"x"},
          success: function(data){
            $("#lod_attlogsxxxx").css("display","none");
          $("#kiosk_logs_container").html(data);
          }
          })
      },1000)
    }


$("#kiosk_loading").css("display","block");
$("#kiosk_ui").css("display","none");


var ftime = '0';
setInterval(function(){
    if (IsKioskEnabled) {
        if (IsNoCameraWorthy == false) {
              checkkiosk();
        }
    }

},60000)

function checkkiosk(){

  if(verifyface == false){
          navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

navigator.getMedia({video: true}, function() {
  //AVALIABLE
  // $("#loginkeys").css("display","block");
  $("#kiosk_alert").css("display","none");
 SetWebcamNow();
$("#kiosk_loading").css("display","none");

$("#kiosk_ui").css("display","block");

if ($("#searchnametext").val() == "") {
  $("#theresemptable").css("display","none");  
}


$("#searchnametext").prop('disabled', false);
$("#searchnametext").prop('placeholder', "Type ID Number to Log");
if(ftime == '0'){
  $("#searchnametext").focus();
  ftime = '1';
}


}, function() {
  // NOT AVAILABLE
  $("#loginkeys").css("display","none");
  $("#kiosk_alert").css("display","block");

$("#kiosk_loading").css("display","none");
$("#theresemptable").css("display","block");
$("#kiosk_ui").css("display","block");
});
  }


}
  function SetWebcamNow(){
    Webcam.set({
    width: 320,
    height: 240,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );

  }


  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      document.getElementById("tome").value = data_uri;
    } );
  }

function Submit_Attendance_Kiosk(emp_id){
  var picturefile = $("#tome").val();
   $.ajax({
    type: "POST",
    url: "php/external_server.php",
    data: {attlogto_kiosk:"x", picture_picdata: picturefile, eid:emp_id},
    success: function(data){
      popnotification(data, "Attendance Kiosk",false);

    }
  })
}

function Submit_Att_NoCam(emp_id,record_att){
   popnotification("Please wait...", "Attendance Kiosk",false);
     $.ajax({
    type: "POST",
    url: "php/external_server.php",
    async:false,
    data: {attlogto_kiosk_no_cam:"x", eid:emp_id,need_record:record_att},
    success: function(data){
      // alert(data);
      if (data !== "Maximum Attendance log exceeded!") {
        
        if(data != null && data != "" && data != "null"){

          data = JSON.parse(data);
          var fullname = data["lname"] + ", " + data["fname"];
          OpenFullScreenProfile("images/logpics/" + data["image"],fullname,"<?php echo date('g:i'); ?> - Login Accepted!" );

        }else{
          popnotification("Invalid Employee ID", "Attendance Kiosk",false);
        }
    }else{
       popnotification("Maximum Attendance log exceeded!", "Attendance Kiosk",false);
    }
      
    }
  })
}
// $("body").css("background-image", "url(images/logpics/5d15cbef2bfc6.jpg)");
function OpenFullScreenProfile(pic_location,fullname, secondarymessgae){
// $("#full_alert").css("display","block");
$("#full_alert_name").html(fullname);
$("#full_alert").css("background-image","url(" + pic_location + ")");
// alert(pic_location);
$("#full_alert_secondary").html(secondarymessgae);
RunBlanker();
 }


 function RunBlanker(){
    setTimeout(function(){
      $("#full_alert_name").html("");
      $("#full_alert").css("background-image","url(images/idle_cover.png)");
      $("#full_alert_secondary").html("");
    },5000)
 }


  </script>



                                                <script type="text/javascript">
                                                    function LoadAuthorityPersmission(){
                                                        $("#page1").css("display","block");
                                                        $("#page2").css("display","none");
                                                    }
                                                    function ContinueToSetup(){
                                                      $("#page1").css("display","none");
                                                        $("#page2").css("display","block");
                                                    }

                                                </script>


