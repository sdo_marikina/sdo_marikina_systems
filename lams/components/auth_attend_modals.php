<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_auth_attend">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <h5 class="ultratitle mb-5">Apply for Authority to Attend</h5>
        <div class="form-group">
          <label>Employee/ Teacher Name/ Employee ID</label>
          <input required="" type="text" autocomplete="off" required="" class="form-control" name="tosend_emp_name" list="empnames" id="auth_empname_attend">
          <datalist id="empnames">
           <?php Disp_EmployeeNames(); ?>
          </datalist>

          <div>
            <input type="hidden" placeholder="employeeid" id="the_emp_code" name="tosend_emp_code">
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <?php
            echo '<input type="hidden" value="' . GetSettings("cn") . '"  class="form-control" name="tosend_station" required="">';
            ?>
          </div>
          <div class="col-sm-12">
        <div class="form-group">
          <label>Destination or Activity Name</label>
         <input type="text" class="form-control" autocomplete="off" name="tosend_destination" required="">
        </div>
          </div>
        </div>
        <div class="form-group">
          <label>Inclusive Dates</label>
          <div class="row">
            <div class="col-sm-4">
              <label><small>From</small></label>
              <input type="Date" class="form-control" name="tosend_date_from" required="">
            </div>
            <div class="col-sm-4">
              <label><small>To</small></label>
              <input type="Date" class="form-control" name="tosend_date_to" required="">
            </div>
            <div class="col-sm-4">
              <label><small>Time</small></label>
            <input type="time" class="form-control" name="tosend_auth_time" required="">
            </div>
          </div>
        </div>
  <div class="form-group">
          <label>Purpose of Travel<br>
           <small style="font-size: 12px; line-height: 50%;">(Please specify the details and attach letter of invitation, memorandum or similar documents)</small></label>
         
        <textarea class="form-control" required="" id="txt_purp" name="tosend_purpose_description">
          
        </textarea>
        <script type="text/javascript">
          $("#txt_purp").html("");
        </script>
        <br>
        <label>Attach Scanned Memo</label><br>
        <input type="file" name="tosend_data_file" style="width: 50%;" required="">
        </div>

          <div class="form-group">
          <!-- <label>Funding Source <span style="color: rgba(0,0,0,0.5);">(Optional)</span></label> -->
        <select class="form-control" style="display: none;" name="tosend_funding_source">
          <option selected="" disabled="" value="">Choose functing souce</option>
          <option>MOOE</option>
          <option>Canteen Funds</option>
        </select>
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="Add_auth_to_attend">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
// $(".modal").draggable({
// handle: ".modal-header"
// });


    $("#auth_empname_attend").change(function(){
      var code = $("#auth_empname_attend").val();
      $.ajax({
        type: "POST",
        url: "php/validation.php",
        data: {GetNameByCode:"x",empcode: code},
        success: function(data){
          data = JSON.parse("[" + data + "]");
          for(var i =0; i < data.length;i++){
            $("#the_emp_code").val(data[i]["eid"]);
            $("#auth_empname_attend").val(data[i]["lname"] + ", " + data[i]["fname"] + " " + data[i]["mname"]);
          }
            
        }
      })
    })

</script>

