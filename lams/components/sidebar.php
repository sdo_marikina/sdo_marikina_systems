<div class="sidebar myautoscroll">
		<div style=" background-color: none; background-image: none !important;">
			<a  class="sidebar_link" id="page_dash" href="dashboard.php"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
			<hr>
		<small class="sidebar_title">Employees</small>
		<a  class="sidebar_link" id="page_manageemployees" href="emp_management.php"><i class="far fa-user-circle"></i> Manage Employees</a>
		<a  class="sidebar_link" id="page_positions" href="emp_positions.php"><i class="fas fa-project-diagram"></i> Positions <?php ShowNoPosBadgeNumber(); ?></a>
		<a  class="sidebar_link" id="page_departments" href="emp_departments.php"><i class="fas fa-layer-group"></i> Departments <?php ShowNoDeptBadgeNumber(); ?></a>
		
		<hr class="separator">
		<small class="sidebar_title">Daily Time Record</small>
		<a  class="sidebar_link" href="attendance_reports_new.php" id="page_attendacereports"><i class="fas fa-scroll"></i> Attendance Reports</a>
		<!-- <a  class="sidebar_link" href="attendance_reports.php" id="page_attendacereports"><i class="fas fa-scroll"></i> Attendance Reports Old</a> -->
		<a  class="sidebar_link" href="attendance_history.php" id="page_attendancehistory"><i class="far fa-clock"></i> Attendance History</a>
		<!-- <a  class="sidebar_link" href="disputecentral/index.php" target="_blank"><i class="fas fa-users-cog"></i> Manual Entry</a> -->
		<!-- <a  class="sidebar_link" id="page_managedisputes" href="DisputeReports.php"><i class="fas fa-user-clock"></i> Manage Disputes -->
			<?php // ShowAllActiveDispute(); ?>
		<!-- </a> -->
		<a  class="sidebar_link admin_comp" id="page_printdtr" href="localchecker.php" ><i class="fas fa-globe"></i> CDTRS Central</a>
		<a  class="sidebar_link"href="#" data-toggle="modal" data-target="#DTRGen_modal" onclick="prepareDTRGeneration()"><i class="fas fa-arrow-circle-right"></i> Generate DTR</a>
		<a  class="sidebar_link" id="page_genformseven" href="formseven.php" ><i class="fas fa-arrow-circle-right"></i> Generate Form 7</a>
		<hr class="separator">

		<small class="sidebar_title">Leave Reports</small>
		<a  class="sidebar_link popdialog"
		data-placement="left"
				data-toggle="popover"
				title="Leave Reports"
				data-content="<div class='thumbimage' style='background-image:url(https://images.pexels.com/photos/2297663/pexels-photo-2297663.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);'></div>Approve, Disaprove, Cancel all Leave Reports filed in your computer."
				data-html="true"
		 href="LeaveReports.php" id="page_leavereports"><i class="fas fa-plane"></i> Leave Reports<?php ShowAllActiveLeaves(); ?></a>
		<a id="page_manageentitlements" class="sidebar_link popdialog"
		data-placement="left"
				data-toggle="popover"
				title="Manage Entitlements"
				data-content="<div class='thumbimage' style='background-image:url(https://images.pexels.com/photos/977246/pexels-photo-977246.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);'></div>Process all employees entitlements added on this system.</div>"
				data-html="true"
		 href="employee_entitlements.php"><i class="fas fa-anchor"></i> Manage Entitlements</a>
		<a id="page_manageentitlements" onclick="LoadParentalLeaveForm()" class="sidebar_link popdialog"
		 data-toggle="modal" data-target="#modal_applyleave" href="#"><i class="fas fa-walking"></i> Apply Parental Leave</a>
		
		<div>
			<hr class="separator">
			<small class="sidebar_title">Miscellaneous</small>
				<a id="page_whereabouts"  
				class="sidebar_link popdialog"
				data-placement="left" href="managewhereabouts.php"
				data-toggle="popover"
				title="Whereabouts"
				data-content="<div class='thumbimage' style='background-image:url(https://images.pexels.com/photos/38640/directory-traffic-note-shield-38640.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);'></div>See where employees go and check if they arrived."
				data-html="true">
				<i class="fas fa-arrow-circle-right"></i> Whereabouts</a>


				<a style="display: none;" id="page_authtravel" 
				class="sidebar_link popdialog" href="auth_travel.php"
				data-placement="left" href="managewhereabouts.php"
				data-toggle="popover"
				title="Authority to Travel"
				data-content="<div class='thumbimage' style='background-image:url(https://images.pexels.com/photos/1483024/pexels-photo-1483024.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);'></div>This feature is still pending. We'll let you know when it's done."
				data-html="true">
				<i class="fas fa-arrow-right"></i> Authority to Travel</a>


				<a id="page_authattend" 
				class="sidebar_link popdialog" href="auth_attend.php"
				data-placement="left" href="managewhereabouts.php"
				data-toggle="popover"
				title="Authority to Attend"
				data-content="<div class='thumbimage' style='background-image:url(https://images.pexels.com/photos/1661004/pexels-photo-1661004.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);'></div>Create and manage Authority to Attend."
				data-html="true">
				<i class="fas fa-arrow-right"></i> Authority to Attend</a>


				<a id="page_holidays" 
				class="sidebar_link popdialog" href="emp_holidays.php"
				data-placement="left" href="managewhereabouts.php"
				data-toggle="popover"
				title="Manage Holidays"
				data-content="<div class='thumbimage' style='background-image:url(https://images.pexels.com/photos/1457691/pexels-photo-1457691.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);'></div>Encode holidays that will reflect in every employee's DTR to prevent undertime."
				data-html="true">
				<i class="fas fa-hospital-symbol"></i> Manage Holidays</a>
		<!-- <a  class="sidebar_link" href="hrmoscreen.php"><i class="fas fa-globe-asia"></i> LAMS HR</a> -->
		</div>
			<!-- <hr class="separator"> -->
			<!-- <h5 class="sidebar_title ultrabold"><i class="fas fa-dharmachakra"></i> System Control</h5>
			<a  class="sidebar_link" href="#" data-toggle="modal" data-target="#LamsConfirguration"><i class="fas fa-wrench"></i> CDTRS Configuration</a>
			<a  class="sidebar_link" href="backup.php"><i class="fas fa-sync"></i> Backup</a>
			<a  class="sidebar_link" href="logs.php"><i class="fas fa-cogs"></i> Action Logs</a>
			<a  class="sidebar_link" href="#" onclick="load_tutorial()" 
			data-toggle="modal" data-target="#howtut"><i class="far fa-question-circle"></i> Help</a> -->
		</div>
	</div>

	<form action="form6.php" method="POST" target="_blank">
		<div class="modal" tabindex="-1" role="dialog" id="modal_applyleave">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		        <h5 class="ultratitle mb-5"><i class="fas fa-walking"></i> Apply Parental Leave</h5>

				<div class="form-group">
					<label>Choose Leave Type</label>
					<select class="form-control" id="selected_leave_type" required="" name="leave_type_now">
						<option>Maternity Leave</option>
						<option>Paternity Leave</option>
					</select>
				</div>

		        <div class="form-group">
		        	<label>Choose Entitled Employee</label>
		        	<select id="auth_emp_for_par_leave" required="" class="form-control" name="employee_id">
		        		
		        	</select>
		        </div>

		       <div class="row">
		       	<div class="col-sm-6">
		       		<label><i class="far fa-calendar-alt"></i> From</label>
		       		<input required="" type="date" class="form-control" id="pat_lv_from" name="date_from">
		       	</div>
		       		<div class="col-sm-6">
		       		<label><i class="far fa-calendar-alt"></i> To</label>
		       		<input required="" type="date" class="form-control" id="pat_lv_to" name="date_to">
		       	</div>
		       	
		       		<div class="col-sm-12">
		       		<label>Duration</label>
		       		<input required="" readonly="" id="pat_leave_dur" type="text" class="form-control" name="leave_days">
		       	</div>
		       </div>

		       
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary"><i class="fas fa-print"></i> Print Form 6</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>

	<script type="text/javascript">
		// popnotification("105th day is auto filled.","105th day  is added to your 'To' field.",false);
    $("#pat_lv_from").change(function(){
    	var starting_date = $(this).val();
    	$.ajax({
    		type : "POST",
    		url : "php/external_server.php",
    		data: {get_default_parentalleavedays:"x",st_day: starting_date},
    		success : function(data){
    		$("#pat_lv_to").val(data);
    		popnotification("105th day is auto filled.","105th day (" + data + ") is added to your 'To' field.",false);
    		ComputeDays_Regardlessly();
    		}
    	})
    })
		$("#selected_leave_type").change(function(){
			LoadParentalLeaveForm();
		})
		function LoadParentalLeaveForm(){
			// Load Authorized Employee 

			var slt = $("#selected_leave_type").val();
			$.ajax({
				type: "POST",
				url: "php/external_server.php",
				data:{load_auth_employee_for_parLeave: "x",selected_leave_type: slt},
				success: function(data){
					$("#auth_emp_for_par_leave").html("<option value='' selected disabled>Choose entitled employee here...</option>");
					$("#auth_emp_for_par_leave").append(data);
				}
			})
		}
	$("#pat_lv_from").change(function(){
		ComputeDays_Regardlessly();
	})
	$("#pat_lv_to").change(function(){
		ComputeDays_Regardlessly();
	})
	function ComputeDays_Regardlessly(){

		var pat_lv_from = $("#pat_lv_from").val();
		var pat_lv_to = $("#pat_lv_to").val();
		if(pat_lv_from != "" && pat_lv_to != ""){
			$.ajax({
				type: "POST",
				url: "php/external_server.php",
				data: {compute_days_reg:"x",dd_f: pat_lv_from,dd_t: pat_lv_to},
				success: function(data){
				$("#pat_leave_dur").val(data);
				}
			})
		}

	}
	</script>
	<!-- GENERATED DAILY TIME RECORD MODAL  -->
<form action="dtr_core/visualizer.php" method="POST" target="_blank">
		<div class="modal" tabindex="-1" role="dialog" id="DTRGen_modal">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content modal-md" style="background-color: white;">
	      <div class="modal-body">
	      	
	      			<div class="container">
	      				<div class="form-group">
	      				<h5 class="ultrabold ultratitle"><i class="fas fa-arrow-circle-right"></i> DTR GENERATION</h5>
	      			</div>

	        		<div class="padder softslide" id="gen_dtr_p1">
	        			<div class="form-group">
	        				      <img src='images/dtr_nano.png' class='content_icon'>
	        				<h1 class="ultrathin">Set Date</h1>
	        				<h6 class="card-subtitle text-muted mb-2">Choose a starting and ending dates.</h6>
	        			</div>
	        		<div class="row">
	        			<div class="col-sm-6">
	        				<div class="form-group">
	        			<label>Starting</label>
	        			<input class="form-control" required required="" type="date" name="dt_from" id="dt_inp_ss_1">
	        		</div>
	        			</div>
	        			<div class="col-sm-6">
	        				<div class="form-group">
	        			<label>Ending</label>
	        			<input class="form-control" required required="" type="date" name="dt_to" id="dt_inp_ss_2">
	        		</div>
	        			</div>
	        			<div class="col-sm-12">
	        				<div class="form-group mt-5">
	        					<button type="button" class="btn btn-light btn-sm" onclick="dtrGen_Next()">Next <i class="fas fa-arrow-right"></i></button>
	        				</div>
	        			</div>
		        		</div>
	        		</div>

	        		

	        		<div class="padder flipper" id="gen_dtr_p2" style="display: none;">
	        			<div class="form-group">
	        				<h1 class="ultrathin">Customize DTR</h1>
	        				<h6 class="card-subtitle text-muted mb-2">Type of employee to be generated and some other preferences.</h6>
	        			</div>
	        		<div class="row">
	        			<div class="col-sm-8">
	        				<div class="form-group">
	        				
	        					<label><i class="fas fa-text-height"></i> Font Size</label>
	        			<select class="form-control" id="fontsizedtr" name="fontdtrsize">
	        				<option value="1.50em">Big</option>
	        				<option value="1.25em" selected="">Standard</option>
	        				<option value="1em">Medium</option>
	        				<option value="0.8em">Small</option>
	        				<option value="0.7em">Nano</option>
	        			</select>
	        			<script type="text/javascript">
	        				
	        				$("#fontsizedtr").change(function(){
	        					ChangeFontSize();
	        				})

	        				function ChangeFontSize(){
	        					$("#ffsize").css("font-size",$("#fontsizedtr").val());
	        					$("#text_trigger").attr("href","dtr_core/test_print.php?font_size=" + $("#fontsizedtr").val());


	        					var fff = OneTimeFormat($("#dt_inp_ss_1").val(),"ff_from");
	        					var ttt = OneTimeFormat($("#dt_inp_ss_2").val(),"ff_to");

								$("#ff_from").html(fff);
								$("#ff_to").html(ttt);
	        					
								$("#text_trigger").attr("href","dtr_core/test_print.php?font_size=" + $("#fontsizedtr").val() + "&ff=" + $("#dt_inp_ss_1").val() + "&tt=" + $("#dt_inp_ss_2").val());
	        				}

	        				function OneTimeFormat(raw_date,content_id){
	        					var toreturn = "";
	        					$.ajax({
	        						type:"POST",
	        						url:"php/external_server.php",
	        						data: {format_date:"x",date_to_fromat:raw_date},
	        						success:function(data){
	        							toreturn= data;
	        							$("#" + content_id).html(toreturn);
	        						}
	        					})
	        					
	        				}
	        			</script>
	        				</div>

	        				<div class="form-group">
	        			
	        			<label><i class="fas fa-user-friends"></i> Employee Type</label>
	        			<select class="form-control" required id="id_inp_genDTR_empl_type" name="emp_type" onchange="GenDTRPrepareEmployeeType()">

	        				<?php
		       				if(GetSettings("usertype") != "1"){
		       				?>
		       				<option value="0">All Employees</option>
		       				<?php }else{
		       					?>
		       				<option value="0">All Employees</option>

		       					<?php
		       				} ?>
		       				<option value="1">Non-Teaching</option>
		       				<?php
		       				if(GetSettings("usertype") != "0"){
		       				?>
		       				<option value="2">Teaching</option>
		       				<?php } ?>


		       				<?php
		       				if(GetSettings("usertype") != "1"){
		       				?>
		       				<option value="3">Division Personnel</option>
		       				<?php } ?>
		       				<option value="4">Select Employee / Advance Filter</option>
	        			</select>
	        		</div>

<div id="empl_selectionPanel">
	<div class="form-group">
	        			<label>Select Employee(s) to Include</label>
	        			<div class="card" style="overflow: hidden; min-height: 345px;">
	        				<div class="card-body">
	        					<div class="loading_indicator" id="lod_of_employee_gen"></div>
	        					<span class="float-right text-muted"><span id="sel_ch">0</span> Selected</span>
	        					<label><i class="fas fa-search"></i> Search</label>
	        					<input type="text" id="mysearchempinputfordtrgen" class="form-control" autocomplete="off" placeholder="Type here..." name="">
	        					<div class="mt-2 mb-2">
	        						<div class="row">
	        							<div class="col-sm-6">
	        								<button type="button" onclick="bydepsearch()" class="btn-sm btn-link btn"><i class="fas fa-search"></i> by Department</button>
	        							</div>
	        						</div>
	        					</div>
	        					<table class="table table-sm " style="margin-bottom: 0px;">
	        						<thead>
	        							<tr>
	        								<th>Employee</th>
	        								<th>Selection</th>
	        							</tr>
	        						</thead>
	        					</table>

<script>
	function bydepsearch(){
		$("#mysearchempinputfordtrgen").val("Dep: ");
		$("#mysearchempinputfordtrgen").focus();
	}
	setInterval(function(){

	if ($("#id_inp_genDTR_empl_type").val() == "4") {
		$("#sel_ch").html($('.sel_emp_chk_dt:checkbox:checked').length);
	}
	})
$(document).ready(function(){
  $("#mysearchempinputfordtrgen").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".mydtrselemptable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	        					<div style="overflow: auto; max-height: 200px; ">
	        						<table class="table table-sm ">
	        						<tbody id="lod_all_emp_dtr" class="mydtrselemptable">
	        						</tbody>
	        					</table>
	        					</div>
	        				</div>
	        			</div>
	        		</div>
</div>

	        			</div>
	        			<div class="col-sm-4">
	        				<div class="card">
	        					<div class="card-body">
	        						<label class="text-muted"><i class="fas fa-text-height"></i> Font Size</label>
	        						<p id="ffsize">Aa Bb Cc</p>
	        						<label class="text-muted"><i class="far fa-calendar"></i> From</label>
	        						<p id="ff_from">Aa Bb Cc</p>
	        						<label class="text-muted"><i class="fas fa-calendar"></i> To</label>
	        						<p id="ff_to">Aa Bb Cc</p>
	        						<a id="text_trigger" target="_blank" href="#"><i class="fas fa-file-alt"></i> Preview DTR</a>
	        					</div>
	        				</div>
	        			</div>
	    
	        	<div class="col-sm-12">
	        		<div class="form-group mt-5">
	        			
	        			<button type="button" class="btn btn-light btn-sm" onclick="dtrGen_Back()"><i class="fas fa-arrow-left"></i> Back</button>
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fas fa-window-close"></i> Cancel</button>
						<button type="submit" class="btn btn-primary btn-sm float-right"><i class="fas fa-database"></i> Generate DTR</button>
	        			
	        		</div>
	        	</div>
	        </div>
	        		</div>
	      			</div>
	      </div>
	    </div>
	  </div>
	</div>
</form>


<script type="text/javascript">
	function GenDTRPrepareEmployeeType(){
		if ($("#id_inp_genDTR_empl_type").val() == "4") {
			
			popnotification("Employee selection is enabled.","Select employee(s) to include in your DTR generation.",false);
			$("#empl_selectionPanel").css("display","block");
			
		}else{
			// $("#lod_of_employee_gen").css("display","none");
			$("#empl_selectionPanel").css("display","none");
		}
	}
	function prepareDTRGeneration(){
		$("#lod_of_employee_gen").css("display","block");
		$("#empl_selectionPanel").css("display","none");
		$("#gen_dtr_p1").css("display","block");
		$("#gen_dtr_p2").css("display","none");
		$("#dt_inp_ss_1").val(null);
		$("#dt_inp_ss_2").val(null);
		$.ajax({
				type: "POST",
				url:"php/external_server.php",
				data: {lod_all_emp_dtr:"x"},
				success: function(data){
					$("#lod_all_emp_dtr").html(data);
					$("#lod_of_employee_gen").css("display","none");
				}
			})
	}
	function dtrGen_Next(){
ChangeFontSize();
		if ($("#dt_inp_ss_1").val() != "" && $("#dt_inp_ss_2").val() != "") {
			$("#gen_dtr_p1").css("display","none");
			$("#gen_dtr_p2").css("display","block");
			GenDTRPrepareEmployeeType();
		}else{
			popnotification("Check your dates","Complete your inputs before the next step.",false);
		}
	}
	function dtrGen_Back(){
			$("#gen_dtr_p1").css("display","block");
			$("#gen_dtr_p2").css("display","none");
	}
</script>

		<div class="modal" tabindex="-1" role="dialog" id="Modal_New_employee">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		      	<?php
		      	 include("components/module_addemployee.php"); 
		      	 ?>
		      </div>

		    </div>
		  </div>
		</div>


<script type="text/javascript">
	// CHECK LAMS IF SDO
	$.ajax({
		type: "POST",
		url: "php/external_server.php",
		data: {checkifnotschool: "x"},
		success: function(data){
			if(data == "0"){
				$(".admin_comp").css("display","block");
			}else if(data == "1"){
				$(".admin_comp").css("display","none");
			}
		}
	})
	function highlight_pagelink(linkid){
		$(linkid).css("color","black");
		$(linkid).css("background-color","rgba(0,0,0,0.1)");
	}
</script>

