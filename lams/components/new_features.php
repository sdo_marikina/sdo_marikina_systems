<div class="modal" tabindex="-1" role="dialog" id="whatsnew">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
      	<h2 class="ultratitle">WHAT'S NEW?</h2>
      <pre style="font-family: sanfranc; max-height: 500px;">
            What's new in 3.4
                  -All New UI Design
                  -Improved DTR Generation, Generate by Department, DTR Print Preview,DTR Printing History
                  -Optimized Form 6
                  -Optimized attendance synching
                  -Centralized signatory syching
                  -Dispute power is deprecated
                  -Single Parent Leave is now Solo Parent Leave
                  -QR Code feature for every form 6 generation
            What's new in 3.3
                  -fix real-time synching of attendance logs in ONLINE PORTAL.
             What's new in 3.2.1.15.3
                  -Fixed principal Form 6 Signatory
             What's new in 3.2.1.15.2
                  -Multiple Attendance Logging prevention
                  -Preliminary design changes
             What's new in 3.2.1.15.1
                  -Changed Design Scheme
                  -Teachers logs is limited to two in DTR Generation to avoid confusions
            What's new in 3.2.1
                  -Refined offline availability
                  -fixed "red" quicksight searchbar occuring in other browser
                  -Offline santa gif file
                  -Spelling and grammar corrected for better comprehension
                  -Report to SDO Feature
            What's new in 3.2
                  -New font Combination (Bebas, SF Camera)
                  -Christmas Update
                  -Reimagined Image Preview in homescreen
                  -New Dashboard looks
                  -Improved viewing of Leave Reports
                  -Minor unmetioned Bug Fixes
                  -Services Panel
            What's new in 3.1
                  -Re-imagined color palette in homescreen.
                  -Offline DTR printing
                  -Attendance History: Manual Operations (Add Record)
            Refined in 3.0.8
                  *Intellisync Optimization in synching logs of attendance.
                  *Performance Improvement in Home Screen
                  *Problem detection in Manage Employees
            Fixed in 3.0.7
                  *Ñ and ñ simplified
                  *Improved Performance
                  *Refined Processing.
            Fixed and Improved in 3.0.6
                  *Leave reprinting
                  *Leave cancellation for all
                  *CTO fixed
            Refined in 3.0.5
                  *OIC Position text under the Principal's DTR.
            Fixed in 3.0.4
                  *School DTR principal name's DTR - Sign by the School Division OIC (fixed).
                  *Fixed Attendance Report miscalculations.
            Refined in 3.0.3
                  *Loading indicator in Apply Leave and Date Selection.
            Fixed new in 3.0.2
                  *Form 6 errors.
                  *Improved some centralized monitoring components.
            Fixed new in 3.0.1
                  *DTR Generation reflection of Leaves, ATA, etc...
                  *Inconsistent ATA recording
                  *Updating of Leave Entitlement Bug
            What's new in 3
                  -Overall Pages
                        20% Faster Loading Time
                  -Manage Employees
                        Profile Picture Support
                        Temporary Employee Number Detection
                        Bug Fixes
                        Layout Refinements
                        Employee Number Duplication Advanced Detection
                        Supports CDTRS Online Portal Information
                  -Positions
                        Realtime Operation Support
                  -New Attendance Reports
                        View detailed information about the employees attendance
                  -New Attendance History
                        Quick attendance history viewing
                  -DTR
                        Dynamic Layout
                        In-charge name is now built-in to the DTR
                        Auto principal detection
                        Minor performance improvement
                  -Leave Reports
                        Total cancellation of Leave Report
                        Form 6 Layout Refinements
                  -Holidays
                        Refined User Interface
                  -Backup
                        All-new, super-fast, smart backup system for the entire system data that
                        enables CDTRS Online Portal and CDTRS Go
                  -New Kiosk biometrics alternative
                        System > Settings > CDTRS Attendance Kiosk (Enable/Disable Kiosk)
                        System > Settings > Allow no camera in Kiosk (Enable/Disable Kiosk Camera Validation)
                  -New Multiple User Account Support
                        Add multiple user to CDTRS
                  -Usable Authority To Attend
                        Employees can now file an Autohrity To Attend in the System.
                        Data-Management
                  -Settings
                        New My Profile Card
                  -New Admin Dashborad
                        View System Preferences
                        View Employee Summary & Breakdown
                        View Station Report
                        View Attendance Graph
                        View Todays Summary
                  -Home Screen
                        Re-imagined home screen to dynamically turn into alternate kiosk.
                  -Navigation
                        New Shortcut to Home Screen
                  -Quick Sight
                        Supported Tutorial Searching
                        Supported Feature Snippet Searching
                        Supported Centralized Information Searching
                        20% Faster Searching
                  -Employee Dashboard
                        Re-imagined Employee Dashboard for good
                        Logs to display option
                        30% faster loading
                        Non-strict apply leave
                  -Info Center
                        View all systems nessesary system information by pressing the (`) key.
                  -Sidebar
                        Refined Layout
                  -Comprehension
                        Fixed some misspelled words.
                  -New CDTRS Online Portal
                        CDTRS 3 is connected to www.Portal.DepedMarikina.ph
                        View Logs in Online Portal
                        Full Feature Employee Dashboard view
                        Apply Leave Reports
                        Quick View selected Leave Reports
                        View Leave Report status
                  -Form 6
                        Fixed not showing information in Form 6
      ___________________________________________________________________________
            Changes in 2.8.15
                  fixed leave reports (form 6 generation) FROM/TO DATE switching.
                  fixed leave reports duplication error.
                  fixed multiple schedule incorrect computation.
                  fixed not showing leave reports.
                  fixed not showing dispute reports.
                  fixed principal name (Renz *******).
                  fixed holiday dummy data (Sh** Day).
                  fixed DTR Generation Employee Type Selection.

                  foundation of Attendance Kiosk Tech.
                  webcam tech foundation.
                  Service Credit Leave has In-Hospital and Out-Patient infomation now.
                  Portable UI

                  *New
                  Apply Parental Leave Feature (Maternity, Paternity Leave).
                  Multiple Schedule Delete Button.
                  Minor Design Changes.
                  Performance Improvement.
      ___________________________________________________________________________
            What's new in 2.8
            -Local Daily Time Record Printing.
                  Now the CDTRS Admin has the power to locally print the Daily Time Record.
                  Multiple Schedule for DTR feature.
                  Fixed some Undertime errors in (Non-teaching, Teaching personnel).
            -New Design Refinements
                  Buttons, Panels, Modals has a new matured look complements in flat ui design.
                  Automatic Wallpaper.
            -Leave Reports and Form 6
                  New Forced Leave Feature.
                  Forced Leave Cancellation Function.
            -Holidays
                  Add, Edit, Delete function.
                  Integrated in DTR, Employee Dashboard, Scheduled Vacation Leave and Form 6 validation.
            -Employee Management
                  Add, Edit, Delete function.
                  Multiple Schedule Management.
            -Department Management
                  Add, Edit, Delete function with (employees with NO Department) detection.
            -Position Management
                  Add, Edit, Delete function with (employees with NO Position) detection.
            -Form 7 Printing
                  Adjusted some of form 7 formulas and computation.
                  New Layout.
            -Refined Whereabouts
                  Advanced printing of whereabouts (From - To selection, Include Coordinates).
            -Refined ATAF Layout
            -Password Change Feature
            -Multiple Admin Account Support

            Changes
            1. Admin Navigation Sidebar selected page link is now highlighted.
            2. Leave Reports Balance Column is removed.
            3. Leave Entitlements Separation of Non-Teaching, Division Personnel to Teaching Personnel tab page view.
            4. Sidebar link re-arranged to a more comprehensive view.
            5. Form 7 "Days" column is replaced by "Amount Per Day".
            6. Whereabouts is now auto disputing when arrived.
            7. Authority to Attend "Add Attachment" is replaced by "Attach Scanned Memo".
            *and contains some unmentioned features.
      ___________________________________________________________________________
            What's new in 2.7
            -UAT Overall Bugs, Feature Request has been fixed and implemented across the system.
            -Powerful Optimization
                  CDTRS 2.7 is 70% faster than all the past version especially in 2.6.
                  100% realtime access with 70% less loading time (Employee Dashboard, Admin Page).
            -Powerful Realtime Backup Intelligent Syncronization
                  CDTRS syncs logs to the central server smarter than ever before without affecting the performance.
            -Re-imagined home page design.
                  More comprehensive and cleaner design.
            -Manual Entry fixes
                  2 Logs is now acceptable to "Manual Entry" for Teaching Employees.
            -Leave Management
                  Employee Dashboard "Leave Without Pay" is now added so employees can apply leave without credits.
            -Todays Logs
                  Pop-up alert of logged employee on the Navigation Bar.
                  Search of today's logs in the home screen.
            -Where Abouts
                  Where abouts (Management and Form) is now on CDTRS 2.7
            -Refined Look
                  More comprehensive small ui details is now emphasized.
                  More detailed iconed buttons.
                  Expressive animations is added when opening a modal and other dialogs.
      ___________________________________________________________________________
      	What's new in 2.6
      	---------------------
      	-System Name
      		LAMS is now renamed to CDTRS(Centralized Daily Time Records System) that
      		represents both DTR and LAMS in one system.
      	-Multiple CTO
      		The admin can now add multiple CTO entitlements to an employee.
      	-Re-Imagined Employee Entitlements table that is design for easy data comprehension.
                  Active CTO yellow border highlight.
                  Gray colored CTO bar for later use highlight.
            -Fixed Form 6 overall bugs
                  None ajusting values.
                  Accurate display of balance.
            -Todays Logs
                  Recent Logs (shows last 5 logs) toggle.
                  Added card animations.
            -Realtime Backup
                  Logs, Employees are now backed-up in realtime.
            -LAMS Sidemenu
                  Contains all the primary LAMS functions.
            -CDTRS Search in LAMS
                  Easily search all the employee status in LAMS admin navigation bar.
      	___________________________________________________________________________
      	What's new in 2.5
      	---------------------
      	-Refinements
      		Fixed FORM 6 bugs (important).
      		Fixed Apply Leave Validations and multi form submission.
      		Backup submission time scheduling early sending(fixed).
      		Employee number data management has been fixed.
      		Database data refinements.
      	-Improvements
      		Quick Access have number of reports in dispute and leave reports notifications.
      		Day name in date(s) in FORM 6 for easy date comprehension.
      		Date and time display in tables are now more comprehensive.
      		Apply leave (date applied for leave is now auto selected in "FROM" input).
      	-Quick Access
      		Profile settings are now replaced by Leave Reports shortcut.
      	-Version Footer
      		Refined design for more emphasis.
      		Update LAMS link including video instruction on how to update LAMS.
      	-Connectivity
      		Supports Backup Command in Central Server.
      	-Security
      		Added logs to the database to monitor LAMS usage.
      	-Design
      		Simplier Today's Logs Design
      		Button design refinements
      		Refined and detailed navigation bar.
      	-New Feedback System
      		Feedback sending is now available at the footer easy access menu.
      	-Pop-up Notification System
      		Now it's minimalistic and informative at the same time for the system to say
      		little information in what's going on with the new side notification pop-up's.
      	___________________________________________________________________________
      	What's new in 2.4
      	---------------------
      	-Automated Backup
      		LAMS can now backup your logs everyday automatically.
      		Backup options are now included in the admin's account.
      		Download LAMS Attendace Logs, Dispute, Authorities, Applied Leaves.
      	-UI
      		Refined dropdown menu select animation.
      	-Bug Fixes
      		Dispute time not showing
      		Dispute submission without time log changes
      	___________________________________________________________________________
      	What's new in 2.3
      	---------------------
      	-Employee Dashboard
      		Refined Date Arrangement (Month - Day - Year).
      		Larger Employee Dashboard User Interface.
      	-Consistency
      		Consistent Employee Name uppercase.
      	-Design
      		Refined titles and input boxes to help emphasize contents more.
      	___________________________________________________________________________
      	What's new in 2.2
      	---------------------
      	-Employee Dashboard
      		More recognizable date view in Employee Dashboard table.
      	-Easier Dispute Manual Time Entry
      		Redesigned Manual Time Entry for faster time input.
      	-All new Design & Fontstyle
      		The all new Design is built for faster and more fammiliar user experience.
      	-Timelog Overall Manual Entry
      		Now the LAMS admin can now fix all employees timelog errors.
      	-Bug Fixes
      		Version Accuracy Improved (No false error messages)
      	-What's New Feature
      		Now you can see what's new in your current LAMS version.
      	-Employee Dashboard Optimized Timelog Viewing
      		Employee Dashborad only show current and last month's log.





            ------- Development of the system started in October 10, 2018 -------

            Centralized Daily Time Record System is a project of Human Resouce Unit and
            developed by I.T Department.

            Development Head: Ryan Lee Regencia
            Programmer: Virmil Talattad

            Property of Schools Division Office Marikina City
      </pre>
      </div>
    </div>
  </div>
</div>