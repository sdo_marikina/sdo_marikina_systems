		<div class="container-fluid">
			<div class="form-group">
				<div class="row">
				<div class="col-sm-12">
		
						<div class="loading_indicator" id="stationInf" style="display: block;">
							
						</div>
						<div class="container-fluid">
							
							<h5 class="ultratitle mt-3"><i class="fas fa-home"></i> <span id="station_name_disp"></span></h5>
							<div class="row">
<div class="col-sm-6">
<p><small style="color:rgba(0,0,0,0.5);">Principal:</small><br>
<span id="disp_prin_name"></span></p>
</div>
<div class="col-sm-6">
<p><small style="color:rgba(0,0,0,0.5);">Admin Officer V:</small><br>
<span id="disp_adminoff_name"></span></p>
</div>
<div class="col-sm-6">
<p><small style="color:rgba(0,0,0,0.5);">HR Head:</small><br>
<span id="disp_hrmo_name"></span></p>
</div>
<div class="col-sm-6">
<p><small style="color:rgba(0,0,0,0.5);">SDO-Marikina OIC:</small><br>
<span id="disp_oic_name"></span></p>
</div>
<div class="col-sm-6">
<p><small style="color:rgba(0,0,0,0.5);">CID Chief:</small><br>
<span id="disp_oic_ass"></span></p>
</div>
							</div>
						</div>
				
				</div>
			</div>
			</div>
		</div>
		<script type="text/javascript">
			$("#stationInf").css("display","block");
			$.ajax({
				type: "POST",
				url: "php/external_server.php",
				data: {get_preferences_info: "x"},
				success: function(data){
						setTimeout(function(){
							$("#stationInf").css("display","none");
						},1000)
					data = data.split("<->");
						// alert(data);
					$("#station_name_disp").html(data[0]);
					$("#disp_prin_name").html(data[1]);
					$("#disp_adminoff_name").html(data[3]);
					$("#disp_hrmo_name").html(data[4]);
					$("#disp_oic_name").html(data[2]);
					$("#disp_oic_ass").html(data[5]);
				}
			})
		</script>