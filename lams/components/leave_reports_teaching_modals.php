<form action="form6.php"  method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_lr_approve">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="loading_indicator" id="lod_applyleave">
        
      </div>
      <div class="modal-body">
        
        <input type="hidden" id="to_app_id" name="id">
        <h2 class="ultrabold">Approve?</h2>
        <p>Are you sure you want to approve this Leave Report?</p>

                <!-- <label>Freezed Infromation <small>Essetials</small></label> -->
        <input type="hidden" readonly="" class="form-control" id="apprx_eid" name="eid">
        <input type="hidden" readonly="" class="form-control" id="apprx_leavetype" name="leavetype">
        <input type="hidden" readonly="" class="form-control" id="apprx_date_from" name="date_from">
        <input type="hidden" readonly="" class="form-control" id="apprx_date_to" name="date_to">
        <input type="hidden" readonly="" class="form-control" id="apprx_leave_taken" name="leave_taken_value">
        <input type="hidden" readonly="" class="form-control" id="apprx_date_applied_totatus" name="date_applied_totatus">
        <!-- <label>Peferences of Form-6 <small>Additional Preferences</small></label> -->
        <input type="hidden" readonly="" class="form-control" id="apprx_paid_days" name="dis_lwp">
        <input type="hidden" readonly="" class="form-control" id="apprx_unpaid_days" name="dis_lnp">
        <input type="hidden" readonly="" class="form-control" id="apprx_fullname" name="dis_fullname">
        <input type="hidden" readonly="" class="form-control" id="apprx_office_agency" name="dis_office_agency">
        <input type="hidden" readonly="" class="form-control" id="apprx_date_of_filling" name="dis_filling_date">
        <input type="hidden" readonly="" class="form-control" id="apprx_position" name="dis_position">
        <input type="hidden" readonly="" class="form-control" id="apprx_salary_monthly" name="dis_salary_monthly">
        <input type="hidden" readonly="" class="form-control" id="apprx_vacation_type" name="dis_vacation_type">
        <input type="hidden" readonly="" class="form-control" id="apprx_vacation_leaveincase" name="dis_vacation_leaveincase">
        <input type="hidden" readonly="" class="form-control" id="apprx_sick_incase" name="date_applied_totatus">
        <input type="hidden" readonly="" class="form-control" id="apprx_sick_incase_reason" name="dis_dis_sick_incase_reason">
        <input type="hidden" readonly="" class="form-control" id="apprx_f_vac_abroad" name="f_vac_abroad">
      </div>
      <div class="modal-footer">
        <button type="submit" onclick="location.reload();" class="btn btn-primary" name="btn_lr_approve">Approve and print Form-6</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>

<form action="form6.php"  method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_lr_dissaprove">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
<div class="loading_indicator" id="lod_dissLeave"></div>
      <div class="modal-body">
        <h2 class="ultrabold">DISAPPROVE?</h2>
         <div class="alert alert-primary" role="alert">
          Disapproving this leave will convert this request to "Leave with no pay".
        </div>
        <input type="hidden" id="to_diss_id" name="id">
        <div>
        <label>Reason for disapproval</label>
        <textarea class="form-control" id="reasonfordissaproval_leaveRep_teach" required="" name="reasonofdiss" placeholder="Type the reason here...">
        </textarea>
        <script type="text/javascript">
          $("#reasonfordissaproval_leaveRep_teach").val("");
        </script>
        <input type="hidden" name="fordissaproval" value="true">
        <!-- <label>Freezed Infromation <small>Essetials</small></label> -->
        <input type="hidden" readonly="" class="form-control" id="dis_eid" name="eid">
        <input type="hidden" readonly="" class="form-control" id="dis_leavetype" name="leavetype">
        <input type="hidden" readonly="" class="form-control" id="dis_date_from" name="date_from">
        <input type="hidden" readonly="" class="form-control" id="dis_date_to" name="date_to">
        <input type="hidden" readonly="" class="form-control" id="dis_leave_taken" name="leave_taken_value">
        <input type="hidden" readonly="" class="form-control" id="dis_date_applied_totatus" name="date_applied_totatus">
        <!-- <label>Peferences of Form-6 <small>Additional Preferences</small></label> -->
        <input type="hidden" readonly="" class="form-control" id="dis_paid_days" name="dis_lwp">
        <input type="hidden" readonly="" class="form-control" id="dis_unpaid_days" name="dis_lnp">
        <input type="hidden" readonly="" class="form-control" id="dis_fullname" name="dis_fullname">
        <input type="hidden" readonly="" class="form-control" id="dis_office_agency" name="dis_office_agency">
        <input type="hidden" readonly="" class="form-control" id="dis_date_of_filling" name="dis_filling_date">
        <input type="hidden" readonly="" class="form-control" id="dis_position" name="dis_position">
        <input type="hidden" readonly="" class="form-control" id="dis_salary_monthly" name="dis_salary_monthly">
        <input type="hidden" readonly="" class="form-control" id="dis_vacation_type" name="dis_vacation_type">
        <input type="hidden" readonly="" class="form-control" id="dis_vacation_leaveincase" name="dis_vacation_leaveincase">
        <input type="hidden" readonly="" class="form-control" id="dis_sick_incase" name="date_applied_totatus">
        <input type="hidden" readonly="" class="form-control" id="dis_sick_incase_reason" name="dis_dis_sick_incase_reason">
        <input type="hidden" readonly="" class="form-control" id="dis_f_vac_abroad" name="f_vac_abroad">
        </div>
       
      </div>
      <div class="modal-footer">
        <button type="submit" onclick="ValidateIfHasReason();" class="btn btn-primary" name="btn_lr_dissaprove"  id="dissaprove_printform6">Disapprove and Print Form-6</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>


<form action="form6.php"  method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_lr_reprint">
  <div class="modal-dialog" role="document">
    <div id="noreprintx" class="padder" style="

      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      display: block; 
      position: absolute; 
      width: 100%; 
      height: 100%;
      min-height: 300px;
      z-index: 100;
      background-color: white;
      padding: 20px;

      ">

          <h5 class="card-title">Form 6 not compatible</h5>
          <h6 class="card-subtitle text-muted mb-5">This Form 6 is created in the older version of CDTRS and it is not possible to reprint in this newer version. Only Form 6 printed in CDTRS 3.0.6 and above.</h6>

      </div>
    <div class="modal-content">
      <div class="loading_indicator" id="lod_reprint">
        
      </div>


      <div class="modal-body">
        
        <input type="hidden" id="to_reprint_id" name="id">
       <h2>Print Form 6</h2>
       <p>Your Form 6 is ready for printing.</p>

                <!-- <label>Freezed Infromation <small>Essetials</small></label> -->
        <input type="hidden" readonly="" class="form-control" id="reprin_eid" name="eid">
        <input type="hidden" readonly="" class="form-control" id="reprin_leavetype" name="leavetype">
        <input type="hidden" readonly="" class="form-control" id="reprin_date_from" name="date_from">
        <input type="hidden" readonly="" class="form-control" id="reprin_date_to" name="date_to">
        <input type="hidden" readonly="" class="form-control" id="reprin_leave_taken" name="leave_taken_value">
        <input type="hidden" readonly="" class="form-control" id="reprin_date_applied_totatus" name="date_applied_totatus">
        <!-- <label>Peferences of Form-6 <small>Additional Preferences</small></label> -->
        <input type="hidden" readonly="" class="form-control" id="reprin_paid_days" name="dis_lwp">
        <input type="hidden" readonly="" class="form-control" id="reprin_unpaid_days" name="dis_lnp">
        <input type="hidden" readonly="" class="form-control" id="reprin_fullname" name="dis_fullname">
        <input type="hidden" readonly="" class="form-control" id="reprin_office_agency" name="dis_office_agency">
        <input type="hidden" readonly="" class="form-control" id="reprin_date_of_filling" name="dis_filling_date">
        <input type="hidden" readonly="" class="form-control" id="reprin_position" name="dis_position">
        <input type="hidden" readonly="" class="form-control" id="reprin_salary_monthly" name="dis_salary_monthly">
        <input type="hidden" readonly="" class="form-control" id="reprin_vacation_type" name="dis_vacation_type">
        <input type="hidden" readonly="" class="form-control" id="reprin_vacation_leaveincase" name="dis_vacation_leaveincase">
        <input type="hidden" readonly="" class="form-control" id="reprin_sick_incase" name="date_applied_totatus">
        <input type="hidden" readonly="" class="form-control" id="reprin_sick_incase_reason" name="dis_dis_sick_incase_reason">
        <input type="hidden" readonly="" class="form-control" id="reprin_f_vac_abroad" name="f_vac_abroad">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="btn_lr_reprint">Print</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
  function ValidateIfHasReason(){
    if($("#reasonfordissaproval_leaveRep_teach").val() != ""){
      location.reload();
    }
    
  }
  function openreprint_leave(to_cont){
$("#noreprintx").css("display","none");
    $("#lod_reprint").css("display","block");
    $("#to_reprint_id").val($(to_cont).data("theid"));
        // GET ADDITIONAL VALUES
    $.ajax({
      type: "POST",
      url: "php/retrieve_all_form6_data.php",
      data: {"applied_leave_id": $(to_cont).data("theid")},
      success: function(data){
        setTimeout(function(){
         
        },1000)
        
        data = JSON.parse(data);

        $("#reprin_eid").val(data[0]["employee_id"]);
        $("#reprin_f_vac_abroad").val(data[0]["vac_leave_abroad_specify"]);
        $("#reprin_leavetype").val(data[0]["leave_type"]);
        $("#reprin_date_from").val(data[0]["date_from"]);
        $("#reprin_date_to").val(data[0]["date_to"]);
        $("#reprin_leave_taken").val(data[0]["leave_taken"]);
        $("#reprin_date_applied_totatus").val(data[0]["date_applied_totatus"]);

        $("#reprin_paid_days").val(data[0]["paid_days"]);
        $("#reprin_unpaid_days").val(data[0]["unpaid_days"]);
        $("#reprin_fullname").val(data[0]["fullname"]);
        $("#reprin_office_agency").val(data[0]["office_agency"]);
        $("#reprin_date_of_filling").val(data[0]["date_of_filling"]);
        $("#reprin_position").val(data[0]["position"]);
        $("#reprin_salary_monthly").val(data[0]["salary_monthly"]);
        $("#reprin_vacation_type").val(data[0]["vacation_type"]);
        $("#reprin_vacation_leaveincase").val(data[0]["vacation_leaveincase"]);
        $("#reprin_sick_incase").val(data[0]["sick_incase"]);
        $("#reprin_sick_incase_reason").val(data[0]["sick_incase_reason"]);
          reprint_validator( $(to_cont).data("theid"));
          $("#lod_reprint").css("display","none");
      }
    })
  }

  function reprint_validator(leaveid){
    $.ajax({
      type : "POST",
      url : "php/external_server.php",
      data: {check_ifreprintable:"x",leave_id:leaveid},
      success : function(data){
        // alert(data);
        if(data == "false"){
          $("#noreprintx").css("display","block");
        }
         
      }
    })
  }
  function openapprove_leave(to_cont){

    $("#lod_applyleave").css("display","block");
    $("#to_app_id").val($(to_cont).data("theid"));
        // GET ADDITIONAL VALUES
    $.ajax({
      type: "POST",
      url: "php/retrieve_all_form6_data.php",
      data: {"applied_leave_id": $(to_cont).data("theid")},
      success: function(data){
        setTimeout(function(){
          $("#lod_applyleave").css("display","none");
        },1000)
        
        data = JSON.parse(data);

        $("#apprx_eid").val(data[0]["employee_id"]);
        $("#apprx_f_vac_abroad").val(data[0]["vac_leave_abroad_specify"]);
        $("#apprx_leavetype").val(data[0]["leave_type"]);
        $("#apprx_date_from").val(data[0]["date_from"]);
        $("#apprx_date_to").val(data[0]["date_to"]);
        $("#apprx_leave_taken").val(data[0]["leave_taken"]);
        $("#apprx_date_applied_totatus").val(data[0]["date_applied_totatus"]);

        $("#apprx_paid_days").val(data[0]["paid_days"]);
        $("#apprx_unpaid_days").val(data[0]["unpaid_days"]);
        $("#apprx_fullname").val(data[0]["fullname"]);
        $("#apprx_office_agency").val(data[0]["office_agency"]);
        $("#apprx_date_of_filling").val(data[0]["date_of_filling"]);
        $("#apprx_position").val(data[0]["position"]);
        $("#apprx_salary_monthly").val(data[0]["salary_monthly"]);
        $("#apprx_vacation_type").val(data[0]["vacation_type"]);
        $("#apprx_vacation_leaveincase").val(data[0]["vacation_leaveincase"]);
        $("#apprx_sick_incase").val(data[0]["sick_incase"]);
        $("#apprx_sick_incase_reason").val(data[0]["sick_incase_reason"]);

      }
    })
  }


  function opendissaprove_leave(to_cont){
    $("#lod_dissLeave").css("display","block");
    $("#to_diss_id").val($(to_cont).data("theid"));
    // GET ADDITIONAL VALUES
    $.ajax({
      type: "POST",
      url: "php/retrieve_all_form6_data.php",
      data: {"applied_leave_id": $(to_cont).data("theid")},
      success: function(data){
          setTimeout(function(){
         $("#lod_dissLeave").css("display","none");
        },1000)

        data = JSON.parse(data);
        $("#dis_eid").val(data[0]["employee_id"]);
        $("#dis_f_vac_abroad").val(data[0]["vac_leave_abroad_specify"]);
        $("#dis_leavetype").val(data[0]["leave_type"]);
        $("#dis_date_from").val(data[0]["date_from"]);
        $("#dis_date_to").val(data[0]["date_to"]);
        $("#dis_leave_taken").val(data[0]["leave_taken"]);
        $("#dis_date_applied_totatus").val(data[0]["date_applied_totatus"]);

        $("#dis_paid_days").val(data[0]["paid_days"]);
        $("#dis_unpaid_days").val(data[0]["unpaid_days"]);
        $("#dis_fullname").val(data[0]["fullname"]);
        $("#dis_office_agency").val(data[0]["office_agency"]);
        $("#dis_date_of_filling").val(data[0]["date_of_filling"]);
        $("#dis_position").val(data[0]["position"]);
        $("#dis_salary_monthly").val(data[0]["salary_monthly"]);
        $("#dis_vacation_type").val(data[0]["vacation_type"]);
        $("#dis_vacation_leaveincase").val(data[0]["vacation_leaveincase"]);
        $("#dis_sick_incase").val(data[0]["sick_incase"]);
        $("#dis_sick_incase_reason").val(data[0]["sick_incase_reason"]);

      }
    })
  }

</script>

<script type="text/javascript">
// $(".modal").draggable({
// handle: ".modal-header"
// });
</script>
