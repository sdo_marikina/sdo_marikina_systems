<!-- <form action="php/external_server.php" method="POST">-->
<div class="modal" tabindex="-1" role="dialog" id="modal_Departure">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
     <div class="container">
        <p class="ultrabold"><i class="far fa-paper-plane"></i> Whereabouts</p>
      <div class="padder softslide" id="w_page1" style="display: none;">
        <h1 class="ultrathin">Ready to Depart?</h1>
        <p>Please type your employee number first to finally enter your destination and purpose.</p>
        <br>
        <div class="form-group">
          <label>Employee Number</label>
          <input id="inpt_employeenumber" placeholder="Type here..." type="text" autocomplete="off" name="" required=""  class="form-control">
        </div>
        <div class="form-group">
          <button class="btn btn-sm btn-primary" type="button" onclick="whereboutsnext()">Next</button>
        </div>
      </div>
      <div class="padder flipper" id="w_page2" style="display: none;">
        <h1 class="ultrathin">Just a little bit more.</h1>
        <p>Where are you going? type that information on the Destination field and put the Purpose of your Departure on the Purpose field.</p>
        <br>
        <div class="form-group">
          <label>Destination</label>
          <input id="inp_destination" placeholder="Type here..." type="text" name="" autocomplete="off"  required="" class="form-control">
        </div>
        <div class="form-group">
          <label>Purpose</label>
          <textarea id="inp_purpose" placeholder="Type here..." class="form-control" autocomplete="off" required=""  rows="4"></textarea>
        </div>
        <div class="form-group">
          <button class="btn btn-light btn-sm" onclick="setupwhereabouts()">Back</button>
           <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="submit_departure()">Submit</button>
        </div>
      </div>
     </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function setupwhereabouts(){
    $("#w_page1").css("display","block");
    $("#w_page2").css("display","none");
  }
  function whereboutsnext(){
    $("#w_page1").css("display","none");
    $("#w_page2").css("display","block");
  }
</script>




<!-- </form> -->

<!-- <form action="php/external_server.php" method="POST"> -->
	<div class="modal" tabindex="-1" role="dialog" id="modal_Arrival">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <center>  <h4 class="ultrabold"><i class="fas fa-home"></i> Arrival</h4></center>
      	      <div class="form-group">
                <br>
      		<input type="text" style="text-align: center;" placeholder="Type employee code..." id="inp_arrivalempnumber" autocomplete="off" name="" required=""  class="form-control">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal" onclick="submit_arrival()">Arrive</button>
        <!-- <button type="button" class="btn btn-light btn-block" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>
<!-- </form> -->

<div class="modal" tabindex="-1" role="dialog" id="modal_Logs">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<center><h4 class="ultrabold"><i class="fas fa-history"></i> Logs</h4></center>
      	<table class="table table-sm table-bordered" id="logs_dt">
      		<thead>
      			<th>Employee Name</th>
      			<th>Destination</th>
      			<th>Purpose</th>
      			<th>Departure</th>
      			<th>Arrival</th>
      		</thead>
      		<tbody id="thewehreaboutslogs">
      			
      		</tbody>
      	</table>
       <center>
          <a href="#" onclick="loadallwhereabouts()" class="btn btn-light"><i class="fas fa-retweet"></i> Load all</a>
       </center>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function loadallwhereabouts(){
       // $("#thewehreaboutslogs").html("");
           $("#logs_dt").dataTable().fnDestroy();
    popnotification("<i class='fas fa-database'></i> Please wait...","CDTRS is loading all Where-Abouts records.",false);
    $.ajax({
      type: "POST",
      url: "php/external_server.php",
      data: {thewehreaboutslogs:"x"},
      success: function(data){
          // alert(data);
          $("#thewehreaboutslogs").html(data);
          popnotification("Done loading.","CDTRS is done loading all Where-Abouts data.",false);
            $("#logs_dt").DataTable();
      }
    })
  }
   $("#logs_dt").DataTable();
	function submit_departure(){
			var inp_destination = $("#inp_destination").val();
			var inp_purpose = $("#inp_purpose").val();
			var inpt_employeenumber = $("#inpt_employeenumber").val();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {add_departure:"x",inp_des:inp_destination,inp_pur:inp_purpose,inp_empnum:inpt_employeenumber},
			success: function(data){
				popnotification(data,"WHERE ABOUTS",false);
					$("#inp_destination").val("");
					$("#inp_purpose").val("");
					$("#inpt_employeenumber").val("");
			}
		})
	}
	function submit_arrival(){
			var inp_arrivalempnumber = $("#inp_arrivalempnumber").val();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {submit_arrival:"x",eid: inp_arrivalempnumber},
			success: function(data){
				popnotification(data,"WHERE ABOUTS",false);
				$("#inp_arrivalempnumber").val("");
			}
		})
	}



	function loadwhereabouts_logs(){
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {get_logs:"x"},
			success: function(data){
				$("#thewehreaboutslogs").html(data);
				$("#logs_dt").DataTable();
			}
		})
	}
</script>