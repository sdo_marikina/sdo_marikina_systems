<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="approveUTmodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
    <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h3 class="ultrabold">Approve?</h3>

          

          <p>Are you sure you want to approve <u id="utempname"></u>'s undertime dispute for <u id="utdateofutdispute"></u>?</p>
          <input type="hidden" id="idofuttoapprove" name="idofutdisputer">

<div class="alert alert-warning" role="alert">
            <i class="fas fa-info-circle"></i><br>Undertime for this dispute will be automatically deducted to the employee's <u>Sick Leave</u> entitlement.
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" name="approvedisputeforUT" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</form>


<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="disapprovedUTmodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
 
        <div class="modal-body">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h3 class="ultrabold">Disapprove?</h3>

          <p>Are you sure you want to disapprove <u id="utempname_dis"></u>'s undertime dispute for <u id="utdateofutdispute_dis"></u>?</p>
          <input type="hidden" id="idofuttoapprove_dis" name="idofutdisputer">



          <div class="form-group">
            <label>Reason for disapproval</label>
            <textarea class="form-control" name="rfordisaaprovalut" placeholder="Explain why this dispute is being disapproved in plain text." required=""></textarea>
          </div>

          <div class="alert alert-danger" role="alert">
            <i class="fas fa-info-circle"></i><br>Undertime for this dispute will be automatically deducted to the employee's <u>Vacation Leave</u> entitlement.
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" name="disapproveddisputeforUT" class="btn btn-primary">Yes, disapproved this dispute!</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</form>



<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_dis_approve">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">

      <div class="modal-body">
        <center><h4 class="ultratitle"><i class="fas fa-dharmachakra"></i> MANUAL ENTRY</h4></center>
          <input id="dt_ap_for" readonly="" type="hidden" class="form-control form-control-lg display-4 labelinput ultrabold" style="font-size: 50px; text-align: right;" name="date_to_apply">
          <br>
          <h4 class="float-right" id="beautydate">March 24, 2019</h4>
         <p>
            <strong class="ultrabold" id="empnamedisplay">Username Here...</strong><br>
          <span id="utypename"></span>
         </p>
          <input type="hidden" id="usertype" name="utype">
        <div class="row">
  
            <div class="col-sm-3">
               <center>AM <small>IN</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t1" name="t1"> -->
                             <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input autocomplete="off" list="timelist" name="t1" type="text" required=""  placeholder="00:00 --" id="t1" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" id="extra_time_out">
                <center>AM <small>OUT</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t2" name="t2"> -->
                              <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                    <input autocomplete="off" list="timelist" name="t2" type="text" required=""  placeholder="00:00 --" id="t2" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" id="extra_time_in">
               <center>PM <small>IN</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t3" name="t3"> -->
                              <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                    <input autocomplete="off" list="timelist" name="t3" type="text" required=""  placeholder="00:00 --" id="t3"class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
               <center>PM <small>OUT</small></center>
              <!-- <input type="time" step="1" required="" class="form-control" id="t4" name="t4"> -->
                              <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input autocomplete="off" list="timelist" name="t4" type="text" required=""  placeholder="00:00 --" id="t4" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <datalist id="timelist">
          <option>7:00 AM</option>
          <option>7:30 AM</option>
          <option>8:00 AM</option>
          <option>8:30 AM</option>
          <option>9:00 AM</option>
          <option>9:30 AM</option>
          <option>10:00 AM</option>
          <option>10:30 AM</option>
          <option>11:00 AM</option>
          <option>11:30 AM</option>
          <option>12:00 PM</option>
          <option>12:30 PM</option>
          <option>1:00 PM</option>
          <option>1:30 PM</option>
          <option>2:00 PM</option>
          <option>2:30 PM</option>
          <option>3:00 PM</option>
          <option>3:30 PM</option>
          <option>4:00 PM</option>
          <option>4:30 PM</option>
          <option>5:00 PM</option>
          <option>5:30 PM</option>
          <option>6:00 PM</option>
           <option>6:30 PM</option>
          <option>7:00 PM</option>
          <option>7:30 PM</option>
          <option>8:00 PM</option>
          <option>8:30 PM</option>
          <option>9:00 PM</option>
          <option>9:30 PM</option>
          <option>10:00 PM</option>
          <option>10:30 PM</option>
          <option>11:00 PM</option>
          <option>11:30 PM</option>
          <option>12:00 AM</option>
          <option>12:30 AM</option>
        </datalist>
                  <br>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                format: 'LT'
                });
                $('#datetimepicker2').datetimepicker({
                format: 'LT'
                });
                $('#datetimepicker3').datetimepicker({
                format: 'LT'
                });
                $('#datetimepicker4').datetimepicker({
                format: 'LT'
                });
            });
        </script>
        <div class="form-group">
          <label><input type="checkbox" name="doc_sub_stats"> Supporting document submitted</label>
        </div>
            <input type="hidden" id="id_dispute_approve" name="dispute_control">
            <input type="hidden" id="theeid" name="eid">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="btn_Approve"><i class="fas fa-check"></i> Apply</button>
        <button type="button" class="btn" data-dismiss="modal"><i class="fas fa-times"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>


<form method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_dis_dissaprove">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="ultrabold">DISSAPROVE?</h2>
        <input type="hidden" id="id_dispute_dissaprove" name="dispute_control">
        <div class="form-group">
         
          <div>
            <label>Reason for dissaproval</label>
            <textarea id="reasonDisputeValue" required="" name="reasonfordissaproval_dispute" placeholder="Add reason here..." class="form-control">
              
            </textarea>
            <script type="text/javascript">
              $("#reasonDisputeValue").val("");
            </script>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger" name="btn_Dissaprove">Dissaprove</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Close</span></button>
      </div>
    </div>
  </div>
</div>
</form>


<script type="text/javascript">
  var manual_entry_type = 4;
  function OpenModalApproveUTDISP(tocontrol){
    $("#utempname").html($(tocontrol).data("empnamedisplay"));
    $("#utdateofutdispute").html($(tocontrol).data("beautydate"));
    $("#idofuttoapprove").val($(tocontrol).data("disid"));
  }
    function OpenModalDisapproveUTDISP(tocontrol){
    $("#utempname_dis").html($(tocontrol).data("empnamedisplay"));
    $("#utdateofutdispute_dis").html($(tocontrol).data("beautydate"));
    $("#idofuttoapprove_dis").val($(tocontrol).data("disid"));
  }
  function OpenModalApprove(tocontrol){

    $("#usertype").val($(tocontrol).data("usertype"));
        switch($("#usertype").val()){
      case "1":
          $("#utypename").html("Non-Teaching");
          enablelunch();
      break;
      case "2":
          $("#utypename").html("Teaching");
          disablelunch();
      break;
      case "3":
          $("#utypename").html("Division Personnel");
          enablelunch();
      break;
    }

    $("#dt_ap_for").val($(tocontrol).data("apdte"));

    $("#id_dispute_approve").val($(tocontrol).data("disid"));

    $("#t1").val($(tocontrol).data("tt1"));
    $("#t2").val($(tocontrol).data("tt2"));
    $("#t3").val($(tocontrol).data("tt3"));
    $("#t4").val($(tocontrol).data("tt4"));
    $("#theeid").val($(tocontrol).data("theeid"));
    $("#beautydate").html($(tocontrol).data("beautydate"));
    $("#empnamedisplay").html($(tocontrol).data("empnamedisplay"));
  }
  function OpenModalDissaprove(tocontrol){
    $("#id_dispute_dissaprove").val($(tocontrol).data("disid"));
  }

  function disablelunch(){
    manual_entry_type = 2;
    $("#extra_time_out").css("display","none");
$("#extra_time_in").css("display","none");

document.querySelector('#t2').required = false;
document.querySelector('#t3').required = false;
  }
  function enablelunch(){
    manual_entry_type = 4;
    $("#extra_time_out").css("display","block");
$("#extra_time_in").css("display","block");

document.querySelector('#t2').required = true;
document.querySelector('#t3').required = true;
  }

</script>
<script type="text/javascript" src="api/wickedpicker/dist/wickedpicker.min.js"></script>
<style type="text/css" href="api/wickedpicker/dist/wickedpicker.min.css"></style>