

<div class="modal" tabindex="-1" role="dialog" id="cocoa">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">

      <div class="modal-body">
      <div class="container">
          <h6 class="ultrabold"><i class="fas fa-arrow-circle-up"></i> System Update</h6>
        <div class="padder">
          <img src="images/cdtrs3.png" style="width: 80px;">
          <h1><span id="u_title"></span></h1>
          <h6 class="card-subtitle text-muted mb-2" id="u_desc"></h6>
          <hr>
          <h6>
            <span class="badge badge-primary">GIT PULL NOW TO <span id="u_vcode"></span></span>
          </h6>
      </div>
      </div>
    </div>
  </div>
</div>

<!-- CHECK FOR SETUP PROBLEMS -->
<form action="php/external_server.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="scname_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">School Name is Missing</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Type your full School Name<br><small>Example: Barangka Elementary School</small></label>
          <input type="text" class="form-control" required="" autocomplete="off" name="sc_name_now" placeholder="Type full school name (not the acronym)" name="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="changeschoolnamenow" class="btn btn-primary">Save and Resolve</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
  $.ajax({
    type: "POST",
    url: "php/external_server.php",
    data: {validate_schoolname:"x"},
    success: function(data){
         // alert(data);
      data = data.split(",");
      var message  = data[1];
      switch(data[0]){
        case "1":
          $("#scname_modal").modal("show");
        break;
        case "2":
          $("#scname_modal").modal("show");
        break;
      }
    }
  })
	var currentv = <?php echo $version; ?>;
	$.ajax({
	type: "POST",
	url: "php/external_server.php",
	data: {getlatestversion: "x"},
	success: function(data){
    if(data != "null" && data != "" && data != null){
    data = JSON.parse(data);
  if(currentv < data["version"]){
  $("#cocoa").modal("show");
  $("#u_title").html(data["message"]);
  $("#u_desc").html(data["description"]);
  $("#u_vcode").html(data["version"]);
  }
    }

	}
	})
</script>