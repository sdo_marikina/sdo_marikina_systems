	<!-- ADD NEW EMPLOYEE MODAL  -->
	<form action="php/external_server.php" method="POST">
		       <h5 class="ultratitle"><i class="fas fa-user-plus"></i> Add New Employee</h5>
		       <br>
		       <div class="container">
		       	
		       	<div id="accordion">
		       
		         <div class="card">
		           <div class="card-header">
		             <a class="card-link" data-toggle="collapse" href="#collapseOne">

		               <h5 class=""><i class="far fa-user-circle"></i> Basic Information</h5>
		             </a>
		           </div>
		           <div id="collapseOne" class="collapse show" data-parent="#accordion">
		             <div class="card-body">
		               		       <div class="row">
<!-- 			       	<div class="col-sm-12">
			       		<div class="form-group">
			       			<h5 class=""><i class="far fa-user-circle"></i> EMPLOYEE INFORMATION</h5>
						</div>
			       	</div> -->
		       		<div class="col-sm-6">

		       			<div class="form-group">
	       					<label>Employee ID</label>
	       					<label class="float-right"><input type="checkbox" name="id_is_temporary"> Is Temporary ID</label>
			       			<input type="text" id="emp_num_inp" class="form-control" required="" autocomplete="off" placeholder="Type here..." name="employeenumber">

			       			<script type="text/javascript">
			       					$("#emp_num_inp").change(function(){
			       						checkempnumifext();
			       					})
			       				function checkempnumifext(){
			       					var emp_num_inp = $("#emp_num_inp").val();
			       					$.ajax({
			       					type: "POST",
			       					url: "php/external_server.php",
			       					data: {checkeid:"x",eid: emp_num_inp},
			       					success: function(data){
			       						if(data != "true"){
			       								$("#messageid").html(data);
			       						}else{
			       							$("#messageid").html("");
			       						}
			       					}
			       				})

			       				}
			       				
			       			</script>

		       			</div>
		       			<div id="messageid">
		       		
				       	</div>
		       		</div>
		       		<div class="col-sm-6">
		       			<div class="form-group">
		       					<label>Basic Pay</label>
		       			<input type="number" class="form-control" required="" autocomplete="off" placeholder="Type here..." name="bazicpay">
		       			</div>
		       		</div>
		       				       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Last Name</label>
		       			<input type="text" class="form-control" required="" autocomplete="off" placeholder="Type here..." name="lname">
		       		</div>
		       	</div>

		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>First Name</label>
		       			<input type="text" class="form-control" required="" autocomplete="off" placeholder="Type here..." name="fname">
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Middle Name <small>(Optional)</small></label>
		       			<input type="text" class="form-control" autocomplete="off" placeholder="Type here..." name="mname">
		       		</div>
		       	</div>

		       		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Position</label>
		       			<select class="form-control" required="" autocomplete="off" name="pozition">
		       				<?php load_drop_position(); ?>
		       			</select>
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Department</label>
		       			<select class="form-control" required="" autocomplete="off" name="deptname">
		       				<?php load_drop_department(); ?>
		       			</select>
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Employee Type</label>
		       			<select class="form-control" required="" id="utypevlidater" name="emptype">
		       				<?php
		       				if(GetSettings("usertype") != "1"){
		       				?>
		       				<option selected="" value="" disabled="">Choose Employee Type</option>
		       				<?php } ?>
		       				<option value="1">Non-Teaching</option>
		       				<?php
		       				if(GetSettings("usertype") != "0"){
		       				?>
		       				<option value="2">Teaching</option>
		       				<?php } ?>
		       				<?php
		       				if(GetSettings("usertype") != "1"){
		       				?>
		       				<option value="3">Division Personnel</option>
		       				<?php } ?>
		       			</select>

		       			<script type="text/javascript">
		       				$("#utypevlidater").change(function(){
								if($("#utypevlidater").val() == "2"){
									$("#br_1").prop('disabled', true);
									$("#br_2").prop('disabled', true);

								}else{
									$("#br_1").prop('disabled', false);
									$("#br_2").prop('disabled', false);
								}
		       				})
		       				
		       			</script>
		       		</div>
		       	</div>


		       </div>
		             </div>
		           </div>
		         </div>
		       
		         <div class="card">
		           <div class="card-header">
		             <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
		              <h5 class=""><i class="fas fa-id-card"></i> Contact</h5>
		             </a>
		           </div>
		           <div id="collapseTwo" class="collapse" data-parent="#accordion">
		             <div class="card-body">
<!-- 	               		       	<div class="col-sm-12">
<div class="form-group">
<h5 class=""><i class="fas fa-id-card"></i> CONTACT INFORMATION</h5>
</div>
</div> -->
					<div class="row">
					   	<div class="col-sm-6">
							<div class="form-group">
								<!-- <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Department_of_Education_%28DepEd%29.svg/2000px-Department_of_Education_%28DepEd%29.svg.png" style="height: 50px;"><br> -->
								<label>DepEd Email Address</label>
								<input type="email" class="form-control" autocomplete="off" placeholder="Ex: Ryan@sample.com" name="emp_email">
							</div>
						</div>
							<div class="col-sm-6">
							<div class="form-group">
								<!-- <img src="https://facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png" style="height:50px;">
								<br> -->
								<label>Facebook Account <small>(Optional)</small></label>
								<input type="text" class="form-control" autocomplete="off" placeholder="Ex: facebook.com/yourname" name="emp_fbacc">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>Home Address <small>(Optional)</small></label>
								<input type="text" class="form-control" autocomplete="off" placeholder="Ex: #194 Ipil Street Marikina Heights, Marikina City" name="emp_homeadd">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Primary Contact Number <small>(Optional)</small></label>
								<input type="text" class="form-control" autocomplete="off" placeholder="Ex: 09xxxxxxxxx" name="emp_primarycontnum">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Emergency Contact Number <small>(Optional)</small></label>
								<input type="text" class="form-control" autocomplete="off" placeholder="Ex: 09xxxxxxxxx" name="emp_cont_num">
							</div>
						</div>
					</div>
		             </div>
		           </div>
		         </div>
		       
		         <div class="card">
		           <div class="card-header">
		             <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
		           <h5 class=""><i class="fas fa-walking"></i> Leave Preferences and Schedule</h5>
		             </a>
		           </div>
		           <div id="collapseThree" class="collapse" data-parent="#accordion">
		             <div class="card-body">
		              		       <div class="row">
	

		       <!-- 	<div class="col-sm-12">
		       		<div class="form-group">
		       			<h5 class=""><i class="fas fa-walking"></i> LEAVE PREFERENCES</h5>
					</div>
		       	</div> -->
		       	<div class="col-sm-6">
		       		<div class="form-group">
		       			<label>Are you a single parent?</label><br>
		       			<label><input type="checkbox" name="issingle"> Yes</label>
<!-- 		      		<select class="form-control" required="" name="issingle">
		      			<option selected="" disabled="">Choose a status</option>
		      			<option value="2">Not a Parent</option>
		      			<option value="0">No</option>
		      			<option value="1">Yes</option>
		      		</select> -->
		       		</div>
		       	</div>
		       	<div class="col-sm-12">
		       		<div class="form-group">
		       			<h5 class="ultrabold"><i class="fas fa-hourglass-half"></i> SCHEDULE</h5>
					</div>
		       	</div>
		       	  	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Time-in</label>
		       			<input type="time" required="" name="tm_in" class="form-control">
		       		</div>
		       	</div>
		       	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Break-In <small>(Optional)</small></label>
		       			<input type="time" id="br_1" name="br_in" value="" class="form-control">
		       		</div>
		       	</div>
		       	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Break-Out <small>(Optional)</small></label>
		       			<input type="time" id="br_2" name="br_out" value="" class="form-control">
		       		</div>
		       	</div>
		       	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Time-Out</label>
		       			<input type="time" required="" name="tm_out" class="form-control">
		       		</div>
		       	</div>
		       </div>
		             </div>
		           </div>
		         </div>
		       
		       </div>

		       </div>
<div class="container" style="text-align: right;">
	 <button type="submit" name="AddNewEmployeeNow" class="btn btn-primary "><i class="fas fa-user-plus"></i> Add Employee</button>
		        <button type="button" class="btn btn-light " data-dismiss="modal">Cancel</button>
</div>
	</form>