<div id="cocoasearch" style="height: 40px; overflow: hidden; transition: 0.3s all !important; background-color: transparent !important; margin-bottom: 0px; padding-bottom: 0px;">
    <input type="text" autocomplete="off" id="searchnametext" placeholder="Type and press enter to search..." class="form-control" placeholder="" name="">
    <br>
    <div id="theresemptable">
        <div id="restable">

        </div>
    </div>
    <script type="text/javascript">
        $("#searchnametext").keyup(function() {
            var searchnametext = $("#searchnametext").val();
            if (searchnametext == "") {
                $("#theresemptable").css("display", "none");
                $("#cocoasearch").css("height", "40px");
            } else {
                $("#theresemptable").css("width", "100%");
                $("#theresemptable").css("display", "block");
                $("#cocoasearch").css("height", "300px");
                $.ajax({
                    type: "POST",
                    url: "php/external_server.php",
                    data: {
                        searchalog: "x",
                        tosearch: searchnametext
                    },
                    success: function(data) {
                        $("#restable").html(data);
                    }
                });
            }
        })
    </script>
</div>
<div class="modal" tabindex="-1" role="dialog" id="emp_log_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="ultrabold">Today's Logs</h4>
                <h1 class="ultrabold" id="TheEmployeeName">RYAN LEE REGENCIA</h1>
                <table class="table table-bordered table-striped" id="logdt">
                    <thead>
                        <tr>
                            <th>Access Type</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody id="thelogofsingle">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secodary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
            </div>
        </div>
    </div>
</div>