<form action="php/external_server.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_dispute">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title">Dispute</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" required="" value="" id="dispute_id_container" name="eid">
        <input type="hidden" id="disputedate" name="date_of_dispute">
      <div class="form-group">
        <label>Reason for dispute</label>
        <textarea class="form-control" required="" type="text" name="reason"></textarea>
      </div>

      <div class="form-group">
        <label><input type="checkbox" required="" name="is_oath" value="1"> Corresponding attachment has been submitted</label>
      </div>
      
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="btn_applydispute">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>

<form action="form6.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_applyleave">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title">Apply Leave<br><small>Please fill this form thoroughly</small></p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<input type="hidden" id="applyleave_id_container" name="eid">
<input type="hidden" name="date_applied_totatus" id="topic_date">
<!-- SELECT LEAVE TYPE -->
<div class="form-group">
   <p class="ptitle">Pick Leave Type<br>
    <small>Only the (Leave Type) with credits will display here.</small></p>
  <select type="text" required="" class="form-control" id="select_availableleavetype" placeholder="Username">
  </select>
  <input type="hidden" id="trueval_type" name="leavetype" >
</div>

<div id="applyLeave_form2">
<div class="form-group">
    <table class="table table-bordered card-body" style="text-align: center !important; ">
      <tr>
        <th>Leave Balance</th>
        <td> <input type="text" readonly=""  value="0.00"  id="txt_leavebalanceofleavetype" class="form-control labelinput" name="leavebalance"></td>
      </tr>
      <tr>
        <th>Days of Leave</th>
        <td><span id="date_day_diff_total">Days of leave</span></td>
      </tr>
      <tr>
        <th>Total</th>
        <td><span id="total_balInFuture">0</span></td>
      </tr>
    </table>
  <input type="hidden" id="id_leave_taken" name="leave_taken_value">
</div>
  <p class="ptitle">Leave Duration</p>
<div class="row">
  <div class="col-sm-6">
    
    <div class="form-group">
  <label>
    From
  </label>
  <input required="" type="date" class="form-control" id="date_ff" name="date_from">
</div>

  </div>
  <div class="col-sm-6">
    
    <div class="form-group">
  <label>
    To
  </label>
  <input required="" type="date" class="form-control" id="date_tt" name="date_to">
</div>

  </div>
</div>

<div class="form-group" id="reasoningselection">
  <p class="ptitle">Leave Info</p>
  <label><input type="radio" name="leavetype_status" value="" id="id_checkbox_1"> <span id="choice_1"></span><br></label>
  <input id="location_1" name="location_1" placeholder="Type here..." maxlength="25" title="MAX INPUT LENGTH IS 25 CHARACTERS ONLY" type="text" class="form-control">
  <br>
  <label><input type="radio" name="leavetype_status" value="" id="id_checkbox_2"> <span id="choice_2"></span><br></label>
  <input id="location_2" name="location_2" placeholder="Type here..." maxlength="25" title="MAX INPUT LENGTH IS 25 CHARACTERS ONLY" type="text" class="form-control">
</div>

<div id="vacation_additional">
      <p class="ptitle">Where the leave will be spent?</p>
<div class="row">
  <div class="col-sm-6">
        <div class="form-group">
      <label><input type="radio" value="0" name="vacationleave_location"> Within the Philippines</label>
    </div>
  </div>
  <div class="col-sm-6">
        <div class="form-group">
       <label><input type="radio" value="1" name="vacationleave_location"> Abroad (Specify)</label>
        <textarea id="toclear_txt" name="f_vac_abroad" class="form-control" maxlength="50" title="MAX INPUT LENGTH IS 50 CHARACTERS ONLY">
          
        </textarea>
        <script type="text/javascript">
          $("#toclear_txt").val("");
        </script>
    </div>
  </div>
</div>
</div>

</div>

      </div>
      <div class="modal-footer" id="submitcontainer">
        <button type="submit" class="btn" name="btn_apply_leave" id="leavesubbtn">Submit</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>


<script type="text/javascript">
  $("#vacation_additional").css("display","none");
  function func_dispute(element){
    $("#dispute_id_container").val($(element).data("empnum"));
    $("#disputedate").val($(element).data("dateofrecord"));
  }
  function func_applyleave(element){
      $("#submitcontainer").css("display","none");
     $("#applyLeave_form2").css("display","none");
    $("#applyleave_id_container").val($(element).data("empnum"));
    $("#topic_date").val($(element).data("topicdate"));

    var empnumber =  $(element).data("empnum");
    //GET AVAILABLE LEAVE TYPE
    $.ajax({
      type: "POST",
      url: "php/external_server.php",
      data: {getavailablenumber:"x", employeenumber:empnumber},
      success: function(data){
          $("#select_availableleavetype").html(data);
          $("#select_availableleavetype").append("<option selected='selected' disabled >Choose leave type...</option>");
      }
    })

  }

  $("#id_checkbox_1").click(function(){
      $("#location_1").attr("disabled",false);
      $("#location_2").attr("disabled",true);
      $("#location_1").val("");
      $("#location_2").val("");
        $("#submitcontainer").css("display","inline-block");
  })
  $("#id_checkbox_2").click(function(){
      $("#location_1").attr("disabled",true);
      $("#location_2").attr("disabled",false);
            $("#location_1").val("");
      $("#location_2").val("");
        $("#submitcontainer").css("display","inline-block");
  })

  $("#select_availableleavetype").change(function(){

    if($("#select_availableleavetype").val() == "5"){
        $("#vacation_additional").css("display","block");
    }else{
       $("#vacation_additional").css("display","none");
    }


    $("#applyLeave_form2").css("display","block");
       $("#location_1").attr("required",true);
      $("#location_2").attr("required",true);

      $("#location_1").attr("disabled",true);
      $("#location_2").attr("disabled",true);
      $("#submitcontainer").css("display","none");
    //DISPLAY CURRENT LEAVE TYPE VALUE
    $("#txt_leavebalanceofleavetype").val($("#select_availableleavetype").val());
    var currentval = $("#select_availableleavetype option:selected").text();
    $("#trueval_type").val(currentval);
  $("#reasoningselection").css("display","none");
    if(currentval == "Sick Leave"){
      document.getElementById("id_checkbox_1").checked = false;
      document.getElementById("id_checkbox_2").checked = false;
      $("#choice_1").html("In Hospital<br><small>Type the name of the hospital</small>");
      $("#choice_2").html("Out Patient<br><small>Type where</small>");
      $("#id_checkbox_1").val("In Hospital");
      $("#id_checkbox_2").val("Out Patient");
        $("#reasoningselection").css("display","block");
    }else if(currentval == "Vacation Leave"){
       $("#vacation_additional").css("display","block");
       document.getElementById("id_checkbox_1").checked = false;
      document.getElementById("id_checkbox_2").checked = false;
      $("#choice_1").html("To Seek Employment");
      $("#choice_2").html("Others (Specify)<br><small>Type reason below</small>");
      $("#id_checkbox_1").val("To Seek Employment");
      $("#id_checkbox_2").val("Others (Specify)");
        $("#reasoningselection").css("display","block");
    }else{

       $("#vacation_additional").css("display","none");
      $("#reasoningselection").css("display","none");
      $("#location_1").attr("required",false);
      $("#location_2").attr("required",false);
        $("#submitcontainer").css("display","inline-block");
    }
  })


  setInterval(function(){
    if($("#date_day_diff_total").html() == "0"){
        $("#leavesubbtn").prop("disabled", true);
         $("#leavesubbtn").html("<span><strong>Days of Leave</strong> must be higher than zero</span>");
    }else{
        $("#leavesubbtn").prop("disabled", false);
        $("#leavesubbtn").html("Submit");
    }
    if( $("#select_availableleavetype option:selected").text() == "Vacation Leave"){
        $("#location_1").val("");
        $("#location_1").attr("required",false);
        $("#location_1").css("display","none");
    }else{
      $("#location_1").css("display","block");
    }

  },100)
    $("#date_ff").change(function(){
    ComputeDayDiff($("#date_ff").val(),$("#date_tt").val());
    })

    $("#date_tt").change(function(){
    ComputeDayDiff($("#date_ff").val(),$("#date_tt").val());
    })
  function ComputeDayDiff(ss,dd){
      if(ss == "" || dd == ""){
        $("#date_day_diff_total").html("0"); 
        $("#id_leave_taken").val("0");  
      }else{
        $.ajax({
        type: "POST",
        url: "php/external_server.php",
        data: {compute_day:"x",starting:ss,ending:dd},
        success: function(data){
          //COMPUTATION OF ALL
         $("#date_day_diff_total").html(data); 
         $("#id_leave_taken").val(data);
         //CONVERT DATA TO NUMBER
          data = parseFloat(data);
          //GET BALANCE
          var bal = parseFloat($("#txt_leavebalanceofleavetype").val());
          bal -= data;
          $("#total_balInFuture").html(bal.toFixed(3));

        }
      })
      }
  }

</script>
