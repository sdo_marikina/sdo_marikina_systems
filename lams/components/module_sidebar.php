<div class="notificationpanel" id="x_notification" style="display: none; overflow: auto;">
    <div class="notificationpanel-header">
    	<button class="close" onclick="CloseNotification_x()">&times;</button>
      <img src='images/information.png' class='content_icon'>
       <h5 class="ultratitle">Information Pane</h5>
    </div>
    <div class="notificationpanel-body" id="x_thenotbody" style="color: black;">

    	 <div class="card">
         	<div class="card-body" >
         		<h5 >You are using</h5>
                <img src="images/V2.2.png" style="height: 130px; width: 130px; border-radius: 5px; margin-bottom:20px; margin-top:20px;">
               
         		<h6 class="text-muted">Your are running <strong>CDTRS <?php echo json_decode($version); ?></strong></h6>
         		<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalfeedback"><i class="fas fa-heart"></i> Feedback</a>
         		<a href="images/lumstut.html" class="btn btn-primary btn-sm" target="_blank"><i class="fas fa-arrow-circle-up"></i> Stay Updated</a></span>
                <hr>
                <h5 >What's new?</h5>
                <h6 class="text-muted">Check what has changed in this version.</h6>
                 <button class="btn btn-primary btn-sm" src="images/V2.2.png" title="Click to see what's new!" id="card_version" class="consistent_shadow versionbar" data-toggle="modal" data-target="#whatsnew">Learn more</button>
         	</div>
         </div>
          <div class="card">
         	<div class="card-body">
                 <img src="images/depedsite.png" style="height: 130px; width: 130px; border-radius: 5px; margin-bottom:20px; margin-top:20px;">
         		<h5 >SDO Marikina Systems</h5>
         		<h6 class="text-muted">Check our site for support.</h6>
         		 <a class="btn btn-primary btn-sm" href="https://DepedMarikina.ph" target="_blank">Go to site</a>
         	</div>
         </div>

         <div class="card">
         	<div class="card-body">
         		<h5 >Network Connection</h5>
         		 <p id="connectivitystatus" class="insidecontent" ></p>
         	</div>
         </div>

        <?php include("components/module_intellisync.php"); ?>
    </div>
</div>




<script>
	status();
	setInterval(function(){
		status();
	},1000)
    function status()
    {
        if(navigator.onLine)
        {
            var phpversion = <?php echo json_encode(phpversion()); ?>;


            var verx = phpversion.split('.');
            // alert(verx[0]);
            // alert(verx[0]);
            if(verx[0]  == "7"){
                $("#connectivitystatus").html("<span><i class='far fa-check-circle' style='color:green;'></i> Connected</span>");

            }else{
                $("#connectivitystatus").html("<span><i class='fas fa-exclamation-triangle' style='color:red;'></i> PHP 7.2.4 or higher is required.</span>");
            }      
        }
        else
        {
            $("#connectivitystatus").html("<span><i class='fas fa-exclamation-triangle' style='color:red;'></i> CDTRS is offline.</span>");
        }
    }
</script>




<div class="modal" tabindex="-1" role="dialog" id="modal_report">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">

      <div class="modal-body">
        <h5 class="ultratitle"><i class="fas fa-bullhorn"></i> COMPLAINT REPORT</h5>
        <p>DepEd Marikina is here to listen and take action.</p>
        <div class="form-group">
            <textarea class="form-control" id="feed_id_desc" rows="10" placeholder="Please be specific about your report and give all the details as you can or it will be ignored."></textarea>
        </div>
            <input type="hidden" id="feed_id_emotion" value="32">


<div class="form-group">
       <button class="btn btn-primary" id="btn_feed_send" data-dismiss="modal">Submit Report</button>
        </div>
        
        </div>
        </div>

        
      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modalfeedback">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <h1 class="ultrabold">Feedback</h1>
        <style type="text/css">
            .embar{
            width: 10%;
            display: block;
            float: left;
            margin-right: 22px;
            margin-left: 22px;
            transition: 0.3s all;
            }
            .embar img{
            width: 100%
            }
        </style>
        <div class="form-group">
            <textarea class="form-control" id="feed_id_desc" placeholder="Write your feedback here..."></textarea>

        </div>
        <div class="form-group">
             <center>How do you feel?</center>
            <input type="range" min="1" id="feed_id_emotion" max="5" style="width: 100%;" name="">
        <div>

            <div class="embar" id="em_1">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/face-with-symbols-over-mouth.png">
                </center>
            </div>
            <div class="embar" id="em_2">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/confused-face.png">
                </center>
            </div>
            <div class="embar" id="em_3">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/slightly-smiling-face.png">
                </center>
            </div>
            <div class="embar" id="em_4">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/smiling-face-with-open-mouth.png">
                </center>
            </div>
            <div class="embar" id="em_5">
                <center>
                    <img src="
                    https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/star-struck.png">
                </center>
            </div>
        </div>
        </div>
        <br>
        <br>
        </br>
        <div class="form-group">
            <center><button class="btn btn-primary" id="btn_feed_send" data-dismiss="modal">Send Feedback</button></center>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
    include("components/new_features.php");
 ?>
<script type="text/javascript">
     visualizeemotion();
setInterval(function(){
     visualizeemotion();
 },100)
function visualizeemotion(){
     var feed_status = $("#feed_id_emotion").val();
     $("#em_1").css("opacity","0.3");
     $("#em_2").css("opacity","0.3");
     $("#em_3").css("opacity","0.3");
     $("#em_4").css("opacity","0.3");
     $("#em_5").css("opacity","0.3");
     $("#em_1").css("transform","scale(0.9)");
$("#em_2").css("transform","scale(0.9)");
$("#em_3").css("transform","scale(0.9)");
$("#em_4").css("transform","scale(0.9)");
$("#em_5").css("transform","scale(0.9)");
     switch(feed_status){
        case "1":
            $("#em_1").css("opacity","1");
            $("#em_1").css("transform","scale(1.1)");
        break;
        case "2":
            $("#em_2").css("opacity","1");
            $("#em_2").css("transform","scale(1.1)");
        break;
        case "3":
            $("#em_3").css("opacity","1");
            $("#em_3").css("transform","scale(1.1)");
        break;
        case "4":
            $("#em_4").css("opacity","1");
             $("#em_4").css("transform","scale(1.1)");
        break;
        case "5":
            $("#em_5").css("opacity","1");
            $("#em_5").css("transform","scale(1.1)");
        break;
     }
}
$("#btn_feed_send").click(function(){
        var feed_description = $("#feed_id_desc").val();
    var feed_status = $("#feed_id_emotion").val();
    if(feed_description === ""){
        popnotification("Try again.","Please write your feedback!",false);
        $("#feed_id_desc").val("");
        $("#feed_id_emotion").val("3");
    }else{
        $.ajax({
        type: "POST",
        url: "php/external_server.php",
        data: {send_feed: "x",desc:feed_description,emotion:feed_status},
        success: function(data){
            popnotification("Feedback sent!","Thank you for giving us your toughts.",false);
            $("#feed_id_desc").val("");
            $("#feed_id_emotion").val("3");
        }
    })
    }
})

</script>


<script type="text/javascript">
	var isnotopen = false;

	function PushNotification(the_title,the_message){
		var layout_ui = "<div class='card'>";
		layout_ui += "<div class='card-body'>";

		layout_ui += "<h5 class='card-title'>" + the_title + "</h5>";
		layout_ui += "<h6 class='text-muted card-subtitle'>" + the_message + "</h6>";

		layout_ui += "</div>";
		layout_ui += "</div>";

		$("#x_thenotbody").html(layout_ui + $("#x_thenotbody").html());

	}
	function GetNotifications(){

	}
	function CloseNotification_x(){
		$("#x_notification").css("margin-right","-400px");
		setTimeout(function(){
		$("#x_notification").css("display","none");
		isnotopen = false;
		},1000)
	}
	function OpenNotification(){
		$("#x_notification").css("display","block");
		$("#x_notification").css("margin-right","20px");

		isnotopen = true;
	}

	$(document).keypress(function(e) {
		// alert(e.which);
    if(e.which == 96) {
        //do stuff
        // alert("Hellow");
        if (isnotopen) {
CloseNotification_x();
        }else{
OpenNotification();
        }
    }
});
</script>