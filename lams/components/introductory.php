<style type="text/css">
    .introimage{
        width: 100%; border-radius: 7px;
        border: 2px solid transparent;
    }
    .introimage:hover{
        /*box-shadow: 0px 0px 30px #1e90ff;*/
          border: 2px solid #1e90ff;
    }
</style>
<div class="container-fluid">
  <h5 class="ultratitle mt-3"><i class="fas fa-hands-helping"></i> SERVICES</h5>
     <div class="row">
        <div class="col-md-6 mb-3">
        <a href="https://www.youtube.com/playlist?list=PLmqWQadN7dxKEOYWYvQKVDFsOgp1luJ2n" target="_blank" title="CDTRS TUTORIALS" href="">
        <img src="images/leavetutorials.png"  class="introimage">
        </a>
        </div>
        <div class="col-md-6 mb-3">
        <a href="https://portal.depedmarikina.ph/" target="_blank" title="ONLINE PORTAL" href="">
        <img src="images/cdtrsonlineportal.png"  class="introimage">
        </a>
        </div>
        <div class="col-md-6 mb-3">
        <a href="https://depedmarikina.ph/" target="_blank" title="DEPED WEBSITE" href="">
        <img src="images/depedwebsite.png"  class="introimage">
        </a>
        </div>

          </div>
</div>