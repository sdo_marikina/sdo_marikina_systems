<style type="text/css">
  .powerlook_resbar{
    position: fixed;
    left: 0;
    bottom: 0;
    top:0;
    width: 100vh;
    height: 80%;
    background-color: white;
    border: 1px solid rgba(0,0,0,0.2);
    z-index: 5;
    margin-top: 57px;
    overflow-x: hidden;
    overflow-y: auto;
  }
  .seachboxanimation{
    animation-name: xseachanim;
    animation-duration: 0.2s;
  }
  @keyframes xseachanim{
  0%{
    opacity: 0;
    margin-top: 300px;
    }
  }
  nav a{
    color: #2f3542 !important;
  }
  nav .dropdown-menu a{
    color: black !important;
  }
</style>
<nav class="navbar navbar-light navbar-expand-lg " style="z-index: 100; 

background: rgb(228,228,228);
background: linear-gradient(180deg, rgba(228,228,228,1) 0%, rgba(206,206,206,1) 100%);

   border-bottom: 2px solid #C9C9C9 !important;">
  <a style="margin-top: -5px;" class="navbar-brand ultratitle" href="dashboard.php"><small>SDOMAR - CDTRS</small></a>
  <button class="navbar-toggler" style="color: white;" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <i class="fas fa-bars"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
 <li class="nav-item">
      <!-- <form class="form-inline"> -->
        <input style="width:250px; border-radius: 20px; bottom: -10px; text-align: center; border: 1px solid rgba(0,0,0,0.1) !important;" class="form-control ultratitle" id="powerlooksearchbar" type="text" placeholder="QUICK SIGHT" autocomplete="off">
        <div class="powerlook_resbar consistent_shadow seachboxanimation" id="resxdial" style=" display: none;  color: black; perspective: 1000px;
        ">

        </div>
<script type="text/javascript">
  $("#resxdial").css("display","none");
  $("#powerlooksearchbar").css("background-color","rgba(255,255,255,0.8)");
      $("#powerlooksearchbar").change(function(){


        $("resxdial").html("<div style='padding-top: 100px;'><center><p>Thinking...</p></center></div>");
        if($(this).val() == ""){
$("#resxdial").css("display","none");
 $("#powerlooksearchbar").css("background-color","rgba(255,255,255,0.8)");
        }else{
          $("#resxdial").css("display","block");
           $("#powerlooksearchbar").css("background-color","rgba(255,255,255,1)");
        }
        setTimeout(function(){
var searchtext = $("#powerlooksearchbar").val();
       $.ajax({
        type: "POST",
        url: "php/external_server.php",
        data: {powerlooksearch:"x",searchtag:searchtext},
        success: function(data){
          // alert(data);
           $("#resxdial").html(data);
        }
       })
        },1000)
      })
        </script>
      <!-- </form> -->
    </li>
         <li class="nav-item dropdown">
        <a href="#"  data-toggle="dropdown" class="nav-link dropdown-toggle ultratitle"><i class="fas fa-dharmachakra"></i> SYSTEM</a>
        <div class="dropdown-menu">
          <a href="#" data-toggle="modal" data-target="#LamsConfirguration" class="nav-link dropdown-item"><i class="fas fa-cog"></i> Settings</a>
 <a  href="backup.php" class="nav-link dropdown-item"><i class="fas fa-toolbox"></i> Maintenance Center</a>
<a  href="logs.php" class="nav-link dropdown-item"><i class="fas fa-shield-alt"></i> Logs</a>
        </div>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
       <a class="btn btn-primary popdial ultratitle" href="https://portal.depedmarikina.ph" target="_blank" name="logout" data-toggle="popover" data-trigger="hover" title="CDTRS Online Portal" data-content="<img src='images/onlineportal.png' style='width:100%;'>View employees attendance logs wherever they are. Make sure that they have a Shared access badge so they can login their Deped Email as email/username and employee number as default password." data-html="true"><i class="fas fa-globe-americas"></i></a>
    </div>
    <div class="form-inline my-2 my-lg-0">

      <a class="btn btn-primary popdial ultratitle" data-toggle="modal" data-target="#modal_report"

 data-toggle="popover" data-trigger="hover" 
 title="Report what is not right in your workplace."
  data-content="<img src='images/report.png' style='width:100%;'>Report to the Schools Division Office - Marikina City directly so we can take action." 
  data-html="true"
      ><i class="fas fa-bullhorn"></i></a>

       <a class="btn btn-primary popdial ultratitle" href="index.php" target="_blank" name="logout" data-toggle="popover" data-trigger="hover" title="Open Homescreen" data-content="<img src='images/homescreen.png' style='width:100%;'>Opens the CDTRS Homescreen to another tab." data-html="true"><i class="fas fa-home"></i></a>
    </div>


       <div class="dropdown" >
        <a class="btn btn-primary ultratitle dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user-circle"></i> <?php echo strtoupper($_SESSION["username"]); ?>
        </a>
      <form method="POST" class="form-inline my-2 my-lg-0">
        <div class="dropdown-menu"  style="margin-left: -45px !important;" aria-labelledby="dropdownMenuLink">
            
 <button title="Sign-out" class="dropdown-item" type="submit" name="logout"><i class="fas fa-sign-out-alt"></i> Sign-out</button>

        </div>
      </div>
        </form>


  </div>
</nav>


<?php

include("components/loginmodals.php");
?>

<script type="text/javascript">
  setInterval(function(){
$('.popdial').popover(); 
  },1200)



</script>