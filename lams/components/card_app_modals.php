<form action="php/app_external_server.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_dispute">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="ultrabold">Apply for Dispute</h4>

        <input type="hidden" required="" value="" id="dispute_id_container" name="eid">
        <input type="hidden" id="disputedate" name="date_of_dispute">

        <div class="form-group">
          <label>Dispute Type</label><br>
          <label><input type="radio" name="disputetype" value="0" checked="true"> For Missing Timelog</label><br>
          <label><input type="radio" name="disputetype" value="1"> For Undertine</label>
        </div>
        
      <div class="form-group">
        <label>Reason for dispute</label>
        <textarea class="form-control" required="" type="text" name="reason"></textarea>
      </div>

      <div class="form-group">
        <label><input type="checkbox" required="" name="is_oath" value="1"> Corresponding attachment has been submitted</label>
      </div>
      
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="btn_applydispute">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>

<form action="form6_app.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_applyleave">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="loading_indicator" id="lod_aaplx" style="display: block;"></div>
      <div class="modal-body">
        <h5 class="ultratitle mb-3">Apply Leave</h5>
<input type="hidden" id="applyleave_id_container" name="eid">
<input type="hidden" name="date_applied_totatus" id="topic_date">
<!-- SELECT LEAVE TYPE -->
<div class="form-group">
  <select type="text" required="" class="form-control" id="select_availableleavetype" placeholder="Username">
  </select>
  <input type="hidden" id="trueval_type" name="leavetype" >
</div>

<div id="applyLeave_form2">
<div class="form-group">
    <table class="table table-bordered">
      <tr>
        <th>Balance</th>
        <td> <input type="text" readonly=""  value="0.00"  id="txt_leavebalanceofleavetype" class="form-control labelinput" name="leavebalance"></td>
      </tr>
      <tr>
        <th>Leave Days</th>
        <td style="text-align:center;"><span id="date_day_diff_total">Days of leave</span></td>
      </tr>
      <tr>
        <th>Total</th>
        <td><input type="text" readonly="" id="total_balInFuture" name="butal" class="labelinput form-control"></td>
      </tr>
    </table>
  <input type="hidden" id="id_leave_taken" name="leave_taken_value">
</div>
  <p class="ptitle">Leave Duration</p>
<div class="row">
  <div class="col-sm-6">
    
    <div class="form-group">
  <label>
    From
  </label>
  <input required="" type="date" class="form-control" id="date_ff" name="date_from">
</div>

  </div>
  <div class="col-sm-6">
    <div class="form-group">
  <label>
    To
  </label>
  <input required="" type="date" class="form-control" id="date_tt" name="date_to">
</div>

  </div>

</div>

<div class="form-group" id="reasoningselection">
  <p class="ptitle">Leave Info</p>
  <label><input type="radio" name="leavetype_status" value="" id="id_checkbox_1"> <span id="choice_1"></span><br></label>
  <input id="location_1" name="location_1" placeholder="Type here..." type="text" class="form-control">
  <br>
  <label><input type="radio" name="leavetype_status" value="" id="id_checkbox_2"> <span id="choice_2"></span><br></label>
  <input id="location_2" name="location_2" placeholder="Type here..." type="text" class="form-control">
</div>

<div id="vacation_additional">
      <p class="ptitle">Where the leave will be spent?</p>
<div class="row">
  <div class="col-sm-6">
        <div class="form-group">
      <label><input type="radio" value="0" name="vacationleave_location"> Within the Philippines</label>
    </div>
  </div>
  <div class="col-sm-6">
        <div class="form-group">
       <label><input type="radio" value="1" name="vacationleave_location"> Abroad (Specify)</label>
        <textarea id="toclear_txt" name="f_vac_abroad" class="form-control">
          
        </textarea>
        <script type="text/javascript">
          $("#toclear_txt").val("");
        </script>
    </div>
  </div>
</div>
</div>

</div>

      </div>
      <div class="modal-footer" id="submitcontainer">
        <button type="submit" class="btn" name="btn_apply_leave" id="leavesubbtn">Submit</button>
        <button type="button" class="btn" data-dismiss="modal"><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</form>


<script type="text/javascript">

    var strictmode = "0";
  $("#vacation_additional").css("display","none");
  function func_dispute(element){
    $("#dispute_id_container").val($(element).data("empnum"));
    $("#disputedate").val($(element).data("dateofrecord"));
  }
  function func_applyleave(element){
      strictmode = $(element).data("strictmode");
      // alert(strictmode);

$("#lod_aaplx").css("display","block");
  $("#date_ff").val($(element).data("topicdate"));
   $("#date_tt").val($(element).data("topicdate"));
      $("#submitcontainer").css("display","none");
     $("#applyLeave_form2").css("display","none");
    $("#applyleave_id_container").val($(element).data("empnum"));
    $("#topic_date").val($(element).data("topicdate"));

    var empnumber =  $(element).data("empnum");
    //GET AVAILABLE LEAVE TYPE
    $.ajax({
      type: "POST",
      url: "php/app_external_server.php",
      data: {getavailablenumber:"x", employeenumber:empnumber},
      success: function(data){
        setInterval(function(){
          $("#lod_aaplx").css("display","none");
        },1000)
          $("#select_availableleavetype").html(data);
          $("#select_availableleavetype").append("<option selected='selected' disabled >Choose leave type...</option>");
      }
    })
  }

  $("#id_checkbox_1").click(function(){
      $("#location_1").attr("disabled",false);
      $("#location_2").attr("disabled",true);
      $("#location_1").val("");
      $("#location_2").val("");
        $("#submitcontainer").css("display","inline-block");
  })
  $("#id_checkbox_2").click(function(){
      $("#location_1").attr("disabled",true);
      $("#location_2").attr("disabled",false);
            $("#location_1").val("");
      $("#location_2").val("");
        $("#submitcontainer").css("display","inline-block");
  })

  $("#select_availableleavetype").change(function(){
 ComputeDayDiff($("#date_ff").val(),$("#date_tt").val());
    if($("#select_availableleavetype").val() == "5"){
        $("#vacation_additional").css("display","block");
    }else{
       $("#vacation_additional").css("display","none");
    }

// date_ff
// date_ff
var reusable_value = $("#select_availableleavetype option:selected").text();
if(reusable_value == "Forced Leave" || reusable_value == "CTO" || reusable_value == "Vacation Leave"){
 $("#date_ff").attr("readonly",false);
 // alert("force");
}else{
  if(strictmode == "1"){
     $("#date_ff").attr("readonly",true);
  }else{
     $("#date_ff").attr("readonly",false);
  }

 // alert("not force");
}
// if($())
    $("#applyLeave_form2").css("display","block");
       $("#location_1").attr("required",true);
      $("#location_2").attr("required",true);

      $("#location_1").attr("disabled",true);
      $("#location_2").attr("disabled",true);
      $("#submitcontainer").css("display","none");
    //DISPLAY CURRENT LEAVE TYPE VALUE
    $("#txt_leavebalanceofleavetype").val($("#select_availableleavetype").val());
    var currentval = $("#select_availableleavetype option:selected").text();
    $("#trueval_type").val(currentval);
  $("#reasoningselection").css("display","none");
    if(currentval == "Sick Leave" || currentval == "Service Credit"){
      document.getElementById("id_checkbox_1").checked = false;
      document.getElementById("id_checkbox_2").checked = false;
      $("#choice_1").html("In Hospital<br><small>Type the name of the hospital</small>");
      $("#choice_2").html("Out Patient<br><small>Type where</small>");
      $("#id_checkbox_1").val("In Hospital");
      $("#id_checkbox_2").val("Out Patient");
        $("#reasoningselection").css("display","block");
          $("#vacation_additional").css("display","none");
    }else if(currentval == "Vacation Leave"){
       $("#vacation_additional").css("display","block");
       document.getElementById("id_checkbox_1").checked = false;
      document.getElementById("id_checkbox_2").checked = false;
      $("#choice_1").html("To Seek Employment");
      $("#choice_2").html("Others (Specify)<br><small>Type reason below</small>");
      $("#id_checkbox_1").val("To Seek Employment");
      $("#id_checkbox_2").val("Others (Specify)");
        $("#reasoningselection").css("display","block");
    }else{

       $("#vacation_additional").css("display","none");
      $("#reasoningselection").css("display","none");
      $("#location_1").attr("required",false);
      $("#location_2").attr("required",false);
        $("#submitcontainer").css("display","inline-block");
    }
  })

  setInterval(function(){

    var reusable_value = $("#select_availableleavetype option:selected").text();


    if($("#date_day_diff_total").html() == "0"){
        $("#leavesubbtn").prop("disabled", true);
         $("#leavesubbtn").html("<span><strong>Days of Leave</strong> must be higher than zero</span>");
    }else{


    if($("#date_ff").val() <= $("#date_tt").val()){
    $("#leavesubbtn").prop("disabled", false);
    $("#leavesubbtn").html("Submit");
    }else{
    $("#leavesubbtn").prop("disabled", true);
    $("#leavesubbtn").html("<span><strong>FROM and TO date is invalid.</span>");
    }

    }
    if( $("#select_availableleavetype option:selected").text() == "Vacation Leave"){
        $("#location_1").val("");
        $("#location_1").attr("required",false);
        $("#location_1").css("display","none");
    }else{
      $("#location_1").css("display","block");
    }
    if(reusable_value == "Forced Leave"){
      // alert("true")
      // console.log($("#txt_leavebalanceofleavetype").val());
      if (parseInt($("#date_day_diff_total").html()) > parseInt($("#txt_leavebalanceofleavetype").val())){
         $("#leavesubbtn").prop("disabled", true);
         $("#leavesubbtn").html("<span>Forced Leave balance not enough.</span>");
      }else{
         $("#leavesubbtn").prop("disabled", false);
         $("#leavesubbtn").html("Submit");
      }
    }
  },100)
    $("#date_ff").change(function(){
      $("#lod_aaplx").css("display","block");
    ComputeDayDiff($("#date_ff").val(),$("#date_tt").val());
    })

    $("#date_tt").change(function(){
      $("#lod_aaplx").css("display","block");
    ComputeDayDiff($("#date_ff").val(),$("#date_tt").val());
    })
  function ComputeDayDiff(ss,dd){
      if(ss == "" || dd == ""){
        $("#date_day_diff_total").html("0"); 
        $("#id_leave_taken").val("0");  
      }else{
        $.ajax({
        type: "POST",
        url: "php/app_external_server.php",
        data: {compute_day:"x",starting:ss,ending:dd},
        success: function(data){
          setInterval(function(){
            $("#lod_aaplx").css("display","none");
          },1000)
          //COMPUTATION OF ALL
         $("#date_day_diff_total").html(data); 
         $("#id_leave_taken").val(data);
         //CONVERT DATA TO NUMBER
          data = parseFloat(data);
          //GET BALANCE
          var bal = parseFloat($("#txt_leavebalanceofleavetype").val());
          bal -= data;
          $("#total_balInFuture").val(bal.toFixed(3));

        }
      })
      }
  }

</script>
