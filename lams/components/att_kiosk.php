<div class="col-sm-7" style=" overflow: hidden;">
<div class="row">
  <div style="display: none;" class="col-sm-6 poptop_anim">
    
  <h4 class="ultrathin " style="color: rgba(255,255,255,0.5);"><i class="far fa-calendar-alt"></i> Attendance Kiosk</h4>
     <div class="form-group">
       <!-- <div class="container"> -->
        
        
        <div >
         

        <div class="card poptop_anim" id="loginkeys">
        <div class="card-body">
          
          <!-- <div class="container" > -->
            <!-- <div class="container"> -->
            <div class="form-group ">
              <input type="text" class="form-control form-control-lg" placeholder="Please type your Employee ID..." name="" id="employee_num">
            </div>
            <div class="row">
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">1</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">2</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">3</button>
              <br>
            </div>

            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">4</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">5</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">6</button>
              <br>
            </div>

            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">7</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">8</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">9</button>
              <br>
            </div>

            <div class="col-sm-4">
              <button class="btn btn-danger btn-block" id="clearempid"><i class="fas fa-times"></i></button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-secondary btn-block num_key">0</button>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-primary btn-block"  id="btn_logg"><i class="far fa-arrow-alt-circle-right"></i></button>
              <br>
            </div>
          </div>
          <!-- </div> -->
        <!-- </div> -->
        </div>
      </div>
        </div>
     </div>

      <!-- </div> -->



  </div>
  <div class="col-sm-12">
    <?php include("components/home_access.php"); ?>
        <div class="form-group poptop_anim" >
      <h4 class="ultrathin" style="color: rgba(255,255,255,0.5);"><i class="fas fa-bars"></i> Recent Logs</h4>
      <div style="overflow: auto; max-height: 300px;">
        <div class="container" id="kiosk_logs_container">
          <div class="padder">
            <h1 class="ultrathin" style="color: white;">Please wait...</h1>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>
	</div>

<div class="modal" tabindex="-1" role="dialog" id="facerecogmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="borderface" style="border: 2px solid #f1c40f;">
      <div class="modal-body" style="text-align: center;">

        <h4 id="empnametopic"></h4>
        <div style="margin-left: -780px; margin-top: -100px;" >
        <video id="video" width="320"  style="border-radius: 20px; border: 2px solid white;" height="240" preload autoplay loop muted ></video>
      <canvas id="canvas" width="320" height="240" ></canvas>
      </div>

<div style="margin-top: 400px;">
  <progress id="proggieface" value="22" max="100"></progress><br>
  <p>Face Verification</p>
  <center><h2 id="kioskmessage">Idle....</h2></center>
</div>
<!-- SECONDARY CAM -->
<div style="display: none;" id="my_camera"></div>
<div id="results">
</div>
  <input type="hidden" id="tome" name="moki">

<!-- SECONDARY CAM -->

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

    <script>

var IsNoCameraWorthy = false;
var verifyface = false;
var face_time = 0;


CheckKioskSettings();


function CheckKioskSettings(){
  $.ajax({
    type : "POST",
    url: "php/external_server.php",
    data: {CheckIfNeedCamera:"x"},
    success: function(data){
      // alert(data);
      IsNoCameraWorthy = true;
      SetupKiosk();
    }
  })
}


function SetupKiosk(){
  if(IsNoCameraWorthy == false){
    $("#searchnametext").prop('disabled', true);
    $("#searchnametext").prop('placeholder', "Loading...");
  }
}


$("#btn_logg").click(function(){
performlog();
})


function performlog(){
    if($("#employee_num").val() != ""){
  if(IsNoCameraWorthy == false){



  $("#facerecogmodal").modal("show");
  verifyface = true;
// $("#proggieface").max('40');
document.getElementById("proggieface").max = "40";
$("#proggieface").val("0");
$("#kioskmessage").html("Please let me see your face....");
$("#loginkeys").css("display","none");
      var video = document.getElementById('video');
      var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');
      var tracker = new tracking.ObjectTracker('face');
      tracker.setInitialScale(4);
      tracker.setStepSize(2);
      tracker.setEdgesDensity(0.1);
      tracking.track('#video', tracker, { camera: true });
      tracker.on('track', function(event) {
        context.clearRect(0, 0, canvas.width, canvas.height);


        event.data.forEach(function(rect) {

          if(verifyface){
            $("#borderface").css("border","10px solid #3498db");
            $("#kioskmessage").html("I saw it. Keep it still...");
            face_time += 1;
            $("#proggieface").val(face_time);
          console.log(face_time);
          if(face_time > 40){
            take_snapshot();
            $("#borderface").css("border","5px solid #2ecc71");
            $("#kioskmessage").html("<img src='https://i.gifer.com/7efs.gif' style='width:160px;'><br>Log Verified!");
            $("#loginkeys").css("display","block");
            Submit_Attendance_Kiosk($("#employee_num").val());
            $("#employee_num").val("");
            verifyface = false;
            face_time = 0;

            $("#proggieface").val(face_time);
            setTimeout(function(){
              $("#borderface").css("border","2px solid #f1c40f");
        $("#kioskmessage").html("Idle...");
        $("#facerecogmodal").modal("hide");
          GetKioskLogs();
            },2000)
            $("#searchnametext").focus();
          }
          }


      
          context.strokeStyle = 'white';
          context.strokeRect(rect.x, rect.y, rect.width, rect.height);
          context.font = '11px sanfranc';
          context.fillStyle = "#fff";
          context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
          context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
        });



      });


      var gui = new dat.GUI();
      gui.add(tracker, 'edgesDensity', 0.1, 0.5).step(0.01);
      gui.add(tracker, 'initialScale', 1.0, 10.0).step(0.1);
      gui.add(tracker, 'stepSize', 1, 5).step(0.1);
    }else{
      Submit_Att_NoCam($("#employee_num").val());
      $("#employee_num").val("");
      GetKioskLogs();
    }
  }else{
      popnotification("Type your Employee ID!","Attendance Kiosk");
    }
}
      $(".num_key").click(function(){
        // alert($(this).html());
        $("#employee_num").val($("#employee_num").val() + $(this).html());
      });

      $("#clearempid").click(function(){
        $("#employee_num").val("");
      });



    GetKioskLogs();
    function GetKioskLogs(){
      setTimeout(function(){
          $.ajax({
          type: "POST",
          url:"php/external_server.php",
          data: {get_kiosk_logs:"x"},
          success: function(data){
          $("#kiosk_logs_container").html(data);
          }
          })
      },1000)
    }


$("#kiosk_loading").css("display","block");
$("#kiosk_ui").css("display","none");


var ftime = '0';
setInterval(function(){
checkkiosk();
},5000)

function checkkiosk(){

  if(verifyface == false){
          navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

navigator.getMedia({video: true}, function() {
  //AVALIABLE
  $("#loginkeys").css("display","block");
  $("#kiosk_alert").css("display","none");
 SetWebcamNow();
$("#kiosk_loading").css("display","none");
$("#kiosk_ui").css("display","block");


$("#searchnametext").prop('disabled', false);
$("#searchnametext").prop('placeholder', "Type ID Number to Log");
if(ftime == '0'){
  $("#searchnametext").focus();
  ftime = '1';
}


}, function() {
  // NOT AVAILABLE
  $("#loginkeys").css("display","none");
  $("#kiosk_alert").css("display","block");

$("#kiosk_loading").css("display","none");
$("#kiosk_ui").css("display","block");
});
  }


}
  function SetWebcamNow(){
    Webcam.set({
    width: 320,
    height: 240,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );

  }


  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      document.getElementById("tome").value = data_uri;
    } );
  }

function Submit_Attendance_Kiosk(emp_id){
  var picturefile = $("#tome").val();
   $.ajax({
    type: "POST",
    url: "php/external_server.php",
    data: {attlogto_kiosk:"x", picture_picdata: picturefile, eid:emp_id},
    success: function(data){
      popnotification(data, "Attendance Kiosk",false);

    }
  })
}
function Submit_Att_NoCam(emp_id){
     $.ajax({
    type: "POST",
    url: "php/external_server.php",
    data: {attlogto_kiosk_no_cam:"x", eid:emp_id},
    success: function(data){
      
      // alert(data["fname"]);

      if (data!= "Maximum Attendance log exceeded!") {
        data = JSON.parse(data);
        var fullname = data["lname"] + ", " + data["fname"];
      OpenFullScreenProfile("images/logpics/" + data["image"] + ".jpg",fullname,"Login Accepted!" );
    }else{
       popnotification(data, "Attendance Kiosk",false);
    }
      
     

    }
  })
}


  </script>


  <div class="col-sm-12">
    <br>
        <?php include("components/dash_summary.php"); ?>
  </div>






