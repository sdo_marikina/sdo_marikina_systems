<form action="php/server.php" method="POST">
  <div class="modal" role="dialog" id="createnewaccount">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h6 class="ultrabold mb-3">Setup CDTRS</h6>

       
    
        <div class="card">
          <div class="card-body">
              <h5 class="card-title"><i class="fas fa-arrow-right"></i> Station Information</h5>
       <h6 class=" card-subtitle text-muted mb-3">Details from your current station.</h6>
            <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
          <label>Station Name</label>
          <input type="text" class="form-control" required="" placeholder="Name of Company..." name="sc">
        </div>
        
          </div>
           <div class="col-sm-6">
            <div class="form-group">
          <label>Principal Name</label>
          <input type="text" class="form-control" required="" placeholder="Name of Principal..." name="pc">
        </div>
          </div>
          <div class="col-sm-12">
              <div class="form-group">
              <label>Is this a school account?</label><br>
            <input type="radio" checked="" value="1" name="isschool_x"> Yes <br>
            <input type="radio" value="0" name="isschool_x"> No <br>
            </div>
          </div>
        </div>
          </div>
        </div>

       <div class="card">
         <div class="card-body">
           <h5 class="card-title"><i class="fas fa-arrow-right"></i> CDTRS Account</h5>
       <h6 class=" card-subtitle text-muted mb-3">Account that will manage the Centralized Daily Time Record System.</h6>
        <div class="form-group">
          <label>Create a new Username/Email</label>
          <input  class="form-control" type="" required="" placeholder="Enter your Email/Username" name="username">
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>New Password</label>
              <input type="password" class="form-control" placeholder="Enter new password" required="" name="password">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>Re-enter new password</label>
              <input type="password" class="form-control" placeholder="Re-enter new password" required="" name="repassword">
            </div>
          </div>
        </div>
         </div>
       </div>

          
      </div>
      <div class="modal-footer">
        <button  class="btn btn-primary" type="submit" name="addnewlamsaccount"><span style="width: 200px;"><i class="fas fa-arrow-circle-right"></i> Setup CDTRS</span></button>
        <a href="recovery.php" class="btn btn-secondary">Recovery Mode</a>
      </div>
    </div>
  </div>
</div>
</form>
<!-- VERIFY IF ACCOUNT IS EXISTING -->
<script type="text/javascript">
  
  $.ajax({
    type: "POST",
    url: "php/check_account.php",
    success: function(data){
      if(data == "0"){
        $("#createnewaccount").modal("show");
      }
    }
  })
</script>

<form method="POST">
  <div class="modal" role="dialog" id="loginlams">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
                    <center>
                      <h3 class="ultratitle"><i class="far fa-user-circle"></i> CDTRS | <span style="color: rgba(0,0,0,0.5);">SIGN-IN</span></h3>
                    <p>Administrator</p>
                    </center>
              <br>
              <br>
              <br>
            <div class="container">
                <div class="row">
                <div class="col-sm-12">
            <div class="form-group">
              <label><i class="far fa-id-card"></i> Username/ Email</label>
            <input required="" autocomplete="off" type="text" placeholder="Type your Username/Email here..." class="form-control" name="login_username">
            </div>
                </div>
                <div class="col-sm-12">
            <div class="form-group">
              <label><i class="fas fa-shield-alt"></i> Password</label>
            <input required="" autocomplete="off" type="password" placeholder="Type your Password here..." class="form-control" name="login_password">
            </div>
                </div>
                <div class="col-sm-12">
                  <button style="margin-top: 5px;" name="btn_login" class="btn btn-primary float-right" type="submit">Sign-in <i class="fas fa-arrow-right"></i></button>
                </div>
              </div>
            </div>
<br>
<br>
<br>
<br>
      </div>

    </div>
  </div>
</div>
</form>


<form method="POST">
  <div class="modal" role="dialog" id="dtr_modal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="../../marikina" title="Toggle full screen" target="_blank" class="btn float-left"><i class="fas fa-external-link-alt"></i></a>
        <p class="modal-title ptitle" style="margin-left: -100px;">SDO MARIKINA - DTR</p>
        <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="dtrux" style="height: 900px; padding: 0px; overflow: hidden;">
      </div>
    </div>
  </div>
</div>
</form>

  <div class="modal" role="dialog" id="howtut">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="../tutorial" target="_blank" title="Toggle full screen" class=" btn float-left round single"><i class="fas fa-external-link-alt"></i></a>
         <a href="#" title="Refresh" class=" btn float-left round single" onclick="load_tutorial()" style="margin-left: 10px;"><i class="fas fa-sync"></i></a>
        <p class="modal-title ptitle" style="margin-left: -100px;">How to?</p>
        <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="howtomdl" style="height: 900px; padding: 0px; overflow: hidden;">
      </div>
    </div>
  </div>
</div>


<div class="modal" role="dialog" id="attloggies">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <p class="modal-title">Employee's Dashboard</p>
        <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
// $(".modal").draggable({
// handle: ".modal-header"
// });
</script>
