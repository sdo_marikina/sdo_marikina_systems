<?php
include("../connection/connect.php");
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="LAMS_BACKUP_AttendanceLogs - ' . date("Y-m-d") . '.csv"');
$data = array("id,eid,access_type,access_image,date,timestamp,ismanual");
$q = "SELECT * FROM attendance_logs";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){
	$toadd = $row["id"] . ',' . $row["eid"] . ','. $row["access_type"] . ','. $row["access_image"] . ','. $row["date"] . ','. $row["timestamp"] . ','. $row["ismanual"];
	array_push($data, $toadd);
}

$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);
?>
