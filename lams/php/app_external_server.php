<?php
include("../connection/connect.php");
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(isset($_POST["GetPositionNameByID"])){

$positionID = $_POST["positionID"];
$q = "SELECT * FROM positions WHERE id='" . $positionID . "'";
$res = mysqli_query($c,$q);
if($res){
	$row = mysqli_fetch_array($res);
	echo $row["name"];
}
}

if(isset($_POST["GetAttendanceOfEmployee"])){
	$d_from = date("Y-m-d");
	$d_to = date('Y-m-d', strtotime('-7 days'));
	if (isset($_POST["d_from"])) {
		$d_from = $_POST["d_from"];
	}
	if (isset($_POST["d_to"])) {
		$d_to = $_POST["d_to"];
	}
	$emp_code = mysqli_real_escape_string($c,$_POST["emp_code"]);
	// SELECTE STARTING DATE
	$q = "SELECT * FROM attendance_logs WHERE eid='$emp_code' LIMIT 1";
	$result = mysqli_query($c,$q);
	$row = mysqli_fetch_array($result);
	if(mysqli_num_rows($result) != 0){
		DisplayWithDignity($emp_code, $row["date"],$d_from,$d_to);
		$_SESSION["empkey"] = $emp_code;
	}else{
		echo "false";
	}
}elseif(isset($_POST["btn_applydispute"])){
  $eid = mysqli_real_escape_string($c,$_POST["eid"]);
  $reason = mysqli_real_escape_string($c,$_POST["reason"]);
  $is_oath = mysqli_real_escape_string($c,$_POST["is_oath"]);
  if($is_oath == ""){
  	$is_oath = "0";
  }
  $date_of_dispute = $_POST["date_of_dispute"];
$q = "INSERT INTO dispute(
eid,
reason,
is_oath,
date_of_dispute,
date_of_record,
status,
documentrecieved,
dispute_type
)VALUES(
'$eid',
'$reason',
'$is_oath',
'$date_of_dispute',
'" . date("Y-m-d") . "',
'0',
'0',
'"  . $_POST["disputetype"] ."'
)";
$res = mysqli_query($c,$q) ;

BackToPage($res,"../card_app.php");

}elseif(isset($_POST["btn_apply_leave"])){
$eid = $_POST["eid"];
$leavetype = $_POST["leavetype"];
$date_from = $_POST["date_from"];
$date_to   = $_POST["date_to"];
$location_1 = $_POST["location_1"];
$location_2 = $_POST["location_2"];
$subtype = $_POST["leavetype_status"];
$leave_take_value = $_POST["leave_taken_value"];
$date_applied_totatus = $_POST["date_applied_totatus"];
$location = $location_1;
if($location_1 == ""){
$location = $location_2;
}
$q = "INSERT INTO applied_leave(
employee_id,
leave_type,
date_from,
date_to,
sub_type,
location,
leave_taken,
date_requested,
time_requested,
date_applied_totatus
)
VALUES(
'$eid',
'$leavetype',
'$date_from',
'$date_to',
'$subtype',
'$location',
'$leave_take_value',
'" . date("Y-m-d") . "',
NOW(),
'$date_applied_totatus')";
$res = mysqli_query($c,$q) ;
BackToPage($res,"../card_app.php");

}elseif(isset($_POST["getavailablenumber"])){
	$employeenumber = $_POST["employeenumber"];
	$q = "SELECT * FROM employees WHERE eid='" . $employeenumber . "'";
	$res = mysqli_query($c,$q);
	
	$frow = mysqli_fetch_array($res);
	$q = "SELECT * FROM entitlements WHERE emp_id='$employeenumber'";
	$res = mysqli_query($c,$q) ;
	while($row = mysqli_fetch_array($res)){
		if($row["service_credit"] != 0){
			echo "<option value='" . $row["service_credit"] . "'>Service Credit</option>";
		}
		if($row["sick_leave"] != 0){
			echo "<option value='" . $row["sick_leave"] . "'>Sick Leave</option>";
		}if($row["vacation_leave"] != 0){
			echo "<option value='" . $row["vacation_leave"] . "'>Vacation Leave</option>";
		}
		if($row["solo_parent_leave"] != 0){
			echo "<option value='" . $row["solo_parent_leave"] . "'>Solo Parent Leave</option>";
		}
			echo "<option value='" . $row["forced_leave"] . "'>Forced Leave</option>";
		if($row["special_leave"] != 0){
			echo "<option value='" . $row["special_leave"] . "'>Special Leave</option>";
		}
		if($frow["type"] != "2"){
			$ent_q = "SELECT * FROM entitlements_cto WHERE empid='" . $employeenumber . "' ORDER BY val_to ASC LIMIT 1";
			$ent_res = mysqli_query($c,$ent_q);
			if(mysqli_num_rows($ent_res) != 0){
				$ent_row =  mysqli_fetch_array(mysqli_query($c,$ent_q));
				echo "<option value='" . $ent_row["entitlement"] . "'>CTO</option>";
			}
		}
	}
	echo "<option value='0'>Leave Without Pay</option>";
}elseif(isset($_POST["GetEmployeeInfoByKey"])){
	$EmployeeKey = $_POST["EmployeeKey"];
	$q = "SELECT * FROM employees WHERE eid='$EmployeeKey' LIMIT 1";
	$res = mysqli_query($c,$q);
	echo json_encode(mysqli_fetch_array($res));
}elseif(isset($_POST["compute_day"])){
$starting = $_POST["starting"];
$ending = $_POST["ending"];
//SEND RESULT
echo ComputeDayDifference($starting,$ending);
}
function DisplayWithDignity($emp_code, $startdate,$sst,$eed){
	global $c;
	// Start date is declared
	$date = $sst;
	// End date is always the current date
	$end_date = $eed;
	$cc = 0;

	while (strtotime($date) >= strtotime($end_date)) {
	$cc += 1;

	$xdate=date_create($date);

	$Current_Holiday = "";
	$currdate = date("m-d", strtotime($date));

	//SELECT IF THIS DAY IS A HOLIDAY
	$q = "SELECT * FROM holidays WHERE date='" . $currdate ."' LIMIT 1";
	$hol_res = mysqli_query($c,$q);

$ispartofleave = 0;
	$days_with_leave = array();
	//SELECT IF THIS DAY IS ON LEAVE
	$leaveday = "SELECT * FROM applied_leave WHERE employee_id='" . $emp_code . "' AND status != '2'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){

		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];

		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if(!in_array($dd_from, $days_with_leave)){
				array_push($days_with_leave,$dd_from);
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
		
	}

	$rescount = mysqli_num_rows($hol_res);
	if($rescount != 0){
		$Current_Holiday .= '<div class="alert alert-primary" role="alert"><span>H - </span>';
		while($row = mysqli_fetch_array($hol_res)){
		$Current_Holiday .= $row["name"] . " ";
	}
	$Current_Holiday .= "</div>";
	}
	
		?>

		<tr>
			<!-- <td> -->
				<?php 
				// echo $cc;
				 ?>
			<!-- </td> -->

			<?php

			$df = date_format($xdate,"M d, Y l");
			$alternative_date = date("Y-m-d",strtotime($df));
				if(date_format($xdate,"D") == "Sat" || date_format($xdate,"D") == "Sun" ){
						$df = "<strong>" . $df  . "</strong>";
				}


				if(in_array($alternative_date, $days_with_leave)){
						$ispartofleave = 1;
					}else{
						$ispartofleave = 0;
					}
			?>
				<td ><?php echo $df; ?></td>
			<!-- OLD l, d M Y -->
			<?php if($rescount != 0){
				?>
			
				<td colspan="6" style="border-right: 0px solid white;">
					<span><?php echo $Current_Holiday; ?></span>
				</td>
				<td style="display: none; padding: 0px; border: 0px solid white; opacity: 0;"></td>
				<td style="display: none; padding: 0px; border: 0px solid white; opacity: 0;"></td>
				<td style="display: none; padding: 0px; border: 0px solid white; opacity: 0;"></td>
				<td style="display: none; padding: 0px; border: 0px solid white; opacity: 0;"></td>
				<td style="display: none; padding: 0px; border: 0px solid white; opacity: 0;"></td>
				<?php
			}else{
				?>
				
				<?php
				echo GetLogsForToday($emp_code , $date,$rescount,$ispartofleave);
			}
			?>
		</tr>
		<?php
		
   
		$date = date ("Y-m-d", strtotime("-1 day", strtotime($date)));
	}
}

function ComputeDayDifference($day_start, $day_end){
global $c;

$emp_code = $_SESSION["empkey"];
$days_with_leave = array();

if(isset($_SESSION["empkey"])){
	//SELECT IF THIS DAY IS ON LEAVE
	$leaveday = "SELECT * FROM applied_leave WHERE employee_id='" . $emp_code . "' AND status != '2'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){

		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];

		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if(!in_array($dd_from, $days_with_leave)){
				array_push($days_with_leave,$dd_from);
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
	}
}

$start = new DateTime($day_start);
$end = new DateTime($day_end);
// otherwise the  end date is excluded (bug?)
$end->modify('+1 day');

$interval = $end->diff($start);

// total days
$days = $interval->days;

// create an iterateable period of date (P1D equates to 1 day)
$period = new DatePeriod($start, new DateInterval('P1D'), $end);

// best stored as array, so you can add more than one
$holidays = array();

$q = "SELECT * FROM holidays";
$res = mysqli_query($c,$q);
while ($row = mysqli_fetch_array($res)) {
	$dd = explode("-", $row["date"]);
	$fullHolidayDate =  date('Y-m-d', mktime(0, 0, 0, $dd[0], $dd[1]));
	array_push($holidays, $fullHolidayDate);
}

foreach($period as $dt) {
    $curr = $dt->format('D');

    // substract if Saturday or Sunday
    if ($curr == 'Sat' || $curr == 'Sun') {
        $days--;
    }

    // (optional) for the updated question
    elseif (in_array($dt->format('Y-m-d'), $holidays)) {
        $days--;
    }elseif(in_array($dt->format('Y-m-d'), $days_with_leave)){
		 $days--;
    }
}
return $days;
}

function GetLogsForToday($code,$date,$hcount,$ispartofleave){
	global $c;
	$dayname = date("D",strtotime($date));
	$is_weekend = false;
	if($dayname == "Sat" || $dayname == "Sun" || $ispartofleave==1){
		$is_weekend = true;
	}
	$hasmissinginput = false;
	$missingcount = 0;
	$toreturn = "";

	$pastin = "";
	$pastout = "";
	for($i = 0; $i < 4;$i++){
		$q = "";
		switch ($i) {
			case 0:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='1'";
			break;
			case 1:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='2'";
			break;
			case 2:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='1' AND timestamp != '$pastin'";
			break;
			case 3:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='2' AND timestamp != '$pastout'";
			break;
		}
		$rez = mysqli_query($c, $q);
		$rr = mysqli_fetch_array($rez);
		$fetched_time = "";
		if($rr["timestamp"]){
			$tmp = explode(" ", $rr["timestamp"]);
			$fetched_time = $tmp[1];
		}else{
			$missingcount += 1;
			$hasmissinginput = true;
		}
		if($i == 0){
			$pastin = $rr["timestamp"];
		}
		if($i == 1){
			$pastout = $rr["timestamp"];
		}

		$toreturn .= "<td>" . $fetched_time . "</td>";
	}
	if($hasmissinginput == true){
			$check_q = "SELECT * FROM dispute WHERE eid='" . $code . "' AND date_of_dispute='" . $date . "'";
			$check_res = mysqli_query($c,$check_q) ;
			$check_data = mysqli_fetch_array($check_res);

			$checkstatusofdispute = "";
			switch($check_data["status"]){
				case "0":
			$checkstatusofdispute  = "Dispute Pending<br>";
				break;
				case "1":
			$checkstatusofdispute  = "Dispute Approved<br>";
				break;
				case "2":
			$checkstatusofdispute  = "Dispute Dissaproved" . '<div class="alert alert-danger" role="alert">
  				'  . $check_data["reason_of_dissaprove"] . '
</div>';
				break;
			}


			$check_row = mysqli_num_rows($check_res);


			$ch_ap_stat = "SELECT * FROM applied_leave WHERE employee_id='" . $code . "' AND date_applied_totatus='" . $date . "'";
			$ch_ap_result = mysqli_query($c,$ch_ap_stat);
			$ch_ap_row = mysqli_fetch_array($ch_ap_result);
			$ch_ap_count_res = mysqli_num_rows($ch_ap_result);

			$status_leave = "";
			//get result
			switch($ch_ap_row["status"]){
			case "0":
				$status_leave = "Leave Pending<br>";
			break;
			case "1":
				$status_leave = "Leave Approved<br>";
			break;
			case "2":
				$status_leave = "Leave Dissaproved<br>" . '<div class="alert alert-danger" role="alert">
  				'  . $ch_ap_row["reason_of_dissaprove"] . '
				</div>';
			break;
			}


	if($ispartofleave == 1){
			
			$badge_paper = '<span class="badge badge-primary">Leave Date<span/>';
				$status_leave .= $badge_paper;
				$checkstatusofdispute .= $badge_paper;
			}

		
		if($missingcount == 4){
			// ALL IS MISSING


			if($ch_ap_count_res == 0){
			if($check_row != 0){

			$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $checkstatusofdispute . '</p></td>';

			$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
			}else{

			$toreturn .= '
		<td>
		' . $checkstatusofdispute . '	
		</td>
		';
			$toreturn .= '
		<td>';

		if($hcount == 0){
			$toreturn .= '
			<div class="dropdown dropleft">
				<button type="button" class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
				<div class="dropdown-menu">';
					if($dayname != 'Sun'){
				// $toreturn .= '
				// 	<a class="dropdown-item" onclick="func_dispute(this)" data-dateofrecord="' . $date . '" data-toggle="modal" data-target="#modal_dispute" data-empnum="' . $code . '">Dispute</a>';
					}else{
						// $toreturn .= '<button disabled class="dropdown-item" onclick="func_dispute(this)" data-dateofrecord="' . $date . '" data-empnum="' . $code . '">Dispute  - <i class="fas fa-info-circle"></i> Not available during Saturdays</button>';
					}

					if($is_weekend == false){
						$toreturn .= '<a class="dropdown-item" onclick="func_applyleave(this)" data-topicdate="' . $date . '" data-toggle="modal" data-target="#modal_applyleave"  data-empnum="' . $code . '"  data-strictmode="1">Apply Leave</a>';
					}else{
						$toreturn .= '<button class="dropdown-item" disabled onclick="func_applyleave(this)" data-topicdate="' . $date . '" data-toggle="modal">Apply Leave - <i class="fas fa-info-circle"></i> Not available during Weekends/Leave Date(s)</button>';
					}
					
				$toreturn .= '</div>
			</div>';
		}else{
			$toreturn .= '
				<button type="button" disabled class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		';
		}
		$toreturn .= '</td>';
			}
		}else{

			$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $status_leave . '</p></td>';

						//Display leave request status
					$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
		}
	
		}else{
			// ONE IS MISSING
			//CHECK IF ALREADY DISPUTED
		if($ch_ap_count_res == 0){
			if($check_row != 0){

		$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $checkstatusofdispute . '</p></td>';
		$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
			}else{
		$toreturn .= '
		<td>
				
		</td>
		';
		$toreturn .= '
		<td>';
			if($hcount == 0){
				if($dayname != 'Sun'){
			// 	$toreturn .='<div class="dropdown">
			// 	<button type="button" class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
			// 	<div class="dropdown-menu">
			// 		<a class="dropdown-item" onclick="func_dispute(this)" data-dateofrecord="' . $date . '" data-toggle="modal" data-target="#modal_dispute"  data-empnum="' . $code . '">Dispute</a>
			// 	</div>
			// </div>';
				}
		}else{
			$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
		}
		$toreturn .= '</td>';

		}
	}else{



		//Display leave request status
		$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $status_leave . '</p></td><td></td>';
	}
	}
		?>

<script type="text/javascript">
	$(function () {
  		$('[data-toggle="popover"]').popover()
	})
</script>
		<?php
	}else{
			$check_q = "SELECT * FROM dispute WHERE eid='" . $code . "' AND date_of_dispute='" . $date . "'";
			$check_res = mysqli_query($c,$check_q) ;
		if(mysqli_num_rows($check_res) == 0){
			$toreturn .= '
			<td>
			</td>';
		}else{
				$toreturn .= '<td>
				Dispute Approved
			</td>';
		}

		$toreturn .='
		<td>
	<button type="button" disabled class="btn btn-primary" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
	}
	$pastin = "";
	$pastout = "";
	$missingcount = 0;
	return $toreturn;
}

//Universal page returner
function BackToPage($res,$pagename){

	if($res){
	echo "<script>
	alert('Success!');
	window.location.href = '" . $pagename . "';
	</script>";
	}else{
	echo "<script>
	alert('Error!');
	window.location.href = '" . $pagename . "';
	</script>";
	}

}
?>