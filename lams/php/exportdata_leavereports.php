<?php
include("../connection/connect.php");
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="LAMS_BACKUP_LeaveReports - ' . date("Y-m-d") . '.csv"');
$data = array("id,employee_id,leave_type,date_from,date_to,sub_type,location,status,date_applied_totatus,reason_of_dissaprove,to_subtract,leave_taken,date_requested,time_requested,paid_days,unpaid_days,vac_leave_abroad_specify,fullname,office_agency,date_of_filling,position,salary_monthly,vacation_type,vacation_leaveincase,sick_incase,sick_incase_reason");
$q = "SELECT * FROM applied_leave";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){
	$toadd = $row["id"] . ',' . $row["employee_id"] . ','. $row["leave_type"] . ','. $row["date_from"] . ','. $row["date_to"] . ','. $row["sub_type"] . ','. $row["location"] . ','. $row["status"] . ','. $row["date_applied_totatus"]. ','. $row["reason_of_dissaprove"]. ','. $row["to_subtract"]. ','. $row["leave_taken"]. ','. $row["date_requested"]. ','. $row["time_requested"]. ','. $row["paid_days"]. ','. $row["unpaid_days"]. ','. $row["vac_leave_abroad_specify"]. ','. $row["fullname"]. ','. $row["office_agency"]. ','. $row["date_of_filling"]. ','. $row["position"]. ','. $row["salary_monthly"]. ','. $row["vacation_type"]. ','. $row["vacation_leaveincase"]. ','. $row["sick_incase"]. ','. $row["sick_incase_reason"];
	array_push($data, $toadd);
}

$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);
?>
