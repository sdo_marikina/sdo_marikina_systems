<?php
include("../connection/connect.php");
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="LAMS_BACKUP_DisputeReports - ' . date("Y-m-d") . '.csv"');
$data = array("id,eid,reason,is_oath,date_of_dispute,date_of_record,status,documentrecieved,reason_of_dissaprove");
$q = "SELECT * FROM dispute";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){
	$toadd = $row["id"] . ',' . $row["eid"] . ','. $row["reason"] . ','. $row["is_oath"] . ','. $row["date_of_dispute"] . ','. $row["date_of_record"] . ','. $row["status"] . ','. $row["documentrecieved"] . ','. $row["reason_of_dissaprove"];
	array_push($data, $toadd);
}

$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);
?>
