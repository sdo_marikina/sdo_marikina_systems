<?php
include_once("../connection/connect.php");
$LAMS_HR_WEBSERVICE_LINK = "http://hrmo.depedmarikina.ph/lams_hr_webservice/";
$backuplink = "https://depedmarikina.ph/cdtrs_webservices/cdtrs_backup/index.php";
$backuplink_local = "http://localhost/sdo_marikina_systems/lams_backup/index.php";



// (SUPPORT FOR OLDER VERSION IS CUTTED OUT)

// CDTRS 3.0 UPDATE DATABASE ADDITIONS

//SUPPORT FOR ATTENDANCE KIOSK
$q = "SELECT label FROM lams_configurations WHERE label='att_allow_no_cam'";
if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
	//create the seeting if none
	$q = "INSERT INTO lams_configurations SET
	label='att_allow_no_cam',
	value='0',
	description='Allow employees to log without taking a picture from the webcam.',
	value_type='Allow no Camera in Kiosk',
	hidden='0'";

	mysqli_query($c,$q);
}
// ADD "origin" column in Authorities table 
AddToColumn("authorities","origin","INT(11) DEFAULT 0");
// ADD "account_type" in lams_users table 
AddToColumn("lams_users","account_type","INT(11) DEFAULT 0");


// CREATE TMP EMPLOYEE NUMBER TABLE 

$q = "CREATE TABLE IF NOT EXISTS `profile_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `image` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
)
";


if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}

//CDTRS 3.4


// ADD THE ASSISTANT OIC TO THE SETTINGS
$q = "SELECT * FROM lams_configurations WHERE label='as_oic'";

if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "INSERT INTO lams_configurations SET
	label='as_oic',
	value='None',
	description='Office of the Assistant Schools Division Superintendent',
	value_type='Chief Education Supervisor<br>Officer-in-Charge<br>Office of the Assistant Schools Division Superintendent',
	hidden='0'";
if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}
}

$q = "SELECT * FROM untask WHERE taskname='replace_oic_title'";
if(mysqli_num_rows(mysqli_query($c,$q)) == 0){


$q = "INSERT INTO untask SET taskname='replace_oic_title'";
if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}

$q = "UPDATE lams_configurations SET value_type='Education Program Supervisor<br>Officer-in-Charge<br>Office of the Schools Division Superintendent' WHERE label='oic'";
if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}
}


$q = "SELECT * FROM untask WHERE taskname='replace_hr_title'";
if(mysqli_num_rows(mysqli_query($c,$q)) == 0){


$q = "INSERT INTO untask SET taskname='replace_hr_title'";
if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}

$q = "UPDATE lams_configurations SET value_type='Head Teacher III/OIC - HRMO' WHERE label='hr'";
if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}
}




// CREATE TMP EMPLOYEE NUMBER TABLE 

$q = "ALTER table entitlements CHANGE single_parent_leave solo_parent_leave float DEFAULT '0'";


if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}


// CREATE profile_pictures table 

$q = "CREATE TABLE IF NOT EXISTS `temp_employee_num` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `date_added` timestamp NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
)
";


if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}
// CREATE quicklook table 
$q = "CREATE TABLE IF NOT EXISTS `quicklook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_tag` varchar(1000) NOT NULL,
  `item_name` varchar(222) NOT NULL,
  `item_html` varchar(500) NOT NULL,
  `item_description` longtext NOT NULL,
  `type` int(11) NOT NULL,
  `is_specific` int(11) NOT NULL,
  PRIMARY KEY (`id`)
)
";


if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}

// CREATE FREEZED BALANCE 
$q = "CREATE TABLE IF NOT EXISTS `freezed_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_id` varchar(222) NOT NULL,
  `service_credit` varchar(222) NOT NULL,
  `vacation_leave` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
)";

if(!mysqli_query($c,$q)){
	echo "failed to update database.";
}

//Replace Ñ and ñ to simple N and n

$q = "SELECT * FROM lams_configurations WHERE label='cn' LIMIT 1";
$res = mysqli_fetch_array(mysqli_query($c,$q));
$scnamex = $res["value"];
$scnamex = str_replace("Ñ", "N", $scnamex);
$scnamex = str_replace("ñ", "n", $scnamex);
$q = "UPDATE lams_configurations SET value='" . $scnamex . "' WHERE label='cn'";
mysqli_query($c,$q);

function AddToColumn($tbl_name, $col_name, $col_datatype){
	global $c;
	$q = "SELECT $col_name FROM $tbl_name";
	$res =  mysqli_query($c,$q);
	if(!$res){
		//Column is not exsiting
		$q = "ALTER TABLE $tbl_name ADD $col_name $col_datatype";
		$res =  mysqli_query($c,$q);
	}
}

ForceLeaveAddLog();
function ForceLeaveAddLog(){
	global $c;
	$q = "SELECT eid FROM employees";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		$eq = "SELECT * FROM logs WHERE action_description LIKE '%" . $row["eid"] . "%' AND action_description LIKE '%Added 5 Forced Leave%' AND timestamp LIKE '%" . date("Y") . "%'";
		$eres = mysqli_query($c,$eq);
		if(mysqli_num_rows($eres) == 0){
			$mq = "INSERT INTO logs SET action_description='" . "Just Added 5 Forced Leave to Employee #" . $row["eid"]
			 ."',
			 performer='System Auto Performer',
			 timestamp='" . date("Y-m-d H:i:s") . "'";
			 mysqli_query($c,$mq);

			 $qu_fl = "UPDATE entitlements SET forced_leave = 5 WHERE emp_id='" . $row["eid"] . "' LIMIT 1";
			 mysqli_query($c,$qu_fl);
		}
	}
}

	$q = "SELECT *, scheduled_forcedleave.id as emp_id,scheduled_forcedleave.eid as sfl_eid FROM scheduled_forcedleave JOIN employees ON scheduled_forcedleave.eid = employees.eid WHERE employees.type !='2' AND scheduled_forcedleave.status ='0'";
	$res = mysqli_query($c,$q);



	$holidays = array();
	$holidayname = array();
	$sq = "SELECT * FROM holidays";
	$sres = mysqli_query($c,$sq);
	while($srow = mysqli_fetch_array($sres)){
	$dd = explode("-", $srow["date"]);
	$holidate =  date('Y-m-d', mktime(0, 0, 0, $dd[0], $dd[1]));
	array_push($holidays,$holidate);
	array_push($holidayname,$srow["name"]);
	}

	while($row = mysqli_fetch_array($res)){

		$startingDate = date("Y-m-d");
		$targetDate = $row["date"];
		$remdays = 0;
		while (strtotime($startingDate) <= strtotime($targetDate)) {
			$remdays++;
			$startingDate = date ("Y-m-d", strtotime("+1 day", strtotime($startingDate)));
		}
		// echo $row["eid"] . " - " . $remdays . "x";
		// echo $row["emp_id"] . "|";
		if($remdays == 0){
			$q = "UPDATE scheduled_forcedleave SET status='2' WHERE id='" . $row["emp_id"] . "'";
			mysqli_query($c,$q);

			$no_deduction = false;

			for($o = 0;$o < count($holidays);$o++){
				if(date("Y-m-d",strtotime($row["date"])) == $holidays[$o]){
					$no_deduction = true;
				}
			}

			if (date("l",strtotime($row["date"])) != "Sunday" && date("l",strtotime($row["date"])) != "Saturday") {
				if($no_deduction == false){
				$qod = "SELECT vacation_leave FROM entitlements WHERE emp_id='" . $row["eid"] ."' LIMIT 1";
				$rez = mysqli_query($c,$qod);
				$xrow = mysqli_fetch_array($rez);
				$old_vl_value = $xrow["vacation_leave"];
				$old_vl_value--;
				$qd = "UPDATE entitlements SET vacation_leave='" . $old_vl_value . "' WHERE eid='" . $row["eid"] . "'";
				mysqli_query($c,$qd);
				}
				log_system_action("Deduction VL for Forced Leave (-1) to employee " . $row["eid"]. " in  date: " . $row["date"] . ".");
			}else{
				log_system_action("No Deduction VL for Forced Leave (-0) to employee " . $row["eid"]. " in  date: " . $row["date"] . ".");
			}

		}
	}


	AnalizeWhereAboutsAutoDispute();
function AnalizeWhereAboutsAutoDispute(){
	global $c;
	$q = "SELECT * FROM where_abouts WHERE arrival IS NOT NULL AND arrival != ''";
	// echo mysqli_num_rows(mysqli_query($c,$q));
	$res=  mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
		$dayname = date("l",strtotime($row["departure"]));
		$day_date = date("Y-m-d",strtotime($row["departure"]));
		// echo $dayname;
		if($dayname != "Sunday"){
			$qx = "SELECT * FROM dispute WHERE date_of_dispute='" . $day_date ."' AND eid='" . $row["eid"] . "'";
			$xres = mysqli_query($c,$qx);
			if(mysqli_num_rows($xres) == 0){
					$qd = "INSERT INTO dispute(
					eid,
					reason,
					is_oath,
					date_of_dispute,
					date_of_record,
					status,
					documentrecieved,
					dispute_type
					)VALUES(
					'" . $row["eid"] . "',
					'" . "WHEREABOUTS AUTO-DISPUTE (Reason): " . $row["purpose"] . "',
					'1',
					'" . $day_date ."',
					'" . date("Y-m-d") . "',
					'0',
					'0',
					'0'
					)";
					mysqli_query($c,$qd) ;
			}
		}
		// echo date("F d, Y l",strtotime($row["departure"])) . "<br>";
	}
}


function GetLamsSettings($setting_label){
	global $c;
	$q = "SELECT * FROM lams_configurations WHERE label='$setting_label' LIMIT 1";
	$res = mysqli_query($c, $q);
	$row = mysqli_fetch_array($res);
	return $row["value"];
}
function log_system_action($message){
	global $c;
	$message = mysqli_real_escape_string($c, $message);
	$message = htmlentities($message);
	$q = "INSERT INTO logs(performer,action_description,timestamp) VALUES('Admin','" . $message . "','" . date("Y-m-d H:i:s") . "')";
	mysqli_query($c,$q) ;
}


 SyncDispute();
 SyncLeaveReports();
 SyncScheduledVacationLeave();
 SyncMultipleSched();



// CHECK IF SUPPORT FOR DATA SYNCHRONIZATION IS ON!
$q = "SELECT value FROM lams_configurations WHERE label='scc'";
$r = mysqli_query($c,$q) ;
$s = mysqli_fetch_array($r);
$ifallowed = $s["value"];

	if($ifallowed == "1"){
 PullAttendanceLogsFrom();
echo UpdateLamsBackupDB();
}



function SyncDispute(){
	global $backuplink;
	global $c;

	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];


	$q = "SELECT * FROM  dispute ORDER BY id DESC LIMIT 500";
	$res = mysqli_query($c,$q);
	$joal = json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));



	// MANAGE ACCOUNT IF EXISTING
	$form_data = array(
	"tag"=>"SyncDisputeReports",
	"json_of_diputereports"=>$joal,
	"company_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	return $output;

}
function SyncLeaveReports(){
	global $backuplink;
	global $c;


	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];


	$q = "SELECT * FROM  applied_leave ORDER BY id DESC LIMIT 100";
	$res = mysqli_query($c,$q);
	$joal = json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	// MANAGE ACCOUNT IF EXISTING
	$form_data = array(
	"tag"=>"SyncLeaveReports",
	"json_of_appliedleave"=>$joal,
	"company_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}
function SyncScheduledVacationLeave(){
	global $backuplink;
	global $c;

	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];

	$q = "SELECT * FROM  scheduled_forcedleave ORDER BY id DESC LIMIT 500";
	$res = mysqli_query($c,$q);
	$joal = json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	// MANAGE ACCOUNT IF EXISTING
	$form_data = array(
	"tag"=>"Syncflreports",
	"json_of_flreports"=>$joal,
	"company_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}
function SyncMultipleSched(){
	global $backuplink;
	global $c;

	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];

	$q = "SELECT * FROM multiple_sched";
	$res = mysqli_query($c,$q);
	$joal = json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	// MANAGE ACCOUNT IF EXISTING
	$form_data = array(
	"tag"=>"Syncmultiple_sched",
	"json_of_multiple_sched"=>$joal,
	"company_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}

function PullAttendanceLogsFrom(){
	global $backuplink_local;
	global $c;

// echo $ifallowed;
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];

	// MANAGE ACCOUNT IF EXISTING
	$form_data = array(
	"tag"=>"Synch_Attlogs_From_Break",
	"company_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink_local);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	$output = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output), true );
	$sync_count = 0;
	for ($i=0; $i < count($output); $i++) { 
		$q = "SELECT * FROM attendance_logs WHERE eid='" . $output[$i]["eid"] . "' AND timestamp='" . $output[$i]["timestamp"] . "' LIMIT 1";
		if(mysqli_num_rows(mysqli_query($c,$q)) == "0"){
		$q = "INSERT INTO attendance_logs SET  eid='" . $output[$i]["eid"] . "', access_type='" . $output[$i]["access_type"] . "', access_image='" . $output[$i]["access_image"] . "', date='" . $output[$i]["date"] . "', timestamp='" . $output[$i]["timestamp"] . "', ismanual='0', synched='0'";
		mysqli_query($c,$q);
		}
		$sync_count ++;
	}

	return "Synched: " . $sync_count;

}

function UpdateLamsBackupDB(){
	global $backuplink_local;
	global $c;

	// MANAGE ACCOUNT IF EXISTING
	$form_data = array(
	"tag"=>"MAINTENANCECOMMAND_UpdateLamsBackupDB",
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink_local);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}
FixNullDispute();
function FixNullDispute(){
	global $c;
	$q = "UPDATE dispute SET dispute_type='0' WHERE dispute_type IS NULL";
	mysqli_query($c,$q);
}
?>