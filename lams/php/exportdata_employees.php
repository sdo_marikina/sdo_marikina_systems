<?php
include("../connection/connect.php");
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="LAMS_BACKUP_Employees - ' . date("Y-m-d") . '.csv"');
$data = array("id,eid,rfid,type,basic_pay,position,department,fname,mname,lname,fid,ftemplate,schedule");
$q = "SELECT * FROM employees";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){
	$toadd = $row["id"] . ',' . $row["eid"]  . ',' . $row["rfid"]  . ',' . $row["type"]  . ',' . $row["basic_pay"]  . ',' . $row["position"]  . ',' . $row["department"]  . ',' . $row["fname"]  . ',' . $row["mname"]  . ',' . $row["lname"]  . ',' . $row["fid"]  . ',' . $row["ftemplate"]  . ',' . str_replace(",", ".", $row["schedule"]) ;
	array_push($data, $toadd);
}

$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);
?>
