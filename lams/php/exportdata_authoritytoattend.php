<?php
include("../connection/connect.php");
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="LAMS_BACKUP_Authorities - ' . date("Y-m-d") . '.csv"');
$data = array("id,emp_code,emp_name,station,destination,date_from,date_to,auth_time,purpose_description,data_file,funding_source,auth_type,record_datetime,auth_status,control_code");
$q = "SELECT * FROM authorities";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){
	$toadd = $row["id"] . ',' . $row["emp_code"] . ',' . $row["emp_name"] . ',' . $row["station"] . ',' . $row["destination"] . ',' . $row["date_from"] . ',' . $row["date_to"] . ',' . $row["auth_time"] . ',' . $row["purpose_description"] . ',' . $row["data_file"] . ',' . $row["funding_source"] . ',' . $row["auth_type"] . ',' . $row["record_datetime"] . ',' . $row["auth_status"] . ',' . $row["control_code"];
	array_push($data, $toadd);
}

$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);
?>
