<?php
include("../connection/connect.php");
// REAL BACKUP LINK
// http://vaerch.com/lams_backup/index.php
// $backuplink = "localhost/lams_backup/index.php";
// $backuplink = "localhost/sync/index.php";
$backuplink = "https://depedmarikina.ph/cdtrs_webservices/cdtrs_backup/index.php";
// $backuplink = "http://localhost:8080/cdtrs_webservices/cdtrs_backup/index.php";
if(session_status() == PHP_SESSION_NONE){
	session_start();
}

if (isset($_POST["LoadLeaveHistory"])) {
	$leaverep = $_POST["leave_report"];
	$istoday  = $_POST["istoday"];
	$date_from  = $_POST["date_from"];
	$date_to = $_POST["date_to"];
	$q = "";
if($istoday == "0"){
	
	// LOAD THE PRESENT
$q = "SELECT *, applied_leave.id as e_id FROM applied_leave  JOIN employees ON applied_leave.employee_id = employees.eid LEFT JOIN entitlements ON applied_leave.employee_id = entitlements.emp_id WHERE applied_leave.status ='" . $leaverep . "' AND applied_leave.date_requested ='" . date("Y-m-d") . "' ORDER BY applied_leave.id DESC LIMIT 465";
}else if($istoday == "1"){
	
	// LOAD THE PAST
	$q = "SELECT *, applied_leave.id as e_id FROM applied_leave  JOIN employees ON applied_leave.employee_id = employees.eid LEFT JOIN entitlements ON applied_leave.employee_id = entitlements.emp_id WHERE applied_leave.status ='" . $leaverep . "' AND applied_leave.date_requested !='" . date("Y-m-d") . "' ORDER BY applied_leave.id DESC LIMIT 465";
}else if($istoday == "3"){

	// LOAD MANUAL

	if($leaverep == "3"){
		$q = "SELECT *, applied_leave.id as e_id FROM applied_leave  JOIN employees ON applied_leave.employee_id = employees.eid LEFT JOIN entitlements ON applied_leave.employee_id = entitlements.emp_id WHERE applied_leave.date_applied_totatus BETWEEN '" . $date_from . "' AND '" . $date_to . "' ORDER BY applied_leave.id DESC";
	}else{
		$q = "SELECT *, applied_leave.id as e_id FROM applied_leave  JOIN employees ON applied_leave.employee_id = employees.eid LEFT JOIN entitlements ON applied_leave.employee_id = entitlements.emp_id WHERE applied_leave.status ='" . $leaverep . "' AND applied_leave.date_applied_totatus BETWEEN '" . $date_from . "' AND '" . $date_to . "' ORDER BY applied_leave.id DESC";
	}
	
}

$res = mysqli_query($c,$q) ;
$stats_name = ["Pending","Approved","Disapproved"];

while($row = mysqli_fetch_array($res)){
if($row["fname"] != ""){
	$xtime = strtotime($row["date_requested"] . " " . $row["time_requested"]);
?>
<tr>
	<td>
		<span class="float-right"><?php echo date("g:i a",$xtime); ?></span>
		<h5 class="card-title"><?php echo date("F d, Y",$xtime); ?></h5>
		<p class="text-muted mb-2 card-subtitle"><?php echo date("F d, Y",strtotime($row["date_from"])); ?> <i class="fas fa-long-arrow-alt-right"></i> <?php echo date("F d, Y",strtotime($row["date_to"])); ?></p>
	</td>
	<td style="text-align: center;"><?php echo $row["fname"] . " " . $row["lname"]; ?></td>
	<td style="text-align: center;"><?php echo $row["leave_type"]; ?></td>
	<td style="text-align: center;"><?php echo $stats_name[$row["status"]]; ?></td>
	<td><?php 

if($row["status"] == "0"){

	//pending
	echo "<center>
<div class='dropdown'>
	<button class='btn btn-sm btn-primary' data-toggle='dropdown'><span>Action</span></button>
	<div class='dropdown-menu'>";
		echo "<a class='dropdown-item' onclick='openapprove_leave(this)' data-theid='" . $row["e_id"] . "' data-toggle='modal' data-target='#modal_lr_approve'>";
		if($row["leave_type"] == "Forced Leave"){
		echo "Print Form 6";
		}	else{
			echo "Approve";
		}
		 echo"</a>";
		if($row["leave_type"] != "Forced Leave"){
			echo "<a class='dropdown-item' onclick='opendissaprove_leave(this)' data-theid='" . $row["e_id"] . "' data-toggle='modal' data-target='#modal_lr_dissaprove'>Dissaprove</a>";
		}

			echo "<a class='dropdown-item'data-toggle='modal' data-target='#modal_obliform6' onclick='PrepareCancelLeaveModal(this)' data-llid='" . $row["e_id"] . "'>Cancel Leave</a>";
	echo "</div>
	</div>
	</center>";
}else{
	echo "<center>
		<div class='dropdown'>
	<button class='btn btn-sm btn-primary' data-toggle='dropdown'><span>Action</span></button>
	<div class='dropdown-menu'>";

	echo "<a class='dropdown-item' onclick='openreprint_leave(this)' data-theid='" . $row["e_id"] . "' data-toggle='modal' data-target='#modal_lr_reprint'>Print</a>";

	echo "<a class='dropdown-item'data-toggle='modal' data-target='#modal_obliform6' onclick='PrepareCancelLeaveModal(this)' data-llid='" . $row["e_id"] . "'>Cancel Leave</a>";

echo "</div>
	</div>
		</center>";

}
 ?></td>
</tr>
<?php
}
}
}
if (isset($_POST["format_date"])) {
	echo date("F d, Y", strtotime($_POST["date_to_fromat"]));
}
if (isset($_POST["get_emps_xinfom"])) {
	$q = "SELECT eid,fname,lname FROM employees ORDER BY lname ASC";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
		echo "<option value='" . $row["eid"] . "'>" . $row["lname"] . ", " . $row["fname"] . "</option>";
	}
}
if (isset($_POST["addmanualrecord"])) {
	$q = "INSERT INTO attendance_logs SET eid='" . $_POST["eid"] . "',access_type='" . $_POST["access_type"] . "',date='" . $_POST["manualdate"] . "',timestamp='" . $_POST["manualdate"] . " " . date("H:i:s", strtotime($_POST["thetimeoflog"]))  . "',access_image='',ismanual='1',synched='0'";
	$res = mysqli_query($c,$q) or die(mysqli_error($c));
	if ($res) {
		BackToPage_Message("Log Added!.","../attendance_history.php");
	}else{
		// BackToPage_Message($_POST["manualdate"] . " " . date("H:i:s", strtotime($_POST["thetimeoflog"])),"../attendance_history.php");
	}
}

if (isset($_POST["checkempmissinginfo"])) {
	// CHECK IF HAS MISSING EMPLOYEE INFO 
	$q = "SELECT * FROM employees WHERE employees.position NOT IN (SELECT id FROM positions)";
	$miss_pos_count = mysqli_num_rows(mysqli_query($c,$q));
	$q = "SELECT * FROM employees WHERE employees.department NOT IN (SELECT id FROM departments)";
	$miss_dep_count = mysqli_num_rows(mysqli_query($c,$q));
	if($miss_pos_count == "0" && $miss_dep_count == "0"){
	echo "false";
	}else{
	echo "true";
	}
}
if (isset($_POST["check_ifreprintable"])) {
	$q = "SELECT * FROM freezed_balance WHERE leave_id='" . $_POST["leave_id"] . "' LIMIT 1";

	$res = mysqli_query($c,$q);

	if(mysqli_num_rows($res) == "1"){
		echo "true";
	}else{
		echo "false";
	}
}
if (isset($_POST["checkIfCDTRS3"])) {
	$q = "SELECT * FROM untask WHERE taskname='updatedtocdtrs3'";
	if(mysqli_num_rows(mysqli_query($c,$q)) ==0){
			echo "0";
				$q = "INSERT INTO untask SET taskname='updatedtocdtrs3'";
		mysqli_query($c,$q);
	}else{
		echo "1";
	}
}
if (isset($_POST["Getallpositions"])) {
		$q = "SELECT * FROM positions ORDER BY name ASC";
	$res = mysqli_query($c,$q);
	echo '<option selected="" value="" disabled="" value="">Choose Employee Position</option>';
	while($row = mysqli_fetch_array($res)){
		echo "<option value='" . $row["id"] . "'>" . $row["name"] . "</option>";
	}
}
if (isset($_POST["sync_sett"])) {
		$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM lams_configurations";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
	'tag'=>"sync_lamsconf",
	'company'=>$scname,
	'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["get_default_parentalleavedays"])) {
	$st_day = $_POST["st_day"];

	echo date("Y-m-d",strtotime($st_day . "+104 day"));
}
if (isset($_POST["lod_all_emp_dtr"])) {
	$q = "SELECT employees.fname,employees.id,employees.lname,departments.name,departments.id FROM employees LEFT JOIN departments ON employees.department = departments.id ORDER BY employees.lname ASC";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
		echo '
			<tr>
			<td>' . strtoupper($row["lname"]) .  "," . strtoupper($row["fname"]) . '<br><small class="text-muted mt-2">Dep: ' . $row["name"] . '</small></td>
			<td><input type="checkbox" class="sel_emp_chk_dt" value="' .  strtoupper($row["id"])  . '" name="selected_emp[]"></td>
			</tr>
		';
	}
}
if(isset($_POST["getleavesummar"])){
	$q = "SELECT status FROM applied_leave WHERE status='0'";
	$pending = mysqli_num_rows(mysqli_query($c,$q));
	$q = "SELECT status FROM applied_leave WHERE status='1'";
	$approved = mysqli_num_rows(mysqli_query($c,$q));
	$q = "SELECT status FROM applied_leave WHERE status='2'";
	$disapproved = mysqli_num_rows(mysqli_query($c,$q));
	echo  $pending . "," . $approved . "," . $disapproved . "";
}
if (isset($_POST["send_bot_signal"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];
		$myIp = file_get_contents("http://ipecho.net/plain");
		$form_data = array(
			'tag'=>"send_signal",
			'a_company'=>$scname,
			'a_ip'=>$myIp,
			'a_version'=>"3.4",
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["sync_ent"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM entitlements";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
	'tag'=>"sync_entmts",
	'company'=>$scname,
	'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["sync_schedfl"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM scheduled_forcedleave";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
	'tag'=>"sync_sfl",
	'company'=>$scname,
	'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["sync_cto"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM entitlements_cto";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
	'tag'=>"sync_ct",
	'company'=>$scname,
	'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}

if (isset($_POST["sync_ata"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM authorities";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
	'tag'=>"sync_authta",
	'company'=>$scname,
	'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["sync_multips"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM multiple_sched";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
	'tag'=>"sync_multis",
	'company'=>$scname,
	'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}

if (isset($_POST["sync_holi"])) {
		$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM holidays";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
		'tag'=>"sync_holid",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["deleteauthtoattend"])) {
	$data_id = $_POST['data_id'];
	$q = "DELETE FROM authorities WHERE id='" . $data_id ."' LIMIT 1";
	$res = mysqli_query($c,$q);
	if ($res) {
		BackToPage_Message("Cancellation Successful.","../auth_attend.php");
	}else{
		BackToPage_Message("Cancellation Failed.","../auth_attend.php");
	}
}
if (isset($_POST["sync_disp"])) {
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$q = "SELECT * FROM dispute ORDER BY id DESC LIMIT 364";
	$res = mysqli_query($c,$q);
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
		'tag'=>"sync_dispu",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	echo $output;
	curl_close($ch);
	$records = json_decode($output,true);

	for ($i=0; $i < count($records); $i++) { 

			$qx = "SELECT * FROM dispute
			WHERE eid='" . $records[$i]["eid"] . "'
			AND date_of_dispute='" . $records[$i]["date_of_dispute"] . "'
			AND date_of_record='" . $records[$i]["date_of_record"] . "'
			LIMIT 1";
					// CHECK IF DATA IS EXISTING

			$res = mysqli_query($c,$qx);
			$row = mysqli_fetch_array($res);
				if(mysqli_num_rows(mysqli_query($c,$qx)) == 0){
					// INSERT NEW DATA
					$qm = "INSERT INTO dispute SET

						eid='" . $records[$i]["eid"] . "',
						reason='" . mysqli_real_escape_string($c,$records[$i]["reason"]) . "',
						is_oath='" . $records[$i]["is_oath"] . "',
						date_of_dispute='" . $records[$i]["date_of_dispute"] . "',
						date_of_record='" . $records[$i]["date_of_record"] . "',
						status='" . $records[$i]["status"] . "',
						documentrecieved='" . $records[$i]["documentrecieved"] . "',
						reason_of_dissaprove='" . mysqli_real_escape_string($c,$records[$i]["reason_of_dissaprove"]) . "',
						dispute_type='" . $records[$i]["dispute_type"] . "'";

						mysqli_query($c,$qm);
				}else{
						if ($records[$i]["status"] != $row["status"]) {
						$qm = "UPDATE dispute  SET
						eid='" . $records[$i]["eid"] . "',
						reason='" . mysqli_real_escape_string($c,$records[$i]["reason"]) . "',
						is_oath='" . $records[$i]["is_oath"] . "',
						date_of_dispute='" . $records[$i]["date_of_dispute"] . "',
						date_of_record='" . $records[$i]["date_of_record"] . "',
						status='" . $records[$i]["status"] . "',
						documentrecieved='" . $records[$i]["documentrecieved"] . "',
						reason_of_dissaprove='" . mysqli_real_escape_string($c,$records[$i]["reason_of_dissaprove"]) . "',
						dispute_type='" . $records[$i]["dispute_type"] . "'

						WHERE eid='" . $records[$i]["eid"] . "'
						AND date_of_dispute='" . $records[$i]["date_of_dispute"] . "'
						AND date_of_record='" . $records[$i]["date_of_record"] . "' LIMIT 1";
						mysqli_query($c,$qm);
					}
				}
		}


}


if (isset($_POST["sync_applyl"])) {
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$q = "SELECT * FROM applied_leave ORDER BY id DESC LIMIT 50";
	$res = mysqli_query($c,$q);
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
		'tag'=>"syncapplyleav",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	// echo $output;
	curl_close($ch);
	$records = json_decode($output,true);

	for ($i=0; $i < count($records); $i++) { 

			$qx = "SELECT * FROM applied_leave
			WHERE employee_id='" . $records[$i]["employee_id"] . "'
			AND leave_type='" . $records[$i]["leave_type"] . "'
			AND date_from='" . $records[$i]["date_from"] . "'
			AND date_to='" . $records[$i]["date_to"] . "'
			AND time_requested='" . $records[$i]["time_requested"] . "' LIMIT 1";
					// CHECK IF DATA IS EXISTING

			$res = mysqli_query($c,$qx);
			$row = mysqli_fetch_array($res);
				if(mysqli_num_rows(mysqli_query($c,$qx)) == 0){
					// INSERT NEW DATA
					$qm = "INSERT INTO applied_leave SET

						employee_id='" . $records[$i]["employee_id"] . "',
						leave_type='" . $records[$i]["leave_type"] . "',
						date_from='" . $records[$i]["date_from"] . "',
						date_to='" . $records[$i]["date_to"] . "',
						sub_type='" . $records[$i]["sub_type"] . "',
						location='" . $records[$i]["location"] . "',
						status='" . $records[$i]["status"] . "',
						date_applied_totatus = '" . $records[$i]["date_applied_totatus"] . "',
						reason_of_dissaprove = '" . $records[$i]["reason_of_dissaprove"] . "',
						to_subtract = '" . $records[$i]["to_subtract"] . "',
						leave_taken = '" . $records[$i]["leave_taken"] . "',
						date_requested = '" . $records[$i]["date_requested"] . "',
						time_requested = '" . $records[$i]["time_requested"] . "',
						paid_days = '" . $records[$i]["paid_days"] . "',
						unpaid_days = '" . $records[$i]["unpaid_days"] . "',
						vac_leave_abroad_specify = '" . $records[$i]["vac_leave_abroad_specify"] . "',
						fullname = '" . $records[$i]["fullname"] . "',
						office_agency = '" . $records[$i]["office_agency"] . "',
						date_of_filling = '" . $records[$i]["date_of_filling"] . "',
						position = '" . $records[$i]["position"] . "',
						salary_monthly = '" . $records[$i]["salary_monthly"] . "',
						vacation_type = '" . $records[$i]["vacation_type"] . "',
						vacation_leaveincase = '" . $records[$i]["vacation_leaveincase"] . "',
						sick_incase = '" . $records[$i]["sick_incase"] . "',
						sick_incase_reason = '" . $records[$i]["sick_incase_reason"] . "'";

						mysqli_query($c,$qm) or die(mysqli_error($c));
				}else{
						if ($records[$i]["status"] != $row["status"]) {
						$qm = "UPDATE applied_leave  SET
						employee_id='" . $records[$i]["employee_id"] . "',
						leave_type='" . $records[$i]["leave_type"] . "',
						date_from='" . $records[$i]["date_from"] . "',
						date_to='" . $records[$i]["date_to"] . "',
						sub_type='" . $records[$i]["sub_type"] . "',
						location='" . $records[$i]["location"] . "',
						status='" . $records[$i]["status"] . "',
						date_applied_totatus = '" . $records[$i]["date_applied_totatus"] . "',
						reason_of_dissaprove = '" . $records[$i]["reason_of_dissaprove"] . "',
						to_subtract = '" . $records[$i]["to_subtract"] . "',
						leave_taken = '" . $records[$i]["leave_taken"] . "',
						date_requested = '" . $records[$i]["date_requested"] . "',
						time_requested = '" . $records[$i]["time_requested"] . "',
						paid_days = '" . $records[$i]["paid_days"] . "',
						unpaid_days = '" . $records[$i]["unpaid_days"] . "',
						vac_leave_abroad_specify = '" . $records[$i]["vac_leave_abroad_specify"] . "',
						fullname = '" . $records[$i]["fullname"] . "',
						office_agency = '" . $records[$i]["office_agency"] . "',
						date_of_filling = '" . $records[$i]["date_of_filling"] . "',
						position = '" . $records[$i]["position"] . "',
						salary_monthly = '" . $records[$i]["salary_monthly"] . "',
						vacation_type = '" . $records[$i]["vacation_type"] . "',
						vacation_leaveincase = '" . $records[$i]["vacation_leaveincase"] . "',
						sick_incase = '" . $records[$i]["sick_incase"] . "',
						sick_incase_reason = '" . $records[$i]["sick_incase_reason"] . "'

						WHERE employee_id='" . $records[$i]["employee_id"] . "'
						AND leave_type='" . $records[$i]["leave_type"] . "'
						AND date_from='" . $records[$i]["date_from"] . "'
						AND date_to='" . $records[$i]["date_to"] . "'
						AND time_requested='" . $records[$i]["time_requested"] . "' LIMIT 1";
						mysqli_query($c,$qm);
					}
				}
		}


}

if(isset($_POST["sync_departm"])){
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM departments ORDER BY RAND() LIMIT 5";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
		'tag'=>"sync_dep",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}



if(isset($_POST["sync_positi"])){
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$q = "SELECT * FROM positions ORDER BY RAND() LIMIT 5";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
		'tag'=>"sync_posi",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}


if(isset($_POST["sync_empl"])){
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$q = "SELECT * FROM employees WHERE synched='0' ORDER BY id DESC LIMIT 64";
	$res = mysqli_query($c,$q) or die(mysqli_error());
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$q = "UPDATE employees SET synched='1' WHERE synched='0' ORDER BY id DESC LIMIT 64";
	$res = mysqli_query($c,$q) or die(mysqli_error());

	$form_data = array(
		'tag'=>"sync_employessnow",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}
if (isset($_POST["sync_whereabouts"])) {
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$q = "SELECT * FROM where_abouts ORDER BY departure DESC LIMIT 57";
	$res = mysqli_query($c,$q);
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$form_data = array(
		'tag'=>"sync_wherab",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	$records = json_decode($output,true);

	for ($i=0; $i < count($records); $i++) { 
			$qx = "SELECT * FROM where_abouts
			WHERE eid='" . $records[$i]["eid"] . "'
			AND departure='" . $records[$i]["departure"] . "' LIMIT 1";
				// CHECK IF DATA IS EXISTING
				$res = mysqli_query($c,$qx);
				$row = mysqli_fetch_array($res);
				if(mysqli_num_rows(mysqli_query($c,$qx)) ==0){
					// INSERT NEW DATA
					$qm = "INSERT INTO where_abouts SET

						eid='" . $records[$i]["eid"] . "',
						destination='" . $records[$i]["destination"] . "',
						purpose='" . $records[$i]["purpose"] . "',
						departure='" . $records[$i]["departure"] . "',
						arrival='" . $records[$i]["arrival"] . "',
						dep_coor='" . $records[$i]["dep_coor"] . "',
						arr_coor='" . $records[$i]["arr_coor"] . "'";
						mysqli_query($c,$qm);
				}else{
					if ($records[$i]["arrival"] != null && $records[$i]["arrival"] != "") {
						if ($row["arrival"] == NULL || $row["arrival"] == "") {
						$qm = "UPDATE where_abouts SET arrival='" . $records[$i]["arrival"] . "' WHERE id='" . $row["id"] . "' LIMIT 1";
						mysqli_query($c,$qm);
					}
					}
					
				}
		}


}


if(isset($_POST["resetsync"])){
	$q = "UPDATE attendance_logs SET synched='0' ORDER BY timestamp DESC LIMIT 3000";
	$res = mysqli_query($c,$q);
}
if (isset($_POST["get_settings_from_central"])) {

	$form_data = array(
		'tag'=>"get_central_signatories",
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);


	$jsoned = json_decode($output,true);
	if(count($jsoned) != 0){

		echo $jsoned[0]["value"]  . " - " . $jsoned[1]["value"]  . " - " . $jsoned[2]["value"]  . " - " . $jsoned[3]["value"];


		for($i = 0; $i < count($jsoned);$i++){
			switch ($jsoned[$i]["pref_name"]) {
				case 'oic_name':
				$q = "UPDATE lams_configurations SET value='" . $jsoned[$i]["value"]  . "' WHERE label='oic'";
				$r = mysqli_query($c,$q) ;
				break;
				case 'oic_asds_name':
				$q = "UPDATE lams_configurations SET value='" . $jsoned[$i]["value"] . "' WHERE label='as_oic'";
				$r = mysqli_query($c,$q) ;
				break;
				case 'adminv':
				$q = "UPDATE lams_configurations SET value='" . $jsoned[$i]["value"] . "' WHERE label='adoff'";
				$r = mysqli_query($c,$q) ;
				break;
				case 'hr_head_name':
				$q = "UPDATE lams_configurations SET value='" . $jsoned[$i]["value"] . "' WHERE label='hr'";
				$r = mysqli_query($c,$q) ;
				break;
			}
		}	
	}
}
if (isset($_POST["sync_attlogs_fast"])) {
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];

	$q = "SELECT * FROM attendance_logs  WHERE synched='0' ORDER BY timestamp DESC LIMIT 100";
	$res = mysqli_query($c,$q);
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$q = "UPDATE attendance_logs SET synched='1' WHERE synched='0' ORDER BY timestamp DESC LIMIT 100";
	$res = mysqli_query($c,$q);

	$form_data = array(
		'tag'=>"sync_attendancelogs",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}




if (isset($_POST["sync_attlogs"])) {
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$q = "SELECT * FROM attendance_logs WHERE synched='0' ORDER BY timestamp DESC LIMIT 36";
	$res = mysqli_query($c,$q);
	$toput =  json_encode(mysqli_fetch_all($res,MYSQLI_ASSOC));

	$q = "UPDATE attendance_logs SET synched='1' WHERE synched='0' ORDER BY timestamp DESC LIMIT 36";
	$res = mysqli_query($c,$q);

	$form_data = array(
		'tag'=>"sync_attendancelogs",
		'company'=>$scname,
		'records'=>$toput,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	echo $output;
}

if (isset($_POST["statusofintellisync"])) {
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$form_data = array(
		'tag'=>"statusofintellisync",
		'company'=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}
if (isset($_POST["get_numbers_online"])) {

	        	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];
	
	$form_data = array(
		'tag'=>"get_numbers",
		'company'=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;

}
if (isset($_POST["get_numbers"])) {
	$q = "SELECT * FROM attendance_logs";
	$toecho = "". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM where_abouts";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM employees";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));

	$q = "SELECT * FROM positions";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM departments";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM applied_leave";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));

	$q = "SELECT * FROM dispute";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM holidays";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM multiple_sched";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));

	$q = "SELECT * FROM authorities";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM entitlements_cto";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM scheduled_forcedleave";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));

	$q = "SELECT * FROM entitlements";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	$q = "SELECT * FROM lams_configurations";
	$toecho .= "-". number_format( mysqli_num_rows(mysqli_query($c,$q)));
	echo $toecho;
}
if (isset($_POST["DisplayAccounts"])) {
	// GET MAY ACCOUNT TYPE 
	$q = "SELECT * FROM lams_users WHERE username='" . $_SESSION["username"] . "' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);


	$myacctype = $row["account_type"];
	$q = "SELECT * FROM lams_users WHERE username !='" . $_SESSION["username"] . "'";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {

		$account_type = "ICT Coordinator";

		if($row["account_type"] == "1"){
			$account_type = "Clerk Account";
		}
		echo "<tr>
			<td>" . $row["username"] . "</td>
			<td>" . $account_type . "</td>
			<td>
				";

					if ($row["account_type"] == "1") {

						if ($myacctype == 0) {
							echo "<button class='btn btn-danger btn-sm'>Delete Account</button>";
						}else{
							echo "No Access";
						}
					
					}else{
						echo "Uncontrollable";
					}

				echo"
			</td>
		</tr>";
	}
}
if (isset($_POST["AddNewAccount"])) {
	$username = mysqli_real_escape_string($c,htmlentities( $_POST["username"]));
	$password = md5($_POST["password"]);
	$repass = md5($_POST["repass"]);
	$acc_type = $_POST["acc_type"];
	$q = "SELECT * FROM lams_users WHERE username='" . $username . "'";

	if ($password == $repass) {
		if(mysqli_num_rows(mysqli_query($c,$q)) == "0"){
			// ADD NEW ACCOUNT

			$q = "INSERT INTO lams_users SET username='" . $username  . "', password='" . $password . "', account_type='" . $acc_type . "'";
			mysqli_query($c,$q);
			BackToPage_Message("Account created successfully!","../dashboard.php");
		}else{
			// ACCOUNT ALREADY IN MOTION

			BackToPage_Message("Account is not created because the username already exist.","../dashboard.php");
		}
	}else{
		BackToPage_Message("Password do not match.","../dashboard.php");
	}

}
if (isset($_POST["DeleteAccount"])) {
	# code...
}
if (isset($_POST["ChaneEmployeeProfilePicture"])) {
$employee_id = $_POST["eid_of_emp"];
$target_dir = "../images/logpics/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

$toecho = "";
// RENAME FILE
$temp = explode(".", $_FILES["fileToUpload"]["name"]);
$newfilename = $employee_id  . '.' . end($temp);

$target_file = $target_dir . $newfilename;

$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $toecho .= "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $toecho .= "File is not an image.";
        $uploadOk = 0;
    }
}
// // Check if file already exists
// if (file_exists($target_file)) {
//     $toecho .= "Sorry, file already exists.";
//     $uploadOk = 0;
// }
// Check file size
if ($_FILES["fileToUpload"]["size"] > 1000000) {
    $toecho .= "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $toecho .= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $toecho .= "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    	$q = "INSERT INTO profile_pictures SET eid='" . $employee_id . "', image='" . $employee_id . "." . end($temp) . "'";
    	mysqli_query($c,$q) or die(mysqli_error($c));

        $toecho .= "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        $toecho .= "Sorry, there was an error uploading your file.";
    }
}

BackToPage_Message($toecho,"../emp_management.php");
}
if (isset($_POST["CheckIfKioskEnabled"])) {
	$q = "SELECT * FROM lams_configurations WHERE label='att_kiosk'";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	echo $row["value"];
}
if (isset($_POST["get_time_format"])) {


	$tval = "<strong style='color : white;'>" . date("g") . "<span class='text-muted'>:</span>" . date("i")  .  "</strong>";

	echo "<span style='color: white;'>" .  date("M d") . ", " . date("Y") . "<br>" . date("D") . "</span>|" . $tval;
}
if (isset($_POST["attlogto_kiosk_no_cam"])) {

	$eid = $_POST["eid"];
	if($eid  != ""){

	$q = "SELECT * FROM employees WHERE eid='" . $eid ."' LIMIT 1";

	if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
	$destined_filename = "blank";
		// echo $eid;
			//AUTO IN/OUT MODULE
	$q = "SELECT * FROM attendance_logs WHERE eid='" . $eid . "' AND date='" . date("Y-m-d") ."'";

	$acc_type = "1";
	$countoflogs_today = mysqli_num_rows(mysqli_query($c,$q));

	$valid_log = true;
	if($_POST["need_record"] == "1"){
	switch ($countoflogs_today) {
		case '0':
			$acc_type = "1";
			break;
		case '1':
			$acc_type = "2";
			break;
		case '2':
			$acc_type = "1";
			break;
		case '3':
			$acc_type = "2";
			break;
		default:
			$acc_type = "2";
			$valid_log = false;
		break;
	}
}
if($valid_log){

	if($_POST["need_record"] == "1"){

		$q = "INSERT INTO attendance_picture_logs SET eid='" . $eid . "', picture='" . $destined_filename . "', timestamp='" . date("Y-m-d H:i:s") . "'";
	
	mysqli_query($c,$q);

	$q = "INSERT INTO attendance_logs SET eid='" . $eid . "', timestamp='" . date("Y-m-d H:i:s") . "',date='" . date("Y-m-d") ."',access_type='" . $acc_type . "',ismanual='0',synched='0',access_image=''";
	mysqli_query($c,$q);

		}
		$toecho = "";


		$mq = "SELECT * FROM  employees LEFT JOIN profile_pictures ON profile_pictures.eid = employees.eid WHERE employees.eid='" . $eid . "' LIMIT 1";
		$mres = mysqli_query($c,$mq);
		echo json_encode(mysqli_fetch_array($mres));

	}else{
		echo "Maximum Attendance log exceeded!";
	}


	}else{
		echo "Employee number not found.";
	}

}else{
	echo "Employee Not Found.";
}

}
if (isset($_POST["CheckIfNeedCamera"])) {
		$q = "SELECT * FROM lams_configurations WHERE label='att_allow_no_cam' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	echo $row["value"];
}
if (isset($_POST["AllowNoCamKiosk"])) {

	$uuu = htmlentities($_POST["uuu"]);
	$ppp = md5($_POST["ppp"]);
	$q = "SELECT * FROM lams_users WHERE username='" . $uuu . "' AND password='" . $ppp  . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
		$q = "UPDATE lams_configurations SET value='1' WHERE label='att_allow_no_cam'";
		if (mysqli_query($c,$q)) {
		BackToPage_Message("Kiosk no camera mode is enabled.","../index.php");
		}else{
		BackToPage_Message("Can't change settings.","../index.php");
		}
	}else{
		BackToPage_Message("Account has no authority to change settings.","../index.php");
	}
	
}
if (isset($_POST["GetEmployeesOfTomorrow"])) {
	$q = "SELECT *, positions.name as position_name,departments.name as dept_name,employees.id as theempid FROM employees JOIN positions ON employees.position = positions.id JOIN departments ON employees.department = departments.id ORDER BY date_created DESC";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {

$profilepic = "addpic.jpg";
$xxx = "SELECT * FROM profile_pictures WHERE eid='" . $row["eid"] . "' LIMIT 1";
$xxx_res = mysqli_query($c,$xxx);
if (mysqli_num_rows($xxx_res) != 0) {
	$xxx_row = mysqli_fetch_array($xxx_res);
	$profilepic  = $xxx_row["image"];
}
		echo "<tr>
		<td>
	<p class='hoverableitem' data-eidofemp='" . $row["eid"] . "' onclick='OpenChngeProfilePicMOdal(this)' style='width:50px; height:50px; background-image: url(images/logpics/" . $profilepic . ");  background-size:cover; background-position:center; background-repeat:no-repeat; border-radius: 5px; margin-bottom:1px;' data-toggle='modal' data-target='#m_addprofilepicmodal' title='Click to manage profile picture' >
				";
				if ($row["cont_email"] != "") {
					echo "<img src='images/shared.png' style='width:20px; display:block; float:right; margin-top:31px; margin-right:-10px;'>";
				}else{
					echo "<img src='images/local.png' style='width:20px; display:block; float:right; margin-top:31px; margin-right:-10px;'>";
				}
				echo "
				</p>
		</td>
			<td>			
			
			
			<span class='popdial' data-toggle='popover' data-trigger='hover' title='More Info' data-content='" . 

"Created: " . date("F d, Y h:i a", strtotime($row["date_created"])) . "<br>Updated: " .  date("F d, Y h:i a", strtotime($row["date_updated"])) . "<hr>";

switch ($row["type"]) {
case '1':
echo "Non-Teaching Personnel";
break;
case '2':
echo "Teaching Personnel";
break;
case '3':
echo "Division Personnel";
break;
}

			 echo "' data-html='true'> ". $row["lname"] . ", " . $row["fname"] . "<br><small>#" . $row["eid"];

$r = "SELECT * FROM temp_employee_num WHERE eid='" . $row["eid"] . "'";
if(mysqli_num_rows(mysqli_query($c,$r)) != 0){
?>
<br><span class="badge badge-warning">Temporary</span>
<?php



}
			echo "</small></span>

			</td>
			<td>" . $row["position_name"] . "</td>
			<td>" . $row["dept_name"] . "</td>
			<td>" . $row["schedule"] . "</td>
			<td>";

?>
<div class="dropdown">
  <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">
  </button>
  <div class="dropdown-menu">
  <form action="employee_profile.php" method="GET">
  		<button type="submit" class="dropdown-item" href="#" ><i class="far fa-user-circle"></i> My Profile</button>
  </form>


	<a class="dropdown-item"
	data-accid="<?php echo $row['theempid']; ?>"


	data-eid="<?php echo  $row['eid']; ?>"

	data-type="<?php echo  $row['type']; ?>"
	data-basic_pay="<?php echo  $row['basic_pay']; ?>"
	data-position="<?php echo  $row['position']; ?>"
	data-department="<?php echo  $row['department']; ?>"	
	data-fname="<?php echo  $row['fname']; ?>"
	data-mname="<?php echo  $row['mname']; ?>"
	data-lname="<?php echo  $row['lname']; ?>"

	data-schedule="<?php echo  $row['schedule']; ?>"
	data-cont_email="<?php echo  $row['cont_email']; ?>"
	data-cont_fb="<?php echo  $row['cont_fb']; ?>"
	data-cont_homeadd="<?php echo  $row['cont_homeadd']; ?>"
	data-cont_prinum="<?php echo  $row['cont_prinum']; ?>"
	data-cont_emernum="<?php echo  $row['cont_emernum']; ?>"

	data-leavepref_issingle="<?php echo $row['leavepref_issingle']; ?>"

	onclick="OpenEditEmp(this)"
	href="#" data-toggle="modal" data-target="#modal_acc_edit"


	><i class="far fa-edit"></i> Edit Information</a>
<?php if($row["type"] == "2"){ ?>
	<a class="dropdown-item" href="#" data-toggle="modal" data-idofemp="<?php echo $row['theempid']; ?>" data-nameofemp="<?php echo ucfirst(strtolower($row['fname'])) . ' ' .  ucfirst(strtolower($row['lname'])); ?>" data-emp_id="<?php echo $row['eid']; ?>" data-target="#manage_sched_modal" onclick="Open_multiple_sched_display(this)"><i class="far fa-calendar-alt"></i> Manage Schedule</a>
<?php }else{
	echo '

<a class="dropdown-item disabled" href="#" data-toggle="modal" data-target="#notavmodal"><i class="far fa-calendar-alt"></i> Manage Schedule</a>


	';
} ?>
	<a class="dropdown-item" href="#" data-toggle="modal" data-idofemp="<?php echo $row['theempid']; ?>" data-nameofemp="<?php echo ucfirst(strtolower($row['fname'])) . ' ' .  ucfirst(strtolower($row['lname'])); ?>" data-target="#modal_acc_delete" onclick="OpenDeleteEmployeeModal(this);"><i class="fas fa-user-times"></i> Remove Employee</a>
  </div>
</div>
<?php
			 echo"</td>
		</tr>";
	}
}
if (isset($_POST["cancelaleavetoday"])) {
	$leavereportid = $_POST["leavereportid"];
	// RETURN LEAVE CREDITS REGARDLESLLY
	$q = "SELECT * FROM applied_leave WHERE id='" . $leavereportid . "' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$oldval = $row["paid_days"];
	$leavetype = strtolower(str_replace(" ", "_", $row["leave_type"]));
	$eid = $row["employee_id"];
	CloudQuery("DELETE FROM applied_leave WHERE employee_id='" . $eid . "' AND date_requested='" . $row["date_requested"] . "' AND time_requested='" . $row["time_requested"] . "'");

	if($row["status"] == 0){

		$r = "DELETE FROM scheduled_forcedleave WHERE applied_leave_id='" . $leavereportid . "'";
		mysqli_query($c,$r);
		$q = "DELETE FROM applied_leave WHERE id='" . $leavereportid . "'";
		mysqli_query($c,$q);
			BackToPage_Message("Leave request cancelled successfully!","../LeaveReports.php");
	}else{
		$is_ok = false;
		if($leavetype == "maternity_leave" || $leavetype == "paternity_leave" || $leavetype == "leave_without_pay"){
			$is_ok  = true;
		}else{
			$q = "UPDATE entitlements SET " . $leavetype . " = " . $leavetype . " + " . $oldval . " WHERE emp_id='" . $eid . "'";
			if(mysqli_query($c,$q) or die(mysqli_error($c))){
				$is_ok = true;
			}
		}
		if($is_ok){
		if($leavetype == "cto"){

		$eq = "SELECT * FROM entitlements_cto WHERE empid='" . $eid . "'";
		$eres = mysqli_query($c,$eq);
		if(mysqli_num_rows($eres) != 0 ){
		while($erow = mysqli_fetch_array($eres)){

		$pq = "SELECT val_to FROM entitlements_cto WHERE empid='" . $eid . "' ORDER BY val_to ASC LIMIT 1";
			$prow = mysqli_fetch_array(mysqli_query($c,$pq));
		$neardate = $prow["val_to"];

		if($neardate == $erow["val_to"]){
		// IN USE
		$q = "UPDATE entitlements_cto SET entitlement = entitlement + " . $oldval . " WHERE id='" . $erow["id"] . "'";
			mysqli_query($c,$q) or die(mysqli_error($c));
		}
		}
		}
		}else{
		$r = "DELETE FROM scheduled_forcedleave WHERE applied_leave_id='" . $leavereportid . "'";
		mysqli_query($c,$r);
		}
		$q = "DELETE FROM applied_leave WHERE id='" . $leavereportid . "'";
		mysqli_query($c,$q);

			BackToPage_Message("Leave successfully cancelled.","../LeaveReports.php");
		}else{
			BackToPage_Message("Failed to cancel leave.","../LeaveReports.php");
		}
	}
	
}
if(isset($_POST["loadempwithnopos"])){

	$q = "SELECT * FROM employees WHERE employees.position NOT IN (SELECT id FROM positions)";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
		echo "
		<tr>
		<td>" . $row["lname"] . ", ".  $row["fname"] . "</td>
		<td>
		<button onclick='OpenAssignNewPosition(this)' class='btn btn-sm btn-sm btn-primary' data-empname='" . $row["lname"] . ", ".  $row["fname"] . " " . $row["mname"] .  "' data-theempid='" . $row["id"] . "' data-toggle='modal' data-target='#assnewposmodal'>Assign New Position</button>
		</td>
		</tr>
		";
	}

}

if (isset($_POST["reloadalladdedpositions"])) {
	$q = "SELECT *,COUNT(employees.position) as emp_affecteds, positions.id as position_id FROM positions LEFT JOIN employees ON positions.id = employees.position   GROUP BY positions.name ORDER BY positions.id DESC";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		?>
		<tr>
			<td><?php echo $row["name"]; ?><br>
				<small style="color: gray;">Created: <?php echo date("F d, Y h:i a",strtotime($row["d_create"])); ?> - Edited: <?php echo date("F d, Y h:i a",strtotime($row["d_update"])); ?></small></td>
			<td><?php echo $row["description"]; ?></td>
			<td>
								<div class="dropdown">
  <button type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown">
  </button>
  <div class="dropdown-menu">
    <button onclick="editposition(this)" data-positondescription="<?php echo $row['description'] ?>" data-positionname="<?php echo $row['name'] ?>" data-positionid="<?php echo $row['position_id'] ?>" class="dropdown-item" href="#" data-toggle="modal"  data-target="#posmodal_edit"><i class="far fa-edit"></i> Edit</button>

    <button onclick="deleteposition(this)" data-empaffect="<?php echo $row['emp_affecteds']; ?>" data-positionname="<?php echo $row['name'] ?>" data-positionid="<?php echo $row['position_id'] ?>" class="dropdown-item" href="#" data-toggle="modal" data-target="#posmodal_delete"><i class="far fa-trash-alt"></i> Delete</button>
  </div>
</div>
			</td>
		</tr>
		<?php
	}
}
if (isset($_POST["attendance_report_insights_new"])) {

	$departured = 0;
	$authoritytoattend = 0;
	$onleave = 0;
	$late = 0;
	$absent = 0;

	$timedout = 0;

	$inpdate = $_POST["inpdate"];
	$inpdate_to = $_POST["inpdate_to"];
$emp_type = $_POST["emp_type"];
	$q = "SELECT * FROM employees";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
	$FullBlownRemarks = "";

		$q = "SELECT * FROM attendance_logs WHERE date='" . $inpdate . "' AND eid='" . $row["eid"] . "'";
		$logres = mysqli_query($c,$q);
		$logarray = mysqli_fetch_all($logres,MYSQLI_ASSOC);
		$logcount = mysqli_num_rows($logres);


		$hastimedout = false;
		switch ($logcount) {
				case '0':
				$absent++;
				break;
				case '2':
				if($logarray[1]["access_type"] == "2"){
					$hastimedout = true;
					$timedout++;
				}
				break;
				case '4':
				if($logarray[3]["access_type"] == "2"){
					$hastimedout = true;
					$timedout++;
				}
				break;

		}
$isabsent = false;
$islate = false;
if($logcount != 0){
	// CHECK IF LATE 
		if(strtotime(date("H:i:s",strtotime($logarray[0]["timestamp"]))) > strtotime("9:30")){
		$late++;
			$islate = true;
		}
}else{
	$isabsent = true;
}
		

if($islate == true || $isabsent == true || $hastimedout == true){


		// CHECK IF THIS DAY IS APPLIED FOR LEAVE 

	$leaveday = "SELECT * FROM applied_leave WHERE employee_id='" . $row["eid"] . "' AND status != '2'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){
		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];
		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if($dd_from == $inpdate){
				$onleave++;
				break;
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
		
	}


		// CHECK FOR WHEREABOUTS
		$q = "SELECT * FROM where_abouts WHERE departure LIKE '%" . $inpdate . "%' AND eid='" . $row["eid"] . "'";
		if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
		$departured++;
		}



	$leaveday = "SELECT * FROM authorities WHERE emp_code='" . $row["eid"] . "' AND auth_type='0'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){
		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];
		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if($dd_from == $inpdate){
				$authoritytoattend++;
				break;
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}	
	}

		if ($FullBlownRemarks == "") {
			// $absent++;
		}
	}
	}
	echo $departured  . "," . $authoritytoattend . "," . $onleave . "," . $late . "," . $absent . "," . $timedout;
}

if (isset($_POST["attendance_report_insights"])) {

	$departured = 0;
	$authoritytoattend = 0;
	$onleave = 0;
	$late = 0;
	$absent = 0;

	$timedout = 0;

	$inpdate = $_POST["inpdate"];

$emp_type = $_POST["emp_type"];
	$q = "SELECT * FROM employees";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
	$FullBlownRemarks = "";

		$q = "SELECT * FROM attendance_logs WHERE date='" . $inpdate . "' AND eid='" . $row["eid"] . "'";
		$logres = mysqli_query($c,$q);
		$logarray = mysqli_fetch_all($logres,MYSQLI_ASSOC);
		$logcount = mysqli_num_rows($logres);


		$hastimedout = false;
		switch ($logcount) {
				case '0':
				$absent++;
				break;
				case '2':
				if($logarray[1]["access_type"] == "2"){
					$hastimedout = true;
					$timedout++;
				}
				break;
				case '4':
				if($logarray[3]["access_type"] == "2"){
					$hastimedout = true;
					$timedout++;
				}
				break;

		}
$isabsent = false;
$islate = false;
if($logcount != 0){
	// CHECK IF LATE 
		if(strtotime(date("H:i:s",strtotime($logarray[0]["timestamp"]))) > strtotime("9:30")){
		$late++;
			$islate = true;
		}
}else{
	$isabsent = true;
}
		

if($islate == true || $isabsent == true || $hastimedout == true){


		// CHECK IF THIS DAY IS APPLIED FOR LEAVE 

	$leaveday = "SELECT * FROM applied_leave WHERE employee_id='" . $row["eid"] . "' AND status != '2'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){
		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];
		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if($dd_from == $inpdate){
				$onleave++;
				break;
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
		
	}


		// CHECK FOR WHEREABOUTS
		$q = "SELECT * FROM where_abouts WHERE departure LIKE '%" . $inpdate . "%' AND eid='" . $row["eid"] . "'";
		if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
		$departured++;
		}



	$leaveday = "SELECT * FROM authorities WHERE emp_code='" . $row["eid"] . "' AND auth_type='0'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){
		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];
		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if($dd_from == $inpdate){
				$authoritytoattend++;
				break;
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}	
	}

		if ($FullBlownRemarks == "") {
			// $absent++;
		}
	}
	}
	echo $departured  . "," . $authoritytoattend . "," . $onleave . "," . $late . "," . $absent . "," . $timedout;
}


if (isset($_POST["get_att_report_new"])) {

	$inpdate = $_POST["inpdate"];
	$inpdate_to = $_POST["inpdate_to"];

	$emp_type = $_POST["emp_type"];
	$q = "SELECT * FROM employees";
	$res = mysqli_query($c,$q);
	$toecho = "";
	while ($row = mysqli_fetch_array($res)) {
	$toecho .="<tr>";
	$startingdate = $inpdate;
	$toecho .= "<td>" . $row["lname"] . ", " . $row["fname"] . "</td>";
	$toecho .= "<td>";
	while (strtotime($startingdate) <= strtotime($inpdate_to)) {
		// CHECK IF ABSENT
		$qx = "SELECT * FROM  attendance_logs WHERE eid='" . $row["eid"] . "' AND date='" . $startingdate ."'";
		$resx = mysqli_query($c,$qx);
		$logcount = mysqli_num_rows($resx);
		$logarray = mysqli_fetch_all($resx,MYSQLI_ASSOC);
		
		$result = "";
		if(mysqli_num_rows($resx) == 0){
			$result .= date("M d, y",strtotime($startingdate)) . " - Absent <br>";
		}else{

			$hastimedout = false;
			// $islate = false;
			if($logcount != 0){
				// CHECK IF LATE 
					if(strtotime(date("H:i:s",strtotime($logarray[0]["timestamp"]))) > strtotime("9:30")){
						$result .= date("M d, y",strtotime($startingdate)) . " - Late <br>";
						// $islate = true;
					}
			}

		}
		$toecho .= $result;
		$startingdate = date ("Y-m-d", strtotime("+1 day", strtotime($startingdate)));
	}
	$toecho .= "</td>";
	$toecho .="</tr>";
	}

	echo $toecho;
}


if (isset($_POST["get_att_report"])) {
$inpdate = $_POST["inpdate"];
$emp_type = $_POST["emp_type"];
	$q = "SELECT * FROM employees";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
	$FullBlownRemarks = "";

		$q = "SELECT * FROM attendance_logs WHERE date='" . $inpdate . "' AND eid='" . $row["eid"] . "'";
		$logres = mysqli_query($c,$q);
		$logarray = mysqli_fetch_all($logres,MYSQLI_ASSOC);
		$logcount = mysqli_num_rows($logres);


		$hastimedout = false;
		switch ($logcount) {
				case '0':
				$FullBlownRemarks .= " <img title='Employee has no logs to the system.' src='images/nologs_nano.png' style='width:20px;'><span style='display:none;'>Absent</span> ";
				break;
				default:
				if($row["type"] == "2" && $logcount >= "2"){
$FullBlownRemarks .= " <img title='Employee is present and timed-out.' src='images/out_nano.png' style='width:20px;'><span style='display:none;'>Timed Out</span> ";
					$hastimedout = true;
				}else if($row["type"] != "2" && $logcount >= "4"){
$FullBlownRemarks .= " <img title='Employee is present and timed-out.' src='images/out_nano.png' style='width:20px;'><span style='display:none;'>Timed Out</span> ";
					$hastimedout = true;
				}
					
				break;
		}
$isabsent = false;
$islate = false;
if($logcount != 0){
	// CHECK IF LATE 
		if(strtotime(date("H:i:s",strtotime($logarray[0]["timestamp"]))) > strtotime("9:30")){
			$FullBlownRemarks .= " <img title='Employee is present but late.' src='images/late_nano.png' style='width:20px;'><span style='display:none;'>undertime late</span> ";
			$islate = true;
		}

}else{
	$isabsent = true;
}
		
// if(){
if($islate == true || $isabsent == true || $hastimedout == true){
			echo "<tr>
			<td>" . $row["lname"] . ", " . $row["fname"] . "</td>";


	

		// CHECK IF THIS DAY IS APPLIED FOR LEAVE 

	$leaveday = "SELECT * FROM applied_leave WHERE employee_id='" . $row["eid"] . "' AND status != '2'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){
		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];
		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if($dd_from == $inpdate){
				$FullBlownRemarks .= "<img title='Employee is on leave.' src='images/leave_nano.png' style='width:20px;'><span style='display:none;'>leave report form 6</span>";
				break;
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
		
	}


		// CHECK FOR WHEREABOUTS
		$q = "SELECT * FROM where_abouts WHERE departure LIKE '%" . $inpdate . "%' AND eid='" . $row["eid"] . "'";
		if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
		$FullBlownRemarks .= "<img title='Employee is departured.' src='images/whereabouts_nano.png' style='width:20px;'><span style='display:none;'>whereabouts</span>";
		}



	$leaveday = "SELECT * FROM authorities WHERE emp_code='" . $row["eid"] . "' AND auth_type='0'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){
		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];
		while (strtotime($dd_from) <= strtotime($dd_to)) {
			if($dd_from == $inpdate){
				$FullBlownRemarks .= "<img title='Employee is attending something.' src='images/authority_to_attend_nano.png' style='width:20px;'><span style='display:none;'>authority to attend</span>";
				break;
			}
			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
		
	}

		// CHECK FOR AUTHORITY TO ATTEND

		echo "<td>" . $FullBlownRemarks . "</td>
		</tr>";
}else{
	echo "<tr>
			<td>" . $row["lname"] . ", " . $row["fname"] . "</td><td>Present</td></tr>";
}
// }

	}
}

if(isset($_POST["cancel_approved_leave_report"])){
	$lv_id = $_POST["leave_report_id"];
	$ret_leave_ent = $_POST["ret_leave_ent"];
	$q = "DELETE FROM leave_reports WHERE id=''";
	if (mysqli_query($c,$q)) {
		echo "Leave Report Deleted.";
	}else{
		echo "Failed to delete Leave Report.";
	}
}

if(isset($_POST["approve_an_ata_form"])){
	$idtoapp = $_POST["idtoapp"];
	$q = "UPDATE authorities SET auth_status='1' WHERE id='" . $idtoapp . "'";
	if(mysqli_query($c,$q)){
		echo "Authority approved and ready for printing.";
	}else{
		echo "Functionality Failure, contact your Division ICTU!";
	}
}
if (isset($_POST["reload_ata_data"])) {

$q = "SELECT * FROM authorities WHERE auth_type='0'";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){


	$origin = "<img style='width:50px;' src='images/home_nano.png'>";
	$currentstats = $row["origin"];
	switch ($currentstats) {
		case '0':
			$currentstats = "<center><button class='btn btn-danger btn-sm' onclick='PrepareAuthorityToAttend(this)' data-dataid='" . $row["id"] ."' title='Cancel' data-toggle='modal' data-target='#m_cancelauth'><i class='fas fa-times'></i></button></center>";
			break;
case '1':
// ON MOBILE PHONES
$origin = "<img style='width:50px;' src='images/phone_nano.png'>";
if($row["auth_status"] == 0){
$currentstats = "<center><button class='btn btn-light btn-sm' data-toggle='modal' data-target='#modalapproveata' onclick='SetupApproveModalX(this)' data-idofthis='" . $row["id"] . "'>Approve</button></center>";
}else{
$currentstats = "<center><button class='btn btn-danger btn-sm' onclick='PrepareAuthorityToAttend(this)' data-dataid='" . $row["id"] ."' title='Cancel' data-toggle='modal' data-target='#m_cancelauth'><i class='fas fa-times'></i></button></center>";
}
			
			break;
		case '2':
			$currentstats = "<center>Dissaproved</center>";
			break;
	}

	
	echo "<tr>
		<td>" . date("F d, Y",strtotime($row["record_datetime"])) . "<br>" . date("h:i",strtotime($row["record_datetime"]))  . "</td>
	<td>" . GetUserNameById($row["emp_code"])  . "</td>

	<td><h5>" . $row["destination"] . "</h5><p>" . $row["purpose_description"] . "</p></td>
	<td>" . date("F d, Y",strtotime($row["date_from"])) . "</td>
	<td>" . date("F d, Y",strtotime($row["date_to"])) . "</td>
	<td>" . $origin . "</td>
	<td>" . $currentstats . "</td>
	</tr>";
}

}
function GetUserNameById($employeeid){
global $c;
$q = "SELECT * FROM employees WHERE eid='" . $employeeid . "' LIMIT 1";
$r = mysqli_query($c,$q);
$s = mysqli_fetch_array($r);
return $s["lname"] . ", " . $s["fname"] . " " . $s["mname"];
}
if (isset($_POST["getattendancelogsofdate"])) {
	$emp_type = $_POST["emp_type"];
	$inpdate = $_POST["inpdate"];

$q = "";
	if($emp_type == "0"){
$q = "SELECT *,attendance_logs.timestamp AS attlogstimestamp FROM attendance_logs LEFT JOIN attendance_picture_logs ON attendance_logs.timestamp = attendance_picture_logs.timestamp JOIN employees ON attendance_logs.eid = employees.eid WHERE attendance_logs.date='" . $inpdate  . "'  ORDER BY attendance_logs.timestamp ASC";
	}else{
$q = "SELECT *,attendance_logs.timestamp AS attlogstimestamp FROM attendance_logs LEFT JOIN attendance_picture_logs ON attendance_logs.timestamp = attendance_picture_logs.timestamp JOIN employees ON attendance_logs.eid = employees.eid WHERE attendance_logs.date='" . $inpdate  . "' AND employees.type='" . $emp_type . "' ORDER BY attendance_logs.timestamp ASC";
	}
	
	$res=  mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){

		$acctype = $row["access_type"];
		if ($acctype == "1") {
			$acctype  = "IN";
		}else{
			$acctype  = "OUT";
		}
		$entrytype = $row["ismanual"];
		if ($entrytype == "1") {
			$entrytype  = "<img src='images/manual_entry_nano.png' style='width:50px;'>";
		}else{
			$entrytype  = "<img src='images/bio_smartentry_nano.png' style='width:50px;'>";
		}



		$picdir = "<img style='border:1px solid gray; box-shadow: 1px 2px 2px rgba(0,0,0,0.5); width:85px;' src='images/logpics/" . $row["picture"] . "'>";

		if ($row["picture"] == "") {
			$picdir = "<img style='width:70px;' src='images/blank_nano.png". "'>";
		}else{
			$entrytype  = "<img  src='images/bio_kiosk_nano.png' style='width:50px;'>";
		}
		echo "<tr>
		<td>" . date("h:i a",strtotime($row["attlogstimestamp"])) . "</td>
		<td>" . $picdir . "</td>
		<td>" . $row["lname"] . " " .  $row["fname"] . "</td>
		<td>" . $acctype . "</td>
		<td>" . $entrytype . "</td>
		</tr>";
	}
}
if (isset($_POST["getempidiftemp"])) {
	$eid = $_POST["eid"];
	$q = "SELECT * FROM temp_employee_num WHERE eid='" . $eid . "' LIMIT 1";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 1){
		echo "true";
	}else{
		echo "false";
	}
}
if (isset($_POST["checkifitisempnum"])) {
	$eid = mysqli_real_escape_string($c,htmlentities($_POST["eid"]));
	$q = "SELECT * FROM employees WHERE eid='" . $eid . "' AND eid != '' LIMIT 1";
	$res = mysqli_query($c,$q);
	if(mysqli_num_rows($res) != 0){
		$row = mysqli_fetch_array($res);
		echo $row["lname"] . ", " . $row["fname"] . " " . $row["mname"];
	}else{
		echo "false";
	}
}
if(isset($_POST["get_preferences_info"])){
	$company_name = "";
	$principal_name = "";
	$oic_name = "";
	$admin_v_name = "";
	$hrmo_name = "";
		$assistant = "";
	$q = "SELECT * FROM lams_configurations";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		switch ($row["label"]) {
			case 'cn':
			// station name
			$company_name = $row["value"];
			break;
			case 'prin':
			// principal name
			$principal_name = $row["value"];
			break;
			case 'oic':
			// oic name
			$oic_name = $row["value"];
			break;
			case 'adoff':
			// admin 5 name
			$admin_v_name  = $row["value"];
			break;
			case 'hr':
			// hrmo name
			$hrmo_name = $row["value"];
			break;
			case 'as_oic':
			// hrmo name
			$assistant = $row["value"];
			break;
		}
	}


	echo $company_name . "<->" . $principal_name . "<->" . $oic_name  . "<->" . $admin_v_name  . "<->" . $hrmo_name . "<->" . $assistant;
}
if(isset($_POST["getlastlogs_chart"])){
	$q = "SELECT * FROM attendance_logs GROUP BY date ORDER BY date DESC LIMIT 14";
	$res = mysqli_query($c,$q);
	$days = "";
	$counts = "";
	while ($row = mysqli_fetch_array($res)) {

		$q = "SELECT *,COUNT(eid) as employeecount FROM attendance_logs WHERE date='" . $row["date"] . "' GROUP BY eid";

		$x = mysqli_query($c,$q);
		$row = mysqli_fetch_array($x);
		$counts .= "," . mysqli_num_rows(mysqli_query($c,$q));
		$days .= "," . $row["date"];
	}
	$q = "SELECT id, department, position,type,leavepref_issingle FROM employees";

	$res = mysqli_query($c,$q);
	$emp_count = mysqli_num_rows($res);
	$divPers = 0;
	$divTeach = 0;
	$divNon = 0;
	$pp = 0;
	$singleGUY = 0;
	while($row = mysqli_fetch_array($res)){
		if($row["type"] == "3"){
			$divPers ++;
		}else if ($row["type"] == "1") {
			$divNon++;
		}else{
			$divTeach++;
		}
		if($row["leavepref_issingle"] == "1"){
$singleGUY++;
		}
	}
$q = "SELECT id FROM departments";
$depcount = mysqli_num_rows(mysqli_query($c,$q));
$q = "SELECT id FROM positions";
$poscount = mysqli_num_rows(mysqli_query($c,$q));

//GET RECENTLY ADDEDN EMPLOYEE
$month_ini = new DateTime("first day of last month");
$q = "SELECT id,date_created FROM employees WHERE date_created BETWEEN '" . $month_ini->format('Y-m-d H:i:s') . "' AND '" . date("Y-m-d H:i:s") . "'";
$countresempecrea = mysqli_num_rows(mysqli_query($c,$q));

// GET EMPLOYEES WITH LEAVE TODAY 
	$employessonleavethisday = 0;
	$days_with_leave = array();
	//SELECT IF THIS DAY IS ON LEAVE
	$leaveday = "SELECT date_from, date_to, status FROM applied_leave WHERE status='1'";
	$res_leaveday = mysqli_query($c,$leaveday);

	while($lv_row = mysqli_fetch_array($res_leaveday)){

		$dd_from = $lv_row["date_from"];
		$dd_to = $lv_row["date_to"];

		while (strtotime($dd_from) <= strtotime($dd_to)) {


			if(date("Y-m-d",strtotime($dd_from)) == date("Y-m-d")){
				$employessonleavethisday++;
			}

			$dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
		}
		
	}

	// GET DEPARTURE TODAY 
		$q = "SELECT * FROM where_abouts WHERE departure LIKE '%" . date("Y-m-d") .  "%'";
	$c_dep = mysqli_num_rows(mysqli_query($c,$q));

	// GET ALL TEMP EMPLOYEE NUMBERS 
	$q = "SELECT id,status FROM temp_employee_num WHERE status='0'";
	$count_tmp_emp = mysqli_num_rows(mysqli_query($c,$q));

	echo $days . "|" . $counts . "|" . $emp_count . "|" . $divPers . "|" .  $divTeach . "|" . $divNon . "|" . $singleGUY . "|" . $depcount . "|" . $poscount . "|" . $countresempecrea . "|" . $employessonleavethisday . "|" . $c_dep  . "|" . $count_tmp_emp;
}
if(isset($_POST["checkeid"])){
	$eid = $_POST["eid"];
	if($eid != ""){
		$q = "SELECT eid,fname,lname,mname FROM employees WHERE eid='" . $eid . "' LIMIT 1";

	$res = mysqli_query($c,$q);

	if(mysqli_num_rows($res) != 0){
		$row = mysqli_fetch_array($res);
		echo "<div class='alert alert-danger' role='alert'>
		<i class='fas fa-exclamation-circle'></i> The employee number you entered already belongs to <u>" . $row["lname"] . ", " . $row["fname"] . " " . $row["mname"] . "</u></div>";

	}else{
		echo "true";
	}
	}
	
}
if(isset($_POST["compute_days_reg"])){
	$dd_f = $_POST["dd_f"];
	$dd_t = $_POST["dd_t"];
	// Declare two date...
	$start_date = strtotime($dd_f); 
	$end_date = strtotime($dd_t ); 
	  
	// Get the difference and divide into...
	// Total no. seconds 60/60/24 to get...
	// Number of day...
	$computation = (($end_date - $start_date) /60/60/24) + 1;
	if($computation > 0){
		echo $computation;
	}else{
		echo 0;
	}
	
}

if(isset($_POST["load_auth_employee_for_parLeave"])){

	$selected_leave_type =$_POST["selected_leave_type"];
		$q  = "SELECT * FROM auth_leaveparental JOIN employees ON auth_leaveparental.eid = employees.eid AND leave_type='" . $selected_leave_type . "'";
		$res = mysqli_query($c,$q);
		while ($row = mysqli_fetch_array($res)) {
			echo "<option value='" . $row["eid"]."'>" . $row["lname"] . ", " . $row["fname"] . " " . $row["mname"] . "</option>";
		}
}

if(isset($_POST["get_today_summary"])){
	$c_in = 0;
	$c_out = 0;
	$c_absent = 0;

	$c_dep = 0;
	$c_arr = 0;

	//GET ALL EMPLOYEES WITH IN
	$q = "SELECT * FROM attendance_logs WHERE attendance_logs.date='" . date("Y-m-d") . "' GROUP BY attendance_logs.eid";
	$c_in = mysqli_num_rows(mysqli_query($c,$q));

	// GET ALL EMPLOYEES WITH OUT
	$q = "SELECT * FROM attendance_logs WHERE id in (SELECT MAX(id) FROM attendance_logs WHERE date='" . date("Y-m-d") . "'  GROUP BY eid) AND date='" . date("Y-m-d") . "'";

	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		if($row["access_type"] == "2"){
				$c_out += 1;
		}
	}
	//GET ALL ABSENT EMPLOYEES
	$q = "SELECT * FROM employees WHERE eid NOT IN(SELECT eid FROM attendance_logs WHERE date='" . date("Y-m-d") . "' GROUP BY eid)";
	$c_absent = mysqli_num_rows(mysqli_query($c,$q));

	$q = "SELECT * FROM where_abouts WHERE departure LIKE '%" . date("Y-m-d") .  "%'";
	$c_dep = mysqli_num_rows(mysqli_query($c,$q));

	$q = "SELECT * FROM where_abouts WHERE departure LIKE '%" . date("Y-m-d") .  "%' AND arrival IS NOT NULL";
	$c_arr = mysqli_num_rows(mysqli_query($c,$q));
	echo  $c_in ."," . $c_out . "," . $c_absent . "," . $c_dep . "," . $c_arr;
}
if(isset($_POST["get_kiosk_logs"])){

$q = "SELECT employees.fname,employees.lname,employees.eid,employees.id,attendance_logs.access_type,attendance_logs.timestamp,attendance_logs.eid FROM attendance_logs JOIN employees ON attendance_logs.eid = employees.eid ORDER BY attendance_logs.timestamp DESC LIMIT 3";

$res= mysqli_query($c,$q);
if(mysqli_num_rows($res) != 0){
while ($row = mysqli_fetch_array($res)) {
	$atype = $row["access_type"];

	if($atype == "1"){
		$atype = '<small class="badge badge-primary" style="border-radius: 5px;">IN</small>';
	}else{
		$atype = '<small class="badge badge-danger" style="border-radius: 5px;">OUT</small>';
	}

	$namevalue = strtolower($row["fname"]);
	$dynapic = "images/logpics/blank.jpg";
	// CHECK IF HAS KIOSK PICTURE 
	$q = "SELECT picture,timestamp FROM attendance_picture_logs WHERE timestamp='" . $row["timestamp"] . "' LIMIT 1";
	$pic_res = mysqli_query($c,$q);
	if(mysqli_num_rows($pic_res) != 0){
		$pic_row = mysqli_fetch_array($pic_res);
		$dynapic = "images/logpics/" . $pic_row["picture"] . ".jpg";
	}

	$q = "SELECT image,eid FROM profile_pictures WHERE eid='" . $row["eid"] . "' ORDER BY id DESC LIMIT 1";
	$pic_res = mysqli_query($c,$q);
	if(mysqli_num_rows($pic_res) != 0){
		$pic_row = mysqli_fetch_array($pic_res);
		$dynapic = "images/logpics/" . $pic_row["image"];
	}

	echo ' 
	<div class="form-group" style="color:while !important; margin-bottom: 0px; padding:8px;">         
            <div class="row">
            	<div class="col-sm-1" style="padding:5px; text-align:center;">
            		<div style="width:60px; display:inline-block; border-radius:5px; height:60px; border:none; box-shadow: 0px 5px 10px rgba(0,0,0,0.2); background: url(' . $dynapic . '); background-position:center; background-size:cover;"></div>
        		</div>
            	<div class="col-sm-11" style="padding:30px; padding-top:5px; padding-bottom:2px;">
            		<span><h5 class="ultratitle attlogtxt float-right"><strong>' . $atype . '</strong><span style="color:while !important; margin-left:10px;">' . date("g:i",strtotime($row["timestamp"])) . '</span>


            		</h5><h5 class="attlogtxt ultratitle"><strong>' . $row["lname"] . "</strong><br><small><small>" .  $row["fname"] . '</small></small></h5>
            		</span>
            	</div>
        </div>
        </div>';
}
}else{
	echo "<div class='form-group' style='color:black;'><p>The're no logs to show right now.</p></div>";
}

echo ' 
	<div class="form-group" style="color:white; margin-bottom: 0px; padding:8px;">         
            <div class="row">
            	<div class="col-sm-12 text-muted" style="padding:5px; text-align:center;">
            		<center><i class="fas fa-info-circle"></i> Login to see the all.</center>
        		</div>
            	</div>
        </div>
        </div>';
	
}
if (isset($_POST["attlogto_kiosk"])) {
		$picdata = $_POST["picture_picdata"];
		$eid = $_POST["eid"];
		if($eid  != ""){

		$q = "SELECT * FROM employees WHERE eid='" . $eid ."' LIMIT 1";

		if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
		list($type, $picdata) = explode(';', $picdata);
		list(, $picdata)      = explode(',', $picdata);
		$picdata = base64_decode($picdata);
		// echo $picdata;
		$destined_filename = uniqid();
		// echo $eid;


			//AUTO IN/OUT MODULE
	$q = "SELECT * FROM attendance_logs WHERE eid='" . $eid . "' AND date='" . date("Y-m-d") ."'";

	$acc_type = "1";
	$countoflogs_today = mysqli_num_rows(mysqli_query($c,$q));

	$valid_log = true;
	switch ($countoflogs_today) {
		case '0':
			$acc_type = "1";
			break;
		case '1':
			$acc_type = "2";
			break;
		case '2':
			$acc_type = "1";
			break;
		case '3':
			$acc_type = "2";
			break;
		default:
			$acc_type = "2";
			$valid_log = false;
		break;
	}

if($valid_log){
		$q = "INSERT INTO attendance_picture_logs SET eid='" . $eid . "', picture='" . $destined_filename . "', timestamp='" . date("Y-m-d H:i:s") . "'";
	mysqli_query($c,$q);

	$q = "INSERT INTO attendance_logs SET eid='" . $eid . "', timestamp='" . date("Y-m-d H:i:s") . "',date='" . date("Y-m-d") ."',access_type='" . $acc_type . "',ismanual='0',synched='0',access_image=''";
	mysqli_query($c,$q);

		file_put_contents('../images/logpics/' . $destined_filename . '.jpg', $picdata);



		$toecho = "";


		$mq = "SELECT * FROM profile_pictures WHERE eid='" . $eid . "' LIMIT 1";
		$mres = mysqli_query($c,$mq);
		if(mysqli_num_rows($mres) != 0){
			$mrow = mysqli_fetch_array($mres);
			$toecho .= "<div class='flipper' style='float: left; margin-right:20px; border-radius: 5px ; background-image: url(images/logpics/" . $mrow["image"] . "); height:100px; width:100px; background-size:cover; background-position:center;'></div>";
		}

		$toecho .= "<span>Login Accepted.</span>";
		echo $toecho;

	}else{
		echo "Maximum Attendance log exceeded!";
	}


	}else{
		echo "Employee number not found.";
	}

}else{
	echo "Employee Not Found.";
}

}


if (isset($_POST["res_positions"])) {
	$q = "DELETE FROM positions";
	mysqli_query($c,$q);
	$company = mysqli_real_escape_string($c,htmlentities($_POST["company"]));

	$form_data = array(
		'tag'=>"res_positions",
		"company"=>$company,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	$json_attlogs =json_decode($output,true);
	$errors = 0;
	$success = 0;
	$total = count($json_attlogs);
	for ($i=0; $i < count($json_attlogs); $i++) { 
		$ins = "INSERT INTO positions SET
		name='" . $json_attlogs[$i]["name"] . "',
		description='" . $json_attlogs[$i]["description"] ."',
		d_create='" . $json_attlogs[$i]["d_create"] ."',
		d_update='" . $json_attlogs[$i]["d_update"] ."'";

		if(mysqli_query($c,$ins)){
			$success++;
		}else{
			$errors++;
		}
	}

	echo  "Inserted " . number_format($success) . "/" . number_format($total) . " positions with " . number_format($errors) . " errors.";
}



if (isset($_POST["res_departments"])) {
	$q = "DELETE FROM departments";
	mysqli_query($c,$q);
	$company = mysqli_real_escape_string($c,htmlentities($_POST["company"]));

	$form_data = array(
		'tag'=>"res_departments",
		"company"=>$company,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	$json_attlogs =json_decode($output,true);
	$errors = 0;
	$success = 0;
	$total = count($json_attlogs);
	for ($i=0; $i < count($json_attlogs); $i++) { 
		$ins = "INSERT INTO departments SET
		name='" . $json_attlogs[$i]["name"] . "',
		description='" . $json_attlogs[$i]["description"] ."',
		d_make='" . $json_attlogs[$i]["d_make"] ."',
		d_modif='" . $json_attlogs[$i]["d_modif"] ."'";

		if(mysqli_query($c,$ins)){
			$success++;
		}else{
			$errors++;
		}
	}

	echo  "Inserted " . number_format($success) . "/" . number_format($total) . " departments with " . number_format($errors) . " errors.";
}

if (isset($_POST["res_attendancelogs"])) {
	$q = "DELETE FROM attendance_logs";
	mysqli_query($c,$q);
	$company = mysqli_real_escape_string($c,htmlentities($_POST["company"]));

	$form_data = array(
		'tag'=>"res_attendancelogs",
		"company"=>$company,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	$json_attlogs =json_decode($output,true);
	$errors = 0;
	$success = 0;
	$total = count($json_attlogs);
	for ($i=0; $i < count($json_attlogs); $i++) { 
		$ins = "INSERT INTO attendance_logs SET
		eid='" . $json_attlogs[$i]["eid"] . "',
		access_type='" . $json_attlogs[$i]["access_type"] ."',
		access_image='" . $json_attlogs[$i]["access_image"] ."',
		date='" . $json_attlogs[$i]["date"] ."',
		timestamp='" . $json_attlogs[$i]["timestamp"] ."',
		ismanual='" . $json_attlogs[$i]["ismanual"] ."',
		synched='1'
		";

		if(mysqli_query($c,$ins)){
			$success++;
		}else{
			$errors++;
		}
	}

	echo  "Inserted " . number_format($success) . "/" . number_format($total) . " attendance_logs with " . number_format($errors) . " errors.";
}
if(isset($_POST["res_employees"])){
		$q = "DELETE FROM employees";
	mysqli_query($c,$q);
	$company = mysqli_real_escape_string($c,htmlentities($_POST["company"]));

	$form_data = array(
		'tag'=>"res_employees",
		"company"=>$company,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	$json_of_employees =json_decode($output,true);
	$errors = 0;
	$success = 0;
	$total = count($json_of_employees);
	for ($i=0; $i < count($json_of_employees); $i++) { 
		$ins = "INSERT INTO employees SET
		eid='" . $json_of_employees[$i]["eid"] . "',
		rfid='" . $json_of_employees[$i]["rfid"] . "',
		type='" . $json_of_employees[$i]["type"] . "',
		basic_pay='" . $json_of_employees[$i]["basic_pay"] . "',
		position='" . $json_of_employees[$i]["position"] . "',
		department='" . $json_of_employees[$i]["department"] . "',
		fname='" . $json_of_employees[$i]["fname"] . "',
		mname='" . $json_of_employees[$i]["mname"] . "',
		lname='" . $json_of_employees[$i]["lname"] . "',
		fid='" . $json_of_employees[$i]["fid"] . "',
		ftemplate='" . $json_of_employees[$i]["ftemplate"] . "',
		schedule='" . $json_of_employees[$i]["schedule"] . "',
		synched='1'
		";

		if(mysqli_query($c,$ins) ){
			$success++;
		}else{
			$errors++;
		}
	}

	echo  "Inserted " . number_format($success) . "/" . number_format($total) . " employees with " . number_format($errors) . " errors.";
}

if(isset($_POST["changeschoolnamenow"])){
	$sc_name_now = mysqli_real_escape_string($c,htmlentities($_POST["sc_name_now"]));
	$q = "UPDATE lams_configurations SET value='" . $sc_name_now . "' WHERE label='cn' LIMIT 1";
	$res =  mysqli_query($c,$q);
		if ($res) {
		log_system_action("Fixed school name.");
		BackToPage_Message("New School Name Applied!","../index.php");
	}else{
		log_system_action("Unable to fix school name.");
		BackToPage_Message("Unable to Apply School Name, Contact the SDO Marikina ICTU to report this problem.","../index.php");
	}
}
if(isset($_POST["validate_schoolname"])){
	$q = "SELECT * FROM lams_configurations WHERE label='cn' LIMIT 1";
	$res = mysqli_query($c,$q);
	if(mysqli_num_rows($res) == 0){
		//NO SETTINGS
		$q = "INSERT INTO lams_configurations SET label='cn',value='', description='The name of your school or division.', value_type='School / Division Office', hidden='0'";
		mysqli_query($c,$q);
		// 0 - CREATE A PREFERENCE AND SHOW RENAME SCHOOL
		echo "1,School Name Preferences is damage.";
	}else{
		// CHECK IF HAS USERNAME
		$row = mysqli_fetch_array($res);
		if ($row["value"] == null || $row["value"] == "") {
			// SHOW RENAME SCHOOL
			echo "2,School Name is empty.";
		}else{
			// ALL IS OKAY
			echo "3,All Okay";
		}
	}


	$q = "SELECT * FROM lams_configurations WHERE label='prin' AND value='Renz Steven Katingtiti' LIMIT 1";
	$cres = mysqli_query($c,$q);
	if(mysqli_num_rows($cres) != 0){
		$cres = mysqli_fetch_array($cres);
		$idofdata = $cres["id"];
		$a = "UPDATE lams_configurations SET value='(No Name - Please Configure CDTRS Settings)' WHERE id='" . $idofdata . "'";
		mysqli_query($c,$a);
	}
}
if (isset($_POST["getlatestversion"])) {
	$hour =date("H",strtotime("hour"));
	$form_data = array(
	'tag'=>"getlatestversion",
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}
if(isset($_POST["addnewschedule"])){
	$new_sched_day = $_POST["new_sched_day"];
	$new_shed_in = $_POST["new_shed_in"];
	$new_shed_out = $_POST["new_shed_out"];
	$eff_date = $_POST["eff_date"];
	$comp_sched = date("h:i a",strtotime($new_shed_in)) . "," . date("h:i a",strtotime($new_shed_out));
	$emp_eid = $_POST["emp_eid"];

	$q = "INSERT INTO multiple_sched SET eid='" . $emp_eid . "', dayname='" . $new_sched_day . "', schedule='" . $comp_sched . "', created='" . date("Y-m-d") . "', effectivity='" . $eff_date . "'";
	$res = mysqli_query($c,$q) ;

	if ($res) {
		log_system_action("Added a new schedule for " . $emp_eid);
		BackToPage_Message("New Schedule Added!","../emp_management.php");
	}
}
if(isset($_POST["multiple_sched_display"])){
	$eid = $_POST["eid"];
	$q = "SELECT * FROM multiple_sched WHERE eid='" . $eid . "' AND effectivity in (SELECT MAX(effectivity) FROM multiple_sched WHERE effectivity < '" . date("Y-m-d H:i:s") . "' AND eid='" . $eid . "' GROUP BY dayname) GROUP BY dayname";
	$res=  mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){

		$shedDisect = explode(",", $row["schedule"]);
		echo "
			<tr>
			<td>" . $row["dayname"] . "</td>
			<td>" . $shedDisect[0] . "</td>
			<td>" . $shedDisect[1] . "</td>
			</tr>
		";
	}
}

if (isset($_POST["multiple_sched_display_ineffectual"])) {
	$eid = $_POST["eid"];
	$q = "SELECT * FROM multiple_sched WHERE eid='" . $eid . "'";
	$res=  mysqli_query($c,$q);
	$res=  mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){


		$shedDisect = explode(",", $row["schedule"]);
		echo "
			<tr>
			<td>" . $row["dayname"] . "</td>
			<td>" . $shedDisect[0] . "-" .  $shedDisect[1] . "</td>
			<td>" . date("F d, Y", strtotime($row["effectivity"])) . "</td>
			<td><button class='btn btn-sm btn-danger'  data-emp_id='" . $row["eid"]  . "' onclick='deleteschedmul(this)' data-delidval='" . $row["id"] ."'>" . '<i class="fas fa-times"></i>' . "</button></td>
			</tr>
		";
	}
}
if(isset($_POST["del_a_sched_mul"])){
	$q = "DELETE FROM multiple_sched WHERE id='" . $_POST["idtodelsched"] . "'";
	$res = mysqli_query($c,$q);
	if($res){
		echo "Schedule Successfully Deleted!";
	}else{
		echo "Schedule Failed to Delete. Contact the SDO MARIKINA ICTU DEPARTMENT to report this issue.";
	}
}
if(isset($_POST["get_quote"])){

	$access_type = $_POST["access_type"];
	$hour =date("H",strtotime("hour"));
		$form_data = array(
			'tag'=>"get_quote",
			'access_type'=>$access_type,
			'hour'=>$hour,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}
if(isset($_POST["getlast_tensecond_log"])){
$q = "SELECT * FROM attendance_logs JOIN employees ON attendance_logs.eid = employees.eid ORDER BY attendance_logs.timestamp DESC LIMIT 1";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);
$diff = (strtotime(date("Y-m-d H:i:s")) - strtotime($row["timestamp"]));
if($diff < 7){
	echo json_encode($row);
}else{
	echo "false";
}
}
if(isset($_POST["addnewdepartment"])){
	$department_name = mysqli_real_escape_string($c,htmlentities($_POST["department_name"]));
	$department_description = mysqli_real_escape_string($c,htmlentities($_POST["department_description"]));
	$q = "SELECT * FROM departments WHERE name='" . $department_name . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
	$q = "INSERT INTO departments SET name='" . $department_name . "', description='" . $department_description . "'";
	log_system_action("Added a new department");
	BackToPage(mysqli_query($c,$q),"../emp_departments.php");
	}else{
	log_system_action("Attempted to add a new department but it already exist in the database.");
	BackToPage_Message("Department name already exist.","../emp_departments.php");
	}
}
if(isset($_POST["editdepartmentinfo"])){
	$e_i = $_POST["e_i"];
	$e_n = mysqli_real_escape_string($c,htmlentities($_POST["e_n"]));
	$e_d = mysqli_real_escape_string($c,htmlentities($_POST["e_d"]));

	$q = "SELECT * FROM departments WHERE id!='" . $e_i . "' AND name='" . $e_n . "'";

	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
	$q = "UPDATE departments SET name='" . $e_n . "', description='" . $e_d . "',d_modif='" . date("Y-m-d H:i:s") . "' WHERE id='" . $e_i . "' LIMIT 1";
	log_system_action("Edited a department information");
	BackToPage(mysqli_query($c,$q),"../emp_departments.php");
	}else{
	log_system_action("Attempted to edit a department but it already exist in the database.");
	BackToPage_Message("Your Department name already exist.","../emp_departments.php");
	}
}
if(isset($_POST["deletedepartment"])){
	$e_id = $_POST["e_id"];
	$q = "DELETE FROM departments WHERE id='" . $e_id . "' LIMIT 1";
	log_system_action("Deleted a department information");
	BackToPage(mysqli_query($c,$q),"../emp_departments.php");
}
if(isset($_POST["get_current_holiday"])){
	$current_date = date("m-d", strtotime(date("Y-m-d")));
	$q = "SELECT * FROM holidays WHERE date='" . $current_date . "'";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	if(mysqli_num_rows($res)){
		?>
		<div class="alert alert-primary" style="background-color: transparent; color: white; border-color: #f1c40f; background-color: rgba(241, 196, 15,0.05);" role="alert">
			<h5 class="badge badge-warning badge-lg"><i  class="far fa-star"></i> TODAY IS <i  class="far fa-star"></i></h5><br>
		  <h4 class="ultrabold"><?php echo $row["name"]; ?></h4>
		</div>
		<?php
	}
}
if(isset($_POST["assignunit_now"])){
	$idofemp = $_POST["idofemp"];
	$NewSelectedPosition = $_POST["NewSelectedPosition"];

	$q = "UPDATE employees SET position='" . $NewSelectedPosition . "' WHERE id='" . $idofemp . "'";

	if (mysqli_query($c,$q)) {
		echo "Employee successfully assigned to new position.";
		log_system_action("Re-assigned a new position for and employee...");
	}else{
		echo "Employee failed to assign to new position.";
		log_system_action("Re-assigned a new position for and employee has failed...");
	}
}



if(isset($_POST["assignunitDepartment_now"])){
	$idofemp = $_POST["idofemp"];
	$NewSelectedDepartment = $_POST["NewSelectedDepartment"];

	$q = "UPDATE employees SET department='" . $NewSelectedDepartment . "' WHERE id='" . $idofemp . "'";

	log_system_action("Re-assigned a new department for and employee...");
	BackToPage(mysqli_query($c,$q),"../emp_departments.php");

}
if (isset($_POST["addnewposition"])) {
	$positionname = mysqli_real_escape_string($c,htmlentities($_POST["positionname"]));
	$positiondescription = mysqli_real_escape_string($c,htmlentities($_POST["positiondescription"]));

	$q = "SELECT * FROM positions WHERE name='" . $positionname . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) ==0){
$q = "INSERT INTO positions SET name='" . $positionname . "', description='" . $positiondescription . "'";
	
	
	// BackToPage(mysqli_query($c,$q),"../emp_positions.php");


	if (mysqli_query($c,$q)) {
		echo "Employee position has been successfully added!";
		log_system_action("Added new position.");
	}
	
	}else{
		// BackToPage_Message("New position cannot be added because it already exist.","../emp_positions.php");
		echo "Position aready exist!";
	}
}
if(isset($_POST["sub_editposition"])){
	$editpostid = $_POST["editpostid"];
	$positionname = mysqli_real_escape_string($c,htmlentities( $_POST["positionname"]));
	$positiondescription = mysqli_real_escape_string($c,htmlentities($_POST["positiondescription"]));

	$q = "SELECT * FROM positions WHERE id != '" . $editpostid . "' AND name='" . $positionname . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "UPDATE positions SET name='" . $positionname . "', description='" . $positiondescription . "',d_update='" . date("Y-m-d H:i:s") . "' WHERE id='" . $editpostid . "'";
		if (mysqli_query($c,$q)) {
			echo "Changes has been saved!";
		}
	log_system_action("Edited a position data.");

}else{
	echo "Error saving changes.";
	log_system_action("Attempted to edit a position data but it already exist.");
	// BackToPage_Message("Name of the position is already taken.","../emp_positions.php");
}
	
}
if (isset($_POST["sub_deleteposition"])) {
	$idofpositiondatatodelete = $_POST["idofpositiondatatodelete"];
	$q = "DELETE FROM positions WHERE id='" . $idofpositiondatatodelete ."'";
	if (mysqli_query($c,$q)) {
		echo "Position has been deleted!";
		log_system_action("Deleted a position.");
	}else{
		echo "Error deleting this position.";
		log_system_action("Failed to delete a position.");
	}
}
if(isset($_POST["delete_acc_now"])){
	$accid = $_POST["accid"];

	$q = "DELETE FROM employees WHERE id='" . $accid . "' LIMIT 1";

	$ss = "SELECT * FROM employees WHERE id='" . $accid . "' LIMIT 1";
	$select_row = mysqli_fetch_array(mysqli_query($c,$ss));


	CloudQuery("DELETE FROM employees WHERE eid='" .  $select_row["eid"] . "'");


	$select_row_jsoned = json_encode($select_row);

	if(mysqli_query($c,$q)){
		log_system_action_admin("Employee Deleted : " . $select_row_jsoned);
		BackToPage_Message("Employee Deleted!","../emp_management.php");
	}else{
		log_system_action_admin("Cannot Delete Employee -> " . $select_row["fname"] . ' ' . $select_row["lname"] . " : " . $select_row["eid"]);
		BackToPage_Message("Cannot Delete Employee.","../emp_management.php");
	}
}
if (isset($_POST["editemployee"])) {
	$oldempnumber =  $_POST["oldempnumber"];
	$data_id = $_POST["data_id"];
	$employeenumber =  htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["employeenumber"])));
	$bazicpay = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["bazicpay"])));
	$fname = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["fname"])));
	$mname = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["mname"])));
	$lname = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["lname"])));
	$pozition = $_POST["pozition"];
	$deptname = $_POST["deptname"];
	$emptype = $_POST["emptype"];
	$emp_email = strtolower(htmlentities($_POST["emp_email"]));
	$emp_fbacc = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_fbacc"])));
	$emp_homeadd = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_homeadd"])));
	$emp_primarycontnum = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_primarycontnum"])));
	$emp_cont_num = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_cont_num"])));
	$issingle = "0";
	if(isset($_POST["issingle"])){
		$issingle = "1";
	}
	$tm_in = strtoupper(date("h:i a", strtotime($_POST["tm_in"])));

	$br_in ="";
$br_out ="";

if(isset($_POST["br_in"]) && isset($_POST["br_out"])){
	if($_POST["br_in"]!= "" && $_POST["br_out"]!= ""){
	$br_in = strtoupper(date("h:i a", strtotime($_POST["br_in"])));
	$br_out = strtoupper(date("h:i a", strtotime($_POST["br_out"])));
}
}
	
	

	$tm_out = strtoupper(date("h:i a", strtotime($_POST["tm_out"])));

	$arraged_schedule = $tm_in . "," . $tm_out . "," . $br_in . "," . $br_out;

	$vq = "SELECT * FROM employees WHERE eid='" . $employeenumber . "' AND id != '" . $data_id . "'";

	if(mysqli_num_rows(mysqli_query($c,$vq)) == 0){
	$q = "UPDATE employees SET
	eid='" . $employeenumber . "',

	type='" . $emptype . "',
	basic_pay='" . $bazicpay . "',
	position='" . $pozition . "',
	department='" . $deptname . "',	
	fname='" . $fname . "',
	mname='" . $mname . "',
	lname='" . $lname . "',


	schedule='" . $arraged_schedule . "',
	cont_email='" . $emp_email . "',
	cont_fb='" . $emp_fbacc . "',
	cont_homeadd='" . $emp_homeadd . "',
	cont_prinum='" . $emp_primarycontnum . "',
	cont_emernum='" . $emp_cont_num . "',
	synched='0',
	leavepref_issingle='" . $issingle . "',
	date_updated='" . date("Y-m-d H:i:s") . "'
	WHERE id='" . $data_id . "'";

	$res = mysqli_query($c,$q) ;

	if($res){
		// UPDATE LEAVE REPORTS
		$q = "UPDATE attendance_logs SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE DISPUTE RECORDS
		$q = "UPDATE dispute SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE ENTITLEMENTS 
		$q = "UPDATE entitlements SET emp_id='" .  $employeenumber . "', emp_n='" . $lname . ", " . $fname . " " . $mname . "' WHERE emp_id='" . $oldempnumber . "'";
		mysqli_query($c,$q);

		// UPDATE ENTITLEMENTS CTO 
		$q = "UPDATE entitlements_cto SET empid='" .  $employeenumber . "' WHERE empid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE SCHEDULED FORCED LEAVE 
		$q = "UPDATE scheduled_forcedleave SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE WHERE ABOUTS 
		$q = "UPDATE where_abouts SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE MULTIPLE SCHEDULE
		$q = "UPDATE multiple_sched SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE AUTH LEAVE PARENTAL 
		$q = "UPDATE auth_leaveparental SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		// UPDATE ATTENDANCE PICTURE LOGS
		$q = "UPDATE attendance_picture_logs SET eid='" .  $employeenumber . "' WHERE eid='" . $oldempnumber . "'";
		mysqli_query($c,$q);
		//UPDATE BASICS FROM ONLINE
		CloudQuery("UPDATE employees SET cont_email='" . $emp_email . "' WHERE eid='" . $employeenumber . "'");

		if(isset($_POST["istmpidchecked"])){
			//Check if employee id is exisiting already
			$q = "SELECT * FROM temp_employee_num WHERE eid='" . $employeenumber . "'";
			$res = mysqli_query($c,$q);
			// Add employee id
			if(mysqli_num_rows($res) ==0){

				$q = "DELETE FROM temp_employee_num WHERE eid='" . $oldempnumber . "'";
				mysqli_query($c,$q);

			
				$q = "INSERT INTO temp_employee_num SET eid='" . $employeenumber . "', date_added='" . date("Y-m-d H:i:s") . "',status='0'";
				mysqli_query($c,$q);
			}
		}else{
			//Employee ID will no longer be temp so it needs to be deleted in the temporary id table. 
			$q = "DELETE FROM temp_employee_num WHERE eid='" . $employeenumber . "'";
			mysqli_query($c,$q);
		}

		 BackToPage_Message("Employee Updated Successfully!","../emp_management.php");
	}else{
		 BackToPage_Message("Error updating new employee. Contact the ICTU Department to report this error.","../emp_management.php");
	}
	}else{
		BackToPage_Message("Employee number already exist!, nothing happen.","../emp_management.php");
	}

}

if (isset($_POST["AddNewEmployeeNow"])) {
	$employeenumber =  htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["employeenumber"])));
	$bazicpay = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["bazicpay"])));
	$fname = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["fname"])));
	$mname = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["mname"])));
	$lname = htmlentities(mysqli_real_escape_string($c,strtoupper($_POST["lname"])));
	$pozition = $_POST["pozition"];
	$deptname = $_POST["deptname"];                              
	$emptype = $_POST["emptype"];
	$emp_email = strtolower(htmlentities($_POST["emp_email"]));
	$emp_fbacc = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_fbacc"])));
	$emp_homeadd = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_homeadd"])));
	$emp_primarycontnum = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_primarycontnum"])));
	$emp_cont_num = strtolower(htmlentities(mysqli_real_escape_string($c,$_POST["emp_cont_num"])));
	$issingle = "0";
	if(isset($_POST["issingle"])){
	$issingle = "1";
	}

	$tm_in = strtoupper(date("h:i a", strtotime($_POST["tm_in"])));


	$br_in ="";
$br_out ="";

if(isset($_POST["br_in"]) && isset($_POST["br_out"])){
	if($_POST["br_in"]!= "" && $_POST["br_out"]!= ""){
	$br_in = strtoupper(date("h:i a", strtotime($_POST["br_in"])));
	$br_out = strtoupper(date("h:i a", strtotime($_POST["br_out"])));
}
}

	$tm_out = strtoupper(date("h:i a", strtotime($_POST["tm_out"])));

	$arraged_schedule = $tm_in . "," . $tm_out . "," . $br_in . "," . $br_out;

	$vq = "SELECT * FROM employees WHERE eid='" . $employeenumber . "'";

	if(mysqli_num_rows(mysqli_query($c,$vq)) == 0){
	$q = "INSERT INTO employees SET
	eid='" . $employeenumber . "',

	type='" . $emptype . "',
	basic_pay='" . $bazicpay . "',
	position='" . $pozition . "',
	department='" . $deptname . "',	
	fname='" . $fname . "',
	mname='" . $mname . "',
	lname='" . $lname . "',


	schedule='" . $arraged_schedule . "',
	cont_email='" . $emp_email . "',
	cont_fb='" . $emp_fbacc . "',
	cont_homeadd='" . $emp_homeadd . "',
	cont_prinum='" . $emp_primarycontnum . "',
	cont_emernum='" . $emp_cont_num . "',
	synched='0',
	leavepref_issingle='" . $issingle . "'";

	$res = mysqli_query($c,$q) ;

	if($res){

		if(isset($_POST["id_is_temporary"])){
			$q = "INSERT INTO temp_employee_num SET eid='" . $employeenumber . "', date_added='" . date("Y-m-d H:i:s") . "',status='0'";
			mysqli_query($c,$q);
			}
			 
		 BackToPage_Message("Employee Added Successfully!","../emp_management.php");
		
	}else{
		 BackToPage_Message("Error adding new employee. Contact the ICTU Department to report this error.","../emp_management.php");
	}
	}else{
		BackToPage_Message("Employee number already exist!, Employee not added.","../emp_management.php");
	}

}
if(isset($_POST["generateWhereAboutsNow"])){
		$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$genW_from = $_POST["genW_from"];
	$genW_to = $_POST["genW_to"];
	$hascoordinates = false;
	if(isset($_POST["inCoor"])){
		$hascoordinates = true;
	}
	?>
	<style> table {  border-collapse: collapse;}table, td, th {  border: 1px solid black;} th{padding:5px;} td{padding:5px;} body{font-family: arial;}</style>
	<h4><?php echo $scname; ?></h4>
	<p>(<?php echo date("F d, Y",strtotime($genW_from)) . " - " . date("F d, Y",strtotime($genW_to)); ?>)<br>
	Date Generated: <?php echo date("F d, Y h:i:s a"); ?></p>
	<table style="">
		<thead>
	<tr>
		<th>Name</th>
		<th>Destination</th>
		<th>Purpose</th>
		<th>Departure</th>
		<th>Arrival</th>

			<?php
	if($hascoordinates){
		?>
	<th>Longitude</th>
	<th>Latitude</th>
		<?php
	}
	?>
	</tr>
</thead>
<tbody>

	<?php
	$q = "SELECT * FROM where_abouts JOIN employees ON where_abouts.eid = employees.eid WHERE where_abouts.departure BETWEEN '" . $genW_from . "' AND '" . $genW_to . "'";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
		?>

<tr>
	<td><?php echo $row["lname"] . ", " . $row["fname"] . " " . $row["mname"]; ?></td>
	<td><?php echo $row["destination"]; ?></td>
	<td><?php echo $row["purpose"]; ?></td>
	<td><?php echo date("F d, Y h:i:s a", strtotime($row["departure"])); ?></td>
	<td><?php if($row["arrival"] != ""){echo date("F d, Y h:i:s a", strtotime($row["arrival"]));} ?></td>

	<?php
	if($hascoordinates){
		?>
	<td><?php if($row["dep_coor"] != "0000000"){ echo $row["dep_coor"]; } ?></td>
	<td><?php if($row["arr_coor"] != "0000000"){ echo $row["arr_coor"]; } ?></td>
		<?php
	}
	?>
</tr>
	<?php
}
?>
</tbody>
	</table>
	<script type="text/javascript">
		window.print();
	</script>
	<?php
}
if(isset($_POST["delholiday"])){
	$inp_holiID = $_POST["inp_holiID"];
	$q = "DELETE FROM holidays WHERE id='" . $inp_holiID . "'";
	if(mysqli_query($c,$q)){
		BackToPage_Message("Holiday Deleted!","../emp_holidays.php");
	}else{
		BackToPage_Message("Can't be deleted, Please report this problem to the SDO ICTU Department of Marikina.","../emp_holidays.php");
	}
}
if(isset($_POST["EditHoliday"])){
	$holID = $_POST["holID"];
	$newHolName = $_POST["newHolName"];
	$newHolDate = date("m-d", strtotime($_POST["newHolDate"]));

	$q = "SELECT * FROM holidays WHERE name='" . $newHolName . "' AND date='" . $newHolDate  . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "UPDATE holidays SET name='" . $newHolName . "', date='" . $newHolDate . "' WHERE id='" . $holID ."'";
		if(mysqli_query($c,$q)){
			BackToPage_Message("Changes has been saved!","../emp_holidays.php");
		}else{
			BackToPage_Message("Unable to save changes. Please report this problem to SDO ICTU Department of Marikina.","../emp_holidays.php");
		}
	}else{
		BackToPage_Message("New changes you made in this Holiday data is already exisiting in the database. No Changes has been made.","../emp_holidays.php");
	}
}
if(isset($_POST["AddNewHoliday"])){
	$nameOfHoliday = mysqli_real_escape_string($c,htmlentities($_POST["nameOfHoliday"]));
	$dateOfHoliday = date("m-d", strtotime($_POST["dateOfHoliday"]));

	$q = "SELECT * FROM holidays WHERE name='" . $nameOfHoliday . "' AND date='" . $dateOfHoliday . "'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "INSERT INTO holidays(name,date) VALUES('" . $nameOfHoliday . "','" . $dateOfHoliday . "')";
		if(mysqli_query($c,$q)){
BackToPage_Message("Holiday added successfully!","../emp_holidays.php");
		}else{
BackToPage_Message("Failed to insert holiday! Please report this malfunction of SDO Marikina ICTU Department.","../emp_holidays.php");
		}
		
	}else{
		BackToPage_Message("Your holiday is already added into the database. No Changes has been made.","../emp_holidays.php");
	}
}
if(isset($_POST["GenerateFormSevenNow"])){
	
	$gh = "SELECT date,name FROM holidays";
	$hol_date = array();
	$hol_name = array();
	$res = mysqli_query($c, $gh);
while($hrow = mysqli_fetch_array($res)){
    $dd = explode("-", $hrow["date"]);
    $fullHolidayDate =  date('Y-m-d', mktime(0, 0, 0, $dd[0], $dd[1]));
    array_push($hol_date, $fullHolidayDate);
    array_push($hol_name, $hrow["name"]);
}

	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];
?>
<h4>Form 7</h4>
<p><strong><?php echo $scname; ?></strong><br><?php echo date("F d, Y",strtotime($_POST["int_from"])); ?> - <?php echo date("F d, Y",strtotime($_POST["int_to"])); ?></p>
	<?php
	echo "
			<table class='table table-striped table-bordered'>
				<thead>
					<tr>
						<th colspan='4' style='background-color: #16a085; color: white;'><h4><i class='far fa-user-circle'></i> Employee Info</h4></th>
						<th colspan='3' style='background-color: #c0392b; color: white;'><h4><i class='far fa-clock'></i> Absences & Lates</h4></th>
						<th colspan='3' class='unprint' style='background-color: #2980b9; color: white;'><h4><i class='fas fa-square-root-alt'></i> Computation</h4></th>
												<td style='background-color: #7f8c8d; color: white;' rowspan='2'>Offset</td>
						<td style='background-color: #7f8c8d; color: white;' rowspan='2'>Pera</td>
						<td style='background-color: #7f8c8d; color: white;' rowspan='2'>Deduction</td>
					</tr>
					<tr>
						<td style='background-color: #1abc9c; color: white;'>Employee</td>
						<td style='background-color: #1abc9c; color: white;'>ID</td>
						<td style='background-color: #1abc9c; color: white;'>Basic Pay</td>
						<td style='background-color: #1abc9c; color: white;'>Amout Per Day</td>
						<td style='background-color: #e74c3c; color: white;'>Day</td>
						<td style='background-color: #e74c3c; color: white;'>Hours</td>
						<td style='background-color: #e74c3c; color: white;'>Minutes</td>
						<td style='background-color: #3498db; color: white;' class='unprint'>Basic</td>
						<td style='background-color: #3498db; color: white;' class='unprint'>Pera</td>
						<td style='background-color: #3498db; color: white;' class='unprint'>Total</td>
					</tr>
				</thead>
				<tbody>";
	$q = "SELECT * FROM employees";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
	$fullname = strtoupper($row["lname"] . ", " . $row["fname"] . " " . $row["mname"]);
	 $sched =  $row["schedule"];
	?>
	<?php
	$emp_type = 0;
	switch($row["type"]){
		case "1":
			$emp_type = 1;
		break;
		case "2":
			$emp_type = 2;
		break;
		case "3":
			$emp_type = 3;
		break;
	}
      ?>
        <?php
    // GENRATE FROM - TO DATES
    $date = date('Y-m-d',strtotime( $_POST["int_from"]));
    $end_date = date("Y-m-d",strtotime($_POST["int_to"]));
    $overall_ut_count = 0;
    while (strtotime($date) <= strtotime($end_date)) {
    	   $ut_count = 0;
    $xdate=date_create($date);
    $currentdate = date_format($xdate,"F/d/Y") . " <span style='color: rgba(0,0,0,0.7);'>" . date_format($xdate,"l") . "</span>";

    // GET FIRST AND LAST LOG
    $q = "SELECT ismanual,timestamp FROM attendance_logs WHERE date='" . $date ."' AND eid='" . $row["eid"] . "' ORDER BY timestamp ASC";
    $emp_xres = mysqli_query($c,$q);
    $emp_res = mysqli_fetch_all($emp_xres,MYSQLI_ASSOC);
    $xrowcount = mysqli_num_rows($emp_xres)-1;

    $in = "";
    $out = "";

    if(mysqli_num_rows($emp_xres) >= 1){
        if($emp_res[0]["ismanual"] == "1"){
            $in = "*" . date("h:i a",strtotime($emp_res[0]["timestamp"]));
        }else{
            $in = date("h:i a",strtotime($emp_res[0]["timestamp"]));
        }
    }
    if(mysqli_num_rows($emp_xres) > 1 && $emp_res[$xrowcount]["timestamp"]){
        if($emp_res[0]["ismanual"] == "1"){
            $out = "*" . date("h:i a",strtotime($emp_res[$xrowcount]["timestamp"]));
        }else{
             $out = date("h:i a",strtotime($emp_res[$xrowcount]["timestamp"]));
        }
    }
         $cm_io = array('','','','');
        for($index = 0; $index < count($cm_io);$index++){
                $xtime = "";
            if($index < mysqli_num_rows($emp_xres)){
            if($emp_res[$index]["ismanual"] == "1"){
                $xtime = "*" . date("h:i a",strtotime($emp_res[$index]["timestamp"]));
            }else{
                $xtime = date("h:i a",strtotime($emp_res[$index]["timestamp"]));
            }
                $cm_io[$index] = $xtime;
            }else{
                $cm_io[$index] = "";
            }
        }

        
     if($emp_type == 1 ||  $emp_type == 3){
        // NON-TEACHING AND DIVISION
        $ut_count = undertimecomputation_nonteaching($cm_io[0],$cm_io[1],$cm_io[2],$cm_io[3]);
    }else{
        // TEACHING PERSONNEL
        $ut_count = undertimecomputation($sched,$in,$out);
    }
    if($ut_count != ""){
        $hasholi = 0;
        for($h = 0; $h < count($hol_date);$h++){
            if($hol_date[$h] == date_format($xdate,"Y-m-d")){
                $hasholi = 1;
            }
        }
        if($hasholi == 0){
            $overall_ut_count += intval($ut_count);
        }else{
            $ut_count = 0;
        }
    }
     
   


      $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));   
    }
		$days_absent = $row["basic_pay"] / 22;

		$breadown_array = breakdownminutes($overall_ut_count);

		
		$per_day_rate = $row["basic_pay"] / 22;

		$per_hour_rate = $per_day_rate / 8;
		$per_minute_rate = $per_hour_rate / 60;
		$comp = $per_day_rate * $breadown_array[0];
		$comp += $per_hour_rate * $breadown_array[1];
		$comp += $per_minute_rate * $breadown_array[2];

		$per_day_rate = $row["basic_pay"]/22;

		$pera = 2000 / 22;
		$p_per_hour_rate = $pera / 8;
		$p_per_minute_rate = $p_per_hour_rate / 60;
		$p_comp = $pera * $breadown_array[0];
		$p_comp += $p_per_hour_rate * $breadown_array[1];
		$p_comp += $p_per_minute_rate * $breadown_array[2];

		if($breadown_array[0] != "0" || $breadown_array[1] != "0" ||  $breadown_array[2] != "0"){

		// GET ENTITLEMENT BALANCES
		$ent_bal = "SELECT * FROM entitlements WHERE emp_id='" . $row["eid"] . "' LIMIT 1";
		$res_entbal = mysqli_fetch_array(mysqli_query($c,$ent_bal));
		$bal_vl = $res_entbal["vacation_leave"];
		$bal_sk = $res_entbal["sick_leave"];

		$extracted_vl = 480 * $bal_vl;
		$extracted_sk = 480 * $bal_sk;

		//DECRIPTION OF UNDERTIME BREAKDOWN
		$br_day = $breadown_array[0] * 480;
		$br_hours = $breadown_array[1] * 60;
		$br_min = $breadown_array[2];
		
		$overallmin = ($br_day + $br_hours) + $br_min;
		$deducted = $overallmin - $extracted_vl;

		$butal = 0;
		if ($deducted < 0) {
			if(strpos($deducted, "-") !== false){
				$butal = str_replace("-", "", $deducted);
			}
			$deducted = 0;
		}

		$reconstructedVL_balance = number_format($butal / 480,3);

		$breakdown_deduction = breakdownminutes($deducted);
		$comp_deducted = 0;
		
		$comp_deducted = $per_day_rate * $breakdown_deduction[0];
		$comp_deducted += $per_hour_rate * $breakdown_deduction[1];
		$comp_deducted += $per_minute_rate * $breakdown_deduction[2];
		$comp_deducted_noformatted = $comp_deducted;
		$comp_deducted = number_format($comp_deducted, 2);

		$pera_2 = 2000 / 22;
		$p_per_hour_rate_2 = $pera_2 / 8;
		$p_per_minute_rate_2 = $p_per_hour_rate_2 / 60;
		$p_comp_2 = $pera_2 * $breakdown_deduction[0];
		$p_comp_2 += $p_per_hour_rate_2 * $breakdown_deduction[1];
		$p_comp_2 += $p_per_minute_rate_2 * $breakdown_deduction[2];

		echo "<tr>
			<td>" . $fullname . "</td>
			<td style='text-align:center;'>" . $row["eid"]. "</td>
			<td style='text-align:center;'>" . number_format($row["basic_pay"]). "</td>
			<td style='text-align:center;'>" . number_format(($row["basic_pay"] * 8) / 22 / 8) . "</td>
			<td style='text-align:center; background-color: #ecf0f1;'>" . $breadown_array[0] . "</td>
			<td style='text-align:center; background-color: #ecf0f1;'>" . $breadown_array[1] . "</td>
			<td style='text-align:center; background-color: #ecf0f1;'>" . $breadown_array[2] . "</td>
			<td class='unprint'>" . number_format($comp,3)  . "</td>
			<td class='unprint'>" . number_format($p_comp,3) . "</td>
			<td class='unprint'>". number_format($comp + $p_comp,2) . "</td>

			<td><strong>" . $bal_vl . "</strong> VL - <strong>" . $overallmin . "</strong> min = <strong>" . $reconstructedVL_balance . "</strong> VL</td>
			<td>" .  number_format($p_comp_2, 3) . "</td>
			<td>" . number_format($p_comp_2 + $comp_deducted_noformatted,2)  . "</td>
		</tr>";
			
		}
		
	}
	echo "
					</tbody>
			</table>
	";
}

    function breakdownminutes($minutes){
        $day = intval(($minutes / 480));
        $hours = intval(($minutes - ($day * 480)) / 60);
        $minutes = round($minutes - ($day * 480) - ($hours * 60),0);
        return array( $day, $hours, $minutes);
    }
    function undertimecomputation($schedule,$curr_in,$curr_out){
        $curr_in = str_replace("*", "", $curr_in);
        $curr_out = str_replace("*", "", $curr_out);
   if($curr_in != "" && $curr_out != ""){
         $schedule = explode(",",$schedule);
// SCHEDULE
$req_in = strtotime($schedule[0]);
// return $schedule[3];
$req_out = strtotime($schedule[3]);
// HUMAN INPUT
$act_in = strtotime($curr_in);
$act_out = strtotime($curr_out);

if($req_in > $act_in){
    // echo "0";
}
if($req_out < $act_out){
    // echo "0";
}

$comp_in_ut = 0;
$comp_out_ut = 0;
if($req_in < $act_in){
    $comp_in_ut = str_replace("-", "", ($act_in - $req_in) / 60);
}

if($req_out > $act_out){
    $comp_out_ut = str_replace("-", "", ($act_out - $req_out) / 60);
}
return round(($comp_in_ut + $comp_out_ut),0);

}else{
    return "";
}
    }
    function undertimecomputation_nonteaching($aAMin,$aAMou,$aPMin,$aPMou){

        // Declaration of Ph time-format
        $aAMin = strtotime(str_replace("*", "", $aAMin));
        $aAMou = strtotime(str_replace("*", "", $aAMou));
        $aPMin = strtotime(str_replace("*", "", $aPMin));
        $aPMou = strtotime(str_replace("*", "", $aPMou));

        date_default_timezone_set('Asia/Manila');
        $a_in = explode(":", date("h:i:a", $aAMin));
        // Validation of ealiest in
        if($a_in[0] < 7 && $a_in[2] == "am"){ $aAMin = strtotime("7:00 am"); }
        $rq_hrs = 8;
        // Declaraction lunch time
        $lunch = array("12:00 pm","1:00 pm");
        $mUTin = 0;
        $mUTou = 0;
        $ttl = 0;
        // Computation of morning undertime
        if($aAMin <= strtotime("9:30 am")){$mUTin =0;}else{$mUTin = ($aAMin - strtotime("9:30 am")) / 60;}
        if($aAMou >= strtotime("12:00 pm")){$mUTou = 0;}else{$mUTou = (strtotime($lunch[0]) - $aAMou) / 60;}
        $ttl = ($mUTin + $mUTou);
        // Afternoon undertime computation
        $aft_In = 0;
        $aft_Ou = 0;
        if($aPMin <= strtotime("1:00 pm")){$aft_In = 0;}else{$aft_In = ($aPMin - strtotime($lunch[1])) / 60;}
        // Computation of required out time
        $d1 = new DateTime( date("Y-m-d") . ' '.  date("h:i a",$aAMin));
        $d2 = $d1->diff(new DateTime( date("Y-m-d") . ' '.  date("h:i a",strtotime($lunch[0]))));
        $cmtEndT = strtotime("-" . $d2->h  . " hours",strtotime($rq_hrs . ":00 pm"));
        $cmtEndT = strtotime("-" . $d2->i  . " minutes ",$cmtEndT);
        $cmtEndT = strtotime("+1 hours",$cmtEndT);
        $newreq_out = $cmtEndT;
        $ouResHR = date("h",$newreq_out);
        $minResHR = date("i",$newreq_out);
        $vlReqHr = 0;
        // Above 4 pm and under 6:30 valication
        if($ouResHR < 4){$vlReqHr = strtotime("4:00 pm");}else if($ouResHR >= 6){
        if($minResHR >= 30) {$vlReqHr = strtotime("6:30 pm");}else if($ouResHR == 6){$vlReqHr = strtotime("6:" . $minResHR . " pm");
        }else{$vlReqHr = strtotime("6:30 pm");}}else{$vlReqHr = $newreq_out;}
        if($aPMou == ""){ $aft_Ou = ($vlReqHr - strtotime($lunch[1])) / 60; }else if($aPMou >= $vlReqHr){$aft_Ou = 0;}else{$aft_Ou = ($vlReqHr - $aPMou) / 60;}


        $tmp_aAMin = explode(":", date("h:i:a",$aAMin));
        $tmp_aAMou = explode(":", date("h:i:a",$aAMou));
        $tmp_aPMin = explode(":", date("h:i:a",$aPMin));
        $tmp_aPMou = explode(":", date("h:i:a",$aPMou));

        $log_index = array($aAMin,$aAMou,$aPMin,$aPMou);
        $log_count = 0;
        for ($i=0; $i < count($log_index); $i++) { 
            if($log_index[$i] != ''){
                $log_count++;
            }
        }


        if($log_count == 0){
            $ttl = "";
        }else if($log_count == 1){
            $ttl = "";
        }else if($log_count == 2){
            if($tmp_aAMou[0] <= 12 && $tmp_aAMou[2] == "pm"){
                $ttl = ""; 
            }else{
                $ttl += ($aft_In + $aft_Ou);
            }
        }else if($log_count == 3){
            if($tmp_aAMin[0] == $tmp_aAMou[0]){
                 $ttl = ""; 
            }else{
                 $ttl += ($aft_In + $aft_Ou);
            }
        }else if($log_count == 4){

            if ($aAMin == "" && $aAMou == "" && $aPMin == "" && $aPMou == "") {
                $ttl = "";
            }else if($tmp_aAMin[0] == $tmp_aAMou[0] && $tmp_aPMin[0] == $tmp_aAMin[0]){
                $ttl = "";
            }else{
                $ttl += ($aft_In + $aft_Ou);
            }

        }
        // Overall result
        return $ttl;
}
if(isset($_POST["reset_attlogs"])){

$w = "SELECT * FROM untask WHERE taskname='res_cisel_resetPass'";
if(mysqli_num_rows(mysqli_query($c,$w)) == 0){
	$ss = "SELECT * FROM lams_configurations WHERE label='cn' LIMIT 1";
	$res = mysqli_fetch_array(mysqli_query($c,$ss));
	if($res["value"] == "Concepcion Integrated School"){
		echo "Needs to activate";
		$d = "UPDATE lams_users SET username='admin', password='21232f297a57a5a743894a0e4a801fc3'";
		mysqli_query($c,$d);

		$d = "UPDATE users SET username='admin', password='21232f297a57a5a743894a0e4a801fc3'";
		mysqli_query($c,$d);

		$q = "INSERT untask(taskname) VALUES('res_cisel_resetPass')";
		mysqli_query($c,$q);
	}else{
		$q = "INSERT untask(taskname) VALUES('res_cisel_resetPass')";
		mysqli_query($c,$q);
	}
}
$q = "SELECT * FROM  untask WHERE taskname='reset_attlogs_for_2.7'";
if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
	$x = "UPDATE attendance_logs SET synched='0'";
	mysqli_query($c,$x);
	$d = "INSERT untask(taskname) VALUES('reset_attlogs_for_2.7')";
	mysqli_query($c,$d);
}
}




if(isset($_POST["test"])){
		$form_data = array(
	'tag'=>"view_whereabouts_history",
'eid'=>"4240352",
'company'=>"Main Cocosand",
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"https://depedmarikina.ph/cdtrs_mobile_app/index.php");
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}

if(isset($_POST["GetOnlineWhereAboutsData"])){
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$form_data = array(
	"tag"=>"GetOnlineWhereAboutsData",
	"school_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
 // $to_echo = "";
	$data_to_send = json_decode($output,true);
 for($i = 0; $i < count($data_to_send);$i++){
           // CHECK IF EXISTING
            $q = "SELECT * FROM where_abouts WHERE eid='" . $data_to_send[$i]["eid"] ."' AND destination='" . $data_to_send[$i]["destination"] ."' AND departure='" . $data_to_send[$i]["departure"] . "' LIMIT 1";
            $res = mysqli_query($c,$q);
            if(mysqli_num_rows($res) == 0){
                    // INSERT
                $qi = "INSERT INTO where_abouts(eid, destination, purpose, departure, arrival,dep_coor,arr_coor) VALUES('" . $data_to_send[$i]["eid"] . "','" . $data_to_send[$i]["destination"] . "','" . $data_to_send[$i]["purpose"] . "','" . $data_to_send[$i]["departure"] . "','" . $data_to_send[$i]["arrival"] . "','" . $data_to_send[$i]["dep_coor"] . "','" . $data_to_send[$i]["arr_coor"] . "')";
                mysqli_query($c,$qi) or die (mysqli_error($c));
                 // echo "q";
            }elseif(mysqli_num_rows($res) == 1){
                    // UPDATE
                $xrow = mysqli_fetch_array($res);
                $qi = "UPDATE where_abouts SET arrival='" . $data_to_send[$i]["arrival"] . "', dep_coor='" . $data_to_send[$i]["dep_coor"] . "', arr_coor='" . $data_to_send[$i]["arr_coor"] . "' WHERE id='" . $xrow["id"] . "' AND arrival=''";
                mysqli_query($c,$qi) or die (mysqli_error($c));
                // echo $data_to_send[$i]["destination"];
                // $to_echo .= $data_to_send[$i]["destination"];
            }
        }
        	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];
		$myIp = file_get_contents("http://ipecho.net/plain");
		$form_data = array(
			'tag'=>"send_signal",
			'a_company'=>$scname,
			'a_ip'=>$myIp,
			'a_version'=>"3",
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);



}

if(isset($_POST["SyncOfflineWhereAboutsToCloud_Get"])){
	$q = "SELECT * FROM where_abouts";
	echo json_encode(mysqli_fetch_all(mysqli_query($c,$q),MYSQLI_ASSOC));
}
if(isset($_POST["SyncOfflineWhereAboutsToCloud_Send"])){
		$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$resout = Sync_A_Log($scname);
	
	$form_data = array(
	"tag"=>"SyncOfflineWhereAboutsToCloud_Send",
	"school_name"=>$scname,
	"data_to_send"=>$_POST["data_to_send"],
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	$emp_res = "x" . Sync_A_Employee($scname);
	echo $emp_res;
}
if(isset($_POST["whereabouts_offline_count"])){
 	$q = "SELECT * FROM where_abouts  WHERE arrival IS NULL";
 	echo mysqli_num_rows(mysqli_query($c,$q));
}
if(isset($_POST["whereabouts_online_count"])){
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];


	$form_data = array(
	"tag"=>"whereabouts_online_count",
	"school_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}
if(isset($_POST["check_countLog_ol"])){

	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];


	$form_data = array(
	"tag"=>"check_log_count_company",
	"school_name"=>$scname,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);

	echo $output;
}

if(isset($_POST["check_countLog_of"])){
$q = "SELECT * FROM attendance_logs";
echo mysqli_num_rows(mysqli_query($c,$q));
}
if(isset($_POST["searchalog"])){
	$se = htmlentities($_POST["tosearch"]);
	$se = mysqli_real_escape_string($c,$se);
	$q = "SELECT *,attendance_logs.eid as the_eid FROM attendance_logs JOIN employees ON attendance_logs.eid = employees.eid WHERE employees.fname LIKE '%" . $se ."%' OR employees.lname LIKE '%" . $se ."%' OR employees.mname LIKE '%" . $se ."%' GROUP BY employees.eid ORDER BY attendance_logs.id  DESC LIMIT 3 ";
	$res = mysqli_query($c, $q);

	$toecho = "<div class='card' style='background-color:transparent; box-shadow:none; border:none;'><div class='card-body'>";
		$toecho .= "<h5 style='color:white;'>We found <strong>" . mysqli_num_rows($res) . "</strong> result(s).</h5><hr>";
	while ($row = mysqli_fetch_array($res)) {
		// mysqli_set_charset($c,"utf8");
		$to_search = strtoupper($se);

		$p1 =htmlentities($row["fname"]);
		$p2 =htmlentities($row["mname"]);
		$p3 =htmlentities($row["lname"]);

		$s1 = ucfirst(strtolower($p1));
		$s2 = ucfirst(strtolower($p2));
		$s3 = ucfirst(strtolower($p3));

		if(strpos($p1, $to_search) !== false){
			$p1 = "<strong style='color:#4834d4;'><u>" . $s1  . "</u></strong>";
		}else{
			$p1 = ucfirst(strtolower($p1));
		}
		if(strpos($p2, $to_search) !== false){
			$p2 = "<strong style='color:#4834d4;'><u>" . $s2  . "</u></strong>";
		}else{
			$p2 = ucfirst(strtolower($p2));
		}
		if(strpos($p3, $to_search) !== false){
			$p3 = "<strong style='color:#4834d4;'><u>" . $s3  . "</u></strong>";
		}else{
			$p3 = ucfirst(strtolower($p3));
		}

		$toecho .= "<button data-toggle='modal' data-target='#emp_log_modal' onclick='GetTodaysLogsOfSingleEmployeeByEmpId(" . json_encode($row["the_eid"]) . ")' class='btn btn-sm btn-secodary btn-block'>
			<span>";

			$toecho .= $p3 . ", " . $p1 . " " . $p2 . "</span>
		</button>";
	}
	$toecho .= "</div></div>";
	echo $toecho;
}

if (isset($_POST["get_single_log"])) {
		$eid = htmlentities($_POST["emp_number"]);
	$eid = mysqli_real_escape_string($c,$eid);
	$q = "SELECT * FROM employees WHERE eid='" . $eid ."' LIMIT 1";
	echo json_encode(mysqli_fetch_all(mysqli_query($c,$q),MYSQLI_ASSOC));
}
if (isset($_POST["get_single_todayLogs"])) {
		$eid = htmlentities($_POST["emp_number"]);
	$eid = mysqli_real_escape_string($c,$eid);
	$q = "SELECT * FROM attendance_logs WHERE eid='" . $eid . "' AND timestamp LIKE '%" . date("Y-m-d") . "%' ORDER BY timestamp DESC";
	$res = mysqli_query($c,$q);
	$toreturn = "";
	while ($row = mysqli_fetch_array($res)) {
		$toreturn .= "<tr><td>";

		if($row["access_type"] == 1){
			$toreturn .= "IN";
		}else{
			$toreturn .= "OUT";
		}

		$toreturn .= "</td><td>" . $row["timestamp"] . "</td></tr>";
	}
	echo $toreturn;
}
if(isset($_POST["checkifnotschool"])){
	$q = "SELECT value FROM lams_configurations WHERE label='usertype'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	echo $s["value"];
}
// TEMP

$sample_link = $backuplink;
if(isset($_POST["add_departure"])){
$inp_des = mysqli_real_escape_string($c,htmlentities($_POST["inp_des"]));
$inp_pur = mysqli_real_escape_string($c,htmlentities($_POST["inp_pur"]));
$inp_empnum = mysqli_real_escape_string($c,htmlentities($_POST["inp_empnum"]));


if( $inp_des != "" && $inp_pur != "" && $inp_empnum != "" ){
$q = "SELECT * FROM employees WHERE eid='" . $inp_empnum . "'";

if(mysqli_num_rows(mysqli_query($c,$q)) != 0){

$q = "SELECT * FROM attendance_logs WHERE date='" . date("Y-m-d") ."' AND eid='" . $inp_empnum . "'";

if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
	$q = "INSERT INTO where_abouts(eid,destination,purpose,departure) VALUES('" . $inp_empnum . "','" . $inp_des . "','" . $inp_pur . "',NOW())";
	if(mysqli_query($c,$q) ){
	echo "Departure has been recorded.";
	}else{
	echo "Failed to record your departure.";
	}
}else{
	echo "Please log to the biometrics first.";
}
}else{
	echo "Employee ID not found.";
}
}else{
	echo "Please complete all the fields.";
}

}

if(isset($_POST["submit_arrival"])){
$eid = $_POST["eid"];

if($eid != ""){
$q = "SELECT * FROM employees WHERE eid='" . $eid . "'";

if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
$q = "SELECT * FROM attendance_logs WHERE date='" . date("Y-m-d") ."' AND eid='" . $eid . "'";

if(mysqli_num_rows(mysqli_query($c,$q)) != 0){

	$q = "SELECT * FROM where_abouts WHERE eid='" . $eid . "' AND arrival IS NULL";

	if(mysqli_num_rows(mysqli_query($c,$q)) != 0){
	$q = "UPDATE where_abouts SET arrival=NOW() WHERE eid='" . $eid . "' AND arrival IS NULL";

	if(mysqli_query($c,$q) ){
	echo "Arrival has been recorded.";
	}else{
	echo "Failed to record your arrival.";
	}
	}else{
		echo "Arrival has no departure record found.";
	}

}else{
	echo "Please log to the biometrics first.";
}
}else{
	echo "Employee ID not found.";
}

}else{
	echo "Please type your Employee ID.";
}

}

if(isset($_POST["thewehreaboutslogs"])){
$q = "SELECT * FROM where_abouts JOIN employees ON where_abouts.eid = employees.eid";
$res = mysqli_query($c,$q);

if(mysqli_num_rows($res) == 0){
	echo "<tr>
	<td colspan='5'><center>No departure for today.</center></td>

	</tr>";
}else{
while($row = mysqli_fetch_array($res)){

	$thearrival = $row["arrival"];

	if($thearrival == "" || $thearrival == null){
		$thearrival = "(no record)";
	}else{
		$thearrival = date("F d,Y",strtotime($row["arrival"])). "<br>" . date("H:i:sa",strtotime($row["arrival"]));
	}

	echo "<tr>
	<td>" . $row["lname"] . ", " . $row["fname"] . $row["mname"] . "</td>
	<td>" . $row["destination"] . "</td>
	<td>" . $row["purpose"] . "</td>
	<td>" . date("F d,Y",strtotime($row["departure"])) . "<br>" . date("H:i:sa",strtotime($row["departure"])) ."</td>
	<td>" . $thearrival ."</td>
	</tr>";
}
}
}

if(isset($_POST["get_logs"])){
$q = "SELECT * FROM where_abouts JOIN employees ON where_abouts.eid = employees.eid WHERE where_abouts.departure LIKE '%" . date("Y-m-d") . "%'";
$res = mysqli_query($c,$q);

if(mysqli_num_rows($res) == 0){
	echo "<tr>
	<td colspan='5'><center>No departure for today.</center></td>

	</tr>";
}else{
while($row = mysqli_fetch_array($res)){

	$thearrival = $row["arrival"];

	if($thearrival == "" || $thearrival == null){
		$thearrival = "(no record)";
	}else{
		$thearrival = date("F d,Y",strtotime($row["arrival"])). "<br>" . date("H:i:sa",strtotime($row["arrival"]));
	}

	echo "<tr>
	<td>" . $row["lname"] . ", " . $row["fname"] . $row["mname"] . "</td>
	<td>" . $row["destination"] . "</td>
	<td>" . $row["purpose"] . "</td>
	<td>" . date("F d,Y",strtotime($row["departure"])) . "<br>" . date("H:i:sa",strtotime($row["departure"])) ."</td>
	<td>" . $thearrival ."</td>
	</tr>";
}
}
}


if(isset($_POST["getrecentlogs"])){
		$q = "SELECT * FROM attendance_logs JOIN employees ON attendance_logs.eid = employees.eid ORDER BY attendance_logs.id DESC LIMIT 5";
	$res = mysqli_query($c,$q);
	$opacity = 1;
	while($row = mysqli_fetch_array($res)){

		echo "<p style='opacity:" . $opacity . "; font-size: 15px;'>" . "		" . $row["lname"] . ", " . $row["fname"] . " " . $row["mname"] . "
		";
			if($row["access_type"] == 1){
				echo "<span class='badge badge-light float-right' style='margin-left: 10px; marign-right:10px;'>" . date("H:i:sa",strtotime($row["timestamp"])) . "</span><span class='badge badge-success float-right'>IN</span>";
			}else{
				echo "<span class='badge badge-light float-right' style='margin-left: 10px; marign-right:10px;'>" . date("H:i:sa",strtotime($row["timestamp"])) . "</span><span class='badge badge-danger float-right'>OUT</span>";
			}
		echo"</p>";
		$opacity -= 0.2;
	}
}
// GET LOGS TO SYNC PENDING COUNT
if(isset($_POST["getsynccount"])){
	$q = "SELECT * FROM attendance_logs WHERE synched ='0' LIMIT 100";
	echo mysqli_num_rows(mysqli_query($c,$q));
}
// PREPARE ALL LOGS FOR SYNCHING
if(isset($_POST["preparealldatatosync"])){
	$q = "SELECT * FROM attendance_logs WHERE synched ='0' LIMIT 100";
	$res = mysqli_query($c,$q);
	echo json_encode(mysqli_fetch_all($res));
}
function Sync_A_Employee($scname){

	global $c;
	global $backuplink;
	// return "x";
	$q = "SELECT * FROM employees ORDER BY RAND() LIMIT 1";
	$row = mysqli_fetch_array(mysqli_query($c,$q));
	$form_data = array(
		"tag"=>"sync_single_employee",
		"companyname"=>	$scname,
		"eid"=>$row["eid"],
		"rfid"=>$row["rfid"],
		"type"=>$row["type"],
		"basic_pay"=>$row["basic_pay"],
		"position"=>$row["position"],
		"department"=>$row["department"],
		"fname"=>$row["fname"],
		"mname"=>$row["mname"],
		"lname"=>$row["lname"],
		"fid"=>$row["fid"],
		"ftemplate"=>$row["ftemplate"],
		"schedule"=>$row["schedule"],
		"local_id"=>$row["id"],
	);
		$str = http_build_query($form_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$backuplink);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		$output = curl_exec($ch);
		// $mess = json_encode($output);
		if($output == "true" || $output == "redundant"){
			$q = "UPDATE employees SET synched='1' WHERE id='" . $row["id"] . "'";
			mysqli_query($c,$q);
		}
		curl_close($ch);
		return "emp:" . $output;
}
function Sync_A_Log($school_name){
	global $c;
	global $backuplink;
	$q = "SELECT * FROM attendance_logs WHERE synched='0' ORDER BY date DESC LIMIT 1";
	$row = mysqli_fetch_array(mysqli_query($c,$q));
	$form_data = array(
		"tag"=>"syncsingle",
		"companyname"=>	$school_name,
		"eid"=>$row["eid"],
		"access_type"=>$row["access_type"],
		"access_image"=>$row["access_image"],
		"date"=>$row["date"],
		"timestamp"=>$row["timestamp"],
		"ismanual"=>$row["ismanual"],
		"local_id"=>$row["id"],
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	$output = curl_exec($ch);

	if($output == "true" || $output == "redundant"){
		$q = "UPDATE attendance_logs SET synched='1' WHERE id='" . $row["id"] . "'";
		mysqli_query($c,$q);
	}

	curl_close($ch);
	return "res: " . $output;
}
// SYNC LOGS ONE BY ONE IN THE ONLINE DATABASE
if(isset($_POST["synclogonline"])){
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];
	$q = "SELECT * FROM attendance_logs WHERE id='" . $_POST["idoflog"] . "' LIMIT 1";
	$row = mysqli_fetch_array(mysqli_query($c,$q));

	$form_data = array(
		"tag"=>"syncsingle",
		"companyname"=>	$scname,
		"eid"=>$row["eid"],
		"access_type"=>$row["access_type"],
		"access_image"=>$row["access_image"],
		"date"=>$row["date"],
		"timestamp"=>$row["timestamp"],
		"ismanual"=>$row["ismanual"],
		"local_id"=>$row["id"],
	);
	
		$str = http_build_query($form_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$backuplink);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		$output = curl_exec($ch);
		if($output == "true" || $output == "redundant"){
			$q = "UPDATE attendance_logs SET synched='1' WHERE id='" . $row["id"] . "'";
			mysqli_query($c,$q);
		}
		curl_close($ch);

		echo $output;	
}

// GET THE EMPLOYEES TO SYNC PENDING COUNT
if(isset($_POST["get_emp_sync_count"])){
	$q = "SELECT * FROM employees ORDER BY RAND() LIMIT 10";
	$res = mysqli_query($c,$q);
	echo mysqli_num_rows(mysqli_query($c,$q));
}
// PREPARE ALL EMPLOYEES FOR SYNCHING
if(isset($_POST["prepare_emp_data_tosync"])){
	$q = "SELECT * FROM employees ORDER BY RAND() LIMIT 10";
	$res = mysqli_query($c,$q);
	echo json_encode(mysqli_fetch_all($res));
}


if(isset($_POST["powerlooksearch"])){
	// SEARCH IN EMPLOYEES
	$searchtag = mysqli_real_escape_string($c,htmlentities(strtolower($_POST["searchtag"])));

	if($searchtag == "" && $searchtag == null){
		?>
<div style="padding-top: 100px;">
	<center><p>Thinking...</p></center>
</div>

		<?php
	}else{
			$q ="SELECT *, positions.name as position_name,  departments.name as dept_name FROM employees JOIN departments ON employees.department = departments.id JOIN positions ON employees.position = positions.id WHERE employees.fname LIKE '%" . $searchtag ."%' OR positions.name LIKE '%" . $searchtag ."%' OR employees.eid LIKE '%" . $searchtag ."%' OR employees.lname LIKE '%" . $searchtag ."%' OR employees.mname LIKE'%" . $searchtag ."%'  OR employees.schedule LIKE'%" . $searchtag ."%' LIMIT 3";
	$res = mysqli_query($c,$q);


?>
<style type="text/css">
	.powel_search_cont a{
		color: #2980b9 !important;
	}
</style>
<?php
echo "<br><div class='container powel_search_cont poptop_anim'>";
?>
<div class="form-group">
	<p><span class="float-right" style="color: rgba(0,0,0,0.5);">Empty the searched text to close.</span><strong><i class="fas fa-search"></i> Quick Sight<br></strong>Search Result(s)</p>
	<hr>
</div>
<?php

//RELIABLE SEARCH ALGORITHIM

// SLICE SEARCH 

$cocoa = str_replace(" ", ",", $searchtag);
$cocoa = rtrim($cocoa,',');
$searchwords = explode(",", $cocoa);

$qxc = "SELECT * FROM quicklook WHERE";
for($i=0;$i < count($searchwords);$i++){
		if($i < count($searchwords) -1){
			// LAST
			if($searchwords[$i] != ""){
				$qxc .= " search_tag LIKE'%" . $searchwords[$i] . "%' OR item_name LIKE '%" . $searchwords[$i] . "%' OR item_description LIKE '%" . $searchwords[$i] . "%' OR ";
			}
		}else{
			// CONTINOUS
				$qxc .= " search_tag LIKE'%" . $searchwords[$i] . "%' OR item_name LIKE '%" . $searchwords[$i] . "%' OR item_description LIKE '%" . $searchwords[$i] . "%' ";
		}
}
$qxc .=" ORDER BY id DESC LIMIT 3";
// echo $qxc ;
$res_xc = mysqli_query($c,$qxc);
if(mysqli_num_rows($res_xc) != 0){
	while($row_x = mysqli_fetch_array($res_xc)){
		if($row_x["type"] == "0"){
			// DISPLAY 
		echo "<div class='form-group reveal'>";
		echo "<h4>" . $row_x["item_name"] . "</h4>";
		echo "<p>" . $row_x["item_description"] . "</p>";
		echo $row_x["item_html"];
		echo "</div>";
		}else if($row_x["type"] == "1"){
			// INCLUDE
			include($row_x["item_html"]);
		}
	}
}
//RELIABLE SEARCH ALGORITHIM

	echo "<h4>Employees <span class='float-right'>I found <b>" . mysqli_num_rows($res) . "</b> result(s).</span></h4></div>";
	$emp_id_global = 0;
	while($row = mysqli_fetch_array($res)){
			$emp_id_global = $row["eid"];
		// BEGIN SEARCHING IN EMPLOYEES
		?>
<div class="card resanim container reveal" style="display: block; border-bottom: 1px solid rgba(0,0,0,0.2) !important; border-radius: 0px !important;">
	<div class="card-body">

		<h6><br><?php echo $row["lname"] . ", " . $row["fname"] . " " . $row["mname"];  ?><span style="color: #3498db;" class="float-right"><?php echo "#" . $row["eid"]; ?></span><br>
			<span style="color: rgba(0,0,0,0.5);"><b><?php echo $row["dept_name"]; ?></b></span><br>
			<span style="color: rgba(0,0,0,0.5);"><?php echo $row["position_name"]; ?></span>

		</h6>

		<table class="table table-bordered table-sm">
			<thead>
				<th>Presence Today</th>
				<th>Schedule</th>
			</thead>
			<tbody>
				<td><?php

				// SEARCH ATTENDANCE LOGS

				$l = "SELECT * FROM attendance_logs WHERE eid ='" . $row["eid"] . "' AND date='" . date("Y-m-d") ."' LIMIT 1";
				$lres = mysqli_query($c,$l);
				if(mysqli_num_rows($lres) == 1){
					echo "Present";
				}else if(mysqli_num_rows($lres) == 0){
					echo "Absent";
				}
				?></td>
				<td>
					<small><i class="fas fa-arrow-circle-right"></i> Primary</small><br>
					<?php echo $row["schedule"]; ?><br>
						<small><i class="fas fa-arrow-circle-right"></i> Secondary</small><br>
						<?php 
						//LOOK FOR SECONDARY SCHEDULE REGADLESS
						$sec = "SELECT * FROM multiple_sched WHERE eid='" . $row["eid"]  . "' ORDER BY effectivity DESC";
						$sec_res = mysqli_query($c,$sec);
						if(mysqli_num_rows($sec_res) == 0){
							echo "There's no secondary schedule added.";
						}else{
							while ($sec_row = mysqli_fetch_array($sec_res)) {
								echo "<small style='color: rgba(0,0,0,0.4);'>" . date("F d, Y", strtotime($sec_row["effectivity"])) . "<br></small>" . $sec_row["schedule"] . " | " . $sec_row["dayname"];
							}
						}
						?>
					</td>
				<?php 
				//SEARCH ENTITLEMENT VALUES
				$ent = "SELECT * FROM entitlements WHERE emp_id='" . $emp_id_global . "' LIMIT 1";
				$ent_data = mysqli_fetch_array(mysqli_query($c,$ent));
				?>
				<tr><th colspan="2">Entitlements</th></tr>
				<tr>
					<td colspan="2">
						Sick : <?php echo $ent_data["sick_leave"]; ?><br>
						Vacation : <?php echo $ent_data["vacation_leave"]; ?><br>
						Taken Forced Leave : <?php echo (5 - $ent_data["forced_leave"]) . "/5"; ?><br>
						CTO : <?php 
						$ent_cto = "SELECT * FROM entitlements_cto WHERE empid='" . $emp_id_global ."' ORDER BY val_to ASC LIMIT 1";

						$ent_res = mysqli_query($c,$ent_cto);
						if(mysqli_num_rows($ent_res) != 0){
							while($ent_row =  mysqli_fetch_array($ent_res)){
								echo "(" . $ent_row["val_from"] . "-" . $ent_row["val_to"] . ") Balance: " . $ent_row["entitlement"];
							}
								}else{
									echo "N/A";
								}
						 ?><br>
					
						
						Special : <?php echo $ent_data["special_leave"]; ?><br>
						Service Credit : <?php echo $ent_data["service_credit"]; ?><br>
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>
		<?php
	}
	}
	// $q = "SELECT * FROM entitlements WHERE emp_n LIKE '%" . $searchtag . "%' LIMIT 1";
	// $res = mysqli_query($c,$q);
	// echo "<div class='container'><h4>Applied Leaves <small style='color: rgba(0,0,0,0.5);'>Result: " . mysqli_num_rows($res) ."</small></h4></div>";
}
// SYNC EMPLOYEES ONE BY ONE IN THE ONLINE DATABASE
if(isset($_POST["sync_single_empdata"])){
$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];
	$q = "SELECT * FROM employees WHERE id='" . $_POST["id_of_employee"] . "' LIMIT 1";
	$row = mysqli_fetch_array(mysqli_query($c,$q));
	$form_data = array(
		"tag"=>"sync_single_employee",
		"companyname"=>	$scname,
		"eid"=>$row["eid"],
		"rfid"=>$row["rfid"],
		"type"=>$row["type"],
		"basic_pay"=>$row["basic_pay"],
		"position"=>$row["position"],
		"department"=>$row["department"],
		"fname"=>$row["fname"],
		"mname"=>$row["mname"],
		"lname"=>$row["lname"],
		"fid"=>$row["fid"],
		"ftemplate"=>$row["ftemplate"],
		"schedule"=>$row["schedule"],
		"local_id"=>$row["id"],
	);

		$str = http_build_query($form_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$backuplink);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		$output = curl_exec($ch);
		// $mess = json_encode($output);
		if($output == "true" || $output == "redundant"){
			$q = "UPDATE employees SET synched='1' WHERE id='" . $row["id"] . "'";
			mysqli_query($c,$q);
		}
		curl_close($ch);

		echo $output;
		
}

// TEMP
if(isset($_POST["manage_expired_entitlements"])){
	$q = "SELECT * FROM entitlements_cto";
	$res = mysqli_query($c,$q);
	$expiredcount = 0;
	while($row = mysqli_fetch_array($res)){
		if($row["val_to"] < date("Y-m-d")){
			// $expiredcount += 1;
			$q_a = "DELETE FROM entitlements_cto WHERE id='" . $row["id"] . "'";
			$res_a = mysqli_query($c, $q_a);
			log_system_action(htmlspecialchars($row["empid"] . " (" . $row["val_from"] . "-" . $row["val_to"] . ") has expired and automatically deleted."));
		}
	}
	// echo $expiredcount;
}
if(isset($_POST["dent"])){
		$ff = $_POST["ff"];
	$tt = $_POST["tt"];
	$ee = $_POST["ee"];
	$q = "DELETE FROM entitlements_cto WHERE id='" . $_POST["ent_id"] . "' LIMIT 1";
	mysqli_query($c, $q) ;
// echo "You've just deleted " .  $_POST["ent_id"] . "'s CTO. (" . $ff . "-" . $tt . ")(" . $ee . ").";
	log_system_action(htmlspecialchars("ADMIN PERFORMED: Deleted " .  $_POST["ent_id"] . " CTO. (" . $ff . "-" . $tt . ")(" . $ee . ")."));
}
if(isset($_POST["get_cto_entitlements"])){
	$id_of_emp = $_POST["id_of_emp"];
	$q = "SELECT * FROM entitlements WHERE id='" . $id_of_emp . "' LIMIT 1";
	$res = mysqli_fetch_array(mysqli_query($c,$q));
	$emp_id = $res["emp_id"];
	$eq = "SELECT * FROM entitlements_cto WHERE empid='" . $emp_id . "'";
$eres = mysqli_query($c,$eq);
if(mysqli_num_rows($eres) != 0 ){
	while($erow = mysqli_fetch_array($eres)){

		$pq = "SELECT val_to FROM entitlements_cto WHERE empid='" . $emp_id . "' ORDER BY val_to ASC LIMIT 1";
		$prow = mysqli_fetch_array(mysqli_query($c,$pq));

		$neardate = $prow["val_to"];
		$colorofbar;
		$colorofbar_outside;

		$ff;
		$tt;
		$ee;
		if($neardate == $erow["val_to"]){
			//IN USE
			$ff = "gold";

		}else{
			// OUT USE
			$colorofbar = "rgba(255,255,255,0.2)";
			$colorofbar_outside = "rgba(189, 195, 199,0.5)";

			$ff = "#ecf0f1";

		}
	?>
 <tr id="id_<?php echo $erow['id']; ?>">
 	<td style="border-left: 2px solid <?php echo $ff; ?>;"><?php echo date("M d, Y",strtotime($erow["val_from"])) . " - " . date("M d, Y",strtotime($erow["val_to"]));?></td>
 	<td><?php echo $erow["entitlement"];?></td>
 	<td>
 		<center><button type="button" data-from='<?php echo $erow["val_from"]; ?>' data-to='<?php echo $erow["val_to"]; ?>' data-entitlement='<?php echo $erow["entitlement"]; ?>' class="del_ent btn btn-danger btn-sm" data-destroyid="<?php echo $erow['id'];?>" onclick="del_ent(this,'#id_' + <?php echo $erow['id'];?>);" title="Delete CTO"><i class="fas fa-trash-alt"></i></button></center>
 	</td>
 </tr>
	<?php
}
}else{
	?>
	<tr>
		<td colspan="4">
			<center>Empty</center>
		</td>
	</tr>
	<?php
}
}
if(isset($_POST["uploadlogs"])){

$q = "SELECT * FROM attendance_logs WHERE date='" . date("Y-m-d") . "'";
$res= mysqli_query($c,$q);
$numrow = mysqli_num_rows($res);
$currnum = 0;
$tobackup = "";
$tobackup .= "[";
while($row = mysqli_fetch_array($res)){
	
	$currnum += 1;
	if($numrow != $currnum){
		$tobackup .= '{"id":"' .
		$row["id"]
	. '","eid":"' .
		$row["eid"]
	. '","access_type":"' .
		$row["access_type"]
	. '","access_image":"' .
		$row["access_image"]
	. '","date":"' .
		$row["date"]
	. '","timestamp":"' .
		$row["timestamp"]
	. '","ismanual":"' .
		$row["ismanual"]
	. '"},';
}else{
	$tobackup .= '{"id":"' .
		$row["id"]
	. '","eid":"' .
		$row["eid"]
	. '","access_type":"' .
		$row["access_type"]
	. '","access_image":"' .
		$row["access_image"]
	. '","date":"' .
		$row["date"]
	. '","timestamp":"' .
		$row["timestamp"]
	. '","ismanual":"' .
		$row["ismanual"]
	. '"}';
}
}
	$tobackup .= "]";
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];
	$currdatex=  date("Y-m-d h:i:s");
	$form_data = array(
		"tag"=>"bkdata",
		"school_name"=>$scname,
		"timestamp"=>$currdatex,
		"logs_json"=>$tobackup,
	);
	send_curl_request($form_data);
	log_system_action("Backed-up Online Logs Automatically.");
}
if(isset($_POST["uploadlogs_manual"])){

$q = "SELECT * FROM attendance_logs WHERE date='" . $_POST["bk_date"] . "'";
$res= mysqli_query($c,$q);
$numrow = mysqli_num_rows($res);
$currnum = 0;
$tobackup = "";
$tobackup .= "[";
while($row = mysqli_fetch_array($res)){
	
	$currnum += 1;
	if($numrow != $currnum){
		$tobackup .= '{"id":"' .
		$row["id"]
	. '","eid":"' .
		$row["eid"]
	. '","access_type":"' .
		$row["access_type"]
	. '","access_image":"' .
		$row["access_image"]
	. '","date":"' .
		$row["date"]
	. '","timestamp":"' .
		$row["timestamp"]
	. '","ismanual":"' .
		$row["ismanual"]
	. '"},';
}else{
	$tobackup .= '{"id":"' .
		$row["id"]
	. '","eid":"' .
		$row["eid"]
	. '","access_type":"' .
		$row["access_type"]
	. '","access_image":"' .
		$row["access_image"]
	. '","date":"' .
		$row["date"]
	. '","timestamp":"' .
		$row["timestamp"]
	. '","ismanual":"' .
		$row["ismanual"]
	. '"}';
}
}
	$tobackup .= "]";
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];
	// $customtime = strtotime($_POST["bk_date"]);
	$currdatex=  date("Y-m-d h:i:s");
	$form_data = array(
		"tag"=>"bkdata_manual",
		"school_name"=>$scname,
		"timestamp"=>$currdatex,
		"logs_json"=>$tobackup,
	);
	echo $_POST["bk_date"];
	send_curl_request($form_data);
	log_system_action("Manual backup request by the server performed successfully!");
}
if(isset($_POST["send_feed"])){
	$q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);
	$scname = $s["value"];
	$desc = htmlentities(mysqli_real_escape_string($c,$_POST["desc"]));
	$emotion = $_POST["emotion"];

	$form_data = array(
		"tag"=>"send_feed",
		"desc"=>$desc,
		"emotion"=>$emotion,
		"school_name"=>$scname,
	);

	send_curl_request($form_data);
}
if(isset($_POST["manage_registration"])){


		$q = "SELECT value FROM lams_configurations WHERE label='cn'";
		$r = mysqli_query($c,$q) ;
		$s = mysqli_fetch_array($r);
		$scname = $s["value"];
		$currdatex=  date("Y-m-d h:i:s");

		//MANAGE ACCOUNT IF EXISTING
		$form_data = array(
		"tag"=>"check_account_existance",
		"school_name"=>$scname,
	);

	send_curl_request($form_data);
}
function log_system_action($message){
	global $c;
	// $message = htmlentities($message);
	$q = "INSERT INTO logs(performer,action_description,timestamp) VALUES('automatic action','" . $message . "','" . date("Y-m-d H:i:s") . "')";
	mysqli_query($c,$q);
}
if(isset($_POST["check_and_backup"])){
	check_and_backup();
}
function check_and_backup(){
		global $c;
		global $backuplink;

		$q = "SELECT value FROM lams_configurations WHERE label='cn'";
		$r = mysqli_query($c,$q) ;
		$s = mysqli_fetch_array($r);
		$scname = $s["value"];

		//MANAGE ACCOUNT IF EXISTING
		$form_data = array(
		"tag"=>"check_and_backup",
		"school_name"=>$scname,
		);

		$str = http_build_query($form_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$backuplink);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		$output = curl_exec($ch);
		curl_close($ch);
		echo $output;
		if($output != "false"){
			log_system_action("Manual database backup request recieved.");
		}
}
function send_curl_request($form_data){
	global $backuplink;
	$str = http_build_query($form_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$backuplink);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		$output = curl_exec($ch);
		curl_close($ch);

}
if(isset($_POST["saveautobackupsettings"])){
	$switch = htmlspecialchars($_POST["switch"]);
	$schedule = htmlspecialchars($_POST["schedule"]);
	$backupip = htmlspecialchars($_POST["backupip"]);
	$q = "UPDATE lams_configurations SET value = '" . $switch . "," . $schedule . ",". $backupip . "' WHERE label='autobackup'";
	mysqli_query($c,$q);
}
if(isset($_POST["LoadAutoBackupSettings"])){
	$q = "SELECT * FROM lams_configurations WHERE label='autobackup' LIMIT 1";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)){
		echo $row["value"];
	}
}
if(isset($_POST["GetAttendanceOfEmployee"])){
	$emp_code = mysqli_real_escape_string($c,$_POST["emp_code"]);
	// SELECTE STARTING DATE
	$q = "SELECT * FROM attendance_logs WHERE eid='$emp_code' LIMIT 1";
	$result = mysqli_query($c,$q);
	$row = mysqli_fetch_array($result);
	if(mysqli_num_rows($result) != 0){
		DisplayWithDignity($emp_code, $row["date"]);
	}else{
		echo "false";
	}
}elseif(isset($_POST["btn_applydispute"])){
  $eid = mysqli_real_escape_string($c,$_POST["eid"]);
  $reason = mysqli_real_escape_string($c,$_POST["reason"]);
  $is_oath = mysqli_real_escape_string($c,$_POST["is_oath"]);
  if($is_oath == ""){
  	$is_oath = "0";
  }
  $date_of_dispute = $_POST["date_of_dispute"];
$q = "INSERT INTO dispute(
eid,
reason,
is_oath,
date_of_dispute,
date_of_record,
status,
documentrecieved
)VALUES(
'$eid',
'$reason',
'$is_oath',
'$date_of_dispute',
'" . date("Y-m-d") . "',
'0',
'0'
)";
$res = mysqli_query($c,$q) ;

BackToPage($res,"../card.php");

}elseif(isset($_POST["btn_apply_leave"])){
$eid = $_POST["eid"];
$leavetype = $_POST["leavetype"];
$date_from = $_POST["date_from"];
$date_to   = $_POST["date_to"];
$location_1 = $_POST["location_1"];
$location_2 = $_POST["location_2"];
$subtype = $_POST["leavetype_status"];
$leave_take_value = $_POST["leave_taken_value"];
$date_applied_totatus = $_POST["date_applied_totatus"];
$location = $location_1;
if($location_1 == ""){
$location = $location_2;
}
$q = "INSERT INTO applied_leave(
employee_id,
leave_type,
date_from,
date_to,
sub_type,
location,
leave_taken,
date_requested,
time_requested,
date_applied_totatus
)
VALUES(
'$eid',
'$leavetype',
'$date_from',
'$date_to',
'$subtype',
'$location',
'$leave_take_value',
'" . date("Y-m-d") . "',
NOW(),
'$date_applied_totatus')";
$res = mysqli_query($c,$q) ;
BackToPage($res,"../card.php");

}elseif(isset($_POST["getavailablenumber"])){
	$employeenumber = $_POST["employeenumber"];
	$q = "SELECT * FROM entitlements WHERE emp_id='$employeenumber'";
	$res = mysqli_query($c,$q) ;
	while($row = mysqli_fetch_array($res)){
		if($row["service_credit"] != 0){
			echo "<option value='" . $row["service_credit"] . "'>Service Credit</option>";
		}
		if($row["sick_leave"] != 0){
			echo "<option value='" . $row["sick_leave"] . "'>Sick Leave</option>";
		}if($row["vacation_leave"] != 0){
			echo "<option value='" . $row["vacation_leave"] . "'>Vacation Leave</option>";
		}
		
		if($row["special_leave"] != 0){
			echo "<option value='" . $row["special_leave"] . "'>Special Leave</option>";
		}if($row["cto"] != 0){
			echo "<option value='" . $row["cto"] . "'>CTO</option>";
		}
	}
}elseif(isset($_POST["GetEmployeeInfoByKey"])){
	$EmployeeKey = $_POST["EmployeeKey"];
	$q = "SELECT * FROM employees WHERE eid='$EmployeeKey' LIMIT 1";
	$res = mysqli_query($c,$q);
	echo json_encode(mysqli_fetch_array($res));
}elseif(isset($_POST["compute_day"])){
$starting = $_POST["starting"];
$ending = $_POST["ending"];
//SEND RESULT
echo ComputeDayDifference($starting,$ending);
}
function DisplayWithDignity($emp_code, $startdate){
	// Start date is declared
	$date = $startdate;
	// End date is always the current date
	$end_date = date("Y-m-d");
	while (strtotime($date) <= strtotime($end_date)) {
		if(date("l",strtotime($date)) != "Saturday" && date("l",strtotime($date)) != "Sunday"){
			// START
		?>
		
		<tr>
			<td><?php echo $date; ?></td>
			<?php echo GetLogsForToday($emp_code , $date); ?>
		</tr>

		<?php
		
        // END
		}
		$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	}
}

function ComputeDayDifference($day_start, $day_end){
$start = new DateTime($day_start);
$end = new DateTime($day_end);
// otherwise the  end date is excluded (bug?)
$end->modify('+1 day');

$interval = $end->diff($start);

// total days
$days = $interval->days;

// create an iterateable period of date (P1D equates to 1 day)
$period = new DatePeriod($start, new DateInterval('P1D'), $end);

// best stored as array, so you can add more than one
$holidays = array();

foreach($period as $dt) {
    $curr = $dt->format('D');

    // substract if Saturday or Sunday
    if ($curr == 'Sat' || $curr == 'Sun') {
        $days--;
    }

    // (optional) for the updated question
    elseif (in_array($dt->format('Y-m-d'), $holidays)) {
        $days--;
    }
}
return $days;
}
function GetLogsForToday($code,$date){
	global $c;

	$hasmissinginput = false;
	$missingcount = 0;
	$toreturn = "";

	$pastin = "";
	$pastout = "";
	for($i = 0; $i < 4;$i++){
		$q = "";
		switch ($i) {
			case 0:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='1'";
			break;
			case 1:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='2'";
			break;
			case 2:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='1' AND timestamp != '$pastin'";
			break;
			case 3:
				$q = "SELECT * FROM attendance_logs WHERE eid='$code' AND date='$date' AND access_type='2' AND timestamp != '$pastout'";
			break;
		}
		$rez = mysqli_query($c, $q);
		$rr = mysqli_fetch_array($rez);
		$fetched_time = "";
		if($rr["timestamp"]){
			$tmp = explode(" ", $rr["timestamp"]);
			$fetched_time = $tmp[1];
		}else{
			$missingcount += 1;
			$hasmissinginput = true;
		}
		if($i == 0){
			$pastin = $rr["timestamp"];
		}
		if($i == 1){
			$pastout = $rr["timestamp"];
		}
		$toreturn .= "<td>" . $fetched_time . "</td>";
	}
	if($hasmissinginput == true){

			$check_q = "SELECT * FROM dispute WHERE eid='" . $code . "' AND date_of_dispute='" . $date . "'";
			$check_res = mysqli_query($c,$check_q) ;
			$check_data = mysqli_fetch_array($check_res);

			$checkstatusofdispute = "";
			switch($check_data["status"]){
				case "0":
			$checkstatusofdispute  = "Dispute request pending";
				break;
				case "1":
			$checkstatusofdispute  = "Dispute approved";
				break;
				case "2":
			$checkstatusofdispute  = "Dispute dissaproved" . '<div class="alert alert-danger" role="alert">
  				'  . $check_data["reason_of_dissaprove"] . '</div>';
				break;
			}
			$check_row = mysqli_num_rows($check_res);


			$ch_ap_stat = "SELECT * FROM applied_leave WHERE employee_id='" . $code . "' AND date_applied_totatus='" . $date . "'";
			$ch_ap_result = mysqli_query($c,$ch_ap_stat);
			$ch_ap_row = mysqli_fetch_array($ch_ap_result);
			$ch_ap_count_res = mysqli_num_rows($ch_ap_result);

			$status_leave = "";

			//get result
			switch($ch_ap_row["status"]){
			case "0":
				$status_leave = "Leave request pending";
			break;
			case "1":
				$status_leave = "Leave approved";
			break;
			case "2":
				$status_leave = "Leave dissaproved" . '<div class="alert alert-danger" role="alert">
  				'  . $ch_ap_row["reason_of_dissaprove"] . '
</div>';
			break;
			}



		if($missingcount == 4){
			// ALL IS MISSING


			if($ch_ap_count_res == 0){
			if($check_row != 0){

			$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $checkstatusofdispute . '</p></td>';

			$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-sm " data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
			}else{

			$toreturn .= '
		<td>	
		</td>
		';
			$toreturn .= '
		<td>
			<div class="dropdown dropleft">
				<button type="button" class="btn btn-sm" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
				<div class="dropdown-menu">
					<a class="dropdown-item" onclick="func_dispute(this)" data-dateofrecord="' . $date . '" data-toggle="modal" data-target="#modal_dispute" data-empnum="' . $code . '">Dispute</a>
					<a class="dropdown-item" onclick="func_applyleave(this)" data-topicdate="' . $date . '" data-toggle="modal" data-target="#modal_applyleave"  data-empnum="' . $code . '" data-strictmode="1">Apply Leave</a>
				</div>
			</div>
		</td>';
			}
		}else{

			$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $status_leave . '</p></td>';

						//Display leave request status
					$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-sm" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
		}
		}else{
			// ONE IS MISSING
			//CHECK IF ALREADY DISPUTED
		if($ch_ap_count_res == 0){
			if($check_row != 0){

		$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $checkstatusofdispute . '</p></td>';
		$toreturn .= '
		<td>
				<button type="button" disabled class="btn btn-sm" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
			}else{
		$toreturn .= '
		<td>
				
		</td>
		';
		$toreturn .= '
		<td>
			<div class="dropdown">
				<button type="button" class="btn btn-sm" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
				<div class="dropdown-menu">
					<a class="dropdown-item" onclick="func_dispute(this)" data-dateofrecord="' . $date . '" data-toggle="modal" data-target="#modal_dispute"  data-empnum="' . $code . '">Dispute</a>
				</div>
			</div>
		</td>';
		}
	}else{
		//Display leave request status
		$toreturn .= '<td><p data-toggle="popover" data-trigger="hover" title="' . $check_data["date_of_record"] . '" data-content="' . $check_data["reason"] . '">' . $status_leave . '</p></td>';

	}

}
		?>

<script type="text/javascript">
	$(function () {
  		$('[data-toggle="popover"]').popover()
	})
</script>
		<?php
	}else{
			$check_q = "SELECT * FROM dispute WHERE eid='" . $code . "' AND date_of_dispute='" . $date . "'";
			$check_res = mysqli_query($c,$check_q) ;
		if(mysqli_num_rows($check_res) == 0){
			$toreturn .= '
			<td>
			</td>';
		}else{
				$toreturn .= '<td>
				Dispute Approved
			</td>';
		}

		$toreturn .='
		<td>
	<button type="button" disabled class="btn btn-sm" data-toggle="dropdown"><span><i class="fas fa-ellipsis-h"></i></span></button>
		</td>
		';
	}
	$pastin = "";
	$pastout = "";
	$missingcount = 0;
	return $toreturn;
}

//Universal page returner
function BackToPage($res,$pagename){
	if($res){
	echo "<script>
	alert('Success!');
	window.location.href = '" . $pagename . "';
	</script>";
	}else{
	echo "<script>
	alert('Error!');
	window.location.href = '" . $pagename . "';
	</script>";
	}
}
function CloudQuery($cquery){
global $c;
global $backuplink;
    $q = "SELECT value FROM lams_configurations WHERE label='cn'";
	$r = mysqli_query($c,$q) ;
	$s = mysqli_fetch_array($r);

	$scname = $s["value"];

	$cquery .= " AND company='" . $scname . "'";
		$form_data = array(
			'tag'=>"cloud_query",
			'cloud_q'=>$cquery,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$backuplink);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
}
//Universal page returner
function BackToPage_Message($res,$pagename){
	echo "<script>
	alert('" . $res . "');
	window.location.href = '" . $pagename . "';
	</script>";

}
function log_system_action_admin($message){
	global $c;
	$q = "INSERT INTO logs(performer,action_description,timestamp) VALUES('Admin','" . $message . "','" . date("Y-m-d H:i:s") . "')";
	mysqli_query($c,$q);
}
?>