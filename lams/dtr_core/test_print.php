<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="../favs/application-vnd.ms-excel.ico" type="text/css" href="">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    

        <!-- CHARSET AND MOBILE VIEW -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="../theme/essentials/core/css/bootstrap.min.css">
    <!-- JQUERY, POPPER, BOOTSRAP JS -->
    <script type="text/javascript" src="../theme/essentials/core/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../theme/essentials/core/popper.min.js"></script>
    <script type="text/javascript" src="../theme/essentials/core/js/bootstrap.min.js"></script>
    <!-- THEME -->
    <!--    <link rel="stylesheet" type="text/css" href="../theme/sahara/style.css"> -->
    <link href="../theme/essentials/core/fontaws/css/all.css" rel="stylesheet">
    <!-- DATA TABLE -->

    <link rel="stylesheet" type="text/css" href="../theme/essentials/core/DataTables/datatables.min.css"/>

    <script type="text/javascript" src="../theme/essentials/core/DataTables/datatables.min.js"></script>



    <style type="text/css">
@font-face{
font-family: 'sanfranc';
src: url('../theme/fonts/sanfrancisco_pro.ttf'); /* IE9 Compat Modes */
}
body{
font-family: sanfranc;
}
.has_watermark{
background-image: url("../images/sdo.png");
background-repeat: no-repeat;
background-position: center center;
/*height: 1350px;*/
/*border: 1px solid black;*/
}
.table tr th{
border-color : rgba(0,0,0,0.2) !important;
background-color: rgba(0,0,0,0.1);
}
.table tr td{
border-color : rgba(0,0,0,0.2) !important;
}
.table{
background-color: transparent;
}
@media print { 
.table td, .table th { 
background-color: transparent !important; 
} 
.table tr th{
background-color: rgba(0,0,0,0.1) !important;
}
}
</style>
    <title>I.S Manager | DTR Visualizer</title>
</head>





<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">CDTRS DTR Preview</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
 

  </div>
</nav>


<div class="container" id="thedtrcontainer">


<?php

$dd_from = $_GET["ff"];
$dd_to = $_GET["tt"];


?>


<p style='page-break-before: always;'></p>
                   <div class="has_watermark"  style=" page-break-after: always; display: inline; height: 1000px; font-size: <?php echo $_GET["font_size"]; ?>;">
                   <!-- DTR FORMAT HEADING -->
                   <span class="float-left text-muted">Page Number</span>
<center><h5>CENTRALIZED DAILY TIME RECORD SYSTEM<br><small>CDTRS SAMPLE DTR</small></h5></center>
  <p style="float: right;"><?php echo date("F d, Y",strtotime($dd_from)); ?> to <?php echo date("F d, Y",strtotime($dd_to)); ?><br><small style="float: right;">EMPLOYEE TYPE</small></p>
  <!-- EMPLOYEE BASIC INFORMATION -->
<h5>
    <small>CDTRS SAMPLE DTR</small><br>
    <b>EMPLOYEE NAME</b>
    <small><br>SCHEDULE INFORMATION</small></h5>

    <!-- EMPLOYEE DTR TABLE -->
<table class="table table-sm table-bordered has_watermark">
    <thead>
        <tr>
            <th>DATE</th>
            <th style="text-align: center;">IN</th>
            <th style="text-align: center;">OUT</th>
            <th style="text-align: center;">IN</th>
            <th style="text-align: center;">OUT</th>
            <th style="text-align: center;">UT</th>
        </tr>
    </thead>
<tbody>

        <?php
        while (strtotime($dd_from) <= strtotime($dd_to)) {
?>

 <tr>
            <td><?php echo date("F d, Y",strtotime($dd_from)); ?></td>
            <td style='text-align: center;'><?php echo date("g:i a"); ?></td>
            <td style='text-align: center;'><?php echo date("g:i a"); ?></td>
            <td style='text-align: center;'><?php echo date("g:i a"); ?></td>
            <td style='text-align: center;'><?php echo date("g:i a"); ?></td>
            <td style="text-align: center;"><?php echo "0"; ?></td>
        </tr>

<?php
  $dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
}
            
        ?>


        <!-- <tfoot> -->
        <tr>
        <th colspan="6" style="text-align: right;"><span style="color: rgba(0,0,0,0.5)">Total Undertime Breakdown: </span>Undertime Breakdown</th>
        </tr>
        <tr>
            <td colspan="6">
                <center>
    <p>The Centralized Daily Time Record System is written by Virmil Talattad with the direction of by Ryan Lee Regencia and a project of Human Resources Department.</p>
    <br>
    <br>
    <div class="row">
        <div class="col-sm-6">
            <span style="border-top: 1px solid black; display: block;"></span>
           <?php echo "<strong>Employee Name</strong>"; ?><p style="margin-top: -5px;">Employee Signature</p>
        </div>
                <div class="col-sm-6">
            <span style="border-top: 1px solid black; display: block;"></span>

<strong>CENTRALIZED DAILY TIME RECORD SYSTEM</strong><p style="margin-top: -5px;">In Charge</p>
        </div>
    </div>
</center>
            </td>
        </tr>
    </tbody>
</table>

   </div>

</div>
</div>
</body>
</html>