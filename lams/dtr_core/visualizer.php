<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="../favs/application-vnd.ms-excel.ico" type="text/css" href="">
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	

		<!-- CHARSET AND MOBILE VIEW -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- BOOTSTRAP -->
	<link rel="stylesheet" type="text/css" href="../theme/essentials/core/css/bootstrap.min.css">
	<!-- JQUERY, POPPER, BOOTSRAP JS -->
	<script type="text/javascript" src="../theme/essentials/core/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="../theme/essentials/core/popper.min.js"></script>
	<script type="text/javascript" src="../theme/essentials/core/js/bootstrap.min.js"></script>
	<!-- THEME -->
	<!-- 	<link rel="stylesheet" type="text/css" href="../theme/sahara/style.css"> -->
	<link href="../theme/essentials/core/fontaws/css/all.css" rel="stylesheet">
	<!-- DATA TABLE -->

	<link rel="stylesheet" type="text/css" href="../theme/essentials/core/DataTables/datatables.min.css"/>

	<script type="text/javascript" src="../theme/essentials/core/DataTables/datatables.min.js"></script>



	<style type="text/css">
@font-face{
font-family: 'sanfranc';
src: url('../theme/fonts/sanfrancisco_pro.ttf'); /* IE9 Compat Modes */
}
body{
font-family: sanfranc;
}
.has_watermark{
background-image: url("../images/sdo.png");
background-repeat: no-repeat;
background-position: center center;
/*height: 1350px;*/
/*border: 1px solid black;*/
}
.table tr th{
border-color : rgba(0,0,0,0.2) !important;
background-color: rgba(0,0,0,0.1);
}
.table tr td{
border-color : rgba(0,0,0,0.2) !important;
}
.table{
background-color: transparent;
}
@media print { 
.table td, .table th { 
background-color: transparent !important; 
} 
.table tr th{
background-color: rgba(0,0,0,0.1) !important;
}
}
</style>
	<title>I.S Manager | DTR Visualizer</title>
</head>
<body >
<?php

	include("../connection/connect.php");

	// GET CURRENT COMPANY NAME 
	$q = "SELECT * FROM lams_configurations WHERE label='cn'";
	$res = mysqli_query($c,$q);
	$comp_name = mysqli_fetch_array($res);
	$comp_name = $comp_name["value"];

// USER INPUT
	$sc_name = $comp_name;
	$dt_from = $_POST["dt_from"];
	$dt_to = $_POST["dt_to"];
	$emp_type = $_POST["emp_type"];
?>
<div id="lod">
	<center>
		<br>
		<br>
		<br>
		<br>
<br>
	<br>
	<div class="container">
		<div class="card" style="border: 1px solid #2c3e50;">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12">
						<center>
							<img style="width: 200px;" src="../images/loading.gif">
							<h1>Generating DTR</h1>
								<p class="text-muted"><strong><?php echo $sc_name; ?></strong><br>
		<span><?php echo date("F d, Y",strtotime($dt_from )) . " - " . date("F d, Y",strtotime($dt_to )) ; ?></span><br><b><?php 
		switch ($emp_type) {
			case 0:
				echo "All Employees";
				break;
					case 1:
				echo "Non-Teaching";
				break;
					case 2:
				echo "Teaching";
				break;
					case 3:
				echo "Division Personnel";
				break;
				case 4:
				echo "Specific Mode";
				break;
		}
		 ?></b></p>
							<br>
							<br>
						</center>
					</div>
		<div class="col-sm-3">
			<center>
				<img style="width: 40px;" src="../images/authority_to_attend_nano.png"><br>
				Authorities
			</center>
		</div>
		<div class="col-sm-3">
			<center>
				<img style="width: 40px;" src="../images/whereabouts_nano.png"><br>
				Whereabouts
			</center>
		</div>
		<div class="col-sm-3">
			<center>
				<img style="width: 40px;" src="../images/leave_nano.png"><br>
				Leaves
			</center>
		</div>
		<div class="col-sm-3">
			<center>
				<img style="width: 40px;" src="../images/bio_kiosk_nano.png"><br>
				Kiosk
			</center>
		</div>
		<div class="col-sm-6">
			<center>
				<img style="width: 40px;" src="../images/bio_smartentry_nano.png"><br>
				Smart Entry
			</center>
		</div>
		<div class="col-sm-6">
			<center>
				<img style="width: 40px;" src="../images/manual_entry_nano.png"><br>
				Manual Entry
			</center>
		</div>
	</div>
			</div>
		</div>
	</div>
	<br>
	<br>

	</center>
</div>
<div id="vis">
<div class="container" id="thedtrcontainer">

</div>
</div>
</body>
</html>
<script type="text/javascript">
	$("#lod").css("display","block");
	$("#vis").css("display","none");
	var x_sc_name = <?php echo json_encode($sc_name); ?>;
	var x_dt_from = <?php echo json_encode($dt_from); ?>;
	var x_dt_to = <?php echo json_encode($dt_to); ?>;
	var x_emp_type = <?php echo json_encode($emp_type); ?>;
	var x_selected_emp = <?php 

	if (isset($_POST["selected_emp"])) {
		echo json_encode($_POST["selected_emp"]);
	}else{
		echo json_encode("x");
	}
	 ?>;
	var x_fontdtrsize = <?php echo json_encode($_POST["fontdtrsize"]); ?>;
	switch(x_emp_type){
		case 0:
		x_emp_type = 0;
		break;
		case 1:
    	x_emp_type = 1;
		break;
		case 2:
  		x_emp_type = 2;
		break;
		case 3:
    	x_emp_type = 3;
		break;
		case 4:
    	x_emp_type = 4;
		break;
	}

	$.ajax({
		type: "POST",
		url: "engine.php",
		data: {sc_name:x_sc_name,dt_from:x_dt_from,dt_to:x_dt_to,emp_type:x_emp_type,selected_emp:x_selected_emp,dtrfontsize:x_fontdtrsize},
		success: function(data){
				$("#lod").css("display","none");
	$("#vis").css("display","block");
			$("#thedtrcontainer").html(data);
		}
	})
</script>
