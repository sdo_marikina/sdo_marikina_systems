<?php

include("../connection/connect.php");

// DELETE DUPLICATES
$gh = "SELECT id,eid,timestamp,ismanual,date FROM
attendance_logs  WHERE date BETWEEN '" . $_POST["dt_from"] . "' AND '" . $_POST["dt_to"] . "'";
$res = mysqli_query($c,$gh);
$dupscount = 0;
while ($row = mysqli_fetch_array($res)) {
$q = "SELECT timestamp,eid,date FROM attendance_logs WHERE timestamp LIKE '%" . date("Y-m-d H:i",strtotime($row["timestamp"])) . "%' AND eid='" . $row["eid"] . "' AND date ='" . $row["date"] . "'";

$xres = mysqli_query($c,$q);

if(mysqli_num_rows($xres) > 1){
$dupscount++;
$q = "DELETE FROM attendance_logs WHERE id ='" . $row["id"] . "' ";
mysqli_query($c,$q);
}
}



$q = "SELECT * FROM lams_configurations WHERE label='oic' LIMIT 1";
$row = mysqli_fetch_array(mysqli_query($c,$q));
$our_oic = $row["value"];
$our_oic_position = $row["value_type"];

$q = "SELECT * FROM lams_configurations WHERE label='usertype' LIMIT 1";
$row = mysqli_fetch_array(mysqli_query($c,$q));

$utype =  $row["value"];

$q = "SELECT * FROM lams_configurations WHERE label='prin' LIMIT 1";
$row = mysqli_fetch_array(mysqli_query($c,$q));

$princname = $row["value"];
$inchargebar = "School Head";



if ($utype == "0") {
    $princname = "DEPARTMENT HEAD";
    $inchargebar = "In Charge";
}
  $gh = "SELECT eid,date,timestamp,ismanual FROM
  attendance_logs WHERE ismanual='1' AND date BETWEEN '" . $_POST["dt_from"] . "' AND '" . $_POST["dt_to"] . "'";
            $res = mysqli_query($c,$gh);

            while($row = mysqli_fetch_array($res)){
                $timestamp_f = date("Y-m-d H:i",strtotime($row["timestamp"]));
                $q = "SELECT eid,date,timestamp,ismanual FROM attendance_logs WHERE date='" . $row["date"] . "' AND timestamp LIKE '%" . $timestamp_f . "%'  AND eid='" . $row["eid"] . "'";
                $xrow = mysqli_query($c,$q);
                if(mysqli_num_rows($xrow) >= 2){
                   $d = "DELETE FROM attendance_logs WHERE date='" . $row["date"] . "' AND timestamp LIKE '%" . $timestamp_f . "%' AND eid='" . $row["eid"] . "' AND ismanual='0'";
                   mysqli_query($c,$d);
                }
            }

            // GET HOLIDAYS
            $gh = "SELECT date,name FROM holidays";
            $hol_date = array();
            $hol_name = array();
            $res = mysqli_query($c, $gh);
            while($hrow = mysqli_fetch_array($res)){
                $dd = explode("-", $hrow["date"]);
                $fullHolidayDate =  date('Y-m-d', mktime(0, 0, 0, $dd[0], $dd[1]));
                array_push($hol_date, $fullHolidayDate);
                array_push($hol_name, $hrow["name"]);
            }

            if($_POST["emp_type"] == 0){
                $q = "SELECT fname,lname,mname, eid,schedule,type FROM employees ORDER BY lname ASC";
            } else if($_POST["emp_type"] == 4){
                $q = "SELECT id,fname,lname,mname, eid,schedule,type FROM employees WHERE ";
                $sel_emp = $_POST["selected_emp"];
                for ($i_all=0; $i_all < count($sel_emp); $i_all++) {

                         if ($i_all == (count($sel_emp) -1)) {
                            $q .= " id='" . $sel_emp[$i_all] . "'  ORDER BY lname ASC";
                         }else{
                             $q .= " id='" . $sel_emp[$i_all] . "' OR";
                         }

                }
                // echo $q;
            }else{
                $q = "SELECT fname,lname,mname, eid,schedule,type FROM employees WHERE type='" . $_POST["emp_type"] . "' ORDER BY lname ASC";
            }
            $res =mysqli_query($c,$q);
    $vis_when_diss = true;

    $pagec = 0;
    $totalemptogen = mysqli_num_rows($res);
            while($row = mysqli_fetch_array($res)){
 $pagec ++;
// CHECK APPLIED LEAVE DATES
    $days_with_leave = array();
    $days_leavetype = array();
    $days_leavestatus = array();
    $days_leavevisiblewhendissaprove = array();
    $leaveday = "SELECT * FROM applied_leave WHERE employee_id='" . $row["eid"] . "' AND status != '2'";
    $res_leaveday = mysqli_query($c,$leaveday);

    while($lv_row = mysqli_fetch_array($res_leaveday)){

        $dd_from = $lv_row["date_from"];
        $dd_to = $lv_row["date_to"];


        while (strtotime($dd_from) <= strtotime($dd_to)) {
            if(!in_array($dd_from, $days_with_leave)){
                 array_push($days_leavestatus,$lv_row["status"]);


                 $leavetype = "";
             
                 switch ($lv_row["leave_type"]) {
                    case 'Service Credit':
                    $leavetype = "SL";
                    break;
                    case 'Sick Leave':
                    $leavetype = "SL";
                    break;
                    case 'Vacation Leave':
                    $leavetype = "VL";
                    $vis_when_diss = false;
                    break;
                    case 'Leave Without Pay':
                    $leavetype = "LWP";
                    break;
                    case 'Single Parent Leave':
                    $leavetype = "SPL";
                    $vis_when_diss = false;
                    break;
                    case 'Forced Leave':
                    $leavetype = "FL";
                    $vis_when_diss = false;
                    break;
                    case 'Paternity Leave':
                    $leavetype = "PL";
                    $vis_when_diss = false;
                    break;
                    case 'Maternity Leave':
                    $leavetype = "ML";
                    $vis_when_diss = false;
                    break;
                    default:
                     $leavetype = $lv_row["leave_type"];
                    break;
                 }
                 
                 array_push($days_leavetype, $leavetype);
                array_push($days_with_leave,$dd_from);
            }
            $dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
        }
        
    }


    // CHECK FOR AUTHORITY TO ATTEND DAYS 
    $auth_days = array();

    $leaveday = "SELECT * FROM authorities WHERE emp_code='" . $row["eid"] . "' AND auth_status != '2'";
    $res_leaveday = mysqli_query($c,$leaveday);
    while($lv_row = mysqli_fetch_array($res_leaveday)){
        $dd_from = $lv_row["date_from"];
        $dd_to = $lv_row["date_to"];
            while (strtotime($dd_from) <= strtotime($dd_to)) {
                if(!in_array($dd_from, $auth_days)){
                    array_push($auth_days,$dd_from);
                }
                $dd_from = date ("Y-m-d", strtotime("+1 day", strtotime($dd_from)));
            }  
    }

            // GET IF HAS MULTIPLE SCHED
            // $get_multiple_att_latest = "SELECT * FROM multiple_sched WHERE eid='" . $row["eid"] . "' AND effectivity in (SELECT MAX(effectivity) FROM multiple_sched WHERE effectivity < '" . date("Y-m-d H:i:s") . "' AND eid='" . $row["eid"] . "' GROUP BY dayname) GROUP BY dayname";

            $get_multiple_att_latest = "SELECT * FROM multiple_sched WHERE eid='" . $row["eid"] . "' AND effectivity in (SELECT MAX(effectivity) FROM multiple_sched WHERE effectivity < '" . date("Y-m-d H:i:s") . "' AND eid='" . $row["eid"] . "' GROUP BY dayname)";
            $res_getmultiple_att_latest = mysqli_query($c,$get_multiple_att_latest);
            $multiple_sched_array_latest = array();



            $get_multiple_att = "SELECT * FROM multiple_sched WHERE eid='" . $row["eid"] . "'";
            $res_getmultiple_att = mysqli_query($c,$get_multiple_att);
            $multiple_sched_array = array();

                        echo "<p style='page-break-before: always;'></p>";

                        if ($row["mname"] != null && $row["mname"] != "") {
                            $mymiddle= $row["mname"][0] . ".";
                        }else{
                            $mymiddle= "";
                        }
                  
                    $fullname = strtoupper($row["lname"] . ", " . $row["fname"] . " " . $mymiddle);

                    $emp_firstname = $row["fname"];
                    $emp_lastname = $row["lname"] ;
                    $sched =  $row["schedule"];
                    $school = strtoupper($_POST["sc_name"]);
                   ?>
                   <div class="has_watermark"  style=" page-break-after: always; display: inline; height: 1000px; font-size: <?php echo $_POST["dtrfontsize"]; ?>;">
                   <!-- DTR FORMAT HEADING -->
                   <span class="float-left text-muted"><?php echo $pagec . "/" . $totalemptogen; ?></span>
<center><h5>SDO-Marikina City<br><small>Daily Time Record</small></h5></center>
  <p style="float: right;"><?php echo date("F j, Y",strtotime($_POST["dt_from"])) . " to " . date("F j, Y",strtotime($_POST["dt_to"])); ?><br><small style="float: right;"><?php
            $emp_type = 0;
        switch($row["type"]){
            case "1":
                echo "Non-Teaching Personnel";
                $emp_type = 1;
            break;
            case "2":
                echo "Teaching Personnel";
                $emp_type = 2;
            break;
            case "3":
                echo "Division Personnel";
                $emp_type = 3;
            break;
            default:

            break;
        }
      ?></small></p>
  <!-- EMPLOYEE BASIC INFORMATION -->
<h5>
    <small><?php echo  $school; ?></small><br>
    <b><?php echo $fullname; ?> <small>(#<?php echo $row["eid"]; ?>)</small><br></b>
    <small><?php 
    $shed_array = explode(",", $sched);

if(count( $shed_array) > 2){
            switch($row["type"]){
            case "1":
                //Non-Teaching Personnel
             echo "<strong>(</strong>" . $shed_array[0] . "<strong>)</strong>-----<strong>(</strong>" . $shed_array[2] . "-" . $shed_array[3] ."<strong>)</strong>-----<strong>(</strong>" . $shed_array[1] . "<strong>)</strong>";
            break;
            case "2":
                //Teaching Personnel
     
            $secondary_sched = "";
            if (mysqli_num_rows($res_getmultiple_att) != 0) {
                while($row_matt = mysqli_fetch_array($res_getmultiple_att)){
            $shedDisect = explode(",", $row_matt["schedule"]);

            array_push($multiple_sched_array,$row_matt["dayname"] . "|" . $row_matt["effectivity"] . "|" . $row_matt["schedule"]);
            $secondary_sched .= $row_matt["dayname"] . ": " . $shedDisect[0] . "-" . $shedDisect[1] . " ";
                }
            }else{
                 $secondary_sched = "None";
            }


            // GET LATEST 
             $secondary_sched_latest = "";
            if (mysqli_num_rows($res_getmultiple_att_latest) != 0) {

                $sec_count = mysqli_num_rows($res_getmultiple_att_latest);
                $sec_curr_count = 0;;
                while($row_matt_x = mysqli_fetch_array($res_getmultiple_att_latest)){
                   


                    if($sec_curr_count == ($sec_count -1)){
                        // LAST 
                        $shedDisect_x = explode(",", $row_matt_x["schedule"]);

            array_push($multiple_sched_array_latest,$row_matt_x["dayname"] . "|" . $row_matt_x["effectivity"] . "|" . $row_matt_x["schedule"]);
            $secondary_sched_latest .= $row_matt_x["dayname"] . ": " . date("g:i a",strtotime($shedDisect_x[0])) . "-" . date("g:i a",strtotime($shedDisect_x[1])) . " since " . date("F j, Y",strtotime($row_matt_x["effectivity"]));

                    }else{
                        //CONTINOUS
                        $shedDisect_x = explode(",", $row_matt_x["schedule"]);

            array_push($multiple_sched_array_latest,$row_matt_x["dayname"] . "|" . $row_matt_x["effectivity"] . "|" . $row_matt_x["schedule"]);
            $secondary_sched_latest .= $row_matt_x["dayname"] . ": " . date("g:i a",strtotime($shedDisect_x[0])) . "-" . date("g:i a",strtotime($shedDisect_x[1])) . " since " . date("F j, Y",strtotime($row_matt_x["effectivity"]))  . " and ";

                    }
             $sec_curr_count ++;
                }
            }else{
                 $secondary_sched_latest = "None";
            }


            echo "<small><i class='far fa-clock'></i></small> Primary: " . date("g:i a", strtotime($shed_array[0])) . " - " . date("g:i a", strtotime($shed_array[1])) . "<br><small><i class='far fa-clock'></i></small> Secondary: " .   $secondary_sched_latest;
            break;
            case "3":
                //Division Personnel
             echo $shed_array[0] . " - " . $shed_array[1];
            break;
        } 
}else{
    echo "(No Added Schedule)";
}

     ?></small></h5>
    <!-- EMPLOYEE DTR TABLE -->
<table class="table table-sm table-bordered has_watermark">
    <thead>
        <tr>
            <th>DATE</th>
            <th style="text-align: center;">IN</th>
            <th style="text-align: center;">OUT</th>
            <th style="text-align: center;">IN</th>
            <th style="text-align: center;">OUT</th>
            <th style="text-align: center;">UT</th>
        </tr>
    </thead>
<tbody>
        <?php
    // GENRATE FROM - TO DATES
    $date = date('Y-m-d', strtotime($_POST["dt_from"]));
    $end_date = date("Y-m-d",strtotime($_POST["dt_to"]));
    $overall_ut_count = 0;
     $c_used = "";
     $teacher_fixed_outin = array("","");
     $dcounter =0;
    while (strtotime($date) <= strtotime($end_date)) {
    $dcounter++;
             
    $xdate=date_create($date);
    $currentdate = date_format($xdate,"M j, Y") . " <span style='color: rgba(0,0,0,0.7);'>" . date_format($xdate,"l") . "</span>";
    // GET FIRST AND LAST LOG
    $q = "SELECT ismanual,timestamp FROM attendance_logs WHERE date='" . $date ."' AND eid='" . $row["eid"] . "' ORDER BY timestamp ASC";
    $emp_xres = mysqli_query($c,$q);
    $emp_res = mysqli_fetch_all($emp_xres,MYSQLI_ASSOC);
    $xrowcount = mysqli_num_rows($emp_xres)-1;

    $in = "";
    $out = "";

    if(mysqli_num_rows($emp_xres) >= 1){
        if($emp_res[0]["ismanual"] == "1"){
            $in = "*" . date("g:i a",strtotime($emp_res[0]["timestamp"]));
        }else{
            $in = date("g:i a",strtotime($emp_res[0]["timestamp"]));
        }
    }
    if(mysqli_num_rows($emp_xres) > 1 && $emp_res[$xrowcount]["timestamp"]){
        $row_count_computed= 0;
        
        if($row["type"] == 2){
            if($xrowcount >= 3){
            $row_count_computed = 3;
        }else{
            $row_count_computed = $xrowcount;
        }
        }

        if($emp_res[0]["ismanual"] == "1"){
            $out = "*" . date("g:i a",strtotime($emp_res[$row_count_computed]["timestamp"]));
        }else{
            $out = date("g:i a",strtotime($emp_res[$row_count_computed]["timestamp"]));
        }
    }
        $ut_count = 0;
            $cm_io = array('','','','');
        for($index = 0; $index < count($cm_io);$index++){
                $xtime = "";
            if($index < mysqli_num_rows($emp_xres)){
            if($emp_res[$index]["ismanual"] == "1"){
                $xtime = "*" . date("g:i a",strtotime($emp_res[$index]["timestamp"]));
            }else{
                $xtime = date("g:i a",strtotime($emp_res[$index]["timestamp"]));
            }
                $cm_io[$index] = $xtime;
            }else{
                $cm_io[$index] = "";
            }
        }
    if($emp_type == 3){
        //DIVISION
        $ut_count = undertimecomputation_division($cm_io[0],$cm_io[1],$cm_io[2],$cm_io[3]);
    }elseif($emp_type == 1){
         //NON-TEACHING

        if(date_format($xdate,"D") != "Sat" && date_format($xdate,"D") != "Sun"){
             $ut_count = undertimecomputation_nonteaching($cm_io[0],$cm_io[1],$cm_io[2],$cm_io[3],$shed_array);
        }else{
            $ut_count = "";
        }
       
    }else{
        // TEACHING PERSONNEL
        //COMPUTE ASSIGNED SCHEDULE FOR THIS DAY
        $hmx = 0;
        for ($xi=0; $xi < count($multiple_sched_array); $xi++) { 
            $disctted_effectivity = explode("|", $multiple_sched_array[$xi]);
            $dnamex = $disctted_effectivity[0];
             // echo "-----" . ;
            if(date_format($xdate,"l") == $dnamex && strtotime($disctted_effectivity[1]) < strtotime(date_format($xdate,"Y-m-d"))){
            $c_sched  = strtoupper($disctted_effectivity[2]) . ",,";
            $c_used =  "";


            $ut_count = undertimecomputation($c_sched,$in,$out);
            $teacher_fixed_outin[0] = $in;
            $teacher_fixed_outin[1] = $out;
            $hmx = 1;
            }
        }
        if($hmx == 0){
            $c_used =  "";
             $ut_count = undertimecomputation($sched,$in,$out);
             $teacher_fixed_outin[0] = $in;
            $teacher_fixed_outin[1] = $out;
        }

    }
    if($ut_count != ""){
        $hasholi = 0;
        for($h = 0; $h < count($hol_date);$h++){
            if($hol_date[$h] == date_format($xdate,"Y-m-d")){
                $hasholi = 1;
            }
        }
        if($hasholi == 0){
            $overall_ut_count += intval($ut_count);
        }else{
            $ut_count = 0;
        }
    }
        ?> 
        <tr>
            <td><?php
            echo $currentdate;
             //CHECK IS THIS HAS CHANGED
                $toInclude = "";
              for($h = 0; $h < count($hol_date);$h++){
                if($hol_date[$h] == date_format($xdate,"Y-m-d")){
                   


                     // $mm_dayname = $multiple_sched_array;
                     //    $mm_effectivity = $multiple_sched_array;
                    
                        echo " <i class='fas fa-h-square'></i> - <span>" . $hol_name[$h] . "</span> " . $toInclude;
                    


                }
              }
               
                if(count($multiple_sched_array) != 0){
                  
                    for ($xi=0; $xi < count($multiple_sched_array); $xi++) { 

                        $disctted_effectivity = explode("|", $multiple_sched_array[$xi]);
                        $dnamex = date("Y-m-d",strtotime($disctted_effectivity[1]));
                    }
                }
                $dd_hasleave = false;
                $dd_approvedleave = false;
                $leave_type_of_now = "";
                if( $vis_when_diss == false){
                    for ($lvt=0; $lvt < count($days_with_leave); $lvt++) { 
                    if(date_format($xdate,"Y-m-d") == $days_with_leave[$lvt]){
                    echo "(" . $days_leavetype[$lvt] . ")";
                    $leave_type_of_now = $days_leavetype[$lvt];
                    $dd_hasleave = true;
                    // CHECK IF APPROVED 
                    if ($days_leavestatus[$lvt] == "1") {
                    $dd_approvedleave = true;
                            }
                        }
                    }
                }

                for ($xatai=0; $xatai < count($auth_days); $xatai++) { 
                    if(date_format($xdate,"Y-m-d") == $auth_days[$xatai]){
                         echo "(ATA)";
                    }
                }
// echo json_encode($auth_days);
             ?></td>

             <?php
                if($emp_type == 2){

                    $manual_in = "*" . $teacher_fixed_outin[0];
                     $manual_out = "*" . $teacher_fixed_outin[1];
                    if(in_array( $manual_in , $cm_io)){
                        $teacher_fixed_outin[0] =  $manual_in ;
                    }
                    if(in_array( $manual_out , $cm_io)){
                        $teacher_fixed_outin[1] =  $manual_out ;
                    }
                    ?>
            <td style='text-align: center;'><?php echo "<div style='width:80px;'></div>" .  $teacher_fixed_outin[0]; ?></td>
            <td style='text-align: center;'><?php echo "<div style='width:80px;'></div>" .  $teacher_fixed_outin[1]; ?></td>
            <td style='text-align: center;'></td>
            <td style='text-align: center;'></td>
                    <?php
                }else{
                    ?>
<td style='text-align: center;'><?php echo "<div style='width:80px;'></div>" .  $cm_io[0]; ?></td>
            <td style='text-align: center;'><?php echo "<div style='width:80px;'></div>" .  $cm_io[1]; ?></td>
            <td style='text-align: center;'><?php echo "<div style='width:80px;'></div>" .  $cm_io[2]; ?></td>
            <td style='text-align: center;'><?php echo "<div style='width:80px;'></div>" .  $cm_io[3]; ?></td>
                    <?php
                }
             ?>
            
            <td style="text-align: center;"><?php
                if($dd_hasleave == true && $dd_approvedleave == true){

                    if($leave_type_of_now == "LWP"){
                        echo $ut_count; 
                    }else{
                        echo "Offset";
                    }
                    
                    // $ut_count = "1";
                    if((int)$ut_count != 0){
                        $overall_ut_count =  ($overall_ut_count - (int)$ut_count);
                    }
                    
                }else{
                    echo $ut_count; 
                }
             ?></td>
        </tr>



        <?php
        $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));   
    }
        ?>
        <!-- <tfoot> -->
        <tr>
        <th colspan="6" style="text-align: right;"><span style="color: rgba(0,0,0,0.5)">Total Undertime Breakdown: </span><?php echo breakdownminutes($overall_ut_count); ?></th>
        </tr>
        <tr>
            <td colspan="6">
                <center>
    <p>I Certify on my honor that the above is a true and correct report of the hours work performed, record of which was daily at the time of arrival and departure from office.</p>
    <br>
    <br>
    <div class="row">
        <div class="col-sm-6">
            <span style="border-top: 1px solid black; display: block;"></span>
           <?php echo "<strong>" .  $fullname . "</strong>"; ?><p style="margin-top: -5px;">Employee Signature</p>
        </div>
                <div class="col-sm-6">
            <span style="border-top: 1px solid black; display: block;"></span>
           <?php 
           $printoecho =  $princname;
           $theincharege = $inchargebar;
          
     
            if(strpos(strtoupper($princname), $emp_firstname) !== false && strpos(strtoupper($princname), $emp_lastname) !== false && $utype == "1"){
$printoecho = $our_oic;
$theincharege = $our_oic_position;
            }
           
 echo  "<strong>" .  $printoecho . "</strong>";  ?><p style="margin-top: -5px;"><?php echo $theincharege; 
           ?></p>
        </div>
    </div>
</center>
            </td>
        </tr>
    </tbody>
</table>

   </div>

<?php

        }

       function undertimecomputation_nonteaching($in_am,$out_am,$in_pm,$out_pm,$sched){
        // $toecho = "";
        if($in_am != "" && $out_am != "" && $in_pm != "" && $out_pm != "" && count($sched) == 4){
            $ot = 0;

            $s_in_am = str_replace("*", "", $sched[0]);
            $s_out_am = str_replace("*", "", $sched[2]);
            $s_in_pm = str_replace("*", "", $sched[3]);
            $s_out_pm = str_replace("*", "", $sched[1]);

            $in_am = str_replace("*", "", $in_am);
            $out_am = str_replace("*", "", $out_am);
            $in_pm = str_replace("*", "", $in_pm);
            $out_pm = str_replace("*", "", $out_pm);
            $v1 =0;
            $v2 =0;
            $v3 =0;
            $v4 =0;

        // $toecho .= $sched[0] . "-----" . $sched[2] . "-" . $sched[3]. "-----" .$sched[1];
         // $toecho .= "<br>" . $in_am . "-----" . $out_am . "-" . $in_pm. "-----" .$out_pm;
 
        if(strtotime($s_in_am) <= strtotime($in_am)){
              // if($in_am > $s_in_am){
            $v1 = str_replace("-", "", strtotime($s_in_am) - strtotime($in_am)) / 60;
            $ot += $v1;
              // }
        }
            // $toecho .= "<br>inam: " .  $v1;
          
        if(strtotime($s_out_am) >= strtotime($out_am)){
           if(strtotime($out_am) < strtotime($s_out_am)){
            $v2 = str_replace("-", "", strtotime($s_out_am) - strtotime($out_am)) / 60;
             $ot += $v2;
           }
        }
            // $toecho .= "<br>outam: " .  $v2;
       
        if(strtotime($s_in_pm) <= strtotime($in_pm)){
             if(strtotime($in_pm) > strtotime($s_in_pm)){
                $v3  = str_replace("-", "", strtotime($s_in_pm) - strtotime($in_pm)) / 60;
                $ot += $v3;
             }
        }
            // $toecho .= "<br>inpm: " .  $v3;
         
        if(strtotime($s_out_pm) >= strtotime($out_pm)){
              if(strtotime($out_pm) < strtotime($s_out_pm)){
                $v4 = str_replace("-", "", strtotime($s_out_pm) - strtotime($out_pm)) / 60;
            $ot += $v4;
             }
        }
            // $toecho .= "<br>outpm: " .  $v4;
        // $toecho .= "<br>Overall:" . $ot;
        return $ot;
    }else if($in_am != "" && $out_am != "" && $in_pm == "" && $out_pm == "" && count($sched) == 4){
          $ot = 0;

            $s_in_am = str_replace("*", "", $sched[0]);
            $s_out_am = str_replace("*", "", $sched[2]);
            $s_in_pm = str_replace("*", "", $sched[3]);
            $s_out_pm = str_replace("*", "", $sched[1]);

            $in_am = str_replace("*", "", $in_am);
            $out_am = str_replace("*", "", $out_am);
            $in_pm = str_replace("*", "", $out_am);
            $out_pm = str_replace("*", "", $out_am);
            $v1 =0;
            $v2 =0;
            $v3 =0;
            $v4 =0;

        // $toecho .= $sched[0] . "-----" . $sched[2] . "-" . $sched[3]. "-----" .$sched[1];
         // $toecho .= "<br>" . $in_am . "-----" . $out_am . "-" . $in_pm. "-----" .$out_pm;
 
        if(strtotime($s_in_am) <= strtotime($in_am)){
              // if($in_am > $s_in_am){
            $v1 = str_replace("-", "", strtotime($s_in_am) - strtotime($in_am)) / 60;
            $ot += $v1;
              // }
        }
            // $toecho .= "<br>inam: " .  $v1;
          
        if(strtotime($s_out_am) >= strtotime($out_am)){
           if(strtotime($out_am) < strtotime($s_out_am)){
            $v2 = str_replace("-", "", strtotime($s_out_am) - strtotime($out_am)) / 60;
             $ot += $v2;
           }
        }
            // $toecho .= "<br>outam: " .  $v2;
          $ot += 300;
            // $toecho .= "<br>outpm: " .  $v4;
        // $toecho .= "<br>Overall:" . $ot;


          if($ot > 480){
            return "";
          }else{
            return $ot;
          }
        
    }else{
        return "";
    }
    }
        function undertimecomputation_division($aAMin,$aAMou,$aPMin,$aPMou){

        // Declaration of Ph time-format
        $aAMin = strtotime(str_replace("*", "", $aAMin));
        $aAMou = strtotime(str_replace("*", "", $aAMou));
        $aPMin = strtotime(str_replace("*", "", $aPMin));
        $aPMou = strtotime(str_replace("*", "", $aPMou));

        date_default_timezone_set('Asia/Manila');
        $a_in = explode(":", date("h:i:a", $aAMin));
        // Validation of ealiest in
        if($a_in[0] < 7 && $a_in[2] == "am"){ $aAMin = strtotime("7:00 am"); }
        $rq_hrs = 8;
        // Declaraction lunch time
        $lunch = array("12:00 pm","1:00 pm");
        $mUTin = 0;
        $mUTou = 0;
        $ttl = 0;
        // Computation of morning undertime
        if($aAMin <= strtotime("9:30 am")){$mUTin =0;}else{$mUTin = ($aAMin - strtotime("9:30 am")) / 60;}
        if($aAMou >= strtotime("12:00 pm")){$mUTou = 0;}else{$mUTou = (strtotime($lunch[0]) - $aAMou) / 60;}
        $ttl = ($mUTin + $mUTou);
        // Afternoon undertime computation
        $aft_In = 0;
        $aft_Ou = 0;
        if($aPMin <= strtotime("1:00 pm")){$aft_In = 0;}else{$aft_In = ($aPMin - strtotime($lunch[1])) / 60;}
        // Computation of required out time
        $d1 = new DateTime( date("Y-m-d") . ' '.  date("g:i a",$aAMin));
        $d2 = $d1->diff(new DateTime( date("Y-m-d") . ' '.  date("g:i a",strtotime($lunch[0]))));
        $cmtEndT = strtotime("-" . $d2->h  . " hours",strtotime($rq_hrs . ":00 pm"));
        $cmtEndT = strtotime("-" . $d2->i  . " minutes ",$cmtEndT);
        $cmtEndT = strtotime("+1 hours",$cmtEndT);
        $newreq_out = $cmtEndT;
        $ouResHR = date("h",$newreq_out);
        $minResHR = date("i",$newreq_out);
        $vlReqHr = 0;
        // Above 4 pm and under 6:30 valication
        if($ouResHR < 4){$vlReqHr = strtotime("4:00 pm");}else if($ouResHR >= 6){
        if($minResHR >= 30) {$vlReqHr = strtotime("6:30 pm");}else if($ouResHR == 6){$vlReqHr = strtotime("6:" . $minResHR . " pm");
        }else{$vlReqHr = strtotime("6:30 pm");}}else{$vlReqHr = $newreq_out;}
        if($aPMou == ""){ $aft_Ou = ($vlReqHr - strtotime($lunch[1])) / 60; }else if($aPMou >= $vlReqHr){$aft_Ou = 0;}else{$aft_Ou = ($vlReqHr - $aPMou) / 60;}


        $tmp_aAMin = explode(":", date("h:i:a",$aAMin));
        $tmp_aAMou = explode(":", date("h:i:a",$aAMou));
        $tmp_aPMin = explode(":", date("h:i:a",$aPMin));
        $tmp_aPMou = explode(":", date("h:i:a",$aPMou));

        $log_index = array($aAMin,$aAMou,$aPMin,$aPMou);
        $log_count = 0;
        for ($i=0; $i < count($log_index); $i++) { 
            if($log_index[$i] != ''){
                $log_count++;
            }
        }


        if($log_count == 0){
            $ttl = "";
        }else if($log_count == 1){
            $ttl = "Invalid";
        }else if($log_count == 2){
            if($tmp_aAMou[0] <= 12 && $tmp_aAMou[2] == "pm"){
                $ttl = "Invalid"; 
            }else{
                $ttl += ($aft_In + $aft_Ou);
            }
        }else if($log_count == 3){
            if($tmp_aAMin[0] == $tmp_aAMou[0]){
                 $ttl = "Invalid"; 
            }else{
                 $ttl += ($aft_In + $aft_Ou);
            }
        }else if($log_count == 4){

            if ($aAMin == "" && $aAMou == "" && $aPMin == "" && $aPMou == "") {
                $ttl = "";
            }else if($tmp_aAMin[0] == $tmp_aAMou[0] && $tmp_aPMin[0] == $tmp_aAMin[0]){
                $ttl = "Invalid";
            }else{
                $ttl += ($aft_In + $aft_Ou);
            }

        }
        // Overall result
        return $ttl;
}
    function breakdownminutes($minutes){
        $day = intval(($minutes / 480));
        $hours = intval(($minutes - ($day * 480)) / 60);
        $minutes = round($minutes - ($day * 480) - ($hours * 60),0);
        echo "Day: " . $day . ", Hours: " . $hours . ", Minutes: " . $minutes;
    }

 function undertimecomputation($schedule,$curr_in,$curr_out){

        $curr_in = str_replace("*", "", $curr_in);
        $curr_out = str_replace("*", "", $curr_out);
   if($curr_in != "" && $curr_out != ""){
         $schedule = explode(",",$schedule);

         if(count($schedule) >= 2){
            // SCHEDULE
$req_in = strtotime($schedule[0]);
// return $schedule[3];
$req_out = strtotime($schedule[1]);
// HUMAN INPUT
$act_in = strtotime($curr_in);
$act_out = strtotime($curr_out);

if($req_in > $act_in){
    // echo "0";
}
if($req_out < $act_out){
    // echo "0";
}

$comp_in_ut = 0;
$comp_out_ut = 0;
if($req_in < $act_in){
    $comp_in_ut = str_replace("-", "", ($act_in - $req_in) / 60);
}

if($req_out > $act_out){
    $comp_out_ut = str_replace("-", "", ($act_out - $req_out) / 60);
}
return round(($comp_in_ut + $comp_out_ut),0);

}else{
    return "";
}

}else{
    return "";
}
    }

?>