
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Manual Backup</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>


</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
						<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="fas fa-toolbox"></i> MAINTENANCE CENTER</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		    </ul>
		  </div>
		</nav>
		<div class="container">
	
<div class="row">
	<div class="col-sm-12">
	<div class="form-group">
	<div class="card">
		<div class="card-body">
		<img src="images/downloadcsv.png" style="width: 80px;">
		<h5 class="card-title">Export Data to CSV</h5>
		<h6 class="card-subtitle text-muted mb-3">Download system medium records.</h6>
<div class="row">
	<div class="col-sm-12">
			<form action="php/exportdata.php" target="_blank">		
		<div class="form-group">
			<button class="btn btn-primary btn-sm"><i class="fas fa-download"></i> Attendance Logs</button>
		</div>
	</form>
	</div>
		<div class="col-sm-12">
			<form action="php/exportdata_dispute.php" target="_blank">		
		<div class="form-group">
			<button class="btn btn-primary btn-sm"><i class="fas fa-download"></i> Dispute Reports</button>
		</div>
	</form>
	</div>
		<div class="col-sm-12">
			<form action="php/exportdata_leavereports.php" target="_blank">		
		<div class="form-group">
			<button class="btn btn-primary btn-sm"><i class="fas fa-download"></i> Leave Reports</button>
		</div>
	</form>
	</div>
		<div class="col-sm-12">
			<form action="php/exportdata_authoritytoattend.php" target="_blank">		
		<div class="form-group">
			<button class="btn btn-primary btn-sm"><i class="fas fa-download"></i> Authority to Attend & Travel</button>
		</div>
	</form>
	</div>
		<div class="col-sm-12">
			<form action="php/exportdata_employees.php" target="_blank">		
		<div class="form-group">
			<button class="btn btn-primary btn-sm"><i class="fas fa-download"></i> Employees</button>
		</div>

	</form>
	</div>
</div>

		</div>
	</div>
				</div>
	</div>
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<img src="images/intellisync.png" style="width: 80px;">
				<h5 class="card-title">Manual Intellisync</h5>
				<table class="table table-sm table-bordered">
					<tr>
						<td>Attendance</td>
						<td><button data-toggle="modal" data-target="#m_resync" class="btn btn-sm btn-primary" onclick="OpenManualSync('ATTENDANCE',0)">Sync</button></td>
					</tr>
			<!-- 		<tr>
						<td>Leave Reports</td>
						<td><button data-toggle="modal" data-target="#m_resync" class="btn btn-sm btn-primary" onclick="OpenManualSync('LEAVE REPORTS',1)">Sync</button></td>
					</tr> -->
				</table>
			</div>
		</div>
	</div>
</div>
		</div>
	</div>
</body>
</html>

<div class="modal" tabindex="-1" id="m_resync" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h5 class="ultratitle">INTELLISYNC : <span id="tosynname"></span></h5>
      	<div class="padder">
      		<p>Please don't close your browser or interact with CDTRS until this finished.</p>
      		<progress style="width: 100%;" id="proggie" value="0" max="100"></progress>
      		<p id="backupperstatus">Progress</p>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="st_sync()"  id="syncbutton" class="btn btn-primary">Begin Sync</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	

	var backed_up = 0;
	var sync_type = "";
	function OpenManualSync(Synctype,sync_module){
		$("#tosynname").html(Synctype);
		switch(sync_module){
			case 0:
				sync_type = "sync_attlogs_fast";
			break;
			case 1:
				sync_type = "sync_applyl";
			break
		}
	}
	function st_sync(){
		if(sync_type == "sync_attlogs_fast"){
		ResetSync();
		}
		
	}

	function ResetSync(){
		$.ajax({
		type: "POST",
		url: "php/external_server.php",
		data: {resetsync:"x"},
		success: function(data){
			// alert(data);
			BeginAttSync();
		}
	})
	}
	function BeginAttSync(){
		backed_up = 0;
		$("#welcome_sync").css("display","none");
		$("#syncbutton").css("display","none");
		for(var i = 0 ; i < 100;i++){
			$.ajax({
		type: "POST",
		url: "php/external_server.php",
		data: {sync_attlogs_fast:"x"},
		success: function(data){
			console.log(data);
			var curr = $("#proggie").val();
			curr++;
			backed_up ++;
			if(backed_up == 100){
				$("#syncbutton").css("display","block");
			}
			$("#proggie").val(curr);
			$("#backupperstatus").html(backed_up +  "%");
		}
	})
		}
	}
</script>

<!--
<div class="modal" tabindex="-1" role="dialog" id="howtoconnectmodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
      	<h1 class="modal-title ultrabold">Setting up Local Back-up Machine</h1>
      	
        <p>Call 275-2941 for assitance</p>
      </div>

    </div>
  </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="syncmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<button data-dismiss="modal" class="close">&times;</button>
      	<center><h1 class="ultrabold" id="headerstatsbackup">Synching</h1><p id="sydesc">This could take a while. Please be patient.</p></center>
      <table class="table table-bordered">
      	<tr>
      		<td> <h3><img id="lod_att" src="https://cdn.dribbble.com/users/172519/screenshots/3520576/dribbble-spinner-800x600.gif" style="width: 100px;"> Attendance</h3></td>
      		</tr>
      		<tr>
      		<td> <h3><img id="lod_disp" src="https://cdn.dribbble.com/users/1751799/screenshots/5512482/check02.gif" style="width: 100px;"> Disputes</h3></td>
      		</tr>
      		<tr>
      		<td> <h3><img id="lod_leaverep" src="https://cdn.dribbble.com/users/172519/screenshots/3520576/dribbble-spinner-800x600.gif" style="width: 100px;"> Authorities</h3></td>
      		</tr>
      		<tr>
      		<td> <h3><img id="lod_auth" src="https://cdn.dribbble.com/users/172519/screenshots/3520576/dribbble-spinner-800x600.gif" style="width: 100px;"> Leave Reports</h3></td>
      		</tr>
      		<tr>
      		<td> <h3><img id="lod_emp" src="https://cdn.dribbble.com/users/172519/screenshots/3520576/dribbble-spinner-800x600.gif" style="width: 100px;"> Employees</h3></td>
      	</tr>
      </table>
      <center>
      	<button id="successbackup" data-dismiss="modal" class="btn btn-info">Close</button>
      </center>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	var sydesc = $("#sydesc");

	var headerstatsbackup = $("#headerstatsbackup");
	setInterval(function(){
CheckConnection();
var lod_att = $("#lod_att");
var lod_disp = $("#lod_disp");
var lod_leaverep = $("#lod_leaverep");
var lod_auth = $("#lod_auth");
var lod_emp = $("#lod_emp");
var closebkbutton = $("#successbackup");

		if(lod_att.prop("src").includes("success") &&lod_disp.prop("src").includes("success") &&lod_leaverep.prop("src").includes("success") &&lod_auth.prop("src").includes("success") && lod_emp.prop("src").includes("success")){
			closebkbutton.css("display","inline-block");
			headerstatsbackup.html("Synchronization Complete.");
			sydesc.html("");
		}else if(lod_att.prop("src").includes("problem") || lod_disp.prop("src").includes("problem") || lod_leaverep.prop("src").includes("problem") || lod_auth.prop("src").includes("problem") ||  lod_emp.prop("src").includes("problem")){
			closebkbutton.css("display","inline-block");
			headerstatsbackup.html("Some records can't be synchronized.");
			sydesc.html("Please try again...");
		}else{
			closebkbutton.css("display","none");
		}
	},500)
	$("#backupswitch").change(function(){
		SaveBackupSettings();
	})
	$("#backupshedinput").change(function(){
		SaveBackupSettings();
	})
	$("#bip").keyup(function(){
		SaveBackupSettings();
	})
	$("#bip").bind("paste", function(){
					SaveBackupSettings();
					} );
	function SaveBackupSettings(){
			swi = $("#backupswitch").val();
			sche = $("#backupshedinput").val();
			backip = $("#bip").val();
		$.ajax({
			type : "POST",
			url : "php/external_server.php",
			data : {saveautobackupsettings: "x",switch:swi,schedule:sche,backupip:backip},
			success: function(data){
				// alert(data);
			}
		})
	}
	LoadAutoBackupSettings();
					function LoadAutoBackupSettings(){
						$.ajax({
							type: "POST",
							url: "php/external_server.php",
							data: {LoadAutoBackupSettings:"x"},
							success: function(data){
								var bstt = data.split(",");

								$("#backupswitch").val(bstt[0]);
								$("#backupshedinput").val(bstt[1]);
								$("#bip").val(bstt[2]);
								checkbackupswitch();
								CheckConnection();
							}
						})
					}
					$("#bip").keyup(function(){
						CheckConnection();
					})	
					$("#bip").bind("paste", function(){
					CheckConnection();
					} );
					
						function CheckConnection(){
							var currentbackupIP = $("#bip").val();
							if(currentbackupIP != ""){
								$.ajax({
							type: "POST",
							url: "connection/backup_connection.php",
							data: {backupIP:currentbackupIP},
							success: function(data){
								if(data == "true"){
									$("#constat").html("Connected");
									$("#begsync").css("display","block");
								}else{
									$("#constat").html("Not Connected <a href='#' data-toggle='modal' data-target='#howtoconnectmodal'>See how to.</a>");
									$("#begsync").css("display","none");
								}
							}
						})
								$("#begsync").css("display","block");
							}else{
								$("#constat").html("Please input your backup computer's IP address.");
								$("#begsync").css("display","none");
							}
						}
	checkbackupswitch();
$("#backupswitch").change(function(){
checkbackupswitch();
})
	function checkbackupswitch(){
		var bs = $("#backupswitch");
		if(bs.val() == 0){
			$("#backset_1").css("display","none");
			$("#backset_2").css("display","none");
		}else{
$("#backset_1").css("display","inline-block");
			$("#backset_2").css("display","inline-block");
		}
	}
	$("#begsync").click(function(){
		sydesc.html("This could take a while. Please be patient.");
		SyncAttendance("att","lod_att");
		SyncAttendance("disp","lod_disp");
		SyncAttendance("leaverep","lod_leaverep");
		SyncAttendance("auth","lod_auth");
		SyncAttendance("emp","lod_emp");
		headerstatsbackup.html("Synching");
		// $('#syncmodal').modal({backdrop: 'static', keyboard: false});
	})
	
	function SyncAttendance(themode,theimageid){
		$("#" + theimageid).prop("src","https://cdn.dribbble.com/users/172519/screenshots/3520576/dribbble-spinner-800x600.gif");
		var currentbackupIP = $("#bip").val();
		$.ajax({
			type : "POST",
			url : "php/syncmaster.php",
			data : {mode:themode,backupip: currentbackupIP},
			success : function(data){
				// alert(data);
				if(data == "true"){
					$("#" + theimageid).prop("src","images/success.png");
				}else{
					$("#" + theimageid).prop("src","images/problem.png");
				}
			}
		})
	}

</script>
 -->
<?php
include("components/modals.php");
?>
