<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="refresh" content="5400">
    <link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
    <title>CDTRS</title>
    <?php
    include("php/server.php");
    include("theme/theme.php");
    include("php/database_updater.php");
    include("components/new_features.php");

    $usertype = GetSettings("usertype");
    $comapnyname = GetSettings("cn");
    ?>
</head>

<body class="c1 blurbg" style=" background: <?php GetLatestWallpaper(); ?>; ">
    
    <div class="backfill">
        <?php
    include("components/loginnav.php");
?>
            <div class="container-fluid indexcont">
                <div class="row" style=" display: block; position: fixed; height: 100%; top: 0; bottom: -50; left: 0; right: 0;  margin: 50px;">
                    <div class="col-sm-12">

                        <div style="width: 100%;">
                            <script type="text/javascript">
                                $.ajax({
                                    type: "POST",
                                    url: "php/external_server.php",
                                    data: {
                                        reset_attlogs: "x"
                                    },
                                    success: function(data) {
                                        // alert(data);
                                    }
                                })
                            </script>
                            <div class="row">

                            <?php 
                                $sizeofattlogs = 12;
                                if(CheckIfKioskIsEnabled() == "true"){
                                $sizeofattlogs = 5;
                                    // include("components/home_access.php");
                                    include("components/module_kiosk.php");
                                    // include("components/module_bigprofile.php");
                                    // include("components/att_kiosk.php");
                                }else{
                                    include("components/home_access.php");
                                }
                            ?>
                            
                            <style type="text/css">
                            .versionbar {
                            display: none;
                            border-radius: 4px;
                            width: 60px;
                            bottom: 0px;
                            left: 0px;
                            position: fixed;
                            margin: 10px;
                            opacity: 0.3;
                            z-index: 20;
                            }
                            .versionbar:hover {
                            opacity: 1;
                            }
                            </style>
                                    <img src="images/V2.2.png" title="Click to see what's new!" id="card_version" class="consistent_shadow versionbar" data-toggle="modal" data-target="#whatsnew">
                                    <div class="form-group ">
                                        <div class="card blurbg poptop_anim" id="realtime_backup_x">
                                            <div class="card-body">
                                                <?php 
                                                    include("components/realtimesync.php");
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                        

                            </div>
                        </div>

                    </div>

</body>

</html>
<?php
include("components/custom_modal.php");
include("components/modals.php");
include("components/loginmodals.php");
include("components/whereaboutsmodals.php");
?>
    <div id="log"></div>

    <script type="text/javascript">
        setTimeout(function() {

            LoadServerTime();

        }, 1000)

        setInterval(function() {
            // P1
            if ($("input:eq(1)").val() == "") {
                LodTodayLogs();
            }

            // CHECK REGISTRATION
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    manage_registration: "x"
                },
                success: function(data) {
                    // alert(data);
                }
            })

            // P4
            // CHECK REGISTRATION
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    manage_expired_entitlements: "x"
                },
                success: function(data) {
                    // alert(data);
                }
            })

        }, 10000);

        function showrecentlogs_mini() {
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    getrecentlogs: "x"
                },
                success: function(data) {
                    $("#reslogscount").html(data);
                }
            })
        }
        setInterval(function() {
            LoadServerTime();
        }, 200);
        setTimeout(function() {
            $("#card_version").css("display", "block");
        }, 930)

        // END ANIMATIONS
        function LodTodayLogs() {
            $("#logsfortoday").load("php/LoadLogsForToday.php");
        }

        function LoadServerTime() {
            $("#servertimex").load("php/LoadServerTime.php");
        }

        function run_manual_backup(datetobackup) {
            $("#autobackupper").css("display", "block");
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    uploadlogs_manual: "x",
                    bk_date: datetobackup
                },
                success: function(data) {
                    setTimeout(function() {
                        $("#autobackupper").css("display", "none");
                        popnotification("Operation Complete", "Logs backup requested by SDO Marikina has been successfully submitted by the system.", false);
                    }, 2000);

                }
            })
        }
    </script>

    <div class="modal" tabindex="-1" role="dialog" id="emp_log_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="ultrabold">Today's Logs</h4>
                    <h1 class="ultrabold" id="TheEmployeeName">Loading...</h1>
                    <table class="table table-bordered table-striped" id="logdt">
                        <thead>
                            <tr>
                                <th>Access Type</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody id="thelogofsingle">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secodary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function GetTodaysLogsOfSingleEmployeeByEmpId(num) {
            $("#logdt").dataTable().fnDestroy();

            // GET EMP INFO
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    get_single_log: "x",
                    emp_number: num
                },
                success: function(data) {
                    data = JSON.parse(data);
                    // alert(data);
                    $("#TheEmployeeName").html(data[0]["lname"] + ", " + data[0]["fname"] + " " + data[0]["mname"]);
                }
            })
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    get_single_todayLogs: "x",
                    emp_number: num
                },
                success: function(data) {
                    // alert(data);
                    $("#thelogofsingle").html(data);
                    $("#logdt").DataTable({
                        "ordering": false
                    });
                }
            })

        }
    </script>

    <?php
include("components/log_welcomer.php");
include("components/updater.php");
?>