<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Whereabouts</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
		include("php/database_updater.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
<script type="text/javascript">
        TriggerWhereaboutsSync();
      </script>
		<nav class="navbar navbar-expand-lg" style="margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="fas fa-arrow-right"></i> WHEREABOUTS</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#" data-toggle="modal" data-target="#genwhereabouts"><i class="fas fa-cogs"></i> Generate Whereabouts</a>
		      </li>
		    </ul>
		  </div>
		</nav>
	<div class="container">
	<div class="alert alert-primary" role="alert">
	  Displaying only recent 100th result(s).
	</div>
	<div class="card">

		<div class="card-body">
			<img src='images/whereabouts_nano.png' class='content_icon'>
			<h5 class="ultratitle mb-3">List of Whereabouts</h5>
			<div id="datawehreaboutz">
		<table class="table table-striped table-bordered" width="100%" id="cocoa">
			<thead>
				<tr>
					<th>Employee Name</th>
					<th>Destination</th>
					<th>Purpose</th>
					<th>Departure</th>
					<th>Arrival</th>
				</tr>
			</thead>
			<tbody>
				<?php
				Disp_Whereabouts();
				?>
			</tbody>
		</table>
	</div>
		</div>
	</div>

		</div>
	</div>
</body>
</html>
<form action="php/external_server.php" method="POST" target="_blank">
	<div class="modal" tabindex="-1" role="dialog" id="genwhereabouts">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
       <h5 class="ultratitle mb-3">Generate Whereabouts</h5>
       <div class="row">
       	<div class="col-sm-6">
       		<div class="form-group">
       			<label>From:</label>
       			<input class="form-control" type="date" required="" name="genW_from">
       		</div>
       	</div>
       	<div class="col-sm-6">
       		<div class="form-group">
       			<label>To:</label>
       			<input class="form-control" type="date" required="" name="genW_to">
       		</div>
       	</div>
       	<div class="col-sm-12">
       		<label><input type="checkbox" name="inCoor"> <i class="fas fa-globe-americas"></i> Include Coordinates<br>
       			<small style="color: rgba(0,0,0,0.5);">- Coordinates sent by the employees with CDTRS Mobile App.</small></label>
       	</div>
       </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="generateWhereAboutsNow" class="btn btn-primary"><i class="fas fa-cogs"></i> Generate</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
	// popnotification("We are still working on this.", "Some features are not yeat ready for this page.",false);
		$("#cocoa").DataTable();
			function printDiv() 
{

  var divToPrint=document.getElementById('datawehreaboutz');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<style>table {  border-collapse: collapse;}table, td, th {  border: 1px solid black;}</style><html><body onload="window.print()" style="font-family: arial;">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
highlight_pagelink("#page_whereabouts");
</script>
<?php
include("components/leave_reports_teaching_modals.php");
include("components/modals.php");
?>