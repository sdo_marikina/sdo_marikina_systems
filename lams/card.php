<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>Employee Card Dashboard</title>
	<?php
	include("php/server.php");
	include("theme/theme.php");
	$usertype = GetSettings("usertype");
	$comapnyname = GetSettings("cn");
	?>
</head>
<body >
	<div style=" display:none;">
		<?php
	include("components/loginnav.php");
	?>
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<div class="row">
		<div class="col-sm-12">
			<h1 class="display-4" id="pagetitle">Employee Dashboard</h1>
			
			<hr>
			<div class="row float-right">
			<div class="col-sm-12">
				<div class="form-group">
				<input class="form-control" autocomplete="off" placeholder="Enter your employee code..." type="text" name="" id="txt_employeeCode">
				</div>
			</div>
			<div class="col-sm-12">		
				<div class="form-group">
					<button type="button" class="btn btn-primary float-right" id="btn_get_employee_code"><span>View Attendance <i class="fas fa-arrow-right"></i></span></button>
				</div>
			</div>
				</div>
			<p id="description_top"><strong>Lams</strong> | Attendance History</p>
		</div>
	</div>
	</div>
</div>

<div class="container">

					<center>
					<img src="icons/sdo.png" style="width: 70px; margin-top: -100px;">
				</center>
<div id="tbl_att_container">
		<table class="table table-bordered table-striped" id="att_table">
		<thead>
			<tr>
				<th>Date</th>
				<th>In</th>
				<th>Out</th>
				<th>In</th>
				<th>Out</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="att_table_container">
		</tbody>
	</table>
</div>
</div>
<!-- <div id="coco"></div> -->
<div id="ccc">
	
</div>
	</div>
<div class="container">
	<div class="padder">
		<h1 class="ultrathin">Ooops!</h1>
	<p>This page is no longer available. <a href="index.php">Go back to CDTRS</a></p>
</div>
</div>
</body>

<?php
include("footer.php");
?>
</html>
<script type="text/javascript">

	$("#txt_employeeCode").keypress(function(e) {
	if(e.which == 13) {
		SearchEmployeeKey();
	}
	});

	HideResult();
	var hasloaded = false;
	$("#btn_get_employee_code").click(function(){
		SearchEmployeeKey();
	})

	function SearchEmployeeKey(){
			if($("#txt_employeeCode").val() != ""){
		$("#att_table_container").html("");
		var txt_ecode = $("#txt_employeeCode").val();
			$.ajax({
		type: "POST",
		url: "php/external_server.php",
		data: {GetAttendanceOfEmployee: "x", emp_code: txt_ecode},
		success: function(data){
			
			if(data.includes("false") !== true){
			//SOW RESULT LAYOUT
			ShowResult();
			//FILL TABLE WITH RESULT
			$("#att_table_container").html(data);
			//DELCARE DATA TABLE AT ONCE
			if(hasloaded == false){
			hasloaded = true;
			$("#att_table").DataTable({
			"order": [[ 0, "desc" ]]
			});
			}

			//Get employee information via key
			$.ajax({
				type : "POST",
				url : "php/external_server.php",
				data : {GetEmployeeInfoByKey: "x", EmployeeKey: txt_ecode},
				success: function(data_2){
					data_2 = JSON.parse(data_2);
					$("#pagetitle").html("Hi, " + data_2["fname"]);
					var position = "";
					switch(data_2["type"]){
						case "1":
							position = "Non-Teaching";
						break;
						case "2":
							position = "Teaching";
						break;
						case "3":
							position = "Division";
						break;
					}
					$("#description_top").html("<strong>Position</strong> : " + position + "<br>");
					$("#description_top").append("<strong>Schedule</strong> : " + data_2["schedule"]);
				}
			})
			
		}else{
			$("#pagetitle").html("Employee Dashboard");
			$("#description_top").html("<strong>Lams</strong> | Attendance History");
			alert("Employee number has no Logs on the database.");
			HideResult();
		}
		}
	})
		}else{
			alert("Enter your employee number.");
			HideResult();
		}
		$("#txt_employeeCode").val("");
	}

	function ShowResult(){
		$("#tbl_att_container").css("display","block");
	}
	function HideResult(){
		$("#tbl_att_container").css("display","none");
	}

</script>

<?php include("components/card_modals.php");
include("components/loginmodals.php"); ?>