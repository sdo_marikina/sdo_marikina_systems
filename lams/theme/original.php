
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script type="text/javascript" src="qrpower/qrcode.js"></script>

	<script src="api/tracking.js-master/build/tracking-min.js"></script>
	<script src="api/tracking.js-master/build/data/face-min.js"></script>
	<script type="text/javascript" src="api/webcamjs-master/webcam.min.js"></script>
	
	<script src="api/chartjs/Chart.bundle.js"></script>
	<script src="api/chartjs/Chart.bundle.min.js"></script>
	<script src="api/chartjs/Chart.js"></script>
	<script src="api/chartjs/Chart.min.js"></script>

	<?php
	include("theme/essentials/essentials.php");
	include("theme/design_MacOS.php");
	?>

	<link rel="icon" href="theme/sdo.ico" type="text/css" href="">
