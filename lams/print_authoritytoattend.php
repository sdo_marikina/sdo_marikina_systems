<?php
// GET AUTHORITY DATA BY ID
include("connection/connect.php");

$authID = $_POST["auth_id"];
$q = "SELECT * FROM authorities JOIN employees ON authorities.emp_code = employees.eid JOIN positions ON employees.position = positions.id WHERE authorities.id='" . $authID  . "' LIMIT 1";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);

//GET PREFERENCES
?>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
	<title>Print | Authority to attend</title>
	<?php
	include("theme/original.php");
	?>
	<style type="text/css">
	body{
		font-family: arial;
	}
		@media print{
			.jumbotron{
				display:none;
			}
		}
		@page {
    margin: 0;
    size: 21cm 29.7cm;
	}
	.line{
		border-top: 1px solid black;
		margin: 5px;
	}
	.signature{
		width: 280px;
		position: relative;
		display: block;
		margin: 0px;
	}
	</style>
</head>
<body>

	<div class="jumbotron jumbotron-fluid">
		<div class="container">
			<h1 class="display-4">Print - Authority to Attend</h1>
		<hr>
		<button class="btn btn-primary" onclick="window.print()">Print</button>
		</div>
	</div>
<center>
	<img src="icons/sdoletterhead.png">
</center>
<div class="container">
	<br>
	<br>
	<center><h3><strong>AUTHORITY TO ATTEND</strong></h3></center>
	<br>
	<br>
	<table class="table table-bordered table-striped" style="font-size: 15px;">
	<tr>
		<th>NAME:</th>
		<td><?php echo $row["emp_name"]; ?></td>
	</tr>
	<tr>
		<th>POSITION:</th>
		<td><?php echo $row["name"]; ?></td>
	</tr>
	<tr>
		<th>DESTINATION: </th>
		<td><?php echo $row["destination"]; ?></td>
	</tr>
		<tr>
		<th>STATION: </th>
		<td><?php echo $row["station"]; ?></td>
	</tr>
			<tr>
		<th>PURPOSE: </th>
		<td><?php echo $row["purpose_description"]; ?></td>
	</tr>
			<tr>
		<th>INCLUSIVE DATE(S): </th>
		<td><?php echo $row["date_from"] . " to " . $row["date_to"]; ?></td>
	</tr>
				<tr>
		<th>FUNDING SOURCE: </th>
		<td><?php echo $row["funding_source"]; ?></td>
	</tr>
</table>
<br>
<br>
<div class="row">
		<div class="col-sm-4">
<br>
<br>
<center>
	<?php echo $row["emp_name"]; ?>
<hr class="line">
<small>SIGNATURE OF REQUESTING PERSONNEL</small>
</center>

	</div>
		<div class="col-sm-4">


	</div>
	<div class="col-sm-4">



	</div>
</div>
<div class="row">
		<div class="col-sm-4">


	</div>
		<div class="col-sm-4">


	</div>
	<div class="col-sm-4">

<span>APPROVED BY:</span>
<center>
	<?php
echo "<img class='signature' src='images/jtt.png'>";
?>
</center>
<center>
<hr class="line">
<small><?php echo LoadPreference("oic"); ?><br><?php echo LoadPreference_valtype("oic"); ?></small>
</center>

	</div>
</div>
<svg id="barcode"></svg>
<script type="text/javascript">JsBarcode("#barcode", <?php echo json_encode($row["control_code"]); ?>,{height:25});</script>
</div>
</body>
</html>

<?php

function LoadPreference($label){
	global $c;
	$q = "SELECT * FROM lams_configurations WHERE label='$label' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	return $row["value"];
}
function LoadPreference_valtype($label){
	global $c;
	$q = "SELECT * FROM lams_configurations WHERE label='$label' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	return $row["value_type"];
}


?>