<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Authority To Attend</title>
	<?php
	include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
		include("php/database_updater.php");
	?>
</head>
<body class="blurbg">
	
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
		<script type="text/javascript">
			TriggerATASync();
			setTimeout(function(){
			TriggerAttendanceLogs();
  },5000)
		</script>
<nav class="navbar navbar-expand-lg" style="margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="fas fa-clipboard-list"></i> AUTHORITY TO ATTEND</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#" data-toggle="modal" data-target="#modal_auth_attend" class="btn btn-primary"><span><i class="fas fa-plus-square"></i> Apply New Authority</span></a>
		      </li>
		    </ul>
		  </div>
		</nav>

		<div class="container">
<!-- 		<div class="alert alert-primary clearfix" role="alert">
		All of the information here are synched to our online database. You can refresh to update.
		<button onclick="location.reload();" class="btn btn-primary float-right"><i class="fas fa-sync-alt"></i> Refresh</button>
		</div> -->

<div class="row">
	<div class="col-sm-9">
		<div class="card">
			<div class="card-body">
				<h4>Overall Logs</h4>
				<table id="auth_att_table" class="table">
		<thead>
		<tr>
		<th>Submitted</th>
		<th>Name</th>
		<th>Description</th>
		<th>From</th>
		<th>To</th>
		<th>Origin</th>
		<th>Status</th>
		</tr>
		</thead>
		<tbody id="ata_table_cont">
		</tbody>
		</table>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="card">
			<div class="card-body">
				<h5><i class="far fa-life-ring"></i> Help</h5>
				<hr>
		<p style="color: rgba(0,0,0,0.5);"><i class="far fa-compass"></i> Legend</p>
		<img src="images/home_nano.png" style="width: 50px;">
		<h5><i class="fas fa-arrow-circle-right"></i> Auto-Approved ATA</h5>
		<p class="text-muted">Only ATA forms that are recorded on the system is auto-approved.</p>
		<img src="images/phone_nano.png" style="width: 50px;">
		<h5><i class="fas fa-arrow-circle-right"></i> Go / Portal</h5>
		<p class="text-muted">Authority Attend that is applied using the CDTRS Go app is synched to the system(this system) but will not be allowed to print until you(admin) approve it.</p>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<p style="color: rgba(0,0,0,0.5);"><i class="fas fa-info-circle"></i> Additional Infomation</p>
		<img src="images/dtr_nano.png" style="width: 50px;">
		<h5><i class="fas fa-arrow-circle-right"></i> Synched to DTR</h5>
		<p class="text-muted">Approved Authority to Attend will reflect on the employees Daily Time Record.</p>
			</div>
		</div>
	</div>
</div>


		</div>
	</div>


</body>
</html>

<form action="php/external_server.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="m_cancelauth">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h5 class="card-title">Authority To Attend Cencellation</h5>
        <div class="alert alert-warning" role="alert">
          <i class="fas fa-info-circle"></i> This will no longer relfect in DTR.
        </div>
        <h6 class="card-subtitle text-muted mb-2">This record wont reflect on this employee's DTR. Sure you want to cancel this Authority?</h6>
        <input type="hidden" id="theauthidnow" name="data_id">
        
      </div>
      <div class="modal-footer">
        <button type="submit" name="deleteauthtoattend" value="x" class="btn btn-primary btn-sm">YES</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">NO</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
	function PrepareAuthorityToAttend(controlOBJ){
		$("#theauthidnow").val($(controlOBJ).data("dataid"));
	}
</script>
<div class="modal" tabindex="-1" role="dialog" id="modalapproveata">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-body">
			<input type="hidden" id="idofemptoapprove" name="">
			<h4 class="ultrabold">Approve Authority</h4>
		<p>Approve this <strong>Authority to Attend</strong> to enable printing.</p>
		</div>
		<div class="modal-footer">
			<button type="button" id="app_ata_now" data-dismiss="modal" class="btn btn-primary">Approve</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		</div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$("#auth_att_table").DataTable();
	highlight_pagelink("#page_authattend");
Reload_ATA_Data();
$("#app_ata_now").click(function(){
	Approve_ATA_Data($("#idofemptoapprove").val());
})
function SetupApproveModalX(controlOBJ){
	$("#idofemptoapprove").val($(controlOBJ).data("idofthis"));
}
	function Reload_ATA_Data(){

		$("#auth_att_table").DataTable().destroy();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {reload_ata_data:"x"},
			success: function(data){
			// alert(data);
				$("#ata_table_cont").html(data);
					$("#auth_att_table").DataTable();
			}
		})
	}
function Approve_ATA_Data(id_to_apporve){
			$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {approve_an_ata_form:"x",idtoapp:id_to_apporve},
			success: function(data){
			 popnotification(data, "Authority to Attend", false);
			Reload_ATA_Data();
			}
		})
}
</script>
<?php
include("components/modals.php");
include("components/auth_attend_modals.php");
?>