<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a style="margin-top: -5px;" class="navbar-brand" href="dash_super.php"><small ><strong>Lams HR</strong> | <?php echo $_SESSION["username"]; ?></small></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Manage Leave
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_addentitlements" id='settingviewer'>Add Entitlements</a>
          <a class="dropdown-item" href="employee_entitlements.php">Employee Entitlements</a>
        </div>
      </li>
    <li class="nav-item">
      <a href="LeaveReports.php" class="nav-link">Leave Reports</a>
      </li>
          <li class="nav-item dropdown">
      <a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports</a>
      <div class="dropdown-menu">
         <a class="dropdown-item" href="DisputeReports.php">Disputes Reports</a>
      </div>
      </li>

      <li class="nav-item dropdown">
        <a href="#"  data-toggle="dropdown" class="nav-link dropdown-toggle">Application</a>
        <div class="dropdown-menu">
          <a href="auth_travel.php" class="nav-link">Authority to Travel</a>
          <a href="auth_attend.php" class="nav-link">Authority to Attend</a>
        </div>
      </li>
      <li class="nav-item">
         <a href="#" data-toggle="modal" data-target="#LamsConfirguration"class="nav-link">Settings</a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0" method="POST">
      <button class="btn my-2 my-sm-0" type="submit" name="logout"><i class="fas fa-sign-out-alt"></i> Sign-out</button>
    </form>
  </div>
</nav>
