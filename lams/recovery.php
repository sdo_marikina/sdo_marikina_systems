<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="refresh" content="1200">
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS</title>
	<?php
	include("php/server.php");
	include("theme/theme.php");
	include("php/database_updater.php");
	include("components/new_features.php");

	?>
</head>

<body class="c1 " style=" background:#ecf0f1;">
<div class="container mt-5">
	<center>
		<h5 class="mb-5">Recovery Mode</h5>
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb" style=" background-color: white; box-shadow: none; width: 500px; text-align: left;">
		    <li class="breadcrumb-item" id="pro_1">Station Name</li>
		    <li class="breadcrumb-item" id="pro_2">Recovery</li>
		    <li class="breadcrumb-item" id="pro_3" aria-current="page">Account Creation</li>
		  </ol>
		</nav>
		<div class="card" id="p1" style=" background-color: white; box-shadow: none; width: 500px; text-align: left;">
		<div class="card-body">
			<h5 class="card-title">1. Station Name</h5>
			<h6 class="card-subtitle mb-4 text-muted">Type your old station name and click Proceed to Recovery.</h6>
			<div class="form-group">
				<label>Station Name</label>
				<input type="text" class="form-control" id="txt_companyname" placeholder="Type here..." name="">
			</div>
			<div class="form-group">
				<button class="btn btn-primary" onclick="getemployees()">Proceed to Recovery</button>
				<button class="btn btn-secondary">Cancel</button>
			</div>
		
		</div>
	</div>
	<div class="card" id="p2" style="display: none; background-color: white; box-shadow: none; width: 500px; text-align: left;">
		<div class="card-body">
			<h5 class="card-title">2. Data Recovery Process</h5>
			<h6 class="card-subtitle mb-4 text-muted">Wait for all the listed items to be completed.</h6>
			<table class="table table-sm table-bordered">
				<tr>
					<td>Employees</td>
					<td><span id="p_1">pending...</span></td>
				</tr>
				<tr>
					<td>Current and last month's attendance</td>
					<td><span id="p_2">pending...</span></td>
				</tr>
				<tr>
					<td>Positions</td>
					<td><span id="p_3">pending...</span></td>
				</tr>
				<tr>
					<td>Departments</td>
					<td><span id="p_4">pending...</span></td>
				</tr>
			</table>
			
		</div>
		
	</div>
	<div class="card" id="p3" style="display: none; background-color: white; box-shadow: none; width: 500px; text-align: left;">
		<div class="card-body">
			<h5 class="card-title">Completed!</h5>
			<h6 class="card-subtitle mb-4 text-muted">Click the buttom below to finally setup the system for finalization.</h6>
			<a class="btn btn-primary" href="index.php">Setup my CDTRS</a>
		</div>
		
	</div>
	</center>
</div>
</body>
</html>
<script type="text/javascript">
	activateprocess(1);
	function getemployees(){
		
	


		var mycompany = $("#txt_companyname").val();
		if(mycompany != ""){
				$("#p_1").html("Processing...");
		$("#p_2").html("Idle...");
			$("#p1").css("display","none");
			$("#p2").css("display","block");
			activateprocess(2);
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {res_employees: "x",company: mycompany},
			success: function(data){
			$("#p_1").html(data);
			getattendancelogs();
			}
		})	
	}else{
		 popnotification("Missing station name","Recovery",false);
	}
		
	}
	function getattendancelogs(){
		$("#p_2").html("Processing...");
		var mycompany = $("#txt_companyname").val();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {res_attendancelogs: "x",company: mycompany},
			success: function(data){
			$("#p_2").html(data);
			getpositions();
			}
		})
	}
	function getpositions(){

		$("#p_3").html("Processing...");
		var mycompany = $("#txt_companyname").val();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {res_positions: "x",company: mycompany},
			success: function(data){
			$("#p_3").html(data);
			getdepartments();
			}
		})
	}

	
	function getdepartments(){
$("#p_4").html("Processing...");
		var mycompany = $("#txt_companyname").val();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {res_departments: "x",company: mycompany},
			success: function(data){
			$("#p_4").html(data);
			$("#p3").css("display","block");
			$("#p2").css("display","none");
				activateprocess(3);
			}
		})
	}

	function activateprocess(procount){
		$("#pro_1").css("color","#bdc3c7");
		$("#pro_2").css("color","#bdc3c7");
		$("#pro_3").css("color","#bdc3c7");
		switch(procount){
			case 1:
$("#pro_1").css("color","#2c3e50");
			break;
			case 2:
$("#pro_2").css("color","#2c3e50");
			break;
			case 3:
$("#pro_3").css("color","#2c3e50");
			break;
		}
	}
</script>