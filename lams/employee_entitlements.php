<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Manage Entitlements</title>
	<?php
	include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<script type="text/javascript">
		TiggerSyncEntitlements();
	</script>
	<div class="rightbar">

		<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="fas fa-anchor"></i> MANAGE ENTITLEMENTS</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#" data-toggle="modal" data-target="#modal_addentitlements" href="#" id='settingviewer'><i class="fas fa-plus-circle"></i> Add New Entitlement</a>
		        	<!--  <li class="nav-item active">
		        <a class="nav-link" href="emp_management.php"><i class="fas fa-plus-circle"></i> Leave Types</a>
		      </li> -->
		      </li>
		    </ul>
		  </div>
		</nav>

		
		
	<div class="container">

		 <ul class="nav nav-pills  mb-3" id="pills-tab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-briefcase"></i> Non-Teaching / Division Personnel</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-chalkboard-teacher"></i> Teaching Personnel</a>
		  </li>
		</ul>


		<div class="tab-content" id="pills-tabContent">
		  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		  <div class="row">
		  	<div class="col-sm-12">
		  			<div class="card mb-3">
		  				<div class="card-body">
		  					<img src='images/entitlements.png' class='content_icon'>
		  					<h4 class="mb-3">List of Entitlements</h4>
		  					<table class="table xtbxc table-bordered table-sm">
			<thead style="background-color: rgba(255,255,255,0.3);">
				<tr>
					<th>Name</th>
					<th>Sick Leave</th>
					<th><center>Vacation Leave : Forced Leave</center></th>
					<th>Special Leave</th>
					<th>CTO<br>
					</th>
					<th>
						Solo Parent Leave
					</th>
					<th><i class="fas fa-cog"></i></th>
				</tr>
			</thead>
			<tbody>
				<?php
					Disp_Entitlements(0);
				?>
			</tbody>
		</table>
		  				</div>
		  			</div>
		  	</div>
		  </div>
		  </div>
		  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
		  	
		  	  <div class="row">
		  	  	<div class="col-sm-12">
		  	  		<div class="card">
		  	  			<div class="card-body">
		  	  					<img src='images/entitlements.png' class='content_icon'>
		  					<h4 class="mb-3">List of Entitlements</h4>
		  	  					<table class="table xtbxc table-bordered table-sm">
			<thead style="background-color: rgba(255,255,255,0.3);">
				<tr>
					<th>Name</th>
					<th>Service Credit</th>
					<th>Solo Parent Leave</th>
			
					<th><i class="fas fa-cog"></i></th>
				</tr>
			</thead>
			<tbody>
				<?php
					Disp_Entitlements(1);
				?>
			</tbody>
		</table>
		  	  			</div>
		  	  		</div>
		  	  	</div>
		  	  </div>

		  </div>
		</div>
	</div>
	</div>
</body>
</html>
<?php
	include("components/modals.php");
?>
<script type="text/javascript">
	highlight_pagelink("#page_manageentitlements");
	$(".xtbxc").DataTable();
</script>
