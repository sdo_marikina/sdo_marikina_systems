<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Manage Employees</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
		<script type="text/javascript">
			TriggerEmployeeSync();
		</script>
		<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="far fa-user-circle"></i> MANAGE EMPLOYEES</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#" data-toggle="modal" data-target="#Modal_New_employee"><i class="fas fa-user-plus"></i> Add New Employee</a>
		      </li>
		    </ul>
		  </div>
		</nav>
			
					<div class="container">

						<div class="row">
							<div class="col-sm-9">



								<div id="emp_miss_prob" style="display: none;" class="alert alert-warning" role="alert">
			  <h5><i class="fas fa-exclamation-circle"></i> Fix missing Position and Department first.</h5>
			  <h6 class="card-subtitle mb-5">Employees with missing Department and Position will not appear in Current Employees table.</h6>
			  <a class="btn btn-danger btn-sm" href="https://www.youtube.com/watch?v=g_k_IeSCET0&list=PLmqWQadN7dxKEOYWYvQKVDFsOgp1luJ2n&index=24&t=2s" target="_target"><i class="fab fa-youtube"></i> See how to fix this problem online</a>
			</div>
			
			<script type="text/javascript">
				$.ajax({
					type: "POST",
					url: "php/external_server.php",
					data: {checkempmissinginfo: "x"},
					success: function(data){
						if(data == "true"){
							$("#emp_miss_prob").css("display","block");
						}else{
							$("#emp_miss_prob").css("display","none");
						}
					}
				})
			</script>


								<div class="form-group">
									<div class="card">
										<div class="card-body">
											      <img src='images/employees.png' class='content_icon'>
											      <h5 class="ultratitle mb-3">List of Employees</h5>
											<div class="loading_indicator" id="emplod"></div>
										
														<table class="table table-sm" id="emp_tbl">
												<thead>
													<tr>
														<th><center><i class="far fa-user-circle"></i></center></th>
														<th>Name</th>
														<th>Position</th>
														<th>Department</th>
														<th>Schedule</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody id="empoftoday">
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title ultratitle">LEGEND</h5>
											<h6 class="text-muted mb-2 card-subtitle">Meaning of symbols/icons on this page.</h6>
											<div id="emp_infoz">
											
											  <div class="card">
											    <div class="card-header">
											      <a class="card-link" data-toggle="collapse" href="#info1">
											       <img src="images/shared.png" style="width:50px;">
											<h6>Shared Access Badge</h6>
											      </a>
											    </div>
											    <div id="info1" class="collapse show" data-parent="#emp_infoz">
											      <div class="card-body">
											        <h6 class="text-muted mb-2 card-subtitle">Employee met all the required information to use all CDTRS Online Services like Online Portal, Go App and more.</h6>
											      </div>
											    </div>
											  </div>
											
											  <div class="card">
											    <div class="card-header">
											      <a class="collapsed card-link" data-toggle="collapse" href="#info2">
											       <img src="images/local.png" style="width:50px;">
											<h6>Local Access Badge</h6>
											      </a>
											    </div>
											    <div id="info2" class="collapse" data-parent="#emp_infoz">
											      <div class="card-body">
											      <h6 class="text-muted mb-2 card-subtitle">Employee is not ellegible to use any of CDTRS online services. All access such as applying leave and viewing logs in only limited on this system.</h6>
											      </div>
											    </div>
											  </div>
											
											
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<!-- </div> -->
			<!-- </div> -->
	</div>

</body>
</html>

<div class="modal" tabindex="-1" role="dialog" id="notavmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-sm">

      <div class="modal-body">
      <center>
		<h4>Awww, sorry.</h4>
      	<p>Adding multiple schedule to a Non-Teaching/ Division personnel is not allowed.</p></center>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary btn-block">Ok</button>
      </div>
    </div>
  </div>
</div>
	
<div class="modal" tabindex="-1" role="dialog" id="manage_sched_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-color: white;">
      <div class="modal-body">
  	<h4 class="ultratitle"><span id="shed_nameofemp"></span><br><small style="color: rgba(0,0,0,0.3);">Schedule Manager</small></h4>
      </div>

        <ul class="nav nav-tabs mb-3 nav-fill" id="pills-tab" role="tablist">
         <li class="nav-item">
           <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#curr_sched_now" role="tab" aria-controls="curr_sched_now" aria-selected="true"><i class="far fa-calendar-alt"></i> Current Schedule</a>
         </li>
         <li class="nav-item">
           <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#add_new_sched" role="tab" aria-controls="add_new_sched" aria-selected="false"><i class="far fa-calendar-plus"></i> New Schedule</a>
         </li>
       </ul>
       <div class="tab-content" id="pills-tabContent">
         <div class="tab-pane fade show active" id="curr_sched_now" role="tabpanel" aria-labelledby="pills-home-tab">
         	<div class="container-fluid">
			<h5>Schedule History<br><small>Schedule that is upcomming or ineffectual.</small></h5>
			<table class="table table-sm table-striped table-bordered">

			<thead>
				<tr>
					<th scope="col">Day</th>
					<th scope="col">In/Out</th>
					<th scope="col">Effectivity</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tbody id="tbl_of_multiple_shed_ineffectual">

			</tbody>

			</table>
         	</div>
         </div>
         <div class="tab-pane fade" id="add_new_sched" role="tabpanel" aria-labelledby="pills-profile-tab">
			<form action="php/external_server.php" method="POST">
         	<input type="hidden" id="theidofempshed" name="emp_eid">
         	<div class="container-fluid" >
         		<div class="form-group">
         			<label>Day</label>
         			<select name="new_sched_day" required="" class="form-control">
         				<option>Monday</option>
         				<option>Tuesday</option>
         				<option>Wednesday</option>
         				<option>Thursday</option>
         				<option>Friday</option>
         				<option>Saturday</option>
         				<option>Sunday</option>
         			</select>
         		</div>
         		<div class="row">
         			<div class="col-sm-6">
         					<div class="form-group">
         				<label>TIME-IN</label>
         				<input required="" class="form-control" type="time" name="new_shed_in">
         			</div>
         			</div>
         			<div class="col-sm-6">
         					<div class="form-group">
         				<label>TIME-OUT</label>
         				<input required="" class="form-control" type="time" name="new_shed_out">
         			</div>
         			</div>
         		</div>
         		<div class="form-group">
         			<label>Schedule Effectivity</label>
         			<input type="date" class="form-control" required="" name="eff_date">
         		</div>
         		<div class="form-group">
         			<button type="submit" name="addnewschedule" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Add Schedule</button>
				</div>
         	</div>
			</form>
         </div>
       </div>
    </div>
  </div>
</div>


<script type="text/javascript">

	function Open_multiple_sched_display(controlOBJ){
		$("#tbl_of_multiple_shed").html("");
		$("#shed_nameofemp").html($(controlOBJ).data("nameofemp"));
		$("#theidofempshed").val($(controlOBJ).data("emp_id"));
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {multiple_sched_display_ineffectual: "x", eid: $(controlOBJ).data("emp_id")},
			success: function(data){
			$("#tbl_of_multiple_shed_ineffectual").html(data);
			}
		})
	}
function Reload_multiple_sched_display(controlOBJ){
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {multiple_sched_display_ineffectual: "x", eid: $(controlOBJ).data("emp_id")},
			success: function(data){
			$("#tbl_of_multiple_shed_ineffectual").html(data);
			}
		})
	}

</script>

<form action="php/external_server.php" method="POST">
		<div class="modal" tabindex="-1" role="dialog" id="modal_acc_edit">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		       <h5 class="ultratitle"><i class="fas fa-user-edit"></i> EDIT EMPLOYEE INFO</h5>
		       <input type="hidden" id="data_id" name="data_id">
		       <br>
		       <div class="row">
			       	<div class="col-sm-12">
			       		<div class="form-group">
			       			<h5 class="ultratitle"><i class="far fa-user-circle"></i> BASIC</h5>
						</div>
			       	</div>
		       		<div class="col-sm-6">
		       			<div class="form-group">
	       				<label><input type="checkbox" name="istmpidchecked" id="id_is_temp_edit" title="Check if this employee's I.D is for temporary purposes only."> Is Temporary ID</label>
		       			<label>Employee Number</label>
		       			<input type="hidden" id="oldemnum" name="oldempnumber">
		       			<input type="text" class="form-control" id="data_eid" required="" autocomplete="off" placeholder="Type here..." name="employeenumber">
		       			</div>
		       		</div>
		       		<div class="col-sm-6">
		       			<div class="form-group">
       					<label>Basic Pay</label>
		       			<input type="text" class="form-control" id="data_basic_pay" required="" autocomplete="off" placeholder="Type here..." name="bazicpay">
		       			</div>
		       		</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>First Name</label>
		       			<input type="text" id="data_fname" class="form-control" required="" autocomplete="off" placeholder="Type here..." name="fname">
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Middle Name <small>(Optional)</small></label>
		       			<input type="text" id="data_mname" class="form-control" autocomplete="off" placeholder="Type here..." name="mname">
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Last Name</label>
		       			<input type="text" id="data_lname" class="form-control" required="" autocomplete="off" placeholder="Type here..." name="lname">
		       		</div>
		       	</div>
		       </div>
		       <div class="row">
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Position</label>
		       			<select class="form-control" id="data_position" required="" autocomplete="off" name="pozition">
		       				<?php load_drop_position(); ?>
		       			</select>
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Department</label>
		       			<select class="form-control" id="data_department" required="" autocomplete="off" name="deptname">
		       				<?php load_drop_department(); ?>
		       			</select>
		       		</div>
		       	</div>
		       	<div class="col-sm-4">
		       		<div class="form-group">
		       			<label>Employee Type</label>
		       			<select class="form-control" required="" id="data_type" name="emptype">
		       				<?php
		       				if(GetSettings("usertype") != "1"){
		       				?>
		       				<option selected="" value="" id="e_val_emptype" disabled="">Choose Employee Type</option>
		       				<?php } ?>

		       				<option value="1">Non-Teaching</option>
		       	
		       				<?php
		       				if(GetSettings("usertype") != "0"){
		       				?>
		       				<option value="2">Teaching</option>
		       				<?php } ?>


		       				<?php
		       				if(GetSettings("usertype") != "1"){
		       				?>
		       				<option value="3">Division Personnel</option>
		       				<?php } ?>
		       			</select>


		       			<script type="text/javascript">
		       				$("#e_val_emptype").change(function(){
								if($("#e_val_emptype").val() == "2"){
									$("#shed_2").prop('disabled', true);
									$("#shed_3").prop('disabled', true);

								}else{
									$("#shed_2").prop('disabled', false);
									$("#shed_3").prop('disabled', false);
								}
		       				})
		       				
		       			</script>


		       		</div>
		       	</div>
		       	<div class="col-sm-12">
		       		<div class="form-group">
		       			<h5 class="ultratitle"><i class="fas fa-id-card"></i> CONTACT</h5>
		       		</div>
		       	</div>
						<div class="col-sm-6">
						<div class="form-group">
						<!-- <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Department_of_Education_%28DepEd%29.svg/2000px-Department_of_Education_%28DepEd%29.svg.png" style="height: 50px;"><br> -->
						<label>DepEd Email Address</label>
						<input type="email" id="data_cont_email" class="form-control" autocomplete="off" placeholder="Ex: Ryan@sample.com" name="emp_email">
						</div>
						</div>
						<div class="col-sm-6">
						<div class="form-group">
						<!-- <img src="https://facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png" style="height:50px;"> -->
<!-- 						<br> -->
						<label>Facebook Account <small>(Optional)</small></label>
						<input type="text" id="data_cont_fb" class="form-control" autocomplete="off" placeholder="Ex: facebook.com/yourname" name="emp_fbacc">
						</div>
						</div>
		       	<div class="col-sm-12">
		       		<div class="form-group">
		       			<label>Home Address <small>(Optional)</small></label>
		       			<input type="text" id="data_cont_homeadd" class="form-control" autocomplete="off" placeholder="Ex: #194 Ipil Street Marikina Heights, Marikina City" name="emp_homeadd">
		       		</div>
		       	</div>
		       	<div class="col-sm-6">
		       		<div class="form-group">
		       			<label>Primary Contact Number <small>(Optional)</small></label>
		       			<input type="text" id="data_cont_prinum" class="form-control" autocomplete="off" placeholder="Ex: 09xxxxxxxxx" name="emp_primarycontnum">
		       		</div>
		       	</div>
		       	<div class="col-sm-6">
		       		<div class="form-group">
		       			<label>Emergency Contact Number <small>(Optional)</small></label>
		       			<input type="text" id="data_cont_emernum" class="form-control" autocomplete="off" placeholder="Ex: 09xxxxxxxxx" name="emp_cont_num">
		       		</div>
		       	</div>
		       	<div class="col-sm-12">
		       		<div class="form-group">
		       			<h5 class="ultratitle"><i class="fas fa-walking"></i> LEAVE PREFERENCE</h5>
					</div>
		       	</div>
		       	<div class="col-sm-4">
		       				<div class="form-group">
		       			<label>Are you a solo parent?</label><br>
		       			<label><input type="checkbox" id="data_leavepref_issingle" name="issingle"> Yes</label>
<!-- 		      		<select class="form-control" required="" name="issingle">
		      			<option selected="" disabled="">Choose a status</option>
		      			<option value="2">Not a Parent</option>
		      			<option value="0">No</option>
		      			<option value="1">Yes</option>
		      		</select> -->
		       		</div>
		       	</div>
		       	<div class="col-sm-12">
		       		<div class="form-group">
		       			<h5 class="ultratitle"><i class="fas fa-hourglass-half"></i> SCHEDULE</h5>
					</div>
		       	</div>
	       	  	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Time-in</label>
		       			<input type="time" required="" id="shed_1"  name="tm_in" class="form-control">
		       		</div>
		       	</div>
		       	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Break-In <small>(Optional)</small></label>
		       			<input type="time"  id="shed_2" name="br_in" class="form-control">
		       		</div>
		       	</div>
		       	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Break-Out <small>(Optional)</small></label>
		       			<input type="time"  id="shed_3" name="br_out" class="form-control">
		       		</div>
		       	</div>
		       	<div class="col-sm-3">
		       		<div class="form-group">
		       			<label>Time-Out</label>
		       			<input type="time" required="" id="shed_4" name="tm_out" class="form-control">
		       		</div>
		       	</div>
		       </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" name="editemployee" class="btn btn-primary"><i class="fas fa-user-edit"></i> Save Changes</button>
		        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>

<form action="php/external_server.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="modal_acc_delete">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultratitle">DELETE ACCOUNT?</h4>
      	<input type="hidden" id="idofemployeetodelete" name="accid">
        <p>Do you want to delete <strong id="nameofemployeetodelete">Employee Name's</strong> Account?</p>
        <div class="container">
        	<div class="card">
        		<div class="card-body">
        			<h5>Employee will not be able to be included in the list below after the deletion.</h5>
	        	<ul>
	        		<li>Generate DTR</li>
	        		<li>Apply Leave</li>
	        		<li>Name Search</li>
	        		<li>Generate Form 7</li>
	        		<li>Whereabouts</li>
	        		<li>CDTRS Go / Online Portal</li>
	        		<li>CDTRS Break</li>
	        		<li>Authority to Attend</li>
	        		<li>Log to Biometrics/ Kiosk</li>
	        	</ul>
        		</div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="delete_acc_now" class="btn btn-danger">Delete Permanently</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">

		function OpenDeleteEmployeeModal(tocontrol){
$("#idofemployeetodelete").val($(tocontrol).data("idofemp"));
$("#nameofemployeetodelete").html($(tocontrol).data("nameofemp") + "'s");
	}
		function OpenEditEmp(tocontrol){

		var eidofme = $(tocontrol).data("eid");
		// alert(eidofme);
		$("#data_id").val($(tocontrol).data("accid"));
		$("#oldemnum").val($(tocontrol).data("eid"));
		$("#data_eid").val($(tocontrol).data("eid"));

		//CHECK IF THIS EMPLOYEE HAS A TEMPORARY Employee ID

		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {getempidiftemp:"x",eid: eidofme},
			success: function(data){
				if(data == "true"){
					$("#id_is_temp_edit").prop("checked",true);
				}else{
					$("#id_is_temp_edit").prop("checked",false);
				}
			}
		})

		$("#data_type").val($(tocontrol).data("type"));
		$("#data_basic_pay").val($(tocontrol).data("basic_pay"));
		$("#data_position").val($(tocontrol).data("position"));
		$("#data_department").val($(tocontrol).data("department"));
		$("#data_fname").val($(tocontrol).data("fname"));
		$("#data_mname").val($(tocontrol).data("mname"));
		$("#data_lname").val($(tocontrol).data("lname"));
		var sched = $(tocontrol).data("schedule");

		sched = sched.split(",");
// alert(ConvToMillTime(sched[1]));
		$("#shed_1").val(ConvToMillTime(sched[0]));
		$("#shed_2").val(ConvToMillTime(sched[2]));
		$("#shed_3").val(ConvToMillTime(sched[3]));
		$("#shed_4").val(ConvToMillTime(sched[1]));

		$("#data_cont_email").val($(tocontrol).data("cont_email"));
		$("#data_cont_fb").val($(tocontrol).data("cont_fb"));
		$("#data_cont_homeadd").val($(tocontrol).data("cont_homeadd"));
		$("#data_cont_prinum").val($(tocontrol).data("cont_prinum"));
		$("#data_cont_emernum").val($(tocontrol).data("cont_emernum"));
		// $("#data_leavepref_issingle").val($(tocontrol).data("leavepref_issingle"));
// alert($(tocontrol).data("leavepref_issingle"));

		if($(tocontrol).data("leavepref_issingle") == "0"){
			$('#data_leavepref_issingle').prop('checked', false);
		}else{
			$('#data_leavepref_issingle').prop('checked', true);
		}
		}

		function ConvToMillTime(xtime){
			if(xtime != ""){
			var time = xtime;
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;
			// alert(sHours + ":" + sMinutes);
			return sHours + ":" + sMinutes;	
		}else{
			return "";
			
		}	

		// popnotification("We are still working on this.", "Some features are not yeat ready for this page.",false);
		}

</script>
<?php
include("components/modals.php");
?>
<script type="text/javascript">
			$("#emplod").css("display","block");
	highlight_pagelink("#page_manageemployees");
	setTimeout(function(){
LoadEmployeesOfToday();
},1000)
	function LoadEmployeesOfToday(){

		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {GetEmployeesOfTomorrow:"x"},
			success: function(data){
				// alert(data);
				$("#empoftoday").html(data);

					$("#emp_tbl").DataTable();
					$("#emplod").css("display","none");
			}
		})
	}
function deleteschedmul(obj){
	  if (confirm("Are you sure you want to delete this schedule?")) {
   // alert($(obj).data("delidval"));
   var idtodel = $(obj).data("delidval");
   $.ajax({
   	type: "POST",
   	url: "php/external_server.php",
   	data: {del_a_sched_mul:"x",idtodelsched: idtodel},
   	success: function(data){
   		alert(data);
   		Reload_multiple_sched_display(obj);
   	}
   })
  }

}

</script>

<form action="php/external_server.php" method="POST" enctype="multipart/form-data">
	<div class="modal" tabindex="-1" role="dialog" id="m_addprofilepicmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add profile picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="idofprofilepic" name="eid_of_emp">
        <div class="form-group">
        	<label>Choose profile picture for this employee from your personal computers local storage.</label>
        	<input type="file" name="fileToUpload">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="ChaneEmployeeProfilePicture" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
	function OpenChngeProfilePicMOdal(toControl){
				$("#idofprofilepic").val($(toControl).data("eidofemp"));
	}
</script>