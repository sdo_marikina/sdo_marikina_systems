<?php
	date_default_timezone_set('Asia/Manila');
	$file = file('tmp/time.txt');

	$timestamped = $file[count($file) - 1];

	$added_time=date('h:i:s', strtotime('+10 minutes', strtotime($timestamped)));

	$realtime=date('h:i:s');
	
	if($realtime<=$added_time) {
		echo "Connected". "<br>";
	} else if ($realtime > $added_time) {
		echo "Not Connected" . "<br>";
	}
?>