<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Leave Reports</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
		include("php/database_updater.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>

	<div class="rightbar">
    <nav class="navbar navbar-expand-lg" style="margin-top: 5px; margin-bottom: 15px;">
      <a class="navbar-brand" href="#"><i class="fas fa-plane"></i> LEAVE REPORTS</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        
<!--           <li class="nav-item active">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#ModalNewHoliday"><i class="far fa-plus-square"></i> New Holiday</a>
          </li> -->
        </ul>
      </div>
    </nav>

	<div class="container">
		
	 <ul class="nav nav-pills  mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-bullhorn"></i> Pending</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-fleave" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-bolt"></i> Scheduled</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-check-circle"></i> Approved</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-times-circle"></i> Disapproved</a>
  </li>
    <li class="nav-item">
    <a class="nav-link" id="pills-history-tab" data-toggle="pill" href="#pills-history" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-search"></i> History</a>
  </li>
</ul>
		<div class="tab-content" id="pills-tabContent">
		  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<div class="row">
			<div class="col-sm-9">
				
	
	<div class="card mb-3">
		<div class="card-body">
			<div class="loading_indicator" id="lod_pending_1"></div>
			<img src='images/day_today.png' class='content_icon'>
			<h5 class="ultratitle card-title"><i class="fas fa-sun"></i> Applied Today</h5>
			<table class="table table-striped table-bordered" width="100%" id="tt_1">
			<thead>
				<tr>
					<th>Application Date</th>
					<th>Name</th>
					<th>Leave Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="pending_today">
				
			</tbody>
		</table>

		</div>
	</div>

		<div class="card">
			<div class="card-body">
				<div class="loading_indicator" id="lod_pending_2"></div>
				<img src="images/older.png" class='content_icon'>
				<h5 class="ultratitle card-title mb-3"><i class="fas fa-history"></i> Older</h5>
	<table class="table table-striped table-bordered" width="100%" id="l1">
			<thead>
				<tr>
					<th>Application Date</th>
					<th>Name</th>
					<th>Leave Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="pending_older">
				
			</tbody>
		</table>
			</div>
		</div>
			</div>
			<div class="col-sm-3">
					<div class="card">
					<div class="card-body">
						<img src='images/leave_nano.png' class='content_icon'>
						<h5 class="card-title ultratitle">SUMMARY</h5>
						<div class="row">
							<div class="col-sm-12">
									<h6 class="text-muted">Peding</h6>
									<h3 id="sum_pending">0</h3>
							</div>
							<div class="col-sm-12">
									<h6 class="text-muted">Approved</h6>
									<h3 id="sum_approved">0</h3>
							</div>
							<div class="col-sm-12">
									<h6 class="text-muted">Disapproved</h6>
									<h3 id="sum_disapproved">0</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<h5 class="card-title ultratitle">NO CLUES?</h5>
				<h6 class="card-subtitle text-muted mb-3">We have a lot of tutorials for you to learn what you can do with CDTRS.</h6>
				 <a href="https://www.youtube.com/playlist?list=PLmqWQadN7dxKEOYWYvQKVDFsOgp1luJ2n" target="_blank" title="CDTRS TUTORIALS" href="">
        <img src="images/leavetutorials.png" style="width: 100%; border-radius: 4px;">
        </a>
					</div>
				</div>


				<script type="text/javascript">
					$.ajax({
						type: "POST",
						url: "php/external_server.php",
						data : {"getleavesummar":"xxx"},
						success: function(data){
							data = data.split(",");
							$("#sum_pending").html(data[0]);
							$("#sum_approved").html(data[1]);
							$("#sum_disapproved").html(data[2]);
						}
					})
				</script>
			</div>
		</div>
		  </div>
		    <div class="tab-pane fade" id="pills-fleave" role="tabpanel" aria-labelledby="pills-profile-tab">
		    	
		    	<div class="row">
		    		<div class="col-sm-9">
		    			<div class="card mb-3">
		    		<div class="card-body">
		    				<table class="table  table-striped table-bordered" width="100%" id="lx">
			<thead>
				<tr>
					<th>Employee Name</th>
					<th>Leave Type</th>
					<th>Deduction Date</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				DisplayForceLeaveDates();
				?>
			</tbody>
		</table>
		    		</div>
		    	</div>
		    		</div>
		    		<div class="col-sm-3">
		    			
		    		</div>
		    	</div>
		    </div>
		  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
<div class="row">
	<div class="col-sm-9">

			<div class="card mb-3">
			<div class="card-body">
					<div class="loading_indicator" id="lod_approved_1"></div>
				<img src='images/day_today.png' class='content_icon'>
				<h5 class="ultratitle card-title"><i class="fas fa-sun"></i> TODAY</h5>
				<h6>Leave Report(s) approved today.</h6>
			<table class="table  table-striped table-bordered" width="100%" id="tt_2">
			<thead>
				<tr>
					<th>Application Date</th>
					<th>Name</th>
					<th>Leave Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_approved_today">
				<?php
					Disp_LeaveReportsTeaching(1,"0",date("Y-md"),date("Y-md"));
				?>
			</tbody>
		</table>
			</div>
		</div>
<div class="card">
	<div class="card-body">
			<div class="loading_indicator" id="lod_approved_2"></div>
		<img src="images/older.png" class='content_icon'>
		
<h5 class="ultratitle card-title mb-3"><i class="fas fa-history"></i> YESTERDAY AND OLDER</h5>
			<table class="table  table-striped table-bordered" width="100%" id="l2">
			<thead>
				<tr>
					<th>Application Date</th>
					<th>Name</th>
					<th>Leave Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_approved_older">
				<?php
					Disp_LeaveReportsTeaching(1,"1",date("Y-md"),date("Y-md"));
				?>
			</tbody>
		</table>
	</div>
</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<div class="card">
				<div class="card-body">
					<p class="lessen">How to?</p>
					<h5>Cancel and delete Approved Form 6</h5>
					<p class="text-muted"><i class="fas fa-arrow-right"></i> Choose the Leave Report you want to cancel and on the right corner of the screen click the "Action" button and choose <strong>Cancel Leave</strong>.</p>
					<hr>
					<h5>What is Cancel Leave?</h5>
					<p  class="text-muted"><i class="fas fa-arrow-right"></i> To delete and return entitlement value back to the employee and allow the dates to be applied for Leave(Form 6) again.</p>
				</div>
			</div>
		</div>
	</div>
</div>
		  </div>
		  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
		<div class="row">
			<div class="col-sm-9">
					<div class="card mb-3">
			<div class="card-body">
				<div class="loading_indicator" id="lod_disapproved_1"></div>
				<img src='images/day_today.png' class='content_icon'>
				<h5 class="ultratitle card-title"><i class="fas fa-sun"></i> TODAY</h5>
				<h6>Leave Report(s) disapproved today.</h6>
			<table class="table  table-striped table-bordered" width="100%" id="tt_3">
			<thead>
				<tr>
					<th>Application Date</th>
					<th>Name</th>
					<th>Leave Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_disapproved_today">
				<?php
				Disp_LeaveReportsTeaching(2,"0",date("Y-md"),date("Y-md"));
				?>
			</tbody>
		</table>
			</div>
		</div>

<div class="card">
	<div class="card-body">
		<div class="loading_indicator" id="lod_disapproved_2"></div>
		<img src="images/older.png" class='content_icon'>
		<h5 class="ultratitle card-title mb-3"><i class="fas fa-history"></i> YESTERDAY AND OLDER</h5>


	<table class="table  table-striped table-bordered" width="100%" id="l3">
			<thead>
				<tr>
					<th>Application Date</th>
					<th>Name</th>
					<th>Leave Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_disapproved_older">
				<?php
				Disp_LeaveReportsTeaching(2,"1",date("Y-md"),date("Y-md"));
				?>
			</tbody>
		</table>
	</div>
</div>
			</div>
		</div>
		  </div>
			<div class="tab-pane fade" id="pills-history" role="tabpanel" aria-labelledby="pills-contact-tab">
				<div class="card mb-3">
					<div class="card-body">
						<img src="images/clock.png" class="content_icon">
						<h5 class="mb-3">Generate Leave History</h5>
						<div class="row">
					<div class="col-sm-4">
						<label>From</label>
						<input type="date" id="dtgen_from" class="form-control" name="">
					</div>
					<div class="col-sm-4">
						<label>To</label>
						<input type="date" id="dtgen_to" class="form-control" name="">
					</div>
					<div class="col-sm-4">
						<label>Leave Status</label>
						<select id="whatstatus" class="form-control">
							<option value="3">All</option>
							<option value="0">Pending</option>
							<option value="1">Approved</option>
							<option value="2">Disapproved</option>
						</select>
					</div>
					<div class="col-sm-12">
						<button onclick="GenerateLeaveHistory()" class="btn btn-primary btn-sm mt-3">Generate</button>
					</div>
				</div>
					</div>
				</div>
				<div class="card" id="gendata" style="display: none;">
					<div class="loading_indicator" id="lodlvhistory"></div>
					<div class="card-body">
						<table class="table table-striped table-bordered">
							<thead>
							<tr>
							<th>Application Date</th>
							<th>Name</th>
							<th>Leave Type</th>
							<th>Status</th>
							<th>Action</th>
							</tr>
							</thead>
							<tbody id="tallhistory">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>



<form action="php/external_server.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="modal_obliform6">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<div class="loading_indicator" id="lod_delleave_modal">
    		
    	</div>
      <div class="modal-body" >
      	<div class="container">
      		<input type="hidden" id="lidoff6" name="leavereportid">
      		<center>
      		<div class="form-group">
      		<img src="https://thumbs.gfycat.com/EquatorialEnragedAustraliancurlew-max-1mb.gif" style="width: 200px;">
      		<br>
      		<h4>Cancel Leave?</h4>
      		<p>You are about to return all taken leave credits to the employee and free the applied dates available again for Form 6 filing.</p>
      	</div>
      		 <div class="form-group">
        	<button type="submit" name="cancelaleavetoday" style="width:200px;" class="btn btn-danger"><i class="fas fa-arrow-circle-right"></i> Proceed</button>
        </div>
		<div class="form-group">
        <button type="button" style="width:200px;" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		</div>
      	</center>
      	</div>
      </div>
    </div>
  </div>
</div>
</form>


</body>
</html>

<script type="text/javascript">


	



		LoadUniversal("0","pending_today","lod_pending_1","00","00","0");
		LoadUniversal("1","pending_older","lod_pending_2","00","00","0");

		LoadUniversal("0","tbl_approved_today","lod_approved_1","00","00","1");
		LoadUniversal("1","tbl_approved_older","lod_approved_2","00","00","1");

		LoadUniversal("0","tbl_disapproved_today","lod_disapproved_1","00","00","2");
		LoadUniversal("1","tbl_disapproved_older","lod_disapproved_2","00","00","2");


		function LoadUniversal(today_xc,tableid,lod_obj,ff,tt,whatstatus){
				$("#" + lod_obj).css("display","block");
			$.ajax({
				type:"POST",
				url: "php/external_server.php",
				data: {LoadLeaveHistory:"x",leave_report:whatstatus,date_from:ff,date_to:tt,ajaxed:"x",istoday:today_xc},
				success: function (data){
				setTimeout(function(){
					$("#" + lod_obj).css("display","none");
				},1000)
					$("#" + tableid).html(data);
				}
			})
		}

		function GenerateLeaveHistory(){
			$("#lodlvhistory").css("display","block");
			var dtgen_from = $("#dtgen_from").val();
			var dtgen_to = $("#dtgen_to").val();
			var whatstatus = $("#whatstatus").val();
			$("#gendata").css("display","block");
			$.ajax({
				type:"POST",
				url: "php/external_server.php",
				data: {LoadLeaveHistory:"x",leave_report:whatstatus,date_from:dtgen_from,date_to:dtgen_to,ajaxed:"x",istoday:"3"},
				success: function (data){
				setTimeout(function(){
					$("#lodlvhistory").css("display","none");
				},1000)
					$("#tallhistory").html(data);
				}
			})
		}

		
	
	$("#lx").DataTable();
	highlight_pagelink("#page_leavereports");
	function PrepareCancelLeaveModal(control_obj){
		$("#lod_delleave_modal").css("display","block");
		var TheId = $("#lidoff6").val();
		$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {cancelaleavetoday:"x",leavereportid:TheId},
			success: function(data){
				$("#lod_delleave_modal").css("display","none");
			$("#lidoff6").val($(control_obj).data("llid"));
			}
		})
	}
</script>
<?php
include("components/leave_reports_teaching_modals.php");
include("components/modals.php");
?>

	<script type="text/javascript">
		 TiggerSyncEntitlements();
	</script>