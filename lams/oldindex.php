<!DOCTYPE html>
<html>

<head>
    <!-- <meta http-equiv="refresh" content="1200"> -->
    <link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
    <title>CDTRS</title>
    <?php
    include("php/server.php");
    include("theme/theme.php");
    include("php/database_updater.php");
    include("components/new_features.php");

    $usertype = GetSettings("usertype");
    $comapnyname = GetSettings("cn");
    ?>
</head>

<body class="c1 blurbg" style=" background: <?php GetLatestWallpaper(); ?>; ">
    
    <div class="backfill">
        <?php
    include("components/loginnav.php");
?>
            <div class="container indexcont">
                <div class="row">
                    <div class="col-sm-12">

                        <div style="width: 100%;">
                            <script type="text/javascript">
                                $.ajax({
                                    type: "POST",
                                    url: "php/external_server.php",
                                    data: {
                                        reset_attlogs: "x"
                                    },
                                    success: function(data) {
                                        // alert(data);
                                    }
                                })
                            </script>
                            <div class="row">
                                <?php 
    $sizeofattlogs = 12;
        if(CheckIfKioskIsEnabled() == "true"){
            $sizeofattlogs = 4;
    include("components/att_kiosk.php");
        }
    ?>
                                    <div class="col-sm-<?php echo $sizeofattlogs; ?>" style="text-align: center;">

                                        <div class="card poptop_anim" style=" border:none; border-radius: 0px; overflow: hidden; background-color: transparent; color: white; box-shadow: none;" id="card_time">
                                            <div class="card-header" id="cocoasearch" style="height: 100px; overflow: hidden; transition: 0.3s all !important; background-color: transparent !important;">
                                                <h4 class="ultrathin"><?php echo $comapnyname; ?></h4>
                                                <input type="text" autocomplete="off" id="searchnametext" class="form-control" style="box-shadow: 0px 5px 20px rgba(0,0,0,0.1);  width: 280px; margin: auto;" placeholder="Today's Logs..." name="">
                                                <br>
                                                <div id="theresemptable">
                                                    <div id="restable">

                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $("#searchnametext").change(function() {
                                                        var searchnametext = $("#searchnametext").val();
                                                        if (searchnametext == "") {
                                                            $("#theresemptable").css("display", "none");
                                                            $("#cbodytime").css("opacity", "1");
                                                            $("#cocoasearch").css("height", "100px");
                                                        } else {
                                                            $("#theresemptable").css("width", "100%");
                                                            $("#theresemptable").css("display", "block");
                                                            $("#cbodytime").css("opacity", "0.3");
                                                            $("#cocoasearch").css("height", "300px");
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "php/external_server.php",
                                                                data: {
                                                                    searchalog: "x",
                                                                    tosearch: searchnametext
                                                                },
                                                                success: function(data) {
                                                                    $("#restable").html(data);
                                                                }
                                                            });
                                                        }
                                                    })
                                                </script>
                                            </div>
                                            <div class="card-body" id="cbodytime" style="transition: 0.3s all !important;">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h3 class="ultralight" id="servertimex" style=" margin-bottom: 0px; padding-bottom: 0px; font-size: 8vh;"></h3>
                                                        <div id="holcont">

                                                        </div>
                                                        <script type="text/javascript">
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "php/external_server.php",
                                                                data: {
                                                                    get_current_holiday: "x"
                                                                },
                                                                success: function(data) {
                                                                    $("#holcont").html(data);
                                                                }
                                                            })
                                                        </script>
                                                    </div>
                                                </div>
                                                <br>

                                                <h4 class="ultrathin"><span id="sync_whereabouts_indic" style="font-size: 15px; color:#4CAF50;"></span> Whereabouts</h4>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="button" data-toggle="modal" title="Submit a Departure" data-target="#modal_Departure" class="btn btn-secondary"><i class="far fa-paper-plane"></i> Departure</button>
                                                    <button type="button" data-toggle="modal" title="Log Arrival" data-target="#modal_Arrival" class="btn btn-secondary"><i class="fas fa-home"></i> Arrival</button>
                                                    <button type="button" data-toggle="modal" title="Recent Where-Abouts Logs" data-target="#modal_Logs" onclick="loadwhereabouts_logs()" class="btn btn-secondary"><i class="fas fa-history"></i></button>
                                                </div>
                                                <br>
                                                <br>
                                                    </center>

                                            </div>
                                        </div>
                                    </div>
                                    <style type="text/css">
                                        .versionbar {
                                            display: none;
                                            border-radius: 4px;
                                            width: 60px;
                                            bottom: 0px;
                                            left: 0px;
                                            position: fixed;
                                            margin: 10px;
                                            opacity: 0.3;
                                            z-index: 20;
                                        }
                                        
                                        .versionbar:hover {
                                            opacity: 1;
                                        }
                                    </style>
                                    <img src="images/V2.2.png" title="Click to see what's new!" id="card_version" class="consistent_shadow versionbar" data-toggle="modal" data-target="#whatsnew">
                                    <div class="form-group ">
                                        <div class="card blurbg poptop_anim" id="realtime_backup_x">
                                            <div class="card-body">
                                                <?php include("components/realtimesync.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    </center>

                            </div>
                        </div>

                        <?php
include("footer.php");
?>
                    </div>

</body>

</html>
<?php
include("components/custom_modal.php");
include("components/modals.php");
include("components/loginmodals.php");
include("components/whereaboutsmodals.php");
?>
    <div id="log"></div>

    <script type="text/javascript">
        setTimeout(function() {

            LoadServerTime();

        }, 1000)

        setInterval(function() {
            // P1
            if ($("input:eq(1)").val() == "") {
                LodTodayLogs();
            }

            // CHECK REGISTRATION
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    manage_registration: "x"
                },
                success: function(data) {
                    // alert(data);
                }
            })

            // P4
            // CHECK REGISTRATION
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    manage_expired_entitlements: "x"
                },
                success: function(data) {
                    // alert(data);
                }
            })

        }, 10000);

        function showrecentlogs_mini() {
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    getrecentlogs: "x"
                },
                success: function(data) {
                    $("#reslogscount").html(data);
                }
            })
        }
        setInterval(function() {
            LoadServerTime();
        }, 200);
        // ANIMATIONS

        // setTimeout(function(){
        //  $("#card_time").css("display","block");
        // },800)
        setTimeout(function() {
            $("#card_version").css("display", "block");
        }, 930)

        // END ANIMATIONS
        function LodTodayLogs() {
            $("#logsfortoday").load("php/LoadLogsForToday.php");
        }

        function LoadServerTime() {
            $("#servertimex").load("php/LoadServerTime.php");
        }

        function run_manual_backup(datetobackup) {
            $("#autobackupper").css("display", "block");
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    uploadlogs_manual: "x",
                    bk_date: datetobackup
                },
                success: function(data) {
                    setTimeout(function() {
                        $("#autobackupper").css("display", "none");
                        popnotification("Operation Complete", "Logs backup requested by SDO Marikina has been successfully submitted by the system.", false);
                    }, 2000);

                }
            })
        }
    </script>

    <div class="modal" tabindex="-1" role="dialog" id="emp_log_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="ultrabold">Today's Logs</h4>
                    <h1 class="ultrabold" id="TheEmployeeName">RYAN LEE REGENCIA</h1>
                    <table class="table table-bordered table-striped" id="logdt">
                        <thead>
                            <tr>
                                <th>Access Type</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody id="thelogofsingle">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secodary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function GetTodaysLogsOfSingleEmployeeByEmpId(num) {
            $("#logdt").dataTable().fnDestroy();

            // GET EMP INFO
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    get_single_log: "x",
                    emp_number: num
                },
                success: function(data) {
                    data = JSON.parse(data);
                    // alert(data);
                    $("#TheEmployeeName").html(data[0]["lname"] + ", " + data[0]["fname"] + " " + data[0]["mname"]);
                }
            })
            $.ajax({
                type: "POST",
                url: "php/external_server.php",
                data: {
                    get_single_todayLogs: "x",
                    emp_number: num
                },
                success: function(data) {
                    // alert(data);
                    $("#thelogofsingle").html(data);
                    $("#logdt").DataTable({
                        "ordering": false
                    });
                }
            })

        }
    </script>

    <?php
include("components/log_welcomer.php");
include("components/updater.php");
?>