<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS - Form 7</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
			
		<div class="card" id="lod" style="overflow: hidden; margin: 0px;display: none; ">
				<div class="card-body blurbg" style="color: white; background-color: rgba(41, 128, 185,0.5);">
					<center>
						<span style="color: rgba(41, 128, 185,1.0);"><i class="far fa-hourglass"></i> Generating Form 7...</span>
					</center>
				</div>
			</div>
	
				<div id="formgen">



							<div class="jumbotron">
			<div class="container">
				<h3 class="ultratitle"><i class="fas fa-arrow-circle-right"></i> Generate Form 7</h3>
			</div>

		</div>

				<div class="container">
					<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
					<label>From</label>
					<input type="date" class="form-control" id="interv_from" name="interv_from">
				</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
					<label>To</label>
					<input type="date" class="form-control" id="interv_to" name="interv_to">
				</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group" style="padding-top: 33px;">
					<button class="btn btn-primary" onclick="GenerateFormSeven()"><i class="fas fa-database"></i> Generate Form 7</button>
				</div>
					</div>
				</div>
				</div>
				
			</div>


			<div class="container-fluid" id="res_contianer" style="display: none;">
				<br>
				<br>
				<div class="form-group">
					<button class="btn btn-primary" id="btn_goback"><i class="fas fa-arrow-circle-left"></i> Go Back</button>
					<button class="btn btn-primary" onclick=" printDiv(0) "><i class="fas fa-print"></i> Print Form 7 with no Computation</button>
					<button class="btn btn-primary" onclick=" printDiv(1) "><i class="fas fa-print"></i> Print Form 7</button>
				</div>
			<div id="cont">
				
			</div>
		</div>
	</div>

</body>
</html>
<script type="text/javascript">
		highlight_pagelink("#page_genformseven");
	$("#btn_goback").click(function(){
		$("#res_contianer").css("display","none");
		$("#lod").css("display","none");
		$("#formgen").css("display","block");
		
	})

	function printDiv(ptype) 
	{
		var divToPrint=document.getElementById('cont');

		var newWin=window.open('','Print-Window');

		newWin.document.open();

		if(ptype == 0){
newWin.document.write('<style> @media print { .unprint { display: none; } }  table {  border-collapse: collapse;}table, td, th {  border: 1px solid black;} tr{padding:3px;} td{padding:3px;}</style><html><body onload="window.print()" style="font-family: arial;">'+divToPrint.innerHTML+'</body></html>');
		}else{
			newWin.document.write('<style> table {  border-collapse: collapse;}table, td, th {  border: 1px solid black;} tr{padding:3px;} td{padding:3px;}</style><html><body onload="window.print()" style="font-family: arial;">'+divToPrint.innerHTML+'</body></html>');
		}
		newWin.document.close();
		setTimeout(function(){newWin.close();},10);
	}
	
	function GenerateFormSeven(){
		popnotification("Generating Form 7", "Please wait while CDTRS is generating Form 7.",false);
		$("#formgen").css("display","none");
		$("#res_contianer").css("display","none");
		$("#lod").css("display","block");
		var interv_from = $("#interv_from").val();
		var interv_to = $("#interv_to").val();

		$.ajax({
			type : "POST",
			url: "php/external_server.php",
			data: {GenerateFormSevenNow: "x",int_from:interv_from,int_to:interv_to},
			success: function(data){
				$("#cont").html(data);
				$("#res_contianer").css("display","block");
				$("#lod").css("display","none");
			}
		})
	}
</script>
<style type="text/css" media="print">@page { size: landscape;}body { writing-mode: tb-rl;}</style>
<?php
include("components/modals.php");
?>
