<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>Employee Card Dashboard</title>
	<?php
	include("php/server.php");
	include("theme/theme.php");
	$usertype = GetSettings("usertype");
	$comapnyname = GetSettings("cn");
	?>
	<style type="text/css">
	.dropdown-menu a:hover{
	cursor: pointer !important;
	/*color: white;*/
	background-color: rgba(0,0,0,0.1);
}
	</style>
</head>
<body>	
	<div id="loadingscreen" style="display: none; height: 100%; width: 100%; top:0; left:0; background-color: rgba(158, 158, 158,0.5); position: fixed; z-index: 100;">
		<br>
		<br>
		<br>
		<center>
			<img src="images/loading.gif" style="border-radius: 4px; box-shadow: 0px 10px 50px rgba(0,0,0,0.7); width: 130px;">
		</center>
	</div>
<div class="jumbotron jumbotron-fluid" style="padding: 5px;">
	<div class="container">
<center id="sdologo">
	<div class="container">
		<div class="alert alert-warning" role="alert" style="text-align: left !important;">
			<a class="btn btn-sm btn-primary float-right" style="margin-top: -5px;" href="https://portal.depedmarikina.ph">Switch to Online Portal</a>
			  <span><i class="fas fa-exclamation-triangle"></i> Employee Dashboard will be removed soon.</span>
			</div>
	</div>
	<img src="icons/sdo.png"  style="width: 70px; margin: 5px;"><p><strong>CDTRS | Employee Dashboard</strong></p>

</center>
<div class="form-group" id="refreshpanel">
	<form action="#" method="POST">
		<button class="btn btn-danger float-right" style="border: 1px solid rgba(0,0,0,0.2);" type="submit" name="logoutnow"><i class="fas fa-angle-left"></i> Sign-out</button>
	</form>
	<?php
		if(isset($_POST["logoutnow"])){
			$_SESSION["empkey"] = "";
		}
	?>
</div>
	<div id="typepanel"><div class="form-group" >
<div class="row">
			<div class="col-sm-4">
</div>
		<div class="col-sm-4">
			<input class="form-control" autocomplete="off" placeholder="Enter your employee code..." type="text" name="" id="txt_employeeCode">
		</div>
<div class="col-sm-4">
</div>
</div>
	</div>
	<div class="form-group" style="text-align: center;">
			<button style=" display:inline-block !important;" type="button" class="btn btn-primary" id="btn_get_employee_code"><span>View Attendance <i class="fas fa-arrow-right"></i></span></button>
		<br>
	</div></div>
	<div class="form-group">


			<h4 id="pagetitle"></h4>
			
			<p id="description_top"></p>

</div>


	</div>
</div>

<div class="container">

<div id="tbl_att_container">
	
	<div class="row">
		<div class="col-sm-9">
			<div class="card">
				<div class="card-body">
					<h4>My Logs</h4>
					<table class="table table-striped table-bordered nowrap" id="att_table">
		<thead>
			<tr>
				<th>Date</th>
				<th>In AM</th>
				<th>Out AM</th>
				<th>In PM</th>
				<th>Out PM</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="att_table_container">
		</tbody>
	</table>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
		<div class="form-group">
			<button class="btn btn-primary btn-block" onclick="func_applyleave(this)" id="applbutton" data-topicdate="<?php echo date('Y-m-d'); ?>"  data-strictmode='0' data-toggle="modal" data-target="#modal_applyleave"><i class="fas fa-bolt"></i> Apply Leave</button>
		</div>
		<div class="form-group">
			<div class="card">
			<div class="card-body">
				<h4>Info</h4>
			<div class="form-group">
		<p>Viewing (<span id="dd_fromx_to"><?php echo date("F d, Y"); ?></span>) and (<span id="dd_fromx_from"><?php echo date('F d, Y', strtotime('-7 days')); ?></span>) including weekends.</p>
		<h4>Logs to display</h4>
		<div class="row">
			<div class="col-sm-12">
				<a href="#" onclick="CustomLoad(<?php echo "'" . date("Y-m-d") . "'"; ?>,<?php echo "'" .  date('Y-m-d', strtotime('-7 days')) . "'"; ?>)"><i class="fas fa-arrow-circle-right"></i> Last 7 Days</a>
			</div>
			<div class="col-sm-12">
				<a href="#" onclick="CustomLoad(<?php echo "'" . date("Y-m-d") . "'"; ?>,<?php echo "'" .  date('Y-m-d', strtotime('first day of last month')) . "'"; ?>)"><i class="fas fa-arrow-circle-right"></i> Current and last month's log</a>
			</div>
		<!-- 	<div class="col-sm-12">
				<a href=""><i class="fas fa-arrow-circle-right"></i> Load All Logs</a>
			</div> -->
		</div>
	</div>
			</div>
		</div>
		</div>
		</div>
	</div>
		
</div>
</div>
<!-- <div id="coco"></div> -->
<div id="ccc">
	
</div>

</body>
</html>
<script type="text/javascript">
				$("#refreshpanel").css("display","none");
	$("#txt_employeeCode").keypress(function(e) {
	if(e.which == 13) {
				$("#typepanel").css("display","none");
				$("#sdologo").css("display","none");
				$("#refreshpanel").css("display","block");
		SearchEmployeeKey();
	}
	});

	HideResult();
	var hasloaded = false;
	$("#btn_get_employee_code").click(function(){
				$("#typepanel").css("display","none");
				$("#sdologo").css("display","none");
				$("#refreshpanel").css("display","block");
		SearchEmployeeKey();
	})
var empidx = "";
	function CustomLoad(ff,tt){
$("#dd_fromx_to").html(ff);
$("#dd_fromx_from").html(tt);
		    $('#att_table').DataTable().destroy();
		var txt_ecode = $("#txt_employeeCode").val();
		$("#loadingscreen").css("display","block");
		$.ajax({
		type: "POST",
		url: "php/app_external_server.php",
		data: {GetAttendanceOfEmployee: "x", emp_code: empidx,d_from:ff,d_to:tt},
		success: function(data){
			// alert(data);
				$("#att_table_container").html(data);
			$("#loadingscreen").css("display","none");
				$("#att_table").DataTable({

  "ordering": false

			});
		}
		})
	}
	function SearchEmployeeKey(){
		   $('#att_table').DataTable().destroy();
		$("#loadingscreen").css("display","block");
			if($("#txt_employeeCode").val() != ""){
		$("#att_table_container").html("");
		var txt_ecode = $("#txt_employeeCode").val();
		empidx = txt_ecode;
			$.ajax({
		type: "POST",
		url: "php/app_external_server.php",
		data: {GetAttendanceOfEmployee: "x", emp_code: txt_ecode},
		success: function(data){
			// alert(data);
			if(data.includes("false") !== true){
			//SHOW RESULT LAYOUT
			ShowResult();
			//FILL TABLE WITH RESULT
			$("#att_table_container").html(data);
			//DELCARE DATA TABLE AT ONCE
			if(hasloaded == false){
			hasloaded = true;
			$("#att_table").DataTable({

  "ordering": false

			});
			}

			//Get employee information via key
			$.ajax({
				type : "POST",
				url : "php/app_external_server.php",
				data : {GetEmployeeInfoByKey: "x", EmployeeKey: txt_ecode},
				success: function(data_2){
					data_2 = JSON.parse(data_2);
					$("#pagetitle").html("Hi, " + data_2["fname"]);
					$("#applbutton").attr("data-empnum",data_2["eid"]);
					var position = "";

					switch(data_2["type"]){
						case "1":
							position = "Non-Teaching";
						break;
						case "2":
							position = "Teaching";
						break;
						case "3":
							position = "Division";
						break;
					}

					$("#description_top").html("<strong>Employee Type</strong> : " + position + "<br>");

					// var positionName = GetPositionByID(data_2["position"]);
						GetPositionByID(data_2["position"]);
					$("#description_top").append("<strong >Position</strong> : <span id='xposi'>...</span><br>");
					$("#description_top").append("<strong>Schedule</strong> : " + data_2["schedule"]);
					$("#loadingscreen").css("display","none");
				}
			})
			
		}else{
			$("#pagetitle").html("Employee Dashboard");
			$("#description_top").html("<strong>Lams</strong> | Attendance History");
			alert("Employee number has no Logs on the database.");
			window.location.reload();
		}
		}
	})
		}else{
			alert("Enter your employee number.");
			HideResult();
		}
		$("#txt_employeeCode").val("");
	}
	function GetPositionByID(posID){
			$.ajax({
				type: "POST",
				url: "php/app_external_server.php",
				data: {GetPositionNameByID:"x",positionID:posID},
				success: function(data){
				$("#xposi").html(data);
				}
			})
	}
	function ShowResult(){
		$("#tbl_att_container").css("display","block");
	}
	function HideResult(){
		$("#tbl_att_container").css("display","none");
	}

</script>
<?php
			if(isset($_SESSION["empkey"]) && $_SESSION["empkey"] != ""){
			?>
				<script type="text/javascript">
					$("#typepanel").css("display","none");
				$("#sdologo").css("display","none");
				$("#refreshpanel").css("display","block");
					$("#txt_employeeCode").val(<?php echo json_encode($_SESSION["empkey"]); ?>);
					SearchEmployeeKey();
				</script>

			<?php

			}
		?>
<?php include("components/card_app_modals.php"); ?>