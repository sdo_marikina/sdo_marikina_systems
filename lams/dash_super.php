<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>LAMS | HR</title>
	<?php
	include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body>
	<?php
	include("components/navbar.php");
	?>
	<h1 class="jumbotron jumbotron-fluid hasbg" style="background-image: url('https://images.pexels.com/photos/417351/pexels-photo-417351.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');">
			<div class="container">
				<h1 class="display-4">LAMS <small>HR</small></h1>
			</div>
		</h1>
	<div class="container">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam orci sem, volutpat eu ex et, fermentum faucibus turpis. Aliquam mattis orci turpis, eget efficitur ante mollis vel. Aliquam volutpat mollis nunc sed tempus. Quisque metus nisl, varius a elit vel, pulvinar scelerisque tellus. Integer molestie gravida lorem at molestie. Proin ultricies aliquet ligula, at sagittis nulla dictum a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam vehicula id mauris quis tincidunt. Morbi arcu mauris, vulputate eget eros a, ullamcorper gravida lacus. Vivamus ac condimentum velit, non consequat eros. Vestibulum non suscipit magna. Nullam suscipit quis nulla eget hendrerit. Aenean nec condimentum nisl. Nunc est ipsum, aliquet scelerisque ultrices elementum, volutpat at quam. Phasellus fringilla a nunc viverra mattis.</p>
	</div>
<?php
	include("footer.php");
?>
</body>
</html>
<?php
include("components/modals.php");
?>