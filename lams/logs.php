<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Action Logs</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">

<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
	<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		<a class="navbar-brand" href="#"><i class="fas fa-clipboard-list"></i> CDTRS ACTION LOGS</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		</ul>
		</div>
	</nav>

		<div class="container-fluid">

		<table class="table table-striped table-sm table-bordered" width="100%" id="leavereports">
			<thead>
				<tr>
					<td>Time</td>
					<td>User</td>
					<td>Performed Action</td>
				</tr>
			</thead>
			<tbody>
				<?php
					Disp_Logs();
				?>
			</tbody>
		</table>
		</div>
	</div>

</body>
</html>
<script type="text/javascript">
$(function () {
$('.eztime').datetimepicker({
format: 'LT'
});
});

$("#leavereports").DataTable(	{
dom: 'Bfrtip',
buttons: [
'copy', 'csv', 'excel', 'pdf', 'print'
]
} );
</script>
<?php
include("components/dispute_reports_modals.php");
include("components/modals.php");
?>

