<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Positions</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">


		<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="fas fa-project-diagram"></i> POSITIONS</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#" data-toggle="modal" data-target="#crenewpos"><i class="fas fa-plus"></i> Add New Position</a>
		      </li>
		    </ul>
		  </div>
		</nav>

<div class="container">
	<div class="card">
		<div class="card-body">
			<img src='images/positions.png' class='content_icon'>
			<h5 class="mb-3">Positions</h5>
		 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#listofpositions" role="tab" aria-controls="listofpositions" aria-selected="true"><i class="fas fa-list"></i> List of Positions</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#employeeswithnopos" role="tab" aria-controls="employeeswithnopos" aria-selected="false"><i class="far fa-frown"></i> Employees With No Position <?php ShowNoPosBadgeNumber(); ?></a>
		  </li>
		</ul>
		<div class="container-fluid">

		
		<div class="tab-content" id="pills-tabContent">
			
		  <div class="tab-pane fade show active" id="listofpositions" role="tabpanel" aria-labelledby="pills-home-tab">
		  	<div class="loading_indicator" id="poslod">
				
			</div>
		  		<table class="table table-bordered table-striped table-sm" id="pos_tbklls">
			<thead>
				<tr>
					<th>Title</th>
					<th>Description</th>
					<th>Options</th>
				</tr>
			</thead>
			<tbody id="id_addedpositions">
			</tbody>
		</table>
		  </div>
		  <div class="tab-pane fade" id="employeeswithnopos" role="tabpanel" aria-labelledby="pills-profile-tab">
		  	<div class="row">
		  		<div class="col-sm-6">
		  					  	<div class="alert alert-warning" role="alert">
		  	 <i class="fas fa-info-circle"></i> The listed employee(s) here will not be visible on the <u>Manage Employees</u> table.
		  	</div>
		  		</div>
		  		<div class="col-sm-6">
		  					  	<div class="alert alert-primary" role="alert">
		  	  <i class="fas fa-location-arrow"></i> Assign them to a new Position so you can manage them again.
		  	</div>
		  		</div>
		  	</div>


				<table class="table table-bordered table-striped table-sm" id="pos_nopostbl">
				<thead>
				<tr>
				<th>Name</th>
				<th>Action</th>
				</tr>
				</thead>
				<tbody id="id_employeewithnoposition">

				</tbody>
				</table>
		  </div>
		</div>
		</div>
	</div>
</div>
		</div>
	</div>


	<!-- <form action="php/external_server.php" method="POST"> -->
		<div class="modal" tabindex="-1" role="dialog" id="assnewposmodal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    	<div class="loading_indicator" id="lod_addposlod"></div>
		      <div class="modal-body">
		      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="ultrabold">Assign Position</h4>
		        <p>Assign <u id="emp_name_id"></u> to a new Position.</p>

		        <input type="hidden" id="employeeidtoassignnewposition" name="idofemp">
		        <div class="form-group">
		        	<label>Choose Position</label>
		        	<select class="form-control" id="newselected" name="NewSelectedPosition" required="">
		        		
		        	</select>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" name="assignunit_now" onclick="AssignNewPostion()" data-dismiss="modal" class="btn btn-primary">Save changes</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
	<!-- </form> -->

	<script type="text/javascript">
	function OpenAssignNewPosition(controlOBJ){
		$("#lod_addposlod").css("display","block");
		$("#emp_name_id").html($(controlOBJ).data("empname"));
		$("#employeeidtoassignnewposition").val($(controlOBJ).data("theempid"));

		$.ajax({
				type: "POST",
				url: "php/external_server.php",
				data: {Getallpositions: "x"},
				success: function(data){
					$("#newselected").html(data);
					$("#lod_addposlod").css("display","none");
				}
			})


		    $('#newselected option').prop('selected', function() {
        return this.defaultSelected;
    });
	}	
	</script>

	<!-- <form action="php/external_server.php" method="POST"> -->
	<div class="modal" tabindex="-1" role="dialog" id="crenewpos">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="ultratitle mb-3">Create Position</h5>

	        <div class="form-group">
	        	<label>Position Name</label>
	        	<input type="text" required="" id="posname" autocomplete="off" class="form-control" placeholder="Type here..." name="positionname">
	        </div>

	         <div class="form-group">
	        	<label>Description</label>
	        	<textarea required="" id="posdesc" placeholder="Type description here..." rows="10" class="form-control" name="positiondescription"></textarea>
	        </div>

	      </div>
	      <div class="modal-footer">
	        <button type="submit" name="addnewposition" data-dismiss="modal" onclick="AddNewPosition()" class="btn btn-primary">Create New Position</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- </form> -->

	<!-- <form action="php/external_server.php" method="POST"> -->
		<div class="modal" tabindex="-1" role="dialog" id="posmodal_edit">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h4 class="ultrabold">Edit Position</h4>
	        <input type="hidden" id="ed_posid" name="editpostid">
	        <div class="form-group">
	        	<label>Position Name</label>
	        	<input type="text" id="ed_posname" required="" autocomplete="off" class="form-control" placeholder="Type here..." name="positionname">
	        </div>

	         <div class="form-group">
	        	<label>Description</label>
	        	<textarea required="" id="ed_posdesc" placeholder="Type description here..." rows="10" class="form-control" name="positiondescription"></textarea>
	        </div>
	       
	      </div>
	      <div class="modal-footer">
	        <button type="submit" name="sub_editposition" data-dismiss="modal" onclick="EditPos()" data-dismiss="modal" class="btn btn-primary">Save changes</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- </form> -->

	<!-- <form action="php/external_server.php" method="POST"> -->
		<div class="modal" tabindex="-1" role="dialog" id="posmodal_delete">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h4 class="ultrabold">Delete Confirmation</h4>
	        <p>Are you sure you want to delete the position <u id="positionnameid">Blank</u>?</p>
	        <input type="hidden" id="positionidtodelete" name="idofpositiondatatodelete">
	         <div class="alert alert-primary" role="alert">
	         The're <u id="loseemployy">20</u> employees that will lose their position.
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" name="sub_deleteposition" onclick="DeletePos()" data-dismiss="modal" class="btn btn-danger">Yes, delete this position.</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- </form> -->

<script type="text/javascript">
	function deleteposition(controlOBJ){
		$("#positionnameid").html($(controlOBJ).data("positionname"));
		$("#positionidtodelete").val($(controlOBJ).data("positionid"));
		$("#loseemployy").html($(controlOBJ).data("empaffect"));
	}

	function editposition(controlOBJ){
		$("#ed_posid").val($(controlOBJ).data("positionid"));
		$("#ed_posname").val($(controlOBJ).data("positionname"));
		$("#ed_posdesc").val($(controlOBJ).data("positondescription"));
	}
</script>


</body>
</html>
<script type="text/javascript">
$("#pos_tbklls").DataTable();
$("#pos_nopostbl").DataTable();
setTimeout(function(){
	ReloadAllData();
},1000)
	
	function ReloadAllData(){
		ReloadAddedPositions();
ReloadEmployeesWithNoPositions();
	}
	function ReloadAddedPositions(){
		$("#poslod").css("display","block");
		$("#pos_tbklls").DataTable().destroy();
		$.ajax({
			type: "POST",
			url:"php/external_server.php",
			data: {reloadalladdedpositions:"x"},
			success: function(data){
				$("#id_addedpositions").html(data);
				$("#pos_tbklls").DataTable({
					"ordering":false
				});
				$("#poslod").css("display","none");
			}
		})
	}
	function ReloadEmployeesWithNoPositions(){
		$("#pos_nopostbl").DataTable().destroy();
		$.ajax({
			type: "POST",
			url:"php/external_server.php",
			data: {loadempwithnopos:"x"},
			success: function(data){
				$("#id_employeewithnoposition").html(data);
					$("#pos_nopostbl").DataTable({
						"ordering":false
					});
			}
		})
	}
	function AddNewPosition(){
		var posname = $("#posname").val();
		var posdesc = $("#posdesc").val();
		$.ajax({
			type: "POST",
			url:"php/external_server.php",
			data: {addnewposition:"x",positionname:posname,positiondescription:posdesc},
			success: function(data){
				// alert(data);
				popnotification(data, "Employee Positions",false);
				$("#posname").val("");
				$("#posdesc").val("");
				ReloadAllData();
			}
		})
	}
		function EditPos(){
		var edxx_posid = $("#ed_posid").val();
		var edxx_posname = $("#ed_posname").val();
		var edxx_posdesc = $("#ed_posdesc").val();
		
		if(edxx_posdesc != "" && edxx_posname != ""){
			$.ajax({
			type: "POST",
			url: "php/external_server.php",
			data: {sub_editposition:"x",editpostid:edxx_posid,positionname:edxx_posname,positiondescription:edxx_posdesc},
			success: function(data){
				// alert(data);
				popnotification(data, "Employee Positions",false);
				$("#ed_posname").val("");
				$("#ed_posdesc").val("");
				ReloadAllData();
			}
		})
		}else{
			popnotification("Not saved. Please fill all fields to save changes.", "Employee Positions",false);
		}
	}

		function DeletePos(){
			var pid = $("#positionidtodelete").val();
		$.ajax({
			type: "POST",
			url:"php/external_server.php",
			data: {sub_deleteposition:"x",idofpositiondatatodelete:pid},
			success: function(data){
				// alert(data);
				popnotification(data, "Employee Positions",false);
				ReloadAllData();
			}
		})
	}
	
	function AssignNewPostion(){
		var idofempx = $("#employeeidtoassignnewposition").val();
		var newselposx = $("#newselected").val();


		if(newselposx != null){
			$.ajax({
			type: "POST",
			url:"php/external_server.php",
			data: {assignunit_now:"x",idofemp:idofempx,NewSelectedPosition:newselposx},
			success: function(data){
				popnotification(data, "Employee Positions",false);
				ReloadAllData();
			}
		})
		}else{
			popnotification("Select a position to assign.", "Employee Positions",false);
		}
		
	}
	


	highlight_pagelink("#page_positions");

</script>
<?php
include("components/modals.php");
?>

