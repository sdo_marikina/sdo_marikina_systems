<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Attendance History</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
  
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
  
	<div class="rightbar">


       <nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
      <a class="navbar-brand" href="#"><i class="far fa-clock"></i> ATTENDANCE HISTORY</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
      </div>
    </nav>

		<div class="container">
<div class="row">
  <div class="col-sm-9">
      <div class="form-group">
      <div class="card">
        <div class="card-body">
          <img src='images/day_today.png' class='content_icon'>
          <h5 class="card-title mb-3">Today's Summary</h5>
      <h6 class="text-muted mb-2 card-subtitle">This are the quick summary of attendance logs for today.</h6>
      <?php include("components/dash_summary.php");?>
        </div>
      </div>
    </div>
<div class="card">
  <div class="card-body" style=" overflow: hidden;">

        <div class="loading_indicator" id="lod_2"></div>
           <img src='images/logs.png' class='content_icon'>
        <h5>Logs</h5>
    <table id="tbl_tableofhistory" class="table table-sm table-striped">
      <thead>
        <tr>
          <th>Time</th>
          <th>Picture</th>
          <th>Employee Name</th>
          <th>Access Type</th>
          <th>Origin</th>
        </tr>
      </thead>
      <tbody id="tbl_atthistory_core">
        
      </tbody>
    </table>
  </div>
</div>
  </div>
  <div class="col-sm-3">
   <div class="card">
     <div class="card-body">
<img src='images/clock.png' class='content_icon'>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <label><i class="far fa-calendar"></i> Select Date</label>
          <input id="inp_dateofatt" class="form-control" type="date" name="" value="<?php echo date('Y-m-d'); ?>">
        </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group">
          <label><i class="fas fa-male"></i> Select Employee Type</label>
         <select  id="inp_emptypeofatt" class="form-control">
           <option value="0">All</option>
           <option value="2">Teaching</option>
           <option value="1">Non-Teaching</option>
           <option value="3">Division Personnel</option>
         </select>
        </div>
      </div>
          <div class="col-sm-12">
      </div>
    </div>
    <div class="form-group">
      <button class="btn btn-primary mt-3 btn-sm btn-block" onclick="GenerateDateAttLogs()"><i class="fas fa-sync-alt"></i> Generate Log</button>
    </div>
     </div>
   </div>

<!-- 
   <div class="card">
     <div class="card-body">
        <h5 class="mb-2"><i class="fas fa-tools"></i> Manual Operations</h5>
        
        <a href="#" data-toggle="modal" data-target="#addrecord"><i class="far fa-plus-square"></i> Add Record</a>
     </div>
   </div>

 -->


   <div class="card">
     <div class="card-body">
        <h5 class="mb-3"><i class="far fa-compass"></i> Legend</h5>

          <div class="row">
            <div class="col-sm-12">
              <!-- <img src="images/bio_smartentry_nano.png"> -->
                 <img src='images/bio_smartentry_nano.png' class='content_icon'>
        <h5>Biometrics</h5>
        <span class="text-muted">Default Biometrics Device with Fingerprint and RFID scanner to log for attendance.</span>
            </div>
                        <div class="col-sm-12">
              <!-- <img src="images/bio_kiosk_nano.png"> -->
                 <img src='images/bio_kiosk_nano.png' class='content_icon'>
        <h5>Kiosk</h5>
        <span class="text-muted">Alternative Biometrics Solution that enables the employee to type his/her employee I.D and take a self portrait as an evidence to log for attendance.</span>
            </div>
                        <div class="col-sm-12">
              <!-- <img src="images/manual_entry_nano.png"> -->
                 <img src='images/manual_entry_nano.png' class='content_icon'>
        <h5>Manual Entry (Deprecated in 3.3)</h5>
        <span class="text-muted">Manually typed attendance log.</span>
            </div>
          </div>
  </div>

     </div>
   </div>


    <script type="text/javascript">
      
 setTimeout(function(){
 GenerateDateAttLogs();
 },1000)

    function GenerateDateAttLogs(){
      $("#lod_2").css("display","block");
    var idt = $("#inp_dateofatt").val();
    var inp_emptypeofatt = $("#inp_emptypeofatt").val();
    // var inp_searchforatt = $("#inp_searchforatt").val();
    $('#tbl_tableofhistory').DataTable().destroy();
    $.ajax({
      type: "POST",
      url: "php/external_server.php",
      data: {getattendancelogsofdate:"x",inpdate:idt,emp_type:inp_emptypeofatt},
      success: function(data){
           $("#lod_2").css("display","none");
        $("#tbl_atthistory_core").html(data);
        $("#tbl_tableofhistory").DataTable();
    },componentWillUnmount: function(){
  if (this.ajaxRequest && this.ajaxRequest.abort){
    this.ajaxRequest.abort()
  }
}
  })
}
    </script>
  </div>
</div>
		</div>
	</div>

</body>
</html>



<script type="text/javascript">
    highlight_pagelink("#page_attendancehistory");
    setTimeout(function(){
      // TriggerWhereaboutsSync();
      // TriggerLeaveSynching();
      TriggerAttendanceLogs();
  },5000)
</script>

<form action="php/external_server.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="addrecord">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-body">
      <h5 class="card-title mb-5">New Manual Attendance Log</h5>

       <div class="form-group">
         <label>Employee ID</label>
       <select name="eid" class="form-control" id="allemps">
         
       </select>
       </div>
       <div class="row">
           <div class="col-sm-6">
            <div class="form-group">
         <label>Date</label>
        <input required="" type="date" class="form-control" name="manualdate">
       </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
         <label>Time</label>
        <input required="" type="time" class="form-control" name="thetimeoflog">
       </div>
         </div>
         <div class="col-sm-12">
           <div class="form-group">
         <label>Access Type</label>
        <select required="" class="form-control" name="access_type">
           <option value="1">Time-in</option>
           <option value="2">Time-out</option>
         </select>
       </div>
         </div>
       </div>
       
       
     </div>
     <div class="modal-footer">
       <button type="submit" name="addmanualrecord" class="btn btn-primary">Add Record</button>
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
     </div>
   </div>
 </div>
</div>
</form>

<script type="text/javascript">
  $.ajax({
    type : "POST",
    url : "php/external_server.php",
    data : {get_emps_xinfom:"x"},
    success: function(data){
      $("#allemps").html(data);
    }
  })
</script>

<?php
  include("components/modals.php");
?>

