<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title></title>
	<?php
	include("connection/connect.php");
	$theleavetype = "";
	// Add form 6 data to database
	$eid = $_POST["eid"];
	// echo $eid;
	$f_vacationtype = "";
	$f_vac_abroad = "";

	if(isset($_POST["vacationleave_location"])){
	$f_vacationtype = $_POST["vacationleave_location"];
	$f_vac_abroad = mysqli_real_escape_string($c,$_POST["f_vac_abroad"]);
	$f_vacationtype = $_POST["vacationleave_location"];
	}
	
	$f_vac_abroad = mysqli_real_escape_string($c,$_POST["f_vac_abroad"]);
	$leavetype = $_POST["leavetype"];
	$date_from = $_POST["date_from"];
	$date_to   = $_POST["date_to"];

	$location_1 = "";
	$location_2 = "";
	if(isset($_POST["location_1"])){
		$location_1 = $_POST["location_1"];
	}
	if(isset($_POST["location_2"])){
		$location_2 = $_POST["location_2"];
	}

	$subtype = "";
	if(isset($_POST["leavetype_status"])){
		$subtype = $_POST["leavetype_status"];
	}

	//Leave taken - current balance or leave type in entitlements

	$leave_take_value = $_POST["leave_taken_value"];


	$date_applied_totatus = $_POST["date_applied_totatus"];
	$location = $location_1;
	if($location_1 == ""){
	$location = $location_2;
	}
		if(!isset($_POST["fordissaproval"]) && !isset($_POST["btn_lr_approve"])){
			// THIS WILL ONLY INSERT THE DATA TO THE DATABASE IF DISSAPROVAL MODE IS FALSE
			$theleavetype = $leavetype;
			$q = "SELECT * FROM applied_leave WHERE employee_id='$eid' AND time_requested=NOW()";
			if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
	$q = "INSERT INTO applied_leave(
	employee_id,
	leave_type,
	date_from,
	date_to,
	sub_type,
	location,
	leave_taken,
	date_requested,
	time_requested,
	date_applied_totatus
	)
	VALUES(
	'$eid',
	'$leavetype',
	'$date_from',
	'$date_to',
	'$subtype',
	'"  . mysqli_real_escape_string($c, $location) . "',
	'$leave_take_value',
	'" . date("Y-m-d") . "',
	NOW(),
	'$date_applied_totatus')";
	log_system_action($eid . " submitted a Leave Report.");
	$res = mysqli_query($c,$q);



			}


}

	$lnpx = $_POST["butal"];
	if(strpos($lnpx, "-") !== false){
		$lnpx = str_replace("-", "", $lnpx);
		$lnpx  = round($lnpx);
	}else{
		$lnpx = "0";
	}
	
	//Get form 6 id to database when proccess is finnished
	$q = "SELECT * FROM applied_leave ORDER BY id DESC LIMIT 1";
	$res = mysqli_query($c,$q);
	$curr_row = mysqli_fetch_array($res);


	include("theme/original.php");
	$leave_id = $curr_row["id"];

	include("php/form6_process.php");
	//Leave taken - current balance or leave type in entitlements
	$lwp = 0;
	$lnp = 0;
	// $getvacation
	// $getsick

	switch ($leavetype) {
		case 'Vacation Leave':
		compute_paidDays($getvacation);
		break;
		case 'Sick Leave':
		compute_paidDays($getsick);
		break;
		default:
		if($usertype != "2"){
			//Non Teaching and division
			switch($leavetype){
				case "Service Credit":
					compute_paidDays($getservicecredit);
				break;
				case "Special Leave":
					compute_paidDays($get_specialLeave);
				break;
				case "Paternity":

				break;
				case "CTO":
					compute_paidDays($get_cto);
				break;
			}
		}else{
					compute_paidDays($getsick);
		}
		
		break;

	}

	$lwp = str_replace("-", "", $lwp);
	$lnp = str_replace("-", "", $lnp);


	function compute_paidDays($balance){
		global $lwp;
		global $lnp;
		global $leave_take_value;
		$leavetake = $leave_take_value;
	
		if($leavetake < $balance){
			$lwp =  intval($leavetake) % intval($balance);
			$lnp = intval($balance) % intval($leavetake);
		}else{

			$lwp = intval($balance) %  intval($leavetake);
			if(intval($leavetake) < intval($balance)){
				$lnp = intval($leavetake)  % intval($balance);
			}else{
				$lnp = intval($balance) % intval($leavetake);
			}
				

		}
		sameval($balance,$leavetake);
	}

	function sameval($c1,$c2){
		global $lnp;
		global $lwp;
		if(intval($c1) > intval($c2)){
			$lnp = "0";
		}else if(intval($c1) == intval($c2)){
			$lwp = intval($c1); 
		}
	}



	if(isset($_POST["fordissaproval"])){
		// THIS WILL ONLY WORK IN DISSAPROVAL MODE
		$f_fullname = $_POST["dis_fullname"];
		$f_fillingdate = $_POST["dis_filling_date"];
		$f_officeagency = $_POST["dis_office_agency"];
		$f_postion = $_POST["dis_position"];
		$f_salary = $_POST["dis_salary_monthly"];

		$lwp = $_POST["dis_lwp"];
		$lnp = $_POST["dis_lnp"];

		$lnp += $lwp;
		$lwp = "0";

		$f_vac_abroad = $_POST["f_vac_abroad"];
		$f_subtype = $_POST["dis_vacation_type"];
		$f_vacationtype = "";
		$f_location = "";
		$leave_id = $_POST["eid"];
		$f_leave_type = "LEAVE WITHOUT PAY";

		$id = $_POST["id"];
		$reasonofdiss = mysqli_real_escape_string($c,$_POST["reasonofdiss"]);
		$q = "UPDATE applied_leave SET status='2',reason_of_dissaprove='$reasonofdiss' WHERE id='$id'";
		log_system_action($leave_id . " Leave Report status changed to dissaproved.");
		$res = mysqli_query($c,$q);
		// BackToPage($res,"LeaveReports.php");
	}else if(isset($_POST["btn_lr_approve"])){
				// THIS WILL ONLY WORK IN APPROVAL MODE
		$f_fullname = $_POST["dis_fullname"];
		$f_fillingdate = $_POST["dis_filling_date"];
		$f_officeagency = $_POST["dis_office_agency"];
		$f_postion = $_POST["dis_position"];
		$f_salary = $_POST["dis_salary_monthly"];

		$lwp = $_POST["dis_lwp"];
		$lnp = $_POST["dis_lnp"];

		$f_vac_abroad = $_POST["f_vac_abroad"];
		$f_subtype = $_POST["dis_vacation_type"];
		$f_vacationtype = $_POST["dis_vacation_leaveincase"];
		$f_location = $_POST["dis_dis_sick_incase_reason"];
		$leave_id = $_POST["eid"];
		$f_leave_type =$_POST["leavetype"];


$id = $_POST["id"];

$q = "SELECT * FROM  applied_leave WHERE id='$id'";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);
$theleavetype = strtolower(str_replace(" ", "_", $row["leave_type"]));

$q = "SELECT * FROM entitlements WHERE emp_id='" . $row["employee_id"] . "' AND $theleavetype <> 0";
$emp_id = $row["employee_id"];
$leavetaken = $row["leave_taken"];
$res = mysqli_query($c,$q);

$row = mysqli_fetch_array($res);

$newval = $row[$theleavetype] - $leavetaken;
if(strpos($newval, "-") !== false){
$newval = 0;
}


if($theleavetype != "leave_without_pay"){


if($theleavetype == "cto"){
	// SEPARATED TABLE TO UPDATE CTO
	$ent_q = "UPDATE entitlements_cto SET entitlement='" . $newval  . "' WHERE empid='" . $emp_id . "' ORDER BY val_to ASC LIMIT 1";
	$ent_res = mysqli_query($c,$ent_q);
}else{
	// DEFAULT TABLE TO UPDATE THE REST OF LEAVE TYPES
	$q = "UPDATE entitlements SET $theleavetype = $newval  WHERE emp_id='" . $emp_id . "' AND $theleavetype <> 0";
	$res = mysqli_query($c,$q);
}

	$q = "UPDATE applied_leave SET status='1',leave_taken='0' WHERE id='$id'";
	log_system_action($emp_id . " Leave Report status changed to approved.");
	$res = mysqli_query($c,$q);
}
		// BackToPage($res,"LeaveReports.php");
	}else{
		//Additional prepared information update
		if($f_vacationtype == null || $f_vacationtype == ""){
			$f_vacationtype = 0;
		}
		if($theleavetype == "leave_without_pay"){
			$lwp = 0;
		}
		
	$q = "UPDATE applied_leave SET fullname='$f_fullname', date_of_filling='$f_fillingdate',office_agency='$f_officeagency', position='$f_postion', salary_monthly='$f_salary' , paid_days='" . $lwp . "', unpaid_days ='$lnpx', vac_leave_abroad_specify='$f_vac_abroad',vacation_type='$f_subtype', vacation_leaveincase='$f_vacationtype', sick_incase='$f_subtype', sick_incase_reason='$f_location' WHERE id='$leave_id'";
	mysqli_query($c,$q);
	}


	?>
	<style type="text/css">
		.indent_0x{
			margin-left: 15px;
		}
		.indent_1x{
			margin-left: 50px;
		}
		.indent_2x{
			margin-left: 100px;
		}
		.centerTextFixed{
			width: 95%;
			margin-left: 15px;
			margin-right: 15px;
			margin-bottom: 5px;
			text-align: center;
		}
		.theinput{
			border:none;
			border-bottom: 1px solid black;
		}
		.row{
			padding-top: 0px !important;
			margin-top: 0px !important;
		}
		.col-sm-6{
			padding-top: 0px !important;
			margin-top: 0px !important;
		}
		hr{
			padding: 0px !important;
			margin: 0px !important;
			border-top: 1px solid rgba(0,0,0,0.1);
		}
		.rightborder{
			border-right: 1px solid rgba(0,0,0,0.1);
		}
		tr{
			margin: 0 !important;
			padding: 2px !important;
		}
		td{
			margin: 0 !important;
			padding: 2px !important;
		}
		.lined{
			border: none;
			border-bottom: 1px solid black;
			width: 274px;
			text-align: center;
		}
		@media print{
			#tohide{
				display: none;
			}
		}
	</style>
</head>
<body>
	<div class="jumbotron jumbotron-fluid" id="tohide">
		<div class="container">
			<h1 class="display-4">Sending Form 6 to pending approval...</h1>
			<?php
	if(isset($_POST["fordissaproval"]) || isset($_POST["btn_lr_approve"])){
echo '<a href="LeaveReports.php" style="margin-right: 10px;"><i class="fas fa-arrow-circle-left"></i> Back</a>';
	}else{
		echo '<script> alert("Leave request submitted!"); window.location.href="card_app.php"; </script>';
	}
			?>
			
			<a href="#" onclick="window.print()"><i class="fas fa-print"></i> Print</a>
		</div>
	</div>
	
	<div class="container">
		<center>
		<p>FORM 6</p>
	<h1>APPLICATION FOR LEAVE</h1>
	</center>
	<p>CSC Form No 6<br>
	Revised 1984</p>
	<table class="table table-bordered">
		<tr>
			<td>
				<h6>1. OFFICE / AGENCY</h6>
				<span  class="indent_0x"><?php echo $f_officeagency; ?></span>
			</td>
			<td colspan="2">
				<h6>2. NAME (LAST) (FIRST) (MIDDLE)</h6>
				<span class="indent_0x"><?php echo $f_fullname; ?></span>
			</td>
		</tr>
		<tr>
			<td>
				<h6>3. DATE OF FILING</h6>
				<span class="indent_0x"><?php echo $f_fillingdate;?></span>
			</td>
			<td>
				<h6>4. POSITION</h6>
				<span class="indent_0x"><?php echo $f_postion; ?></span>
			</td>
			<td>
				<h6>5. SALARY MONTHLY</h6>
				<span class="indent_0x"><?php echo number_format($f_salary);  ?></span>
			</td>
		</tr>
	</table>
	<center>
		<p>DETAILS OF APPLICATION</p>
	</center>
	<hr>
	<div class="row">
		<div class="col-sm-6 rightborder">
			<span>6.</span>
			<h6 class="indent_0x">a) TYPE OF LEAVE</h6>
			<p class="indent_1x"><input id='id_vacationleave' type="checkbox" disabled="" name=""> Vacation</p>
			<label class="indent_2x"><input type="radio" disabled="" id="vac_leave_toseek" name="va_leave"> To Seek Employment</label class="indent_1x"><br>
			<label class="indent_2x"><input type="radio" disabled="" id="vac_leave_other" name="va_leave"> Others (Specify) 
				<input type="text" readonly class="lined" style="width: 224px;" name="va_otherspecifyreason" id="va_leave_other_reason"></label><br>
			<label class="indent_1x"><input id="id_sickleave" type="checkbox" disabled="" name=""> Sick</label><br>
			<label class="indent_1x"><input id="id_maternity" type="checkbox" disabled="" name=""> Maternity</label><br>
			<label class="indent_1x"><input id="id_others" type="checkbox" disabled="" name=""> Others (Specify) <input  type="text" readonly class="lined" name="" id="id_leavetype"></label><br>
			<h6 class="indent_0x">6. c) NUMBER OF WORKING DAYS APPLIED FOR</h6>
			<input class="centerTextFixed theinput" id='nowd' type="text" readonly name="">
			<script type="text/javascript">
				$("#nowd").val(<?php echo $f_leavetaken; ?>);
			</script>
			<br>
			<h6 class="indent_0x">INCLUSIVE DATES</h6>
			<input class="centerTextFixed theinput" id="incdates" type="text" readonly name=""><br>
			
			<script type="text/javascript">
				$("#incdates").val(<?php echo json_encode($f_inclusivedates); ?>);
			</script>
			<br>
			<br>
		</div>
		<div class="col-sm-6">
			<h6>6. b) WHERE LEAVE WILL BE SPENT:</h6>


			<p>1. IN CASE OF VACATION LEAVE</p>
			<labe class="indent_0x"><input type="radio" disabled="" id="vac_1" name="vacl_ltype"> Within the Philippines</label><br>
			<label class="indent_0x"><input type="radio" disabled="" id="vac_2" name="vacl_ltype"> Abroad (Specify)</label>
			<input class="centerTextFixed theinput" id="vac_specify" type="text" readonly name="">
			<h6>2. IN CASE OF SICK LEAVE</h6>
			<label class="indent_0x"><input id="id_sick_reason_inhospital" type="radio" disabled="" name=""> In Hospital</label>
			<input class="centerTextFixed theinput" id="id_sick_location_inhos" type="text" readonly name="">
			<label class="indent_0x"><input id="id_sick_reason_outpatient" type="radio" disabled="" name=""> Out Patient (Specify)</label>
			<input class="centerTextFixed theinput" id="id_sick_location_outpat" type="text" readonly name="">
			<br>
			<br>
			<h6 class="indent_0x">d) COMMUTATION</h6>
			<label class="indent_0x"><input type="radio" checked="checked" name="commutation" disabled=""> Requested <input disabled="" class="indent_0x" type="radio" disabled="" name="commutation"> Not Requested</label>
			<br>
			<br>
			<br>
			<center>
				<span>Signature of Applicant</span>
			</center>
		</div>
	</div>

	<script type="text/javascript">
		var vacationt_loc = <?php echo json_encode($f_vacationtype); ?>;
		if(vacationt_loc == "0"){
			$("#vac_1").prop("checked",true);
		}else if(vacationt_loc == "1"){
			$("#vac_2").prop("checked",true);
			$("#vac_specify").val(<?php echo json_encode($f_vac_abroad); ?>);
		}else{
			$("#vac_1").prop("checked",false);
			$("#vac_2").prop("checked",false);
		}
	</script>
	<hr>
	<center>
		<h6>DETAILS OF ACTION OF APPLICATION</h6>
	</center>
	<hr>
<div class="row">
	<div class="col-sm-6 rightborder">
		<h6 class="indent_0x">7. a) CERTIFICATION OF LEAVE CREDITS as of</h6>
		<input class="centerTextFixed theinput" type="text" readonly="" id="cer_lv_as_of" name="">
		<script type="text/javascript">
			$("#cer_lv_as_of").val(<?php echo json_encode(date('Y-m-d',strtotime("-1 days"))); ?>);
		</script>
		<table class="table table-bordered" style="text-align: center;">
			<tr>
				<th>Vacation</th>
				<th>Sick</th>
				<th>Total</th>
			</tr>
			<tr>
				<td><?php echo $getvacation; ?></td>
				<td><?php echo $getsick; ?></td>
				<td><?php echo json_encode($getvacation + $getsick); ?></td>
			</tr>
			<tr>
				<td>Days</td>
				<td>Days</td>
				<td>Days</td>
			</tr>
		</table>
		<h6 class="indent_0x">Leave Balance as of <?php echo date("Y-m-d") . " " . date("h:i:sa"); ?></h6>
				<table class="table table-bordered" style="text-align: center;">
			<tr>
				<th>Vacation</th>
				<th>Sick</th>
				<th>Total</th>
			</tr>
			<?php
if($leavetype == "Vacation Leave"){
	//LEAVE TYPE - Vacation
?>

<tr>
<td>
	<?php 

		if($f_leave_type != "LEAVE WITHOUT PAY"){
			$theval = $getvacation - $leave_take_value;
		}else{
			$theval = $getvacation;
		}

		  if(strpos($theval,"-") !== false){
		  		echo "0";
		  }	else{
		  	echo $theval;
		  }	
	?></td>
<td><?php echo $getsick; ?></td>


<td><?php 
if(strpos(number_format(($getvacation - $leave_take_value) + $getsick,3), "-") !== false){
echo "0";
}else{
echo number_format(($getvacation - $leave_take_value) + $getsick,3);
} ?></td>


</td>
</tr>
			
<?php
}else if($leavetype == "Sick Leave" || $leavetype == "Service Credit" AND $usertype == "2"){
	//LEAVE TYPE - Sick
?>

<tr>
<td><?php echo $getvacation; ?></td>
<td>
	<?php $theval =  $getsick - $leave_take_value;
		  if(strpos($theval,"-") !== false){
		  		echo "0";
		  }	else{
		  	echo $theval;
		  }	
	?></td>



<td><?php 
if(strpos(number_format($getvacation + ($getsick - $leave_take_value),3), "-") !== false){
echo "0";
}else{
echo number_format($getvacation + ($getsick - $leave_take_value),3);
} ?></td>


</tr>
			
<?php
}else{
	//LEAVE TYPE - Default
?>

<tr>
<td><?php echo $getvacation; ?></td>
<td><?php echo $getsick; ?></td>
<td><?php 
if(strpos(number_format($getvacation + $getsick,3), "-") !== false){
echo "0";
}else{
echo number_format($getvacation + $getsick,3);
}
; ?></td>
</tr>

<?php
}
			?>
		</table>
		<br>
			<center>
				<h6><?php echo $hrmo; ?></h6>
				<p><?php echo $f_valtype_hr; ?></p>
			</center>
	</div>
	<div class="col-sm-6">
		<h6>7. b) RECOMMENDATION</h6>
				<label class="indent_0x"><input type="checkbox" disabled="" name=""> Approval</label><br>
			<label class="indent_0x"><input type="checkbox" disabled="" name=""> Disapproval due to</label><br>
			<textarea readonly class="indent_0x" style="width: 100%;">
				
			</textarea>
			<br>
			<br>
				<br>
					
			<center>
	<?php
	if($f_accounttype == 1){
		?>
		<!-- SCHOOL -->
	<h5><?php echo $f_princname; ?></h5>
	<p><?php echo $f_princpostion;?></p>

		<?php
	}else{
		?>
		<!-- DIVISION -->
	<h5><?php echo $administrativeofficer; ?></h5>
	<p><?php echo $f_valtype_aoff;?></p>

		<?php
	}
	function log_system_action($message){
	global $c;
	$message = mysqli_real_escape_string($c, $message);
	$message = htmlentities($message);
	$q = "INSERT INTO logs(performer,action_description,timestamp) VALUES('Admin','" . $message . "','" . date("Y-m-d H:i:s") . "')";
	mysqli_query($c,$q);
}
	?>

</center>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<h5>7. c) APPROVED FOR:</h5>
		<label><?php echo '<input style="text-align:center;" type="text" readonly="" name="" value="' . $lwp . '"> days with pay</label>'; ?>
		<label><?php echo '<input style="text-align:center;" type="text" readonly="" name="" value="' . $lnp . '"> days without pay</label>'; ?>
		<label><input type="text" style="text-align:center;" readonly="" name=""> other (Specify)</label>
	</div>
	<div class="col-sm-6">
		<h5>7. b) DISAPPROVED DUE TO:</h5>
		<textarea id="duetodissaprovereason" readonly class="indent_0x" style="width: 100%;"></textarea>
	</div>
</div>
<br>
<center>
	<?php
	if($f_accounttype == 1){
		?>
		<!-- SCHOOL -->
	<h5><?php echo $administrativeofficer; ?></h5>
	<p><?php echo $f_valtype_aoff;?></p>

		<?php
	}else{
		?>
		<!-- DIVISION -->
	<h5><?php echo $superintendent; ?></h5>
	<p><?php echo $f_valtype_superinten;?></p>

		<?php
	}
	?>

</center>
	</div>
</body>
</html>


<script type="text/javascript">
	//The Leave Type Manager
	var leave_type = <?php echo json_encode($f_leave_type); ?>;
	switch(leave_type){
		case "Vacation Leave":
			$("#id_vacationleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "Others (Specify)":

					$("#vac_leave_other").prop("checked",true);
					$("#va_leave_other_reason").val(<?php echo json_encode($f_location); ?>);
				break;

			}
		break;
		case "Sick Leave":
			$("#id_sickleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "In Hospital":
					$("#id_sick_reason_inhospital").prop("checked",true);
					$("#id_sick_location_inhos").val(<?php echo json_encode($f_location); ?>);
				break;
				case "Out Patient":
					$("#id_sick_reason_outpatient").prop("checked",true);
					$("#id_sick_location_outpat").val(<?php echo json_encode($f_location); ?>);
				break;
				
			}
			
		break;
		case "Maternity":
			$("#id_maternity").prop("checked",true);
		break;
		default:
			$("#id_others").prop("checked",true);
			$("#id_leavetype").val(leave_type);
		break;
	}

</script>

<?php

	if(isset($_POST["fordissaproval"])){
		echo "<script>
			$('#duetodissaprovereason').val('" . mysqli_real_escape_string($c,$_POST["reasonofdiss"]) . "');
		</script>";
	}

?>