<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Holidays</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
      <script type="text/javascript">
        TriggerHolidaySync();
      </script>
    <nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
      <a class="navbar-brand" href="#"><i class="fas fa-hospital-symbol"></i> HOLIDAYS</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#ModalNewHoliday"><i class="far fa-plus-square"></i> New Holiday</a>
          </li>
        </ul>
      </div>
    </nav>


		<div class="container">
			<div class="row">
        <div class="col-sm-9">
          <div class="form-group">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Currently Added</h5>
                <h6 class="card-subtitle mb-2 text-muted">Added supension and holidays.</h6>
                <table class="table table-bordered table-striped" id="emp_tbl">
      <thead>
        <tr>
          <th>Name</th>
          <th>Date</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        <?php ShowHolidays(); ?>
      </tbody>
    </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <div class="card">
              <div class="card-body">
                  <h5 class="card-title">Check holidays</h5>
                <h6 class="card-subtitle mb-4 text-muted">See additional holiday information from trusted sources.</h6>
                <a target="_blank" href="https://www.timeanddate.com/holidays/philippines/"><i class="fas fa-globe-asia"></i> timeanddate.com</a><br>
                 <a target="_blank" href="https://www.rappler.com/nation/209681-list-2019-holidays-philippines"><i class="fas fa-globe-asia"></i> rappler.com</a><br>
                 <a target="_blank" href="https://publicholidays.ph/2019-dates/"><i class="fas fa-globe-asia"></i> publicholidays.ph</a>
              </div>
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>

</body>
</html>
<form action="php/external_server.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="ModalNewHoliday">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultrabold"><i class="far fa-sun"></i> New Holiday</h4>
       <div class="form-group">
       	<label>Holiday Name</label>
       	<input class="form-control" required="" autocomplete="off" type="text" placeholder="Type holiday name here..." name="nameOfHoliday">
       </div>
        <div class="form-group">
       	<label>Date</label>
       	<input class="form-control" required="" autocomplete="off" type="date" name="dateOfHoliday">
       </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="AddNewHoliday" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add Holiday</button>
        <button type="button" class="btn btn-light" data-dismiss="modal"><i class="far fa-window-close"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>


<form action="php/external_server.php" method="POST">
<div class="modal" tabindex="-1" role="dialog" id="modal_edit_holiday">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultrabold">Edit Holiday</h4>
      	<input type="hidden" name="holID" id="updt_id" value="">
        <div class="form-group">
        	<label>New Holiday Name</label>
        	<input type="text"  id="updt_holname" class="form-control" required="" name="newHolName">
        </div>
        <div class="form-group">
        	<label>New Date</label>
        	<input type="date"  id="updt_date" class="form-control" required="" name="newHolDate">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="EditHoliday" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>	
</form>

<script type="text/javascript">
    // popnotification("We are still working on this.", "Some features are not yeat ready for this page.",false);
	function ShowUpdateModal(c_object){
	 	// PREPARE UPDATE INFORMATION
		$("#updt_id").val($(c_object).data("updt_id"));
		$("#updt_holname").val($(c_object).data("updt_holname"));
		$("#updt_date").val($(c_object).data("updt_date"));
	}
</script>


<form action="php/external_server.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="modal_del_holiday">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4 class="ultrabold">Delete This Holiday?</h4>
        <p>This will be deleted forever!</p>
        <input type="hidden" id="inp_holiID" name="inp_holiID">
      </div>
      <div class="modal-footer">
        <button type="submit" name="delholiday" class="btn btn-primary"><i class="far fa-trash-alt"></i> Delete</button>
        <button type="button" class="btn btn-light" data-dismiss="modal"><i class="far fa-window-close"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
	function DeleteHol(c_object){
		$("#inp_holiID").val($(c_object).data("holid"));
	}
</script>
<script type="text/javascript">
	$("#emp_tbl").DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
  highlight_pagelink("#page_holidays");
</script>
<?php
include("components/dispute_reports_modals.php");
include("components/modals.php");
?>

