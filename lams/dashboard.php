<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Dashboard</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar" style=" background-size: cover; background-attachment: fixed; background-repeat: none;">
		<div class="container mt-3">
			<div class="row">
				<div class="col-sm-12">
					<div class="container">
						<div class="row">
						<div class="col-sm-4">
							<div class="card">
								<div class="card-body">
									<center>
										<img src='images/employees.png' class='content_icon'>
										<h5>Employees</h5>
									</center>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="card">
								<div class="card-body">
									<center>
										<img src='images/leave_big.png' class='content_icon'>
										<h5>Leave Report</h5>
									</center>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="card">
								<div class="card-body">
									<center>
										<img src='images/entitlements.png' class='content_icon'>
										<h5>Entitlements</h5>
									</center>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			<div class="col-sm-6">
<?php include("components/introductory.php"); ?>

			</div>
			<div class="col-sm-6">
				<?php include("components/module_station_info.php");?>
			</div>
			<div class="col-sm-12">
				<?php include("components/module_insights.php");?>
			</div>
		</div>
		</div>
	</div>
</body>
</html>

<?php
include("components/modals.php");
?>

<script type="text/javascript">
highlight_pagelink("#page_dash");
function deleteschedmul(obj){
if (confirm("Are you sure you want to delete this schedule?")) {
   // alert($(obj).data("delidval"));
   var idtodel = $(obj).data("delidval");
   $.ajax({
   	type: "POST",
   	url: "php/external_server.php",
   	data: {del_a_sched_mul:"x",idtodelsched: idtodel},
   	success: function(data){
   		alert(data);
   		Reload_multiple_sched_display(obj);
   	}
   })
  }
}
TriggerSyncSettings();
setTimeout(function(){
      TriggerWhereaboutsSync();
      TriggerLeaveSynching();
},5000)
</script>