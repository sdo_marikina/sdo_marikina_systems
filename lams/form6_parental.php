<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="favs/application-vnd.wordperfect.ico" type="text/css" href="">
	<title>Form 6 Printing</title>
	<script type="text/javascript" src="api/html2canvas/html2canvas.js"></script>
	<script type="text/javascript" src="api/html2canvas/html2canvas.min.js"></script>
	<?php
	include("connection/connect.php");
	// Add form 6 data to database

	$eid = $_POST["employee_id"];
	$f_vacationtype = "";
	$f_vac_abroad = "";

	if(isset($_POST["vacationleave_location"])){
		$f_vacationtype = $_POST["vacationleave_location"];
	$f_vac_abroad = mysqli_real_escape_string($c,"");
	$f_vacationtype = $_POST["vacationleave_location"];
	}
	
	$f_vac_abroad = mysqli_real_escape_string($c,"");
	$leavetype = $_POST["leave_type_now"];

	$theleavetype = $leavetype;
	$date_from = $_POST["date_from"];
	$date_to   = $_POST["date_to"];

	$location_1 = "";
	$location_2 = "";
	if(isset($_POST["location_1"])){
		$location_1 = $_POST["location_1"];
	}
	if(isset($_POST["location_2"])){
		$location_2 = $_POST["location_2"];
	}

	$subtype = "";
	if(isset($_POST["leavetype_status"])){
		$subtype = $_POST["leavetype_status"];
	}

	//Leave taken - current balance or leave type in entitlements

	$leave_take_value = $_POST["leave_days"];

$lwp = $leave_take_value;
	$lnp = 0;

	$date_applied_totatus = date("Y-m-d");
	$location = $location_1;
	if($location_1 == ""){
	$location = $location_2;
	}
	// echo$location;
		if(!isset($_POST["fordissaproval"]) && !isset($_POST["btn_lr_approve"])){
			// THIS WILL ONLY INSERT THE DATA TO THE DATABASE IF DISSAPROVAL MODE IS FALSE
			$vq = "SELECT * FROM employees WHERE eid='" . $eid . "'";
			$vres = mysqli_query($c,$vq);
			$vrow = mysqli_fetch_array($vres);
	$q = "INSERT INTO applied_leave(employee_id,
	leave_type,
	date_from,
	date_to,
	sub_type,
	location,
	leave_taken,
	date_requested,
	time_requested,
	date_applied_totatus,
	paid_days,
	unpaid_days,
	fullname,
	status
	)
	VALUES
	('$eid',
	'$leavetype',
	'$date_from',
	'$date_to',
	'$subtype',
	'"  . mysqli_real_escape_string($c,
	 $location) . "',
	'$leave_take_value',
	'" . date("Y-m-d") . "',
	NOW(),
	'$date_applied_totatus',
	'$lwp',
	'$lnp',
	'" . $vrow["lname"] . ", "  . $vrow["fname"] . " " . $vrow["mname"] . "',
	'1')";
	$res = mysqli_query($c,$q);
log_system_action($eid . " submitted a Leave Report.");
}
	
	//Get form 6 id to database when proccess is finnished
	$q = "SELECT * FROM applied_leave ORDER BY id DESC LIMIT 1";
	$res = mysqli_query($c,$q);
	$curr_row = mysqli_fetch_array($res);

	include("theme/original.php");
	$leave_id = $curr_row["id"];

	include("php/form6_process.php");

	$qstb = "SELECT * FROM lams_configurations WHERE label='cn'";
	$resxm = mysqli_fetch_array(mysqli_query($c,$qstb));

	$scnamex = $resxm["value"];
	//Leave taken - current balance or leave type in entitlements	
	$q = "UPDATE applied_leave SET office_agency='" . $scnamex . "', date_of_filling='" . date("Y-m-d") . "', position='" . $f_postion . "', salary_monthly='" . $f_salary . "' WHERE id='" . $leave_id . "'";
	$res = mysqli_query($c,$q);

	// FREEZE LEAVE ENTITLEMENTS

	$qf = "SELECT * FROM freezed_balance WHERE leave_id='" . $leave_id . "' LIMIT 1";

	if(mysqli_num_rows(mysqli_query($c,$qf)) == "0"){
	$qf = "INSERT INTO freezed_balance SET leave_id='" . $leave_id . "',service_credit='" . $getsick . "',vacation_leave='" . $getvacation  . "'";
	$res = mysqli_query($c,$qf);
	}


?>
	<style type="text/css" media="all">
		body{
			font-family: Times Roman !important;
		}
		.indent_0x{
			margin-left: 15px;
		}
		.indent_1x{
			margin-left: 50px;
		}
		span{
			font-size: 19px;
		}
		input{
			font-size: 19px;
		}
		.indent_2x{
			margin-left: 100px;
		}
		.centerTextFixed{
			width: 95%;
			margin-left: 15px;
			margin-right: 15px;
			margin-bottom: 5px;
			text-align: center;
		}
		.theinput{
			border:none;
			border-bottom: 1px solid black;
		}
		.row{
			padding-top: 0px !important;
			margin-top: 0px !important;
		}
		.col-sm-6{
			padding-top: 0px !important;
			margin-top: 0px !important;
		}
		hr{
			padding: 0px !important;
			margin: 0px !important;
			border-top: 1px solid black;
		}
		.rightborder{
			border-right: 1px solid black !important;
		}
		tr{
			margin: 0 !important;
			padding: 4px !important;
			border-color: black !important;
		}
		td{
			margin: 0 !important;
			padding: 4px !important;
			border-color: black !important;
		}
		th{
			margin: 0 !important;
			padding: 4px !important;
			border-color: black !important;
		}
		textarea{
			border-color: black !important;
		}
		.lined{
			border: none;
			border-bottom: 1px solid black !important;
			width: 274px;
			text-align: center;
		}
		@media print{

			.rightborder{
			border-right: 1px solid black !important;
		}
			#tohide{
				display: none;
			}
			#nodeToRenderAsPDF{
				padding: 0px;
				padding-right: 0px !important;
				padding-left: 0px !important;
				margin:0px;
			}

		}
		.lbl_detail{
			font-weight: bold;
			/*text-decoration: underline;*/
			font-size: 19px;
			padding: 0px !important;
			margin: 0px !important;
		}
		.table{
			margin: 0px;
		}
		.indentbox{
			padding-left: 20px;
		}
	</style>
</head>
<body style="font-family: serif;">
	<div class="jumbotron jumbotron-fluid" id="tohide">
		<div class="container" >
			<h1 class="display-4">Form 6 Printing</h1>
			<a href="#" onclick="window.print()"><i class="fas fa-print"></i> Print</a>
		</div>
	</div>
	
	<div class="container" id="nodeToRenderAsPDF" style="padding-top: 20px; padding-bottom: 20px; padding-left: 75px; padding-right: 75px;">
		<center>
		<span>FORM 6</span>
	<h4>APPLICATION FOR LEAVE</h4>
	</center>
	<span>CSC Form No 6<br>
	Revised 1984</span>
	<table class="table table-bordered">
		<?php
			$qxa = "SELECT * FROM employees WHERE eid = '" . $eid . "' LIMIT 1";
			$resxa = mysqli_query($c,$qxa);
			$rowxa = mysqli_fetch_array($resxa);


		?>
		<tr>
			<td>
				<h6><span class="lbl_detail">1.</span> OFFICE / AGENCY<br>
				<strong  class="indent_0x"><?php echo $f_officeagency; ?></strong>
				</h6>
			</td>
			<td >
				<h6><span class="lbl_detail">2.</span> LAST<br>
				<strong class="indent_0x"><?php echo $rowxa["lname"]; ?></strong>
				</h6>
			</td>
			<td >
				<h6>FIRST<br>
				<strong class="indent_0x"><?php echo $rowxa["fname"]; ?></strong>
				</h6>
			</td>
			<td >
				<h6>MIDDLE<br>
				<strong class="indent_0x"><?php echo $rowxa["mname"]; ?></strong>
				</h6>
			</td>
		</tr>
		<tr>
			<td>
				<h6><span class="lbl_detail">3.</span> DATE OF FILING</br>
				<strong class="indent_0x"><?php echo date("F d, Y",strtotime($f_fillingdate));?></strong>
				</h6>
			</td>
			<td colspan="2">
				<h6><span class="lbl_detail">4.</span> POSITION</br>
				<strong class="indent_0x"><?php echo $f_postion; ?></strong>
				</h6>
			</td>
			<td>
				<h6><span class="lbl_detail">5.</span> SALARY MONTHLY</br>
				<strong class="indent_0x">PhP <?php echo number_format($f_salary,2);  ?></strong>
				</h6>
			</td>
		</tr>
	</table>
	<center>
		<!-- <span>DETAILS OF APPLICATION<br></span> -->
		<span class="lbl_detail">DETAILS OF APPLICATION</span>
	</center>
	<hr>
	<div class="row">
		<div class="col-sm-6 rightborder">
			
			<h6 class="indent_0x"><span class="lbl_detail">6.</span> a) TYPE OF LEAVE</h6>
			<div class="indentbox">
				<span class="indent_1x"><input id='id_vacationleave' type="checkbox" disabled="" name=""> Vacation<br></span>
			<label class="indent_2x"><input type="radio" disabled="" id="vac_leave_toseek" name="va_leave"> To Seek Employment</label class="indent_1x"><br>
			<label class="indent_2x"><input type="radio" disabled="" id="vac_leave_other" name="va_leave"> Others (Specify) 
				<input type="text" readonly class="lined" style="width: 100%;" name="va_otherspecifyreason" id="va_leave_other_reason"></label><br>
			<label class="indent_1x"><input id="id_sickleave" type="checkbox" disabled="" name=""> Sick</label><br>
			<label class="indent_1x"><input id="id_maternity" type="checkbox" disabled="" name=""> Maternity</label><br>
			<label class="indent_1x"><input id="id_others" type="checkbox" disabled="" name=""> Others (Specify) <input  type="text" readonly class="lined" name="" style="width: 100%;" id="id_leavetype"></label><br>
			<h6 class="indent_0x">c) NUMBER OF WORKING DAYS APPLIED FOR</h6>
			<input class="centerTextFixed theinput" id='nowd' type="text" readonly name="">
			<script type="text/javascript">
				$("#nowd").val(<?php echo $f_leavetaken; ?>);
			</script>
			<br>
			<h6 class="indent_0x">INCLUSIVE DATES</h6>

			<?php
				if($f_leavetaken == 1){
				?>
				<input class="centerTextFixed theinput" id="incdates" type="text" readonly name=""><br>
				<script type="text/javascript">
				$("#incdates").val(<?php echo json_encode($f_inclusivedates_single); ?>);
			</script>
				<?php
				}else{
				?>
				<input class="centerTextFixed theinput" id="incdates" type="text" readonly name=""><br>
				<script type="text/javascript">
				$("#incdates").val(<?php echo json_encode($f_inclusivedates); ?>);
			</script>
				<?php
				}
			?>
			
			</div>
			
			
			<br>
			<br>
		</div>
		 <div class="col-sm-6" style="padding-left: 20px;">
			<h6><span class="lbl_detail">6.</span> b) WHERE LEAVE WILL BE SPENT:</h6>

			<div class="indentbox">
				<span><span class="lbl_detail">1.</span> IN CASE OF VACATION LEAVE<br></span>
			<labe class="indent_0x"><input type="radio" disabled="" id="vac_1" name="vacl_ltype"> Within the Philippines</label><br>
			<label class="indent_0x"><input type="radio" disabled="" id="vac_2" name="vacl_ltype"> Abroad (Specify)</label>
			<input class="centerTextFixed theinput" id="vac_specify" type="text" readonly name="">
			<h6><span class="lbl_detail">2.</span> IN CASE OF SICK LEAVE</h6>
			<label class="indent_0x"><input id="id_sick_reason_inhospital" type="radio" disabled="" name=""> In Hospital</label>
			<input class="centerTextFixed theinput" id="id_sick_location_inhos" type="text" readonly name="">
			<label class="indent_0x"><input id="id_sick_reason_outpatient" type="radio" disabled="" name=""> Out Patient (Specify)</label>
			<input class="centerTextFixed theinput" id="id_sick_location_outpat" type="text" readonly name="">
			<br>
			<br>
			<h6 class="indent_0x"><span class="lbl_detail">d)</span> COMMUTATION</h6>
			<label class="indent_0x"><input type="radio" checked="checked" name="commutation" disabled=""> Requested <input disabled="" class="indent_0x" type="radio" disabled="" name="commutation"> Not Requested</label>
			<br>
			<br>
			<br>
			<center>
					<input style="width: 60%;" class="centerTextFixed theinput" type="text" readonly name="">
				<br>Signature of Applicant
			</center>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$ltype = <?php echo json_encode($theleavetype) ?>;

var vacationt_loc = <?php echo json_encode($f_vacationtype); ?>;
		if($ltype != "leave_without_pay"){

		if(vacationt_loc == "0"){
			$("#vac_1").prop("checked",true);
		}else if(vacationt_loc == "1"){
			$("#vac_2").prop("checked",true);
			$("#vac_specify").val(<?php echo json_encode($f_vac_abroad); ?>);
		}else{
			$("#vac_1").prop("checked",false);
			$("#vac_2").prop("checked",false);
		}
	}else{
		$("#vac_1").prop("checked",false);
			$("#vac_2").prop("checked",false);
	}
		
	</script>
	<hr>
	<center>
		<!-- <h6>DETAILS OF ACTION OF APPLICATION</h6> -->
		<span class="lbl_detail">DETAILS OF ACTION OF APPLICATION</span>
	</center>
	<hr>
<div class="row">
	<div class="col-sm-6 rightborder">
		<h6 class="indent_0x"><span class="lbl_detail">7.</span> a) CERTIFICATION OF LEAVE CREDITS as of</h6>
	<div class="indentbox">
			<input class="centerTextFixed theinput" type="text" readonly="" id="cer_lv_as_of" name="">
		<script type="text/javascript">
			$("#cer_lv_as_of").val(<?php echo json_encode(date('F d, Y',strtotime("-1 days"))); ?>);
		</script>
		<table class="table table-bordered" style="text-align: center;">
			<tr>
				<th>Vacation</th>
				<th>Sick</th>
				<th>Total</th>
			</tr>
			<tr>
				<td><?php
				if($getvacation == ""){
					echo "0";
				}else{
					echo $getvacation;
				}
				 ?></td>
				
				<td><?php 

				if($getsick == ""){
					echo "0";
				}else{
					echo $getsick;
				}
				 ?></td>
				<td><?php echo number_format($getvacation + $getsick,3); ?></td>
			</tr>
			<tr>
				<td>Days</td>
				<td>Days</td>
				<td>Days</td>
			</tr>
		</table>
		<span class="indent_0x">Leave Balance as of <?php echo date("F d, Y"); ?></span>
				<table class="table table-bordered" style="text-align: center;">
			<tr>
				<th>Vacation</th>
				<th>Sick</th>
				<th>Total</th>
			</tr>
			<?php
if($leavetype == "Vacation Leave"){
	//LEAVE TYPE - Vacation
	//LEAVE TYPE - Vacation
	//LEAVE TYPE - Vacation
?>
<tr>
<td>
	<?php
	if($f_leave_type != "LEAVE WITHOUT PAY"){
		$VacationEchoedValue = $getvacation - $leave_take_value;
	}else{
		$VacationEchoedValue = $getvacation;
	}
	if(strpos($VacationEchoedValue,"-") !== false){
	  	echo "0";
	}else{
	  	echo $VacationEchoedValue;
	}	
	?>
</td>
<td><?php 
$xval = 0;
if($f_leave_type != "LEAVE WITHOUT PAY"){
	if ($leavetype == "Sick Leave") {
		$xval = $getsick - $leave_take_value;
	}else{
		$xval = $getsick;
	}
		
	}else{
		$xval = $getsick;
	}

	if(strpos($xval,"-") !== false){
	  	echo "0";
	}else{
	  	echo $xval;
	}
 ?></td>


<td><?php 
$VacComputeValue = $VacationEchoedValue;

if($VacComputeValue < 0){
	$VacComputeValue = 0;
}

echo number_format($VacComputeValue + $xval,3);

?>
	
</td>


</td>
</tr>
			
<?php
//LEAVE TYPE - SICK WITH SECRVICE CREDIT VALICATION FOR TEACHER
//LEAVE TYPE - SICK WITH SECRVICE CREDIT VALICATION FOR TEACHER
//LEAVE TYPE - SICK WITH SECRVICE CREDIT VALICATION FOR TEACHER
}else if($leavetype == "Sick Leave" || $leavetype == "Service Credit" AND $usertype == "2"){

?>
<tr>
<td><?php 
if($getvacation == ""){
$getvacation=0;
echo "0";
}else{
echo $getvacation;
} ?></td>
<td>
	<?php
	$theval = 0;
	// if($leavetype == "Sick Leave"){
		$theval =  $getsick - $leave_take_value;
	// }
		  if(strpos($theval,"-") !== false){
		  		echo "0";
		  }	else{
		  	if($theval == ""){
		  		$theval = 0;
		  		echo "0";
		  	}else{
		  		echo $theval;
		  	}
		  }	
	?>
</td>
<td>
<?php 
$SickComputedLeave_SCVersion = $theval;
if($SickComputedLeave_SCVersion < 0){
	$SickComputedLeave_SCVersion = 0;
}
echo number_format($SickComputedLeave_SCVersion + $getvacation,3);
?></td>


</tr>
			
<?php
}else{
	//LEAVE TYPE - DEFAULT SICK LEAVE
	//LEAVE TYPE - DEFAULT SICK LEAVE
	//LEAVE TYPE - DEFAULT SICK LEAVE
?>

<tr>
<td><?php 
if($getvacation == ""){
	$getvacation = 0;
echo "0";
}else{
echo $getvacation;
} ?></td>
<td><?php 
$ltakeval = $leave_take_value;
if($f_leave_type != "LEAVE WITHOUT PAY"){
if($theleavetype == "leave_without_pay"){
	$ltakeval = 0;
}
}
$xval = 0;
if($f_leave_type != "LEAVE WITHOUT PAY"){
		if ($leavetype == "Sick Leave") {
		$xval = $getsick - $ltakeval;
	}else{
		$xval = $getsick;
	}
	}else{
		$xval = $getsick;
	}

	if(strpos($xval,"-") !== false){
	  	echo "0";
	  	$xval = 0;
	}else{
		if($xval == ""){
			$xval = 0;
			echo $xval;
		}else{
			echo $xval;
		}
	  	
	}
 ?></td>
<td><?php 

$SickComputedLeave = $xval;
if($SickComputedLeave < 0){
$SickComputedLeave = 0;
}
echo number_format($SickComputedLeave + $getvacation,3);
 ?></td>
</tr>

<?php
}
			?>
		</table>
	</div>
		<br>
			<center>
				<input style="width: 60%;" class="centerTextFixed theinput" type="text" readonly name="">
				<h6><strong><?php echo $hrmo; ?></strong><br>
				<span><?php echo $f_valtype_hr; ?></span></h6>
				
			</center>
	</div>
	<div class="col-sm-6">
		<h6><span class="lbl_detail">7.</span> b) RECOMMENDATION</h6>
			<div class="indentbox">
					<label class="indent_0x"><input type="checkbox" disabled="" name=""> Approval</label><br>
			<label class="indent_0x"><input type="checkbox" disabled="" name=""> Disapproval due to</label><br>
			<textarea readonly class="indent_0x" style="width: 100%;" rows="4">
				
			</textarea>
			</div>
			<br>
			<br>
			<br>
					
			<!-- <center> -->
<div style="position: absolute; bottom: 0; left: 0; right: 0; text-align: center;">
	<input style="width: 60%;" class="centerTextFixed theinput" type="text" readonly name="">
		<?php
	if($f_accounttype == 1){
		?>
		<!-- SCHOOL -->
	<h6><strong><?php echo $f_princname; ?></strong><br><span><?php echo $f_princpostion;?></span></h6>
	

		<?php
	}else{
		?>
		<!-- DIVISION -->
	<h6><strong><?php echo $administrativeofficer; ?></strong><br><span><?php echo $f_valtype_aoff;?></span></h6>
	

		<?php
	}
	?>
</div>
<!-- </center> -->
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<h5><span class="lbl_detail">7.</span> c) APPROVED FOR:</h5>
	<div class="indentbox">
			<label><?php echo '<input style="border:none; border-bottom: 1px solid black; text-align:center;" type="text" readonly="" name="" value="' . $lwp . '"> days with pay</label>'; ?>
		<label><?php echo '<input style="border:none; border-bottom: 1px solid black; text-align:center;" type="text" readonly="" name="" value="' . $lnp . '"> days without pay</label>'; ?>
		<label><input type="text" style="border:none; border-bottom: 1px solid black; text-align:center;" readonly="" name=""> other (Specify)</label>
	</div>
	</div>
	<div class="col-sm-6">
		<h5><span class="lbl_detail">7.</span> b) DISAPPROVED DUE TO:</h5>
		<div class="indentbox">
			<textarea id="duetodissaprovereason" readonly class="indent_0x" style="width: 100%;" rows="4"></textarea>
		</div>
	</div>
</div>
<br>
<br>
<br>
<center>
	<input style="width: 40%;" class="centerTextFixed theinput" type="text" readonly name="">
	<?php
	if($f_accounttype == 1){
		if(strpos(strtolower($f_princname),strtolower($rowxa["fname"])) !== false && strpos(strtolower($f_princname),strtolower($rowxa["lname"])) !== false){
		?>
<h5><strong><?php echo $superintendent; ?></strong><br><label><?php echo $f_valtype_superinten;?></label></h5>
		<?php 
		}else{
?>
<h5><strong><?php echo $administrativeofficer; ?></strong><br><label><?php echo $f_valtype_aoff;?></label></h5>
<?php
		}

		?>
		<!-- SCHOOL -->
		<?php
	}else{
		?>
		<!-- DIVISION -->
	<h5><strong><?php echo $superintendent; ?></strong><br><label><?php echo $f_valtype_superinten;?></label></h5>
	

		<?php
	}
	?>

</center>
	</div>
</body>
</html>


<script type="text/javascript">
	//The Leave Type Manager
	var leave_type = <?php echo json_encode($f_leave_type); ?>;
	switch(leave_type){
		case "Vacation Leave":
			$("#id_vacationleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "Others (Specify)":

					$("#vac_leave_other").prop("checked",true);
					$("#va_leave_other_reason").val(<?php echo json_encode($f_location); ?>);
				break;

			}
		break;
		case "Sick Leave":
			$("#id_sickleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "In Hospital":
					$("#id_sick_reason_inhospital").prop("checked",true);
					$("#id_sick_location_inhos").val(<?php echo json_encode($f_location); ?>);
				break;
				case "Out Patient":
					$("#id_sick_reason_outpatient").prop("checked",true);
					$("#id_sick_location_outpat").val(<?php echo json_encode($f_location); ?>);
				break;
			}
		break;
		case "Maternity Leave":
			$("#id_maternity").prop("checked",true);
		break;
		default:
			$("#id_others").prop("checked",true);
			$("#id_leavetype").val(leave_type);
		break;
	}

</script>

<?php

	if(isset($_POST["fordissaproval"])){
		echo "<script>
			$('#duetodissaprovereason').val('" . mysqli_real_escape_string($c,$_POST["reasonofdiss"]) . "');
		</script>";
	}

?>

<script type="text/javascript">
	function printx() {
		const filename  = 'ThisIsYourPDFFilename.pdf';
		html2canvas(document.querySelector('#nodeToRenderAsPDF')).then(canvas => {
			let pdf = new jsPDF('p', 'mm', 'a4');
			pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);
			pdf.save(filename);
		});
	}
	print_now();
	function print_now(quality = 2) {
		const filename  = 'LAMS_FORM_6_' + <?php echo json_encode($f_fullname . " - " .  date("Y-m-d (H i sa)")); ?> + '.pdf';

		html2canvas(document.querySelector('#nodeToRenderAsPDF'), 
								{scale: quality}
						 ).then(canvas => {
			let pdf = new jsPDF('p', 'mm', 'a4');
			pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);
			pdf.save(filename);
		});
	}
</script>
	<script src="js/jspdf.js"></script>
	<script src="js/jquery-2.1.3.js"></script>
	<script src="js/pdfFromHTML.js"></script>
	<?php
function log_system_action($message){
	global $c;
	$message = mysqli_real_escape_string($c, $message);
	$message = htmlentities($message);
	$q = "INSERT INTO logs(performer,action_description,timestamp) VALUES('Admin','" . $message . "','" . date("Y-m-d H:i:s") . "')";
	mysqli_query($c,$q);
}
	?>