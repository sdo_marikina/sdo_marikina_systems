<!DOCTYPE html>
<html>
<head>


<style type="text/css">
	.reasonbox input{
		padding:0px !important;
		margin:0px !important;
		font-size: 15px;
	}
	.reasonbox label{
		padding:0px !important;
		margin:0px !important;
		font-size: 15px;
	}
	.reasonbox textarea{
		padding:0px !important;
		margin:0px !important;
		font-size: 15px;
	}

	br{
		padding: 0px !important;
		margin: 0px !important;
	}
</style>

	<link rel="icon" href="favs/application-vnd.wordperfect.ico" type="text/css" href="">
	<title></title>
	<script type="text/javascript" src="api/qr_core/qrcode.js"></script>
	<script type="text/javascript" src="api/html2canvas/html2canvas.js"></script>
	<script type="text/javascript" src="api/html2canvas/html2canvas.min.js"></script>
	<?php
	include("connection/connect.php");
	// Add form 6 data to database

	$freezed_sickleave = 0;
	$freezed_vacationleave = 0;
	$isreprint = false;
	if(isset($_POST["btn_lr_reprint"])){
		$isreprint = true;
		// echo "reprinting";
	}
	$eid = "";

	if(isset($_POST["employee_id"])){
		$eid = $_POST["employee_id"];
	}else{
		$eid = $_POST["eid"];
	}
	
	$f_vacationtype = "";
	$f_vac_abroad = "";

	if(isset($_POST["vacationleave_location"])){
		$f_vacationtype = $_POST["vacationleave_location"];
	$f_vac_abroad = mysqli_real_escape_string($c,$_POST["f_vac_abroad"]);
	$f_vacationtype = $_POST["vacationleave_location"];
	}
	
	$f_vac_abroad = "";


	if(isset($_POST["f_vac_abroad"])){
		$f_vac_abroad = mysqli_real_escape_string($c,$_POST["f_vac_abroad"]);
	}

	$leavetype = "";
	if(isset( $_POST["leave_type_now"])){
		// FOR PARENTAL LEAVE
		$leavetype = $_POST["leave_type_now"];
	}else{
		$leavetype = $_POST["leavetype"];
	}

	$theleavetype = strtolower(str_replace(" ", "_",$leavetype));
	$f_leave_type = $leavetype;
	
	$date_from = $_POST["date_from"];
	$date_to   = $_POST["date_to"];

	$location_1 = "";
	$location_2 = "";
	if(isset($_POST["location_1"])){
		$location_1 = $_POST["location_1"];
	}
	if(isset($_POST["location_2"])){
		$location_2 = $_POST["location_2"];
	}

	$subtype = "";
	if(isset($_POST["leavetype_status"])){
		$subtype = $_POST["leavetype_status"];
	}

	//Leave taken - current balance or leave type in entitlements

	$leave_take_value = "";

	if(isset($_POST["leave_days"])){
		$leave_take_value = $_POST["leave_days"];
	}else if(isset($_POST["leave_taken_value"])){
		$leave_take_value = $_POST["leave_taken_value"];
	}


	$date_applied_totatus = date("Y-m-d");
	if (isset($_POST["date_applied_totatus"])) {
		$date_applied_totatus = $_POST["date_applied_totatus"];
	}
	$location = $location_1;
	if($location_1 == ""){
	$location = $location_2;
	}
	// echo$location;
		if(!isset($_POST["fordissaproval"]) && !isset($_POST["btn_lr_approve"]) && 	$isreprint == false){

		$status_fixed = "0";
		if (isset($_POST["leave_type_now"])) {
			// AUTO APPROVED IF PARENTAL
		$status_fixed=  "1";
		}

			// THIS WILL ONLY INSERT THE DATA TO THE DATABASE IF DISSAPROVAL MODE IS FALSE
	$q = "INSERT INTO applied_leave(
	employee_id,
	leave_type,
	date_from,
	date_to,
	sub_type,
	location,
	leave_taken,
	date_requested,
	time_requested,
	date_applied_totatus,
	status
	)
	VALUES(
	'$eid',
	'$leavetype',
	'$date_from',
	'$date_to',
	'$subtype',
	'"  . mysqli_real_escape_string($c, $location) . "',
	'$leave_take_value',
	'" . date("Y-m-d") . "',
	NOW(),
	'$date_applied_totatus',
	'$status_fixed')";
	$res = mysqli_query($c,$q);

}

		$form6_id = "";
	if (isset($_POST["leave_type_now"])) {

		$qx_parental = "SELECT * FROM applied_leave WHERE employee_id='" . $eid . "' AND leave_type='" . $leavetype . "' AND date_from='" . $date_from . "' AND date_to='" . $date_to . "' AND sub_type='" . $subtype . "' ORDER BY id DESC LIMIT 1";
		$res_parental = mysqli_query($c,$qx_parental);
		$row_parental = mysqli_fetch_array($res_parental);
		$form6_id = $row_parental["id"];

	}else{
		$form6_id = $_POST["id"];
	}
	//Get form 6 id to database when proccess is finnished
	// $q = "SELECT * FROM applied_leave ORDER BY id DESC LIMIT 1";
	$q = "SELECT * FROM applied_leave WHERE id='" .$form6_id . "'";
	$res = mysqli_query($c,$q);
	$curr_row = mysqli_fetch_array($res);

	include("theme/original.php");
	$leave_id = $curr_row["id"];
	include("php/form6_process.php");
	//Leave taken - current balance or leave type in entitlements
	$lwp = 0;
	$lnp = 0;
	// $getvacation
	// $getsick
	switch ($leavetype) {
		case 'Vacation Leave':
		compute_paidDays($getvacation);
		break;
		case 'Sick Leave':
		compute_paidDays($getsick);
		break;
		default:
		if($usertype != "2"){
			//Non Teaching and division
			switch($leavetype){
				case "Service Credit":
					compute_paidDays($getservicecredit);
				break;
				case "Special Leave":
					compute_paidDays($get_specialLeave);
				break;
				case "Paternity":

				break;
				case "CTO":
					compute_paidDays($get_cto);
				break;
			}
		}else{
					compute_paidDays($getsick);
		}
		break;
	}

	$lwp = str_replace("-", "", $lwp);
	$lnp = str_replace("-", "", $lnp);

	function compute_paidDays($balance){
		global $lwp;
		global $lnp;
		global $leave_take_value;
		$leavetake = $leave_take_value;

		if($leavetake < $balance){
			$lwp =  intval($leavetake) % intval($balance);
			$lnp =  fmod(intval($balance),intval($leavetake));
		}else{

			$lwp = intval($balance) %  intval($leavetake);
			if(intval($leavetake) < intval($balance)){
				$lnp = intval($leavetake)  % intval($balance);
			}else{
				$lnp = intval($balance) % intval($leavetake);
			}

		}
		sameval($balance,$leavetake);
	}

	function sameval($c1,$c2){
		global $lnp;
		global $lwp;
		if(intval($c1) > intval($c2)){
			$lnp = "0";
		}else if(intval($c1) == intval($c2)){
			$lwp = intval($c1); 
		}
	}
	if(isset($_POST["fordissaproval"])){
		// THIS WILL ONLY WORK IN DISSAPROVAL MODE
		$f_fullname = $_POST["dis_fullname"];
		$f_fillingdate = $_POST["dis_filling_date"];
		$f_officeagency = $_POST["dis_office_agency"];
		$f_postion = $_POST["dis_position"];
		$f_salary = $_POST["dis_salary_monthly"];

		$lwp = $_POST["dis_lwp"];
		$lnp = $_POST["dis_lnp"];
		if($lwp == "" || $lwp == null){
			$lwp = "0";
		}
		if($lwp == 0){
			$lnp = $lwp;
		}else{
			$lnp += $lwp;	
		}
		
		$lwp = "0";

		$f_vac_abroad = $_POST["f_vac_abroad"];
		$f_subtype = $_POST["dis_vacation_type"];
		$f_vacationtype = "";
		$f_location = "";
		$leave_id = $_POST["eid"];
		$f_leave_type = "LEAVE WITHOUT PAY";



		$id = $_POST["id"];
		$reasonofdiss = mysqli_real_escape_string($c,$_POST["reasonofdiss"]);
		$q = "UPDATE applied_leave SET status='2',reason_of_dissaprove='$reasonofdiss' WHERE id='$id'";
		$res = mysqli_query($c,$q);


		$qf = "SELECT * FROM freezed_balance WHERE leave_id='" . $id . "' LIMIT 1";

if(mysqli_num_rows(mysqli_query($c,$qf)) == "0"  && $isreprint == false){
	$qf = "INSERT INTO freezed_balance SET leave_id='" . $id . "',service_credit='" . $getsick . "',vacation_leave='" . $getvacation  . "'";
	$res = mysqli_query($c,$qf);
}

		// BackToPage($res,"LeaveReports.php");
	}elseif(isset($_POST["btn_lr_reprint"])){
		$isreprint = true;
		// THIS WILL ONLY WORK IN RE PRINTING MODE
		$f_fullname = $_POST["dis_fullname"];
		// echo $f_fullname;
		$f_fillingdate = $_POST["dis_filling_date"];
		$f_officeagency = $_POST["dis_office_agency"];
		$f_postion = $_POST["dis_position"];
		$f_salary = $_POST["dis_salary_monthly"];

		$lwp = $_POST["dis_lwp"];
		$lnp = $_POST["dis_lnp"];

		$f_vac_abroad = $_POST["f_vac_abroad"];
		$f_subtype = $_POST["dis_vacation_type"];
		$f_vacationtype = $_POST["dis_vacation_leaveincase"];
		
		if($leavetype  == "Vacation Leave"){
	$f_location = $_POST["dis_dis_sick_incase_reason"];
		}else{
	$f_location = $_POST["dis_dis_sick_incase_reason"];
		}
	// echo "POSTED EID = " . $_POST["eid"];
		$leave_id = $_POST["eid"];
		$f_leave_type =$leavetype ;


$id = $_POST["id"];
// echo "THE ID = " . $id;

$q = "SELECT * FROM  applied_leave WHERE id='$id'";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);




	// GET FREEZED LEAVE SICK AND VACATION LEAVE 
	$flx = "SELECT * FROM freezed_balance WHERE leave_id='" . $id . "' LIMIT 1";
	$flx_res = mysqli_query($c,$flx);
	$flx_row = mysqli_fetch_array($flx_res);

	$getvacation = (float)$flx_row["vacation_leave"];
	$getsick = (float)$flx_row["service_credit"];

		// echo "For reprinting....";
	}else if(isset($_POST["btn_lr_approve"])){

	
				// THIS WILL ONLY WORK IN APPROVAL MODE
		$f_fullname = $_POST["dis_fullname"];
		$f_fillingdate = $_POST["dis_filling_date"];
		$f_officeagency = $_POST["dis_office_agency"];
		$f_postion = $_POST["dis_position"];
		$f_salary = $_POST["dis_salary_monthly"];

		$lwp = $_POST["dis_lwp"];
		$lnp = $_POST["dis_lnp"];

		$f_vac_abroad = $_POST["f_vac_abroad"];
		$f_subtype = $_POST["dis_vacation_type"];
		$f_vacationtype = $_POST["dis_vacation_leaveincase"];
		
		if($leavetype  == "Vacation Leave"){
	$f_location = $_POST["dis_dis_sick_incase_reason"];
		}else{
	$f_location = $_POST["dis_dis_sick_incase_reason"];
		}
	// echo "POSTED EID = " . $_POST["eid"];
		$leave_id = $_POST["eid"];
		$f_leave_type =$leavetype ;


$id = $_POST["id"];
// echo "THE ID = " . $id;
	$checkofstat = "SELECT * FROM applied_leave WHERE id='$id' AND status='1'";
		if (mysqli_num_rows(mysqli_query($c,$checkofstat)) != "0") {
			header("location:index.php");
		}



$q = "SELECT * FROM  applied_leave WHERE id='$id'";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);
$theleavetype = strtolower(str_replace(" ", "_", $row["leave_type"]));
// echo "THE ID = " . $row["employee_id"];
$q  = "";
// echo $theleavetype;
	if($theleavetype == "cto"){
		// echo "yes";
	// $q = "SELECT * FROM entitlements WHERE emp_id='" . $row["employee_id"] . "' AND $theleavetype <> 0";
	$q = "SELECT * FROM entitlements_cto WHERE empid='" .  $row["employee_id"] . "' ORDER BY val_to ASC LIMIT 1";
}else{
	$q = "SELECT * FROM entitlements WHERE emp_id='" . $row["employee_id"] . "' AND $theleavetype <> 0";
}


$newval = 0;
$entvalue = 0;
$emp_id = $row["employee_id"];
$leavetaken = $row["leave_taken"];

if($theleavetype == "leave_without_pay"){
$entvalue = 0;
}else{
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);

if($theleavetype == "cto"){
	$entvalue = $row["entitlement"];
}else{
	$entvalue = $row[$theleavetype];
}

}

	$newval = $entvalue - $leavetaken;


if(strpos($newval, "-") !== false){
$newval = 0;
}

if($theleavetype == "cto"){
	// SEPARATED TABLE TO UPDATE CTO
	if($newval <= 0 || $newval == 0){
		// DELETES THE ENTITLEMENT IF ZERO
		$ent_q = "DELETE FROM entitlements_cto WHERE empid='" . $emp_id . "' ORDER BY val_to ASC LIMIT 1";
		$ent_res = mysqli_query($c,$ent_q);
	}else{
		$ent_q = "UPDATE entitlements_cto SET entitlement='" . $newval  . "' WHERE empid='" . $emp_id . "' ORDER BY val_to ASC LIMIT 1";
		$ent_res = mysqli_query($c,$ent_q);
	}
	
}else{
	if($theleavetype != "leave_without_pay"){
		// DEFAULT TABLE TO UPDATE THE REST OF LEAVE TYPES
		$q = "UPDATE entitlements SET $theleavetype = $newval  WHERE emp_id='" . $emp_id . "' AND $theleavetype <> 0";
		$res = mysqli_query($c,$q);


	}
}

		$res_statingdate = date("Y-m-d",$ff);
		$res_endingdate = date("Y-m-d",$tt);

	// Start date is declared
	$date = date('Y-m-d', strtotime($res_statingdate));
	// End date is always the current date
	$end_date = date("Y-m-d",strtotime($res_endingdate));
	$cc = 0;
	while (strtotime($date) <= strtotime($end_date)) {


		if($theleavetype == "forced_leave" || $theleavetype == "vacation_leave" || $theleavetype == "cto"){
			$val_qselect = "SELECT * FROM scheduled_forcedleave WHERE status='0' AND date='" . $date . "' AND applied_leave_id='" . $id . "' AND eid='" . $emp_id . "'";
			$val_re_sel = mysqli_query($c, $val_qselect);
			if(mysqli_num_rows($val_re_sel) == 0 && $isreprint == false){
			$qqueryForForcedLeave = "INSERT INTO scheduled_forcedleave SET date='" . $date . "', applied_leave_id='" . $id . "', status='0', eid='" . $emp_id . "'";
			mysqli_query($c,$qqueryForForcedLeave);
			}
			
		}
		$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	}
// echo "IDDDD = " . $leave_id;

$q = "UPDATE applied_leave SET status='1' WHERE id='$id'";
$res = mysqli_query($c,$q);


// FREEZE LEAVE ENTITLEMENTS

$qf = "SELECT * FROM freezed_balance WHERE leave_id='" . $id . "' LIMIT 1";

if(mysqli_num_rows(mysqli_query($c,$qf)) == "0"  && $isreprint == false){
	$qf = "INSERT INTO freezed_balance SET leave_id='" . $id . "',service_credit='" . $getsick . "',vacation_leave='" . $getvacation  . "'";
	$res = mysqli_query($c,$qf);
}

  } else{

			//Additional prepared information update
	$q = "UPDATE applied_leave SET fullname='$f_fullname', date_of_filling='$f_fillingdate',office_agency='$f_officeagency', position='$f_postion', salary_monthly='$f_salary' , paid_days='$lwp', unpaid_days ='$lnp', vac_leave_abroad_specify='$f_vac_abroad',vacation_type='$f_subtype', vacation_leaveincase='$f_vacationtype', sick_incase='$f_subtype', sick_incase_reason='$f_location' WHERE id='$leave_id'";
	mysqli_query($c,$q);
	}
	?>
	<style type="text/css">
		body{
			font-family: Times Roman !important;
			margin: 0px !important;
		}
		label{
			font-size: 23px;
			padding: 0px;
			margin: 0px;
		}
		span{
			font-size: 23px;
			padding: 0px;
			margin: 0px;
		}
		input{
						padding: 0px;
			margin: 0px;
			font-size: 22px;
		}
		br{
					padding: 0px;
			margin: 0px;
		}
		.indent_0x{
			margin-left: 15px;
		}
		.indent_1x{
			margin-left: 50px;
		}
		.indent_2x{
			margin-left: 100px;
		}
		.centerTextFixed{
			width: 95%;
			margin-left: 15px;
			margin-right: 15px;
			margin-bottom: 5px;
			text-align: center;
		}
		.theinput{
			border:none;
			border-bottom: 1px solid black;
		}
		.row{
			padding-top: 0px !important;
			margin-top: 0px !important;
		}
		.col-sm-6{
			padding-top: 0px !important;
			margin-top: 0px !important;
		}
		hr{
			padding: 0px !important;
			margin: 0px !important;
			border-top: 1px solid black;
		}
		.rightborder{
			border-right: 1px solid black;
		}
		tr{
			margin: 0 !important;
			padding: 0px !important;
			border-color: black !important;
				font-size: 21px;
		}
		td{
			margin: 0 !important;
			padding: 0px !important;
			border-color: black !important;
				font-size: 21px;
		}
		th{
			margin: 0 !important;
			padding: 0px !important;
			border-color: black !important;
				font-size: 21px;
		}
		textarea{
			border-color: black !important;
		}
		h6{
			padding: 0px !important;
			margin: 0px !important;
		}
		.lined{
			border: none;
			border-bottom: 1px solid black;
			width: 274px;
			text-align: center;
		}
		@media print{
			#tohide{
				display: none;
			}
			#nodeToRenderAsPDF{
				padding: 0px;
				padding-right: 0px !important;
				padding-left: 0px !important;
				margin:0px;
			}
		}
		.lbl_detail{
			font-weight: bold;
			/*text-decoration: underline;*/
			font-size: 19px;
			padding: 0px !important;
			margin: 0px !important;
		}
		.table{
			margin: 0px;
		}
		.indentbox{
			padding-left: 22px;
		}

		.loading_indicator{
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	position: absolute;
	height: 100%;
	width: 100%;
	z-index: 100;
	background-color: white;
	background: url(images/loading.gif);
	background-position: center;
	background-repeat: no-repeat;
	background-size: 150px auto;
	background-color: white;
	animation-name: poptop;
	animation-duration: 0.5s;
	display: none;
}


	</style>
</head>
<body style="font-family: times new roman;">
	<div class="loading_indicator" id="log_cocoa_form6" style="display: block;">
        
      </div>
	<div class="jumbotron jumbotron-fluid" id="tohide">
		<div class="container" >
			<h1 class="display-4">Form 6 Printing</h1>
			<?php
	if(isset($_POST["fordissaproval"]) || isset($_POST["btn_lr_approve"]) || isset($_POST["btn_lr_reprint"])){

		//DOUBLE CHECK
if(isset($_POST["btn_lr_approve"])){
//APPROVE
	$q = "UPDATE applied_leave SET status='1' WHERE id='" . $id . "'";
	$res = mysqli_query($c,$q);
	if($res){
		echo '<a href="LeaveReports.php" style="margin-right: 10px;"><i class="fas fa-arrow-circle-left"></i> Back</a>';
	}
}else if(isset($_POST["fordissaproval"])){
//DISSAPROVE
	$q = "UPDATE applied_leave SET status='2' WHERE id='" . $id . "'";
	$res = mysqli_query($c,$q);
	if($res){
		echo '<a href="LeaveReports.php" style="margin-right: 10px;"><i class="fas fa-arrow-circle-left"></i> Back</a>';
	}
}else{
	echo '<a href="LeaveReports.php" style="margin-right: 10px;"><i class="fas fa-arrow-circle-left"></i> Back</a>';
}


	}
			?>
			<a href="#" onclick="window.print()"><i class="fas fa-print"></i> Print</a>
		</div>
	</div>
	<div class="container" id="nodeToRenderAsPDF" style="padding-top: 20px; padding-bottom: 20px; padding-left: 75px; padding-right: 75px;">
		<center>
	<h4><strong>APPLICATION FOR LEAVE</strong></h4>
	</center>
	<small>CSC Form No 6<br>
	Revised 1984</small>
	<table class="table table-bordered">

		<?php
			$qxa = "SELECT * FROM employees WHERE eid = '" . $eid . "' LIMIT 1";
			$resxa = mysqli_query($c,$qxa);
			$rowxa = mysqli_fetch_array($resxa);
		?>
		<tr>
			<td>
				<h5><small><span class="lbl_detail">1.</span> OFFICE / AGENCY</small><br>
				<strong  class="indent_0x"><?php echo $f_officeagency; ?></strong>
				</h5>
			</td>
			<td style="border-right: 1px solid transparent !important;">
				<h5><small><span class="lbl_detail">2.</span>NAME (LAST)</small><br>
				<strong class="indent_0x"><?php echo $rowxa["lname"]; ?></strong>
				</h5>
			</td>
			<td style="border-right: 1px solid transparent !important;">
				<h5><small>(FIRST)</small><br>
				<strong class="indent_0x"><?php echo $rowxa["fname"]; ?></strong>
				</h5>
			</td>
			<td >
				<h5><small>(MIDDLE)</small><br>
				<strong class="indent_0x"><?php echo $rowxa["mname"]; ?></strong>
				</h5>
			</td>
		</tr>
		<tr>
			<td>
				<h5><small><span class="lbl_detail">3.</span> DATE OF FILING</small></br>
				<strong class="indent_0x"><?php echo date("F d, Y",strtotime($f_fillingdate));?></strong>
				</h5>
			</td>
			<td colspan="2">
				<h5><small><span class="lbl_detail">4.</span> POSITION</small></br>
				<strong class="indent_0x"><?php echo $f_postion; ?></strong>
				</h5>
			</td>
			<td>
				<h5><small><span class="lbl_detail">5.</span> SALARY MONTHLY</small></br>
				<strong class="indent_0x">PhP <?php echo number_format($f_salary,2);  ?></strong>
				</h5>
			</td>
		</tr>
	</table>
	<center>
		<!-- <p>DETAILS OF APPLICATION</p> -->
		<span class="lbl_detail">DETAILS OF APPLICATION</span>
	</center>
	<hr>
	<div class="row">
		<div class="col-sm-6 rightborder">
			
			<h5 class="indent_0x"><span class="lbl_detail">6.</span> a) TYPE OF LEAVE</h5>
			<div class="indentbox">
				<label class="indent_1x"><input id='id_vacationleave' type="checkbox" disabled="" name=""> Vacation</label><br>
			<label class="indent_2x"><input type="radio" disabled="" id="vac_leave_toseek" name="va_leave"> To Seek Employment</label class="indent_1x"><br>
			<label class="indent_2x"><input type="radio" disabled="" id="vac_leave_other" name="va_leave"> Others (Specify) 
				<input type="text" readonly class="lined" style="width: 100%;" name="va_otherspecifyreason" id="va_leave_other_reason"></label><br>
			<label class="indent_1x"><input id="id_sickleave" type="checkbox" disabled="" name=""> Sick</label><br>
			<label class="indent_1x"><input id="id_maternity" type="checkbox" disabled="" name=""> Maternity</label><br>
			<label class="indent_1x"><input id="id_others" type="checkbox" disabled="" name=""> Others (Specify) <input  type="text" readonly class="lined" name="" style="width: 100%;" id="id_leavetype"></label><br>
			<h5 class="indent_0x">c) NUMBER OF WORKING DAYS APPLIED FOR</h5>
			<input class="centerTextFixed theinput" id='nowd' type="text" readonly name="">
			<script type="text/javascript">
				$("#nowd").val(<?php echo $f_leavetaken; ?>);
			</script>
			<br>
			<h5 class="indent_0x">INCLUSIVE DATES</h5>

			<?php
				if($f_leavetaken == 1){
				?>
				<input class="centerTextFixed theinput" id="incdates" type="text" readonly name=""><br>
				<script type="text/javascript">
				$("#incdates").val(<?php echo json_encode($f_inclusivedates_single); ?>);
			</script>
				<?php
				}else{
				?>
				<input class="centerTextFixed theinput" id="incdates" type="text" readonly name=""><br>
				<script type="text/javascript">
				$("#incdates").val(<?php echo json_encode($f_inclusivedates); ?>);
			</script>
				<?php
				}
			?>
			
			</div>
			
			
			<br>
			<br>
		</div>
		 <div class="col-sm-6" style="padding-left: 20px;">
			<h5><span class="lbl_detail">6.</span> b) WHERE LEAVE WILL BE SPENT:</h5>

			<div class="indentbox">
				<h5><span class="lbl_detail">1.</span> IN CASE OF VACATION LEAVE</h5>
			<label class="indent_0x"><input type="radio" disabled="" id="vac_1" name="vacl_ltype"> Within the Philippines</label><br>
			<label class="indent_0x"><input type="radio" disabled="" id="vac_2" name="vacl_ltype"> Abroad (Specify)</label>
			<input class="centerTextFixed theinput" id="vac_specify" type="text" readonly name="">
			<h5><span class="lbl_detail">2.</span> IN CASE OF SICK LEAVE</h5>
			<label class="indent_0x"><input id="id_sick_reason_inhospital" type="radio" disabled="" name=""> In Hospital</label>
			<input class="centerTextFixed theinput" id="id_sick_location_inhos" type="text" readonly name="">
			<label class="indent_0x"><input id="id_sick_reason_outpatient" type="radio" disabled="" name=""> Out Patient (Specify)</label>
			<input class="centerTextFixed theinput" id="id_sick_location_outpat" type="text" readonly name="">
			<br>

			<h5 class="indent_0x"><span class="lbl_detail">d)</span> COMMUTATION</h5>
			<label class="indent_0x"><input type="radio" checked="checked" name="commutation" disabled=""> Requested <input disabled="" class="indent_0x" type="radio" disabled="" name="commutation"> Not Requested</label>
			<br>
			<br>

			<center>
					<input style="width: 60%;" class="centerTextFixed theinput" type="text" readonly name="">
				<br><small>Signature of Applicant</small>
			</center>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$ltype = <?php echo json_encode($theleavetype); ?>;

var vacationt_loc = <?php echo json_encode($f_vacationtype); ?>;
		if($ltype != "leave_without_pay"){
// alert(vacationt_loc);
		if(vacationt_loc == "0"){
			$("#vac_1").prop("checked",true);
		}else if(vacationt_loc == "1"){
			$("#vac_2").prop("checked",true);
			$("#vac_specify").val(<?php echo json_encode($f_vac_abroad); ?>);
		}else{
			$("#vac_1").prop("checked",false);
			$("#vac_2").prop("checked",false);
		}
	}else{
		$("#vac_1").prop("checked",false);
			$("#vac_2").prop("checked",false);
	}
		
	</script>
	<hr>
	<center>
		<!-- <h6>DETAILS OF ACTION OF APPLICATION</h6> -->
		<span class="lbl_detail">DETAILS OF ACTION OF APPLICATION</span>
	</center>
	<hr>
<div class="row">
	<div class="col-sm-6 rightborder">
		<h5 class="indent_0x"><span class="lbl_detail">7.</span> a) CERTIFICATION OF LEAVE CREDITS as of</h5>
	<div class="indentbox">
			<input class="centerTextFixed theinput" type="text" readonly="" id="cer_lv_as_of" name="">
		<script type="text/javascript">
			$("#cer_lv_as_of").val(<?php echo json_encode(date('F d, Y',strtotime("-1 days"))); ?>);
		</script>
		<table class="table table-bordered" style="text-align: center;">
			<tr>
				<th><span>Vacation</span></th>
				<th><span>Sick</span></th>
				<th><span>Total</span></th>
			</tr>
			<tr>
				<td><?php
				if($getvacation == ""){
					echo "0";
				}else{
					echo $getvacation;
				}
				 ?></td>
				
				<td><?php 

				if($getsick == ""){
					echo "0";
				}else{
					echo $getsick;
				}
				 ?></td>
				<td><?php echo number_format($getvacation + $getsick,3); ?></td>
			</tr>
			<tr>
				<td><span>Days</span></td>
				<td><span>Days</span></td>
				<td><span>Days</span></td>
			</tr>
		</table>
		<small class="indent_0x">Leave Balance as of <?php echo date("F d, Y"); ?></small>
				<table class="table table-bordered" style="text-align: center;">
			<tr>
				<th><span>Vacation</span></th>
				<th><span>Sick</span></th>
				<th><span>Total</span></th>
			</tr>
			<?php
if($leavetype == "Vacation Leave"){
	//LEAVE TYPE - Vacation
	//LEAVE TYPE - Vacation
	//LEAVE TYPE - Vacation
?>
<tr>
<td>
	<?php
	if($f_leave_type != "LEAVE WITHOUT PAY"){
		$VacationEchoedValue = $getvacation - $leave_take_value;
	}else{
		$VacationEchoedValue = $getvacation;
	}
	if(strpos($VacationEchoedValue,"-") !== false){
	  	echo "0";
	}else{
	  	echo $VacationEchoedValue;
	}	
	?>
</td>
<td><?php 
$xval = 0;
if($f_leave_type != "LEAVE WITHOUT PAY"){
	if ($leavetype == "Sick Leave") {
		$xval = $getsick - $leave_take_value;
	}else{
		$xval = $getsick;
	}
		
	}else{
		$xval = $getsick;
	}

	if(strpos($xval,"-") !== false){
	  	echo "0";
	}else{
	  	echo $xval;
	}
 ?></td>


<td><?php 
$VacComputeValue = $VacationEchoedValue;

if($VacComputeValue < 0){
	$VacComputeValue = 0;
}

echo number_format($VacComputeValue + $xval,3);

?>
	
</td>
</td>
</tr>
<?php
//LEAVE TYPE - SICK WITH SECRVICE CREDIT VALICATION FOR TEACHER
//LEAVE TYPE - SICK WITH SECRVICE CREDIT VALICATION FOR TEACHER
//LEAVE TYPE - SICK WITH SECRVICE CREDIT VALICATION FOR TEACHER
}else if($leavetype == "Sick Leave" || $leavetype == "Service Credit" AND $usertype == "2"){

?>
<tr>
<td><?php 
if($getvacation == ""){
$getvacation=0;
echo "0";
}else{
echo $getvacation;
} ?></td>
<td>
	<?php
	$theval = 0;
	// if($leavetype == "Sick Leave"){
		$theval =  $getsick - $leave_take_value;
	// }
		  if(strpos($theval,"-") !== false){
		  		echo "0";
		  }	else{
		  	if($theval == ""){
		  		$theval = 0;
		  		echo "0";
		  	}else{
		  		echo $theval;
		  	}
		  }	
	?>
</td>
<td>
<?php 
$SickComputedLeave_SCVersion = $theval;
if($SickComputedLeave_SCVersion < 0){
	$SickComputedLeave_SCVersion = 0;
}
echo number_format($SickComputedLeave_SCVersion + $getvacation,3);
?></td>


</tr>
			
<?php
}else{
	//LEAVE TYPE - DEFAULT SICK LEAVE
	//LEAVE TYPE - DEFAULT SICK LEAVE
	//LEAVE TYPE - DEFAULT SICK LEAVE
?>

<tr>
<td><?php 
if($getvacation == ""){
	$getvacation = 0;
echo "0";
}else{
echo $getvacation;
} ?></td>
<td><?php 
$ltakeval = $leave_take_value;
if($f_leave_type != "LEAVE WITHOUT PAY"){
if($theleavetype == "leave_without_pay"){
	$ltakeval = 0;
}
}
$xval = 0;
if($f_leave_type != "LEAVE WITHOUT PAY"){
		if ($leavetype == "Sick Leave") {
		$xval = $getsick - $ltakeval;
	}else{
		$xval = $getsick;
	}
	}else{
		$xval = $getsick;
	}

	if(strpos($xval,"-") !== false){
	  	echo "0";
	  	$xval = 0;
	}else{
		if($xval == ""){
			$xval = 0;
			echo $xval;
		}else{
			echo $xval;
		}
	  	
	}
 ?></td>
<td><?php 

$SickComputedLeave = $xval;
if($SickComputedLeave < 0){
$SickComputedLeave = 0;
}
echo number_format($SickComputedLeave + $getvacation,3);
 ?></td>
</tr>

<?php
}
			?>
		</table>
	</div>
		<br>
			<center>
				<input style="width: 60%;" class="centerTextFixed theinput" type="text" readonly name="">
				<h5><strong><?php echo $hrmo; ?></strong><br>
				<small><?php echo $f_valtype_hr; ?></small></h5>
				
			</center>
	</div>
	<div class="col-sm-6">
		<h5><span class="lbl_detail">7.</span> b) RECOMMENDATION</h5>
			<div class="indentbox">
					<label class="indent_0x"><input type="checkbox" disabled="" name=""> Approval</label><br>
			<label class="indent_0x"><input type="checkbox" disabled="" name=""> Disapproval due to</label><br>
			<textarea readonly class="indent_0x" style="width: 100%;" rows="4">
				
			</textarea>
			</div>
			<br>
			<br>
			<br>
					
			<!-- <center> -->
<div style="position: absolute; bottom: 0; left: 0; right: 0; text-align: center;">
	<input style="width: 60%;" class="centerTextFixed theinput" type="text" readonly name="">
		<?php
	if($f_accounttype == 1){

		$lowfname = strtolower($rowxa["fname"]);
		$lowllname = strtolower($rowxa["lname"]); 
		$lowpriname = strtolower($f_princname);
		if(strpos(strtolower(	$lowpriname), $lowfname) !== false && strpos(	$lowpriname, $lowllname) !== false){
?>

<h5><strong><?php echo $administrativeofficer; ?></strong><br><small><?php echo $f_valtype_aoff;?></small></h5>
<?php
		}else{


		
		?>
		<!-- SCHOOL -->



	
	<h5><strong><?php echo $f_princname; ?></strong><br><small><?php echo $f_princpostion;?></small></h5>

		<?php
		}
	}else{
		?>
		<!-- DIVISION -->
	<h5><strong><?php echo $administrativeofficer; ?></strong><br><small><?php echo $f_valtype_aoff;?></small></h5>
	

		<?php
	}
	?>
</div>
<!-- </center> -->
	</div>
</div>
<hr>
<style type="text/css">
	.myqr_placer{
		position: absolute;
		display: block;
		width: 90px;
		height: 90px;
		background-color: black;
		margin-top: 60px;
	}
</style>
<div class="row">
	<div class="col-sm-6">
		<h5><span class="lbl_detail">7.</span> c) APPROVED FOR:</h5>
	<div class="indentbox reasonbox">
	
				<label><?php echo '<input style="border:none; border-bottom: 1px solid black; text-align:center;" type="text" readonly="" name="" value="' . $lwp . '"> days with pay</label>'; ?>
		<label><?php echo '<input style="border:none; border-bottom: 1px solid black; text-align:center;" type="text" readonly="" name="" value="' . $lnp . '"> days without pay</label>'; ?>
		<label><input type="text" style="border:none; border-bottom: 1px solid black; text-align:center;" readonly="" name=""> other (Specify)</label>
		<div id="qrcode" class="myqr_placer">

	</div>	
	</div>
	
	</div>
	<div class="col-sm-6">
		<h5><span class="lbl_detail">7.</span> b) DISAPPROVED DUE TO:</h5>
		<div class="indentbox reasonbox">
			<textarea id="duetodissaprovereason" readonly class="indent_0x" style="width: 100%;" rows="3"></textarea>
		</div>
	</div>
</div>
<br>


<center>
	<input style="width: 40%;" class="centerTextFixed theinput" type="text" readonly name="">
	<?php
	if($f_accounttype == 1){
		if(strpos(strtolower($f_princname),strtolower($rowxa["fname"])) !== false && strpos(strtolower($f_princname),strtolower($rowxa["lname"])) !== false){
		?>
<h5><strong><?php echo $superintendent_assistant; ?></strong><br><small><?php echo $superintendent_assistant_valtype;?></small></h5>
		<?php 
		}else{
?>
<h5><strong><?php echo $administrativeofficer; ?></strong><br><small><?php echo $f_valtype_aoff;?></small></h5>
<?php
		}
		?>
		<!-- SCHOOL -->
		<?php
	}else{
		?>
		<!-- DIVISION -->
	<h5><strong><?php echo $superintendent_assistant; ?></strong><br><small><?php echo $superintendent_assistant_valtype;?></small></h5>
		<?php
	}
	?>

</center>
	</div>
</body>
</html>


<script type="text/javascript">
	//The Leave Type Manager
	var leave_type = <?php echo json_encode($leavetype); ?>;
	switch(leave_type){
		case "Vacation Leave":
			$("#id_vacationleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "Others (Specify)":

					$("#vac_leave_other").prop("checked",true);
					$("#va_leave_other_reason").val(<?php echo json_encode($f_location); ?>);
				break;
				case "To Seek Employment":
					$("#vac_leave_toseek").prop("checked",true);
				break;

			}
		break;
		case "Sick Leave":
			$("#id_sickleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "In Hospital":
					$("#id_sick_reason_inhospital").prop("checked",true);
					$("#id_sick_location_inhos").val(<?php echo json_encode($f_location); ?>);
				break;
				case "Out Patient":
					$("#id_sick_reason_outpatient").prop("checked",true);
					$("#id_sick_location_outpat").val(<?php echo json_encode($f_location); ?>);
				break;
			}
		break;
		case "Service Credit":
			$("#id_sickleave").prop("checked",true);
			var subtype = <?php echo json_encode($f_subtype); ?>;
			switch(subtype){
				case "In Hospital":
					$("#id_sick_reason_inhospital").prop("checked",true);
					$("#id_sick_location_inhos").val(<?php echo json_encode($f_location); ?>);
				break;
				case "Out Patient":
					$("#id_sick_reason_outpatient").prop("checked",true);
					$("#id_sick_location_outpat").val(<?php echo json_encode($f_location); ?>);
				break;
			}
		break;
		case "Maternity":
			$("#id_maternity").prop("checked",true);
		break;
		default:
			$("#id_others").prop("checked",true);
			$("#id_leavetype").val(leave_type);
		break;
	}
</script>

<?php

	if(isset($_POST["fordissaproval"])){
		echo "<script>
			$('#duetodissaprovereason').val('" . mysqli_real_escape_string($c,$_POST["reasonofdiss"]) . "');
		</script>";
	}


	$xfrom = $date_from;
	$xto = $date_to;

	$wholesomedate = $xfrom . "-" . $xto;
	if($xfrom == $xto){
		$wholesomedate = $xfrom;
	}


?>

<script type="text/javascript">


		var qrcode = new QRCode("qrcode",{
							   width: 90,
							    height: 90,
						});

						
					    var elText = <?php echo json_encode($eid .  "," . $leavetype . "," . $wholesomedate) ; ?>;

					    qrcode.makeCode(elText);
					    // alert(elText);
					   setTimeout(function(){
					   	 print_now();

					   	},500)
	
	function print_now(quality = 1) {
		const filename  = 'LAMS_FORM_6_' + <?php echo json_encode($f_fullname . " - " .  date("Y-m-d (H i sa)")); ?> + '.pdf';
		html2canvas(document.querySelector('#nodeToRenderAsPDF'), 
								{scale: quality}
						 ).then(canvas => {
			let pdf = new jsPDF('p', 'mm', 'a4');
			pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);

			pdf.save(filename);
			
			$("#log_cocoa_form6").css("display","none");
			
		});
	}

</script>
	<script src="js/jspdf.js"></script>
	<script src="js/jquery-2.1.3.js"></script>
	<script src="js/pdfFromHTML.js"></script>
