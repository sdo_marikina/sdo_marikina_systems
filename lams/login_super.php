<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>LAMS | HR</title>
	<?php
	include("php/superserver.php");
	include("theme/theme.php");
	?>
</head>
<body style="background-color: #111; color: white;">
	<div class="jumbotron jumbotron-fluid hasbg" style="background-image: url('https://images.pexels.com/photos/220201/pexels-photo-220201.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260');">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h1 class="display-4" style="text-shadow: 0px 0px 30px white;">
							<strong>LAMS</strong> <small>HR</small>
						</h1>
						<hr>
						<p>
						Shools Division Office | <strong>Marikina City</strong>
						</p>
					</div>
					<div class="col-sm-4">
						<h4>Login</h4>
						<form method="POST">
							<div class="row">
								<div class="col-sm-12">
						<div class="form-group">
						<input required="" autocomplete="off" type="text" placeholder="Username/Email" class="form-control" name="login_username">
						</div>
								</div>
								<div class="col-sm-12">
						<div class="form-group">
						<input required="" autocomplete="off" type="password" placeholder="Password" class="form-control" name="login_password">
						</div>
								</div>
								<div class="col-sm-12">
									<button style="margin-top: 5px;" name="btn_loginsuper" class="btn btn-primary btn-block float-right"><span>Login</span></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<div class="container">
		<?php
		include("feed.php");
		?>
	</div>
<?php
include("footer.php");
?>
</body>
</html>
<?php
include("components/modals.php");
?>