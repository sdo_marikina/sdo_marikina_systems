<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Departments</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">


		<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="fas fa-layer-group"></i> DEPARTMENTS</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#" data-toggle="modal" data-target="#addnewdepartment_modal"><i class="fas fa-plus-circle"></i> Add New Department</a>
		      </li>
		    </ul>
		  </div>
		</nav>


					<div class="container">
						<div class="card">
							<div class="card-body">
								<img src='images/departments.png' class='content_icon'>
										<h5 class="mb-3">Departments</h5>
								 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#lods" role="tab" aria-controls="lods" aria-selected="true"><i class="fas fa-list"></i> List of Departments</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#lwnd" role="tab" aria-controls="lwnd" aria-selected="false"><i class="far fa-frown"></i> Employees with No Department <?php ShowNoDeptBadgeNumber(); ?></a>
			  </li>
			</ul>



		<div class="container-fluid">



			<div class="tab-content" id="pills-tabContent">
			  <div class="tab-pane fade show active" id="lods" role="tabpanel" aria-labelledby="pills-home-tab">
			  				<table class="table table-bordered table-striped table-sm" id="emp_tbl">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Options</th>
				</tr>
			</thead>
			<tbody>
				<?php echo ShowDepartments(); ?>
			</tbody>
		</table>
			  </div>
			  <div class="tab-pane fade" id="lwnd" role="tabpanel" aria-labelledby="pills-profile-tab">
			  	  	<div class="row">
		  		<div class="col-sm-6">
		  					  	<div class="alert alert-warning" role="alert">
		  	 <i class="fas fa-info-circle"></i> The listed employee(s) here will not be visible on the <u>Manage Employees</u> table.
		  	</div>
		  		</div>
		  		<div class="col-sm-6">
		  					  	<div class="alert alert-primary" role="alert">
		  	  <i class="fas fa-location-arrow"></i> Assign them to a new Department so you can manage them again.
		  	</div>
		  		</div>
		  	</div>
			  	<table class="table table-striped table-sm table-bordered" id="idofttt">
			  	  <thead>
			  	    <tr>
			  	      <th>Employee Name</th>
			  	      <th>Action</th>
			  	    </tr>
			  	  </thead>
			  	  <tbody>
			  	  	<?php ShowNoDeptEmployees(); ?>
			  	  </tbody>
			  	</table>
			  </div>
			</div>





		</div>
							</div>
						</div>
					</div>
	</div>




	<form action="php/external_server.php" method="POST">
		<div class="modal" tabindex="-1" role="dialog" id="assdpt">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="ultrabold">Assign Department</h4>
		        <p>Assign <u id="emp_name_id"></u> to a new Department.</p>

		        <input type="hidden" id="employeeidtoassignnewposition" name="idofemp">
		        <div class="form-group">
		        	<label>Choose Department</label>
		        	<select class="form-control" id="newselected" name="NewSelectedDepartment" required="">
		        		<?php load_drop_department(); ?>
		        	</select>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" name="assignunitDepartment_now" class="btn btn-primary">Save changes</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>



	<script type="text/javascript">
	function opassDept(controlOBJ){
		$("#emp_name_id").html($(controlOBJ).data("empname"));
		$("#employeeidtoassignnewposition").val($(controlOBJ).data("theempid"));
		    $('#newselected option').prop('selected', function() {
        return this.defaultSelected;
    });
	}	
	</script>


<form action="php/external_server" method="POST">
	
<div class="modal" tabindex="-1" role="dialog" id="addnewdepartment_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button></button>
    	<h5 class="ultratitle mb-3">Add New Department</h5>
       <div class="form-group">
       	<label>Department Name</label>
       	<input type="text" class="form-control" placeholder="Type here..." autocomplete="off" required="" name="department_name">
       </div>
       <div class="form-group">
       	<label>Add Description</label>
       	<textarea required="" rows="10" placeholder="Type here..." class="form-control" name="department_description"></textarea>
       </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="addnewdepartment" class="btn btn-primary">Add Department</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>

<form action="php/external_server" method="POST">
<div class="modal" tabindex="-1" role="dialog" id="editdepartment_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
      <h4 class="ultrabold">Edit Department</h4>

      <input type="hidden" name="e_i" id="deptid_id">
        <div class="form-group">
        	<label>Department Name</label>
        	<input type="text" id="deptname_id" class="form-control" placeholder="Type here..." autocomplete="off" required="" name="e_n">
        </div>
        <div class="form-group">
        	<label>Add Description</label>
        	<textarea required="" id="deptdesc_id" rows="10" placeholder="Type here..." name="e_d" class="form-control"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="editdepartmentinfo" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>

<form action="php/external_server" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="deletedepartment_modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	       <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
			    <h4 class="ultrabold">Delete?</h4>
		        <p>Are you sure you want to delete the following department?<br>
	        	<strong><u id="id_deptnamedisplay">Department Name</u></strong></p>
	        	<input type="hidden" id="id_idofddd" name="e_id">
	      </div>
	      <div class="modal-footer">
	        <button type="submit" name="deletedepartment" class="btn btn-danger">Yes, Delete this Department</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>

<script type="text/javascript">
	function op_deldept(controlOBJ){
		$("#id_idofddd").val($(controlOBJ).data("deptid"));
		$("#id_deptnamedisplay").html($(controlOBJ).data("deptname"));
	}
	function op_editdept(controlOBJ){
$("#deptid_id").val($(controlOBJ).data("deptid"));
		$("#deptname_id").val($(controlOBJ).data("deptname"));
		$("#deptdesc_id").val($(controlOBJ).data("deptdesc"));
	}
</script>

</body>
</html>
<script type="text/javascript">
		// popnotification("We are still working on this.", "Some features are not yeat ready for this page.",false);
	$("#emp_tbl").DataTable(	{
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
	$("#idofttt").DataTable(	{
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

	
		highlight_pagelink("#page_departments");
</script>
<?php
include("components/dispute_reports_modals.php");
include("components/modals.php");
?>

