<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/cdtrs_icon.ico" type="text/css" href="">
	<title>CDTRS | Employee Profile</title>
	<?php
		include("php/auth.php");
	include("php/server.php");
	include("theme/theme.php");
	?>
</head>
<body class="blurbg">
	<?php
	include("components/navbar.php");
	include("components/sidebar.php");
	?>
	<div class="rightbar">
		<script type="text/javascript">
			TriggerEmployeeSync();
		</script>
		<nav class="navbar navbar-expand-lg" style=" margin-top: 5px; margin-bottom: 15px;">
		  <a class="navbar-brand" href="#"><i class="far fa-user-circle"></i> EMPLOYEE PROFILE</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="emp_management.php"><i class="fas fa-arrow-circle-left"></i> Back</a>
		      </li>
		    </ul>
		  </div>
		</nav>
	
			
					<div class="container">
							<div class="alert alert-warning" role="alert">
		  <h4>Feature is not yeat ready.</h4>
		  <p>Coming soon in the next version of CDTRS.</p>
		</div>
								<div style="width:80px; height:80px; background-color: #0a3d62; background-size:cover; background-position:center; background-repeat:no-repeat; border-radius: 5px;"></div>
								<h5 class="mt-3 ultratitle">Merlinda S. Meralda</h5>
								<h6 class="mt-2">Department: <span class="text-muted">SGOD</span></h6>

					
							<div class="row">
								<div class="col-sm-12">
									<h5 class="mt-3 ultratitle">EMPLOYEE</h5>
								</div>
								<div class="col-lg-4">
								<div class="card">
									<div class="card-body">
										<h6 class="ultratitle">BASIC INFO</h6>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card">
									<div class="card-body">
										<h6 class="ultratitle">CONTACT</h6>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card">
									<div class="card-body">
										<h6 class="ultratitle">SCHEDULE</h6>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
									<h5 class="mt-3 ultratitle">DAILY TIME RECORD</h5>
								</div>
								<div class="col-sm-12">
									<div class="card">
										<div class="card-body">
											<div class="row">
												<div class="col-sm-4">
													<h6 class="ultratitle">DISPUTE COUNT</h6>
												</div>
												<div class="col-sm-4">
													<h6 class="ultratitle">LAST DAY PRESENT</h6>
												</div>
												<div class="col-sm-4">
													<h6 class="ultratitle">ATTENDANCE</h6>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<h5 class="mt-3 ultratitle">LEAVE REPORT</h5>
								</div>
								<div class="col-sm-12">
									<div class="card">
										<div class="card-body">
											<div class="row">
												<div class="col-sm-4">
													<h6 class="ultratitle">OVERALL LEAVE</h6>
												</div>
								
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<h5 class="mt-3 ultratitle">MISC</h5>
								</div>
								<div class="col-sm-12">
									<div class="card">
										<div class="card-body">
											<div class="row">
												<div class="col-sm-4">
													<h6 class="ultratitle">WHEREABOUTS</h6>
												</div>
								<div class="col-sm-4">
													<h6 class="ultratitle">AUTHORITIES</h6>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
				<!-- </div> -->
			<!-- </div> -->
	</div>

</body>
</html>


<?php
include("components/modals.php");
?>

<script type="text/javascript">
	function OpenChngeProfilePicMOdal(toControl){
				$("#idofprofilepic").val($(toControl).data("eidofemp"));
	}
</script>