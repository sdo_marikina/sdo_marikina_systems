
           <style type="text/css">
                                        .versionbar {
                                            display: none;
                                            border-radius: 4px;
                                            width: 60px;
                                            bottom: 0px;
                                            left: 0px;
                                            position: fixed;
                                            margin: 10px;
                                            opacity: 0.3;
                                            z-index: 20;
                                        }
                                        
                                        .versionbar:hover {
                                            opacity: 1;
                                        }
                                    </style>
                                    <img src="images/V2.2.png" title="Click to see what's new!" id="card_version" class="consistent_shadow versionbar" data-toggle="modal" data-target="#whatsnew">



                                    
<div class="modal" tabindex="-1" role="dialog" id="modalfeedback">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <h1 class="ultrabold">Feedback</h1>
        <style type="text/css">
        .embar{
            width: 10%;
            display: block;
            float: left;
            margin-right: 22px;
            margin-left: 22px;
            transition: 0.3s all;
        }
            .embar img{
                width: 100%
            }
        </style>
        <div class="form-group">
            <textarea class="form-control" id="feed_id_desc" placeholder="Write your feedback here..."></textarea>

        </div>
        <div class="form-group">
             <center>How do you feel?</center>
            <input type="range" min="1" id="feed_id_emotion" max="5" style="width: 100%;" name="">
        <div>

            <div class="embar" id="em_1">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/face-with-symbols-over-mouth.png">
                </center>
            </div>
            <div class="embar" id="em_2">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/confused-face.png">
                </center>
            </div>
            <div class="embar" id="em_3">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/slightly-smiling-face.png">
                </center>
            </div>
            <div class="embar" id="em_4">
                <center>
                    <img src="https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/smiling-face-with-open-mouth.png">
                </center>
            </div>
            <div class="embar" id="em_5">
                <center>
                    <img src="
                    https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/star-struck.png">
                </center>
            </div>
        </div>
        </div>
        <br>
        <br>
        </br>
        <div class="form-group">
            <center><button class="btn btn-primary" id="btn_feed_send" data-dismiss="modal">Send Feedback</button></center>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
     visualizeemotion();
setInterval(function(){
     visualizeemotion();
 },100)
function visualizeemotion(){
     var feed_status = $("#feed_id_emotion").val();
     $("#em_1").css("opacity","0.3");
     $("#em_2").css("opacity","0.3");
     $("#em_3").css("opacity","0.3");
     $("#em_4").css("opacity","0.3");
     $("#em_5").css("opacity","0.3");
     $("#em_1").css("transform","scale(0.9)");
$("#em_2").css("transform","scale(0.9)");
$("#em_3").css("transform","scale(0.9)");
$("#em_4").css("transform","scale(0.9)");
$("#em_5").css("transform","scale(0.9)");
     switch(feed_status){
        case "1":
            $("#em_1").css("opacity","1");
            $("#em_1").css("transform","scale(1.1)");
        break;
        case "2":
            $("#em_2").css("opacity","1");
            $("#em_2").css("transform","scale(1.1)");
        break;
        case "3":
            $("#em_3").css("opacity","1");
            $("#em_3").css("transform","scale(1.1)");
        break;
        case "4":
            $("#em_4").css("opacity","1");
             $("#em_4").css("transform","scale(1.1)");
        break;
        case "5":
            $("#em_5").css("opacity","1");
            $("#em_5").css("transform","scale(1.1)");
        break;
     }
}
$("#btn_feed_send").click(function(){
        var feed_description = $("#feed_id_desc").val();
    var feed_status = $("#feed_id_emotion").val();
    if(feed_description === ""){
        popnotification("Try again.","Please write your feedback!",false);
        $("#feed_id_desc").val("");
        $("#feed_id_emotion").val("3");
    }else{
        $.ajax({
        type: "POST",
        url: "php/external_server.php",
        data: {send_feed: "x",desc:feed_description,emotion:feed_status},
        success: function(data){
            popnotification("Feedback sent!","Thank you for giving us your toughts about the system.",false);
            $("#feed_id_desc").val("");
            $("#feed_id_emotion").val("3");
        }
    })
    }
})

</script>

<div class="modal" tabindex="-1" role="dialog" id="modalhelpdesk">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content blurbg">

      <div class="modal-body">
        <h1 class="ultrabold">Helpdesk</h1>
        <div class="row">
            <div class="col-sm-4">
                <h3>Topics</h3>
            </div>
            <div class="col-sm-8">
                <h3>Discussions</h3>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<br>
<br>
<style type="text/css">
    .insidecontent{
        padding-right: 20px;
padding-left: 10px;
color: white;
    }
    .xfoot{
        padding: 10px;
        border-radius: 4px;
        background-color: transparent;
        border : none !important;
        color: white;
    }
    .xfoot a{
    
        color: white !important;
    }
</style>
<div class="container ">
		<footer class="xfooter " >
		<div class="container" style="text-align: right;">
			<center>
                <span class="blurbg xfoot">
                <span id="connectivitystatus" class="insidecontent" ></span>
                |<span class="insidecontent" style="border-radius: 20px;">v <?php echo json_decode($version); ?></span>
                |<span class="insidecontent" style="border-radius: 20px;"><a href="images/lumstut.html" target="_blank"><i class="fas fa-arrow-circle-up"></i> Update</a></span>
                |<span class="insidecontent" style="border-radius: 20px;"><a href="#" data-toggle="modal" data-target="#modalfeedback"><i class="fas fa-heart"></i> Feedback</a></span>

                |<span class="insidecontent" style="border-radius: 20px;"><a href="https://DepedMarikina.ph" target="_blank"><i class="fas fa-globe-americas"></i> DepedMarikina.ph</a></span>
                <!-- |<span class="insidecontent" style=" color: white; color: black !important; border-radius: 20px;"><a href="#" data-toggle="modal" data-target="#modalhelpdesk"><i class="fas fa-headset"></i> Helpdesk</a> --></span>
            </span></span></center>
		</div>
	</footer>
</div>

<script>
	status();
	setInterval(function(){
		status();
	},1000)
    function status()
    {
        if(navigator.onLine)
        {
            var phpversion = <?php echo json_encode(phpversion()); ?>;


            var verx = phpversion.split('.');
            // alert(verx[0]);
            // alert(verx[0]);
            if(verx[0]  == "7"){
                $("#connectivitystatus").html("<span><i class='far fa-check-circle' style='color:green;'></i> Connected</span>");

            }else{
                $("#connectivitystatus").html("<span><i class='fas fa-exclamation-triangle' style='color:red;'></i> PHP 7.2.4 or higher is required.</span>");
            }      
        }
        else
        {
            $("#connectivitystatus").html("<span><i class='fas fa-exclamation-triangle' style='color:red;'></i> CDTRS is offline.</span>");
        }
    }
</script>