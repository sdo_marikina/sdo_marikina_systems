-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 17, 2020 at 07:51 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marikina`
--

-- --------------------------------------------------------

--
-- Table structure for table `applied_leave`
--

DROP TABLE IF EXISTS `applied_leave`;
CREATE TABLE IF NOT EXISTS `applied_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(222) DEFAULT NULL,
  `leave_type` varchar(222) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `sub_type` text NOT NULL,
  `location` varchar(222) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `date_applied_totatus` date DEFAULT NULL,
  `reason_of_dissaprove` varchar(500) NOT NULL DEFAULT '',
  `to_subtract` int(11) NOT NULL DEFAULT '0',
  `leave_taken` int(11) NOT NULL,
  `date_requested` date DEFAULT NULL,
  `time_requested` time NOT NULL,
  `paid_days` int(11) DEFAULT NULL,
  `unpaid_days` int(11) DEFAULT NULL,
  `vac_leave_abroad_specify` varchar(222) DEFAULT NULL,
  `fullname` varchar(222) DEFAULT NULL,
  `office_agency` varchar(222) DEFAULT NULL,
  `date_of_filling` date DEFAULT NULL,
  `position` varchar(222) DEFAULT NULL,
  `salary_monthly` varchar(222) DEFAULT NULL,
  `vacation_type` varchar(222) DEFAULT NULL,
  `vacation_leaveincase` int(11) DEFAULT NULL,
  `sick_incase` varchar(222) DEFAULT NULL,
  `sick_incase_reason` varchar(222) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied_leave`
--

INSERT INTO `applied_leave` (`id`, `employee_id`, `leave_type`, `date_from`, `date_to`, `sub_type`, `location`, `status`, `date_applied_totatus`, `reason_of_dissaprove`, `to_subtract`, `leave_taken`, `date_requested`, `time_requested`, `paid_days`, `unpaid_days`, `vac_leave_abroad_specify`, `fullname`, `office_agency`, `date_of_filling`, `position`, `salary_monthly`, `vacation_type`, `vacation_leaveincase`, `sick_incase`, `sick_incase_reason`) VALUES
(16, '123456', 'Leave Without Pay', '2020-02-07', '2020-02-07', '', '', 1, '2020-02-07', '', 0, 1, '2020-03-04', '13:45:50', 0, 1, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-04', 'Sample Man', '34534534', '', 0, '', ''),
(17, '123456', 'Solo Parent Leave', '2020-03-31', '2020-03-31', '', '', 1, '2020-03-04', '', 0, 1, '2020-03-04', '11:53:30', 0, 0, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-04', 'Sample Man', '34534534', '', 0, '', ''),
(18, '123456', 'Leave Without Pay', '2020-02-03', '2020-02-03', '', '', 1, '2020-02-03', '', 0, 1, '2020-03-04', '11:54:32', 0, 1, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-04', 'Sample Man', '34534534', '', 0, '', ''),
(19, '123456', 'Solo Parent Leave', '2020-03-26', '2020-03-26', '', '', 1, '2020-03-04', '', 0, 1, '2020-03-04', '08:42:54', 0, 0, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-04', 'Sample Man', '34534534', '', 0, '', ''),
(20, '123456', 'Solo Parent Leave', '2020-03-23', '2020-03-23', '', '', 2, '2020-03-04', 'hehehe ayaw', 0, 1, '2020-03-04', '08:43:39', 0, 0, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-04', 'Sample Man', '34534534', '', 0, '', ''),
(21, '123456', 'Leave Without Pay', '2020-02-26', '2020-02-26', '', '', 1, '2020-02-26', '', 0, 1, '2020-03-03', '09:41:24', 0, 1, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-03', 'Sample Man', '34534534', '', 0, '', ''),
(22, '12345', 'Maternity Leave', '2020-03-07', '2020-06-19', '', '', 1, '2020-03-03', '', 0, 105, '2020-03-03', '09:42:16', 105, 0, '', 'TEODORO, MARCELINO D', 'cocoa now', '2020-03-03', 'Sample Man', '209333', '', 0, '', ''),
(23, '123456', 'Leave Without Pay', '2020-03-03', '2020-03-03', 'In Hospital', '', 1, '2020-03-03', '', 0, 1, '2020-03-03', '09:17:42', 0, 1, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-03', 'Sample Man', '34534534', 'In Hospital', 0, 'In Hospital', ''),
(24, '12345', 'Maternity Leave', '2020-03-11', '2020-06-23', '', '', 1, '2020-03-03', '', 0, 105, '2020-03-03', '09:20:06', 105, 0, '', 'TEODORO, MARCELINO D', 'cocoa now', '2020-03-03', 'Sample Man', '209333', '', 0, '', ''),
(25, '123456', 'Sick Leave', '2020-03-02', '2020-03-02', 'In Hospital', 'fwefdewqf', 1, '2020-03-02', '', 0, 1, '2020-03-03', '09:21:26', 1, 0, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-03', 'Sample Man', '34534534', 'In Hospital', 0, 'In Hospital', 'fwefdewqf'),
(26, '123456', 'Forced Leave', '2020-02-28', '2020-02-28', '', '', 1, '2020-02-28', '', 0, 1, '2020-03-03', '09:28:34', 0, 0, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-03', 'Sample Man', '34534534', '', 0, '', ''),
(27, '123456', 'Vacation Leave', '2020-03-03', '2020-03-04', 'To Seek Employment', '', 1, '2020-03-03', '', 0, 1, '2020-03-03', '09:33:38', 1, 0, 'sa bakasyon', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-03', 'Sample Man', '34534534', 'To Seek Employment', 0, 'To Seek Employment', ''),
(28, '123456', 'Single Parent Leave', '2020-02-27', '2020-02-27', '', '', 1, '2020-02-27', '', 0, 1, '2020-03-03', '09:36:54', 0, 0, '', 'TORRECAMPO, JOEL T', 'cocoa now', '2020-03-03', 'Sample Man', '34534534', '', 0, '', ''),
(29, '123456', 'Sick Leave', '2020-02-24', '2020-02-24', 'In Hospital', 'edwdw', 1, '2020-02-13', '', 0, 1, '2020-02-13', '14:17:31', 0, 0, '', 'SAM, KAT MAL', 'cocoa now', '2020-02-13', 'Sample Man', '34534534', 'In Hospital', 0, 'In Hospital', 'edwdw'),
(30, '123456', 'Sick Leave', '2020-02-12', '2020-02-12', 'In Hospital', 'fwefwefwefewf', 1, '2020-02-13', '', 0, 1, '2020-02-13', '14:18:11', 0, 0, '', 'SAM, KAT MAL', 'cocoa now', '2020-02-13', 'Sample Man', '34534534', 'In Hospital', 0, 'In Hospital', 'fwefwefwefewf'),
(31, '12345', 'Leave Without Pay', '2020-02-26', '2020-02-26', '', '', 1, '2020-02-13', '', 0, 1, '2020-02-13', '14:26:21', 0, 1, '', 'STEFAN, COCO MANGA SATURNO', 'cocoa now', '2020-02-13', 'Sample Man', '209333', '', 0, '', ''),
(34, '12345', 'Leave Without Pay', '2020-02-29', '2020-03-13', 'In Hospital', '', 1, '2020-03-13', '', 0, 5, '2020-03-13', '16:46:18', 0, 5, '', 'TEODORO, MARCELINO D', 'cocoa now', '2020-03-13', 'Sample Man', '209333', 'In Hospital', 0, 'In Hospital', ''),
(33, '12345', 'Sick Leave', '2020-02-29', '2020-03-13', 'In Hospital', 'sadwafdwedf', 2, '2020-03-13', 'dwqdqw', 0, 5, '2020-03-13', '16:37:49', 5, 0, '', 'TEODORO, MARCELINO D', 'cocoa now', '2020-03-13', 'Sample Man', '209333', 'In Hospital', 0, 'In Hospital', 'sadwafdwedf'),
(35, '12345', 'Sick Leave', '2020-02-01', '2020-02-08', 'In Hospital', 'sssdewdwdwdw', 2, '2020-03-13', 'wdwdwd', 0, 5, '2020-03-13', '16:47:47', 5, 0, '', 'TEODORO, MARCELINO D', 'cocoa now', '2020-03-13', 'Sample Man', '209333', 'In Hospital', 0, 'In Hospital', 'sssdewdwdwdw'),
(36, '12345', 'Sick Leave', '2020-02-09', '2020-02-11', 'In Hospital', 'sfdewfwef', 2, '2020-03-13', 'efwwefwef', 0, 2, '2020-03-13', '16:51:57', 2, 0, '', 'TEODORO, MARCELINO D', 'cocoa now', '2020-03-13', 'Sample Man', '209333', 'In Hospital', 0, 'In Hospital', 'sfdewfwef');

-- --------------------------------------------------------

--
-- Table structure for table `attendance_logs`
--

DROP TABLE IF EXISTS `attendance_logs`;
CREATE TABLE IF NOT EXISTS `attendance_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(256) NOT NULL,
  `access_type` int(11) NOT NULL,
  `access_image` text NOT NULL,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ismanual` int(11) NOT NULL DEFAULT '0',
  `synched` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance_logs`
--

INSERT INTO `attendance_logs` (`id`, `eid`, `access_type`, `access_image`, `date`, `timestamp`, `ismanual`, `synched`) VALUES
(1, '12345', 1, '', '2020-01-06', '2020-01-06 04:54:29', 0, 1),
(2, '12345', 1, '', '2020-01-17', '2020-01-17 07:12:17', 0, 1),
(3, '12345', 1, '', '2020-01-18', '2020-01-18 01:21:01', 0, 1),
(4, '12345', 2, '', '2020-01-18', '2020-01-18 01:26:57', 0, 1),
(5, '12345', 1, '', '2020-02-04', '2020-02-04 09:07:57', 0, 1),
(6, '12345', 2, '', '2020-02-04', '2020-02-04 09:08:28', 0, 1),
(8, '12345', 2, '', '2020-02-04', '2020-02-04 09:09:57', 0, 1),
(9, '123456', 1, '', '2020-02-13', '2020-02-13 06:11:21', 0, 1),
(10, '12345', 1, '', '2020-03-02', '2020-03-02 09:28:25', 0, 1),
(11, '123456', 1, '', '2020-03-16', '2020-03-16 02:09:58', 0, 1),
(12, '123456', 2, '', '2020-03-16', '2020-03-16 05:11:46', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `attendance_picture_logs`
--

DROP TABLE IF EXISTS `attendance_picture_logs`;
CREATE TABLE IF NOT EXISTS `attendance_picture_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `picture` varchar(222) NOT NULL,
  `timestamp` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance_picture_logs`
--

INSERT INTO `attendance_picture_logs` (`id`, `eid`, `picture`, `timestamp`) VALUES
(1, '12345', 'blank', '2020-01-06 04:54:28'),
(2, '12345', 'blank', '2020-01-17 07:12:17'),
(3, '12345', 'blank', '2020-01-18 01:21:01'),
(4, '12345', 'blank', '2020-01-18 01:26:57'),
(5, '12345', 'blank', '2020-02-04 09:07:57'),
(6, '12345', 'blank', '2020-02-04 09:08:28'),
(7, '12345', 'blank', '2020-02-04 09:09:21'),
(8, '12345', 'blank', '2020-02-04 09:09:57'),
(9, '123456', 'blank', '2020-02-13 06:11:21'),
(10, '12345', 'blank', '2020-03-02 09:28:25'),
(11, '123456', 'blank', '2020-03-16 02:09:58'),
(12, '123456', 'blank', '2020-03-16 05:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(222) DEFAULT NULL,
  `emp_name` varchar(222) NOT NULL DEFAULT '',
  `station` varchar(222) NOT NULL DEFAULT '',
  `destination` varchar(222) NOT NULL DEFAULT '',
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `auth_time` time DEFAULT NULL,
  `purpose_description` varchar(1000) NOT NULL DEFAULT '',
  `data_file` varchar(500) NOT NULL DEFAULT '',
  `funding_source` varchar(200) NOT NULL DEFAULT '',
  `auth_type` int(11) NOT NULL DEFAULT '0',
  `record_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auth_status` int(11) NOT NULL DEFAULT '0',
  `control_code` varchar(255) NOT NULL DEFAULT '',
  `origin` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_leaveparental`
--

DROP TABLE IF EXISTS `auth_leaveparental`;
CREATE TABLE IF NOT EXISTS `auth_leaveparental` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `leave_type` varchar(222) NOT NULL,
  `date_authorized` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_leaveparental`
--

INSERT INTO `auth_leaveparental` (`id`, `eid`, `leave_type`, `date_authorized`) VALUES
(1, '12345', 'Maternity Leave', '2020-01-06 05:04:22'),
(2, '12345', 'Paternity Leave', '2020-03-12 03:05:22');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `d_make` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `d_modif` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `d_make`, `d_modif`) VALUES
(1, 'Depatment OSD', 'DOSD', '2020-01-06 04:50:37', '2020-01-06 04:50:37');

-- --------------------------------------------------------

--
-- Table structure for table `dispute`
--

DROP TABLE IF EXISTS `dispute`;
CREATE TABLE IF NOT EXISTS `dispute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) DEFAULT NULL,
  `reason` varchar(500) NOT NULL,
  `is_oath` varchar(4) NOT NULL,
  `date_of_dispute` date NOT NULL,
  `date_of_record` date NOT NULL,
  `status` int(11) NOT NULL,
  `documentrecieved` int(11) NOT NULL,
  `reason_of_dissaprove` varchar(500) NOT NULL DEFAULT '',
  `dispute_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(256) NOT NULL,
  `rfid` varchar(256) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `basic_pay` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `fname` tinytext NOT NULL,
  `mname` tinytext NOT NULL,
  `lname` tinytext NOT NULL,
  `fid` int(11) DEFAULT NULL,
  `ftemplate` tinytext,
  `schedule` tinytext,
  `synched` int(11) NOT NULL,
  `cont_email` varchar(100) DEFAULT NULL,
  `cont_fb` varchar(100) DEFAULT NULL,
  `cont_homeadd` varchar(100) DEFAULT NULL,
  `cont_prinum` varchar(13) DEFAULT NULL,
  `cont_emernum` varchar(13) DEFAULT NULL,
  `leavepref_issingle` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `eid`, `rfid`, `type`, `basic_pay`, `position`, `department`, `fname`, `mname`, `lname`, `fid`, `ftemplate`, `schedule`, `synched`, `cont_email`, `cont_fb`, `cont_homeadd`, `cont_prinum`, `cont_emernum`, `leavepref_issingle`, `date_created`, `date_updated`) VALUES
(1, '12345', NULL, 3, 209333, 1, 1, 'MARCELINO', 'D', 'TEODORO', NULL, NULL, '08:00 AM,05:00 PM,,', 1, '', '', '', '', '', 0, '2020-01-06 04:51:29', '2020-03-03 01:15:15'),
(2, '123456', NULL, 1, 34534534, 1, 1, 'JOEL', 'T', 'TORRECAMPO', NULL, NULL, '08:00 AM,05:00 PM,,', 1, '', '', '', '', '', 1, '2020-02-13 06:11:04', '2020-03-04 05:43:59');

-- --------------------------------------------------------

--
-- Table structure for table `entitlements`
--

DROP TABLE IF EXISTS `entitlements`;
CREATE TABLE IF NOT EXISTS `entitlements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(222) DEFAULT NULL,
  `service_credit` float NOT NULL DEFAULT '0',
  `val_fr` varchar(222) DEFAULT NULL,
  `val_to` varchar(222) DEFAULT NULL,
  `emp_n` varchar(222) NOT NULL,
  `cto` float NOT NULL DEFAULT '0',
  `sick_leave` float NOT NULL DEFAULT '0',
  `vacation_leave` float NOT NULL DEFAULT '0',
  `special_leave` float NOT NULL DEFAULT '0',
  `forced_leave` float NOT NULL DEFAULT '0',
  `solo_parent_leave` float DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entitlements`
--

INSERT INTO `entitlements` (`id`, `emp_id`, `service_credit`, `val_fr`, `val_to`, `emp_n`, `cto`, `sick_leave`, `vacation_leave`, `special_leave`, `forced_leave`, `solo_parent_leave`) VALUES
(2, '123456', 0, '', '', 'TORRECAMPO, JOEL T', 0, 0, 0, 2, 0, 0),
(3, '12345', 0, '2020-03-04', '2020-03-05', 'TEODORO, MARCELINO D', 34, 23, 233, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `entitlements_cto`
--

DROP TABLE IF EXISTS `entitlements_cto`;
CREATE TABLE IF NOT EXISTS `entitlements_cto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empid` varchar(222) NOT NULL,
  `entitlement` float NOT NULL,
  `val_from` date NOT NULL,
  `val_to` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entitlements_cto`
--

INSERT INTO `entitlements_cto` (`id`, `empid`, `entitlement`, `val_from`, `val_to`) VALUES
(105, '12345', 23, '2020-03-04', '2020-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `freezed_balance`
--

DROP TABLE IF EXISTS `freezed_balance`;
CREATE TABLE IF NOT EXISTS `freezed_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_id` varchar(222) NOT NULL,
  `service_credit` varchar(222) NOT NULL,
  `vacation_leave` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `freezed_balance`
--

INSERT INTO `freezed_balance` (`id`, `leave_id`, `service_credit`, `vacation_leave`) VALUES
(1, '34', '23', '233'),
(2, '36', '23', '233');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE IF NOT EXISTS `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` tinytext CHARACTER SET latin1 NOT NULL,
  `name` tinytext CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lams_configurations`
--

DROP TABLE IF EXISTS `lams_configurations`;
CREATE TABLE IF NOT EXISTS `lams_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(222) NOT NULL,
  `value` varchar(222) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `value_type` varchar(222) NOT NULL,
  `hidden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lams_configurations`
--

INSERT INTO `lams_configurations` (`id`, `label`, `value`, `description`, `value_type`, `hidden`) VALUES
(11, 'cn', 'cocoa now', 'The name of your school or division.', 'School / Division Office', 0),
(2, 'prin', 'Cocoa Mamba', 'Name of the School Principal if you are using Lams for school.', 'School Principal', 0),
(3, 'oic', 'SHERYLL T. GAYOLA', 'Name of your Division Schools Division Superintendent', 'Education Program Supervisor<br>Officer-in-Charge<br>Office of the Schools Division Superintendent', 0),
(4, 'adoff', 'CLARO L. CAPCO', 'Name of your Division Administrative Officer V', 'Administrative Officer V', 0),
(5, 'hr', 'ARACELI D. DY', 'Name of your Division Administrative Officer IV - HRMO', 'Head Teacher III/OIC - HRMO', 0),
(6, 'usertype', '0', 'Check if this is a school account or leave if not.', 'Check if this is a school account', 1),
(7, 'autobackup', '0,0,', 'LAMS Records Backup System', 'AutomaticBackup', 1),
(10, 'scc', '1', 'Enabling this feature gives to support for data management of CDTRS and other special functions like CDTRS Break. (0-Disabled, 1-Enabled)', 'Support for Data Centralization', 0),
(13, 'att_kiosk', '1', 'Enable attendance kiosk to let employees log with their Employee ID on the CDTRS Home Screen', 'CDTRS Attendance Kiosk', 0),
(14, 'att_allow_no_cam', '1', 'Allow employees to log without taking a picture from the webcam.', 'Allow no Camera in Kiosk', 0),
(18, 'as_oic', 'ELISA O. CERVEZA', 'Office of the Assistant Schools Division Superintendent', 'Chief Education Supervisor<br>Officer-in-Charge<br>Office of the Assistant Schools Division Superintendent', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lams_users`
--

DROP TABLE IF EXISTS `lams_users`;
CREATE TABLE IF NOT EXISTS `lams_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(222) NOT NULL,
  `password` varchar(100) NOT NULL,
  `account_type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lams_users`
--

INSERT INTO `lams_users` (`id`, `username`, `password`, `account_type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

DROP TABLE IF EXISTS `leave_types`;
CREATE TABLE IF NOT EXISTS `leave_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(222) NOT NULL,
  `ft` int(11) NOT NULL,
  `forteacher` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`id`, `name`, `ft`, `forteacher`) VALUES
(1, 'CTO', 1, 0),
(2, 'Vacation Leave', 0, 0),
(3, 'Special Leave', 0, 0),
(4, 'Sick Leave', 0, 0),
(5, 'Service Credit', 0, 1),
(12, 'Maternity Leave', 0, 0),
(13, 'Maternity Leave', 0, 1),
(14, 'Paternity Leave', 0, 0),
(15, 'Paternity Leave', 0, 1),
(16, 'Solo Parent Leave', 0, 0),
(17, 'Solo Parent Leave', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `performer` varchar(222) NOT NULL,
  `action_description` varchar(1000) NOT NULL,
  `timestamp` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `performer`, `action_description`, `timestamp`) VALUES
(1, 'System Auto Performer', 'Just Added 5 Forced Leave to Employee #12345', '2020-03-03 01:27:50'),
(2, 'System Auto Performer', 'Just Added 5 Forced Leave to Employee #123456', '2020-03-03 01:27:50'),
(3, 'Admin', '123456 submitted a Leave Report.', '2020-03-03 01:28:34'),
(4, 'Admin', 'Deduction VL for Forced Leave (-1) to employee 123456 in  date: 2020-02-28.', '2020-03-03 01:28:48'),
(5, 'Admin', '123456 submitted a Leave Report.', '2020-03-03 01:33:38'),
(6, 'Admin', 'Added +7 single_parent_leave to 123456', '2020-03-03 01:36:32'),
(7, 'Admin', '123456 submitted a Leave Report.', '2020-03-03 01:36:54'),
(8, 'Admin', '123456 submitted a Leave Report.', '2020-03-03 01:41:24'),
(9, 'Admin', '12345 submitted a Leave Report.', '2020-03-03 01:42:16'),
(10, 'Admin', 'logged-out.', '2020-03-03 01:57:59'),
(11, 'Admin', 'Logged-in.', '2020-03-03 01:58:34'),
(12, 'Admin', 'logged-out.', '2020-03-03 08:30:34'),
(13, 'Admin', 'Logged-in.', '2020-03-03 08:30:49'),
(14, 'Admin', 'Deduction VL for Forced Leave (-1) to employee 123456 in  date: 2020-03-03.', '2020-03-04 00:23:40'),
(15, 'Admin', 'Logged-in.', '2020-03-04 00:23:48'),
(16, 'Admin', 'logged-out.', '2020-03-04 00:30:22'),
(17, 'Admin', 'Logged-in.', '2020-03-04 00:30:29'),
(18, 'Admin', 'logged-out.', '2020-03-04 00:31:05'),
(19, 'Admin', 'Logged-in.', '2020-03-04 00:31:51'),
(20, 'Admin', 'Added +2 solo_parent_leave to 123456', '2020-03-04 00:32:25'),
(21, 'Admin', 'logged-out.', '2020-03-04 00:32:30'),
(22, 'Admin', 'Logged-in.', '2020-03-04 00:32:39'),
(23, 'Admin', '123456 submitted a Leave Report.', '2020-03-04 00:42:54'),
(24, 'Admin', '123456 submitted a Leave Report.', '2020-03-04 00:43:39'),
(25, 'Admin', 'logged-out.', '2020-03-04 01:55:48'),
(26, 'Admin', 'Logged-in.', '2020-03-04 01:56:00'),
(27, 'Admin', 'Logged-in.', '2020-03-04 02:04:58'),
(28, 'Admin', 'logged-out.', '2020-03-04 02:06:38'),
(29, 'Admin', 'Logged-in.', '2020-03-04 02:08:01'),
(30, 'Admin', 'logged-out.', '2020-03-04 02:08:11'),
(31, 'Admin', 'Logged-in.', '2020-03-04 02:08:24'),
(32, 'Admin', 'logged-out.', '2020-03-04 02:42:35'),
(33, 'Admin', 'Logged-in.', '2020-03-04 02:42:43'),
(34, 'Admin', 'logged-out.', '2020-03-04 02:43:40'),
(35, 'Admin', 'Logged-in.', '2020-03-04 02:44:30'),
(36, 'Admin', 'logged-out.', '2020-03-04 02:45:17'),
(37, 'Admin', 'Logged-in.', '2020-03-04 02:45:28'),
(38, 'Admin', 'logged-out.', '2020-03-04 02:56:33'),
(39, 'Admin', 'Logged-in.', '2020-03-04 02:56:42'),
(40, 'Admin', 'logged-out.', '2020-03-04 03:01:46'),
(41, 'Admin', 'Logged-in.', '2020-03-04 03:01:53'),
(42, 'Admin', 'logged-out.', '2020-03-04 03:16:09'),
(43, 'Admin', 'Logged-in.', '2020-03-04 03:16:15'),
(44, 'Admin', 'logged-out.', '2020-03-04 03:17:15'),
(45, 'Admin', 'Logged-in.', '2020-03-04 03:21:49'),
(46, 'Admin', 'logged-out.', '2020-03-04 03:53:10'),
(47, 'Admin', '123456 submitted a Leave Report.', '2020-03-04 03:53:30'),
(48, 'Admin', 'Logged-in.', '2020-03-04 03:53:37'),
(49, 'Admin', 'logged-out.', '2020-03-04 03:54:11'),
(50, 'Admin', '123456 submitted a Leave Report.', '2020-03-04 03:54:32'),
(51, 'Admin', 'Logged-in.', '2020-03-04 03:54:39'),
(52, 'Admin', 'Logged-in.', '2020-03-04 03:57:54'),
(53, 'Admin', 'logged-out.', '2020-03-04 05:04:10'),
(54, 'Admin', 'Logged-in.', '2020-03-04 05:04:16'),
(55, 'Admin', 'Logged-in.', '2020-03-04 05:38:33'),
(56, 'Admin', 'logged-out.', '2020-03-04 05:38:47'),
(57, 'Admin', 'Logged-in.', '2020-03-04 05:42:28'),
(58, 'Admin', 'logged-out.', '2020-03-04 05:44:24'),
(59, 'Admin', 'Logged-in.', '2020-03-04 05:44:31'),
(60, 'Admin', 'logged-out.', '2020-03-04 05:45:21'),
(61, 'Admin', '123456 submitted a Leave Report.', '2020-03-04 05:45:50'),
(62, 'Admin', 'Logged-in.', '2020-03-04 05:45:59'),
(63, 'Admin', 'Logged-in.', '2020-03-04 07:45:38'),
(64, 'Admin', 'Added +57 cto (from: 2020-03-04,To: 2020-03-31) to 12345', '2020-03-04 08:47:04'),
(65, 'Admin', 'Deduction VL for Forced Leave (-1) to employee 123456 in  date: 2020-03-04.', '2020-03-05 00:18:49'),
(66, 'Admin', 'Logged-in.', '2020-03-05 07:46:47'),
(67, 'Admin', 'Logged-in.', '2020-03-12 01:15:25'),
(68, 'Admin', 'Logged-in.', '2020-03-12 02:11:41'),
(69, 'Admin', 'Added +23 sick_leave to 12345', '2020-03-12 03:02:32'),
(70, 'Admin', 'Added +233 vacation_leave to 12345', '2020-03-12 03:02:54'),
(71, 'Admin', 'logged-out.', '2020-03-12 06:10:18'),
(72, 'Admin', 'Logged-in.', '2020-03-12 06:13:48'),
(73, 'Admin', 'logged-out.', '2020-03-12 08:13:01'),
(74, 'Admin', 'Logged-in.', '2020-03-12 08:23:59'),
(75, 'Admin', 'logged-out.', '2020-03-12 08:39:38'),
(76, 'Admin', 'Logged-in.', '2020-03-12 08:39:53'),
(77, 'Admin', 'Logged-in.', '2020-03-13 00:08:03'),
(78, 'Admin', 'logged-out.', '2020-03-13 01:36:20'),
(79, 'Admin', 'Logged-in.', '2020-03-13 01:36:45'),
(80, 'Admin', 'logged-out.', '2020-03-13 01:38:20'),
(81, 'Admin', 'Logged-in.', '2020-03-13 01:38:33'),
(82, 'Admin', 'logged-out.', '2020-03-13 01:49:43'),
(83, 'Admin', 'Logged-in.', '2020-03-13 01:51:26'),
(84, 'Admin', 'logged-out.', '2020-03-13 03:14:58'),
(85, 'Admin', 'Logged-in.', '2020-03-13 03:15:09'),
(86, 'Admin', 'Logged-in.', '2020-03-13 05:27:41'),
(87, 'Admin', '12345 submitted a Leave Report.', '2020-03-13 08:29:26'),
(88, 'Admin', '12345 submitted a Leave Report.', '2020-03-13 08:37:49'),
(89, 'Admin', '12345 submitted a Leave Report.', '2020-03-13 08:46:18'),
(90, 'Admin', '12345 submitted a Leave Report.', '2020-03-13 08:47:47'),
(91, 'Admin', '12345 submitted a Leave Report.', '2020-03-13 08:51:57'),
(92, 'Admin', 'logged-out.', '2020-03-13 08:55:24'),
(93, 'Admin', 'Logged-in.', '2020-03-13 08:59:47'),
(94, 'Admin', 'Logged-in.', '2020-03-15 23:54:02'),
(95, 'Admin', 'Logged-in.', '2020-03-15 23:54:13'),
(96, 'Admin', 'logged-out.', '2020-03-16 00:28:29'),
(97, 'Admin', 'Logged-in.', '2020-03-16 00:55:32'),
(98, 'Admin', 'Logged-in.', '2020-03-16 01:12:22'),
(99, 'Admin', 'logged-out.', '2020-03-16 02:05:45'),
(100, 'Admin', 'Logged-in.', '2020-03-16 02:09:43'),
(101, 'Admin', 'logged-out.', '2020-03-16 02:09:51'),
(102, 'Admin', 'Logged-in.', '2020-03-16 02:39:18'),
(103, 'Admin', 'logged-out.', '2020-03-16 02:48:37'),
(104, 'Admin', 'Logged-in.', '2020-03-16 03:21:26'),
(105, 'Admin', 'logged-out.', '2020-03-16 03:21:42'),
(106, 'Admin', 'Logged-in.', '2020-03-16 03:38:41'),
(107, 'Admin', 'logged-out.', '2020-03-16 03:38:53'),
(108, 'Admin', 'Logged-in.', '2020-03-16 03:40:05'),
(109, 'Admin', 'logged-out.', '2020-03-16 05:11:31'),
(110, 'Admin', 'Logged-in.', '2020-03-16 05:32:34'),
(111, 'Admin', 'logged-out.', '2020-03-16 05:32:59'),
(112, 'Admin', 'Logged-in.', '2020-03-16 05:36:39'),
(113, 'Admin', 'logged-out.', '2020-03-16 05:45:33'),
(114, 'Admin', 'Logged-in.', '2020-03-16 05:50:24'),
(115, 'Admin', 'logged-out.', '2020-03-16 05:53:15'),
(116, 'Admin', 'Logged-in.', '2020-03-16 05:58:37'),
(117, 'Admin', 'logged-out.', '2020-03-16 06:00:13'),
(118, 'Admin', 'Logged-in.', '2020-03-16 06:21:00'),
(119, 'Admin', 'logged-out.', '2020-03-16 06:27:39'),
(120, 'Admin', 'Logged-in.', '2020-03-16 06:29:29'),
(121, 'Admin', 'logged-out.', '2020-03-16 06:30:03'),
(122, 'Admin', 'Logged-in.', '2020-03-16 06:42:01'),
(123, 'Admin', 'logged-out.', '2020-03-16 06:45:02'),
(124, 'Admin', 'Logged-in.', '2020-03-16 07:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `missed`
--

DROP TABLE IF EXISTS `missed`;
CREATE TABLE IF NOT EXISTS `missed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(256) NOT NULL,
  `type` tinytext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `multiple_sched`
--

DROP TABLE IF EXISTS `multiple_sched`;
CREATE TABLE IF NOT EXISTS `multiple_sched` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `schedule` varchar(222) NOT NULL,
  `effectivity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dayname` varchar(222) NOT NULL,
  `created` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `description` text NOT NULL,
  `d_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `d_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `description`, `d_create`, `d_update`) VALUES
(1, 'Sample Man', 'SM', '2020-01-06 04:50:21', '2020-01-06 04:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pictures`
--

DROP TABLE IF EXISTS `profile_pictures`;
CREATE TABLE IF NOT EXISTS `profile_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `image` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pictures`
--

INSERT INTO `profile_pictures` (`id`, `eid`, `image`) VALUES
(1, '12345', '12345.JPG'),
(2, '123456', '123456.png'),
(3, '12345', '12345.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `quicklook`
--

DROP TABLE IF EXISTS `quicklook`;
CREATE TABLE IF NOT EXISTS `quicklook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_tag` varchar(1000) NOT NULL,
  `item_name` varchar(222) NOT NULL,
  `item_html` varchar(500) NOT NULL,
  `item_description` longtext NOT NULL,
  `type` int(11) NOT NULL,
  `is_specific` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quicklook`
--

INSERT INTO `quicklook` (`id`, `search_tag`, `item_name`, `item_html`, `item_description`, `type`, `is_specific`) VALUES
(1, 'city,landscape,lights,afternoon,sunset', 'Picture of a City', '<img style=\'width: 50%;\'  src=\'https://images.pexels.com/photos/374870/pexels-photo-374870.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260\'>', 'He determined to drop his litigation with the monastry, and relinguish his claims to the wood-cuting and fishery rihgts at once. He was the more ready to do this becuase the rights had becom much less valuable, and he had indeed the vaguest idea where the wood and river in quedtion were.', 0, 0),
(2, 'employee,employees,station,report,attendance,chart,14,last 14 days,on leave,on departure,deparetured,position,department,temporary,reports', 'Insights', '../components/module_insights.php', 'Insights Graph', 1, 0),
(3, 'leave,apply,tutorial,form 6,employee dashboard,employee', 'How to Apply Leave(Form 6) Tutorial', '<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/wRaAGvcIhlU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 'This video tutorial will show you how to Apply and Approve Leave in CDTRS 2.8 and above.', 0, 0),
(4, 'cocoa,choco,black,seed,coffee,virmil,talattad,chocolate,yummy', 'A Picture of Cocoa', '<img style=\'width: 50%;\'  src=\'https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/8/3/1/4/5974138-3-eng-GB/European-cocoa-processing-rises-2-in-Q2-but-bean-prices-to-stay-low_wrbm_large.jpg\'>', 'The cocoa bean or simply cocoa.', 0, 0),
(5, 'oic,superintendent,officer,principal,hrmo,human resource,admin', 'Station Officers/ Infomation', '../components/module_station_info.php', 'Station Info', 1, 0),
(6, 'deped, website,sdo,marikina', 'SDO Marikina Official Website', '<a target=\'_blank\' href=\'https://depedmarikina.ph\'><i class=\"fas fa-arrow-circle-right\"></i> Go to SDO Marikina Website</a>', '<img style=\'width: 100px;\'  src=\'https://pbs.twimg.com/profile_images/827074293825560576/IUOWtSWD.jpg\'><br>The Schools Division of Marikina City became an independent Division after Marikina was converted into a highly urbanized city by virtue of Republic Act 8223. It was inaugurated on January 2, 1998 with Dr. Abella C. Macarandan, Assistant Schools Division Superintendent as Officer-In-Charge. She held office at the historic Gabaldon building within the premises of Marikina Elementary School.<hr>CONTACT INFORMATION\r\n191 Shoe Ave., Sta. Elena, Marikina City, 1800, Philippines\r\n(02) 682-2472 / 682-3989 \r\nsdo.marikina@deped.gov.ph', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_forcedleave`
--

DROP TABLE IF EXISTS `scheduled_forcedleave`;
CREATE TABLE IF NOT EXISTS `scheduled_forcedleave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `applied_leave_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `eid` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET latin1 NOT NULL,
  `value` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'fixedsched', '8:30 AM,5:30 PM,12:00 PM,1:00 PM'),
(2, 'flexisched', '9:30 AM,6:30 PM,12:00 PM,1:00 PM'),
(3, 'offset', '30'),
(4, 'graceperiod', '15'),
(5, 'key', 'marikina-15395814545bc4260ea4752'),
(6, 'secret', 'f0f0c2b7055e073f7da893e57549195a494ffe2f'),
(7, 'remote_url', 'http://beta.dtr.depedmarikina.ph');

-- --------------------------------------------------------

--
-- Table structure for table `temp_employee_num`
--

DROP TABLE IF EXISTS `temp_employee_num`;
CREATE TABLE IF NOT EXISTS `temp_employee_num` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `date_added` timestamp NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `untask`
--

DROP TABLE IF EXISTS `untask`;
CREATE TABLE IF NOT EXISTS `untask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskname` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `untask`
--

INSERT INTO `untask` (`id`, `taskname`) VALUES
(2, 'replace_oic_title'),
(3, 'replace_hr_title');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` tinytext NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `where_abouts`
--

DROP TABLE IF EXISTS `where_abouts`;
CREATE TABLE IF NOT EXISTS `where_abouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(222) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `purpose` varchar(1000) NOT NULL,
  `departure` varchar(222) DEFAULT NULL,
  `arrival` varchar(222) DEFAULT NULL,
  `dep_coor` varchar(222) NOT NULL DEFAULT '0000000',
  `arr_coor` varchar(222) NOT NULL DEFAULT '0000000',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
